<?php

use app\components\Config;

require(__DIR__.'/lang.php');
require(__DIR__.'/../components/Config.php');

$language = LANGUAGE;

/*if (YII_ENV_DEV) {
	session_start();
	if (isset($_SESSION['language_for_test'])) {
		$language = $_SESSION['language_for_test'];
	}
}*/

$log_db = '/log_db.php';

define('LANGUAGE_CONFIG_DIR', __DIR__.DIRECTORY_SEPARATOR.$language);

$config = [
	'id'             => 'basic',
	'basePath'       => dirname(__DIR__),
	'bootstrap'      => ['log','debug'],
	'language'       => $language,
	'components'     => array(
		'request'      => array(
			'cookieValidationKey'  => 'fcjk89Jhsk',
			'enableCsrfValidation' => true,
		),
		'view' => array(
			'class' => 'app\components\FluidableView',
		),
		'curl' => array(
			'class' => 'firstgoer\CurlComponent',
			'options' => array(
				'timeout' => 5
			)
		),
		'cache'        => array(
			'class' => 'yii\caching\FileCache',
		),
		'user'         => array(
			'identityClass'   => 'app\models\Users',
			'enableAutoLogin' => true,
		    'identityCookie' => array('name' => '_identity', 'httpOnly' => false)
		),
		'errorHandler' => array(
			'errorAction' => 'site/error',
		),
		'mailer'       => Config::load('mailer'),
		'urlManager'   => array(
			'class'           => 'yii\web\UrlManager',
			'showScriptName'  => false,
			'enablePrettyUrl' => true,
			'rules' => array(
				'page/faq' => 'faq',
				'page/blog' => 'site/blog',
				'page/blog/<alias:[\w-]+>' => 'site/blog',
				'admin/regions/<id:\w+>' => 'admin/regions',
				'js/zengram-i18n.js' => 'site/language-js',
				'admin/<controller:\w+(-\w+)*>/<action:\w+(-\w+)*>' => 'admin/<controller>/<action>',
				'admin/<controller:\w+(-\w+)*>/<action:\w+(-\w+)*>/<id:\w+>' => 'admin/<controller>/<action>',
				'instashpion/<controller:\w+(-\w+)*>/<action:\w+(-\w+)*>' => 'instashpion/<controller>/<action>',
				'instashpion/<controller:\w+(-\w+)*>/<action:\w+(-\w+)*>/<id:\w+>' => 'instashpion/<controller>/<action>',
				'partnership/<controller:\w+(-\w+)*>/<action:\w+(-\w+)*>' => 'partnership/<controller>/<action>',
				'partnership/<controller:\w+(-\w+)*>/<action:\w+(-\w+)*>/<id:\w+>' => 'partnership/<controller>/<action>',
				'management/<controller:\w+(-\w+)*>/<action:\w+(-\w+)*>' => 'management/<controller>/<action>',
				'management/<controller:\w+(-\w+)*>/<action:\w+(-\w+)*>/<id:\w+>' => 'management/<controller>/<action>',
				'debug/<controller:\w+(-\w+)*>/<action:\w+(-\w+)*>' => 'debug/<controller>/<action>',
				'<controller:\w+(-\w+)*>/<action:\w+(-\w+)*>/<id:\w+>' => '<controller>/<action>',
				'<controller:[\w-]+>/<action:[\w-]+' => '<controller>/<action>',
			)
		),
		'log' => array(
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets'    => array(
				array(
					'class'  => 'yii\log\FileTarget',
					'levels' => array('error', 'warning'),
				),
			),
		),
		'assetManager' => array(
			'class'     => 'yii\web\AssetManager',
			'forceCopy' => YII_ENV_DEV,
			'hashCallback' => function ($path) {
				return hash('md4', $path);
			}
		),
		'db'           => Config::load('db'),
		'log_db'       => Config::load('log_db'),
		'postdb'       => Config::load('postgre'),
		'payPalRest'   => array(
			'class'          => 'betsuno\paypal\RestAPI',
			'pathFileConfig' => LANGUAGE_CONFIG_DIR.'/paypal-local.php',
			'successUrl'     => '/payment/success_paypal',
			'cancelUrl'      => '/payment/cancel_paypal',
		),
		'i18n' => array(
			'translations' => array(
				'*' => array(
					'class' => 'yii\i18n\PhpMessageSource',
				),
			),
		),

		'instagram' => array(
			'class' => 'app\components\InstagramComponent'
		),


	),
	'modules' => [
		'admin' => [
			'class' => 'app\modules\admin\Module',
		],
		'management' => [
			'class' => 'app\modules\management\Module',
		],
		'instashpion' => [
			'class' => 'app\modules\instashpion\Module',
		],
		'partnership' => [
			'class' => 'app\modules\partnership\Module',
		],
		'finances' => [
			'class' => 'app\modules\finances\Module',
		],
		'debug' => [
			'class'      => 'yii\debug\Module',
			'allowedIPs' => ['89.22.247.50',  '83.219.*'],
		]
	],
	'controllerMap' => [
		'gearman' => [
			'class'            => 'filsh\yii2\gearman\GearmanController',
			'gearmanComponent' => 'gearman',
		],
	],
	'params' => Config::load('params'),
];

if (YII_ENV_DEV) {
	$ips = ['37.213.*', '78.25.*', '92.127.*', '127.0.0.1', '192.168.56.*', '83.219.*', '::1', '85.113.39.78', '89.22.247.50'];
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][]      = 'debug';
	$config['modules']['debug'] = [
		'class'      => 'yii\debug\Module',
		'allowedIPs' => $ips,
	];

	$config['bootstrap'][]    = 'gii';
	$config['modules']['gii'] = [
		'class'      => 'yii\gii\Module',
		'allowedIPs' => $ips,
	];
}

return \yii\helpers\ArrayHelper::merge(
	$config,
	Config::load('web')
);
