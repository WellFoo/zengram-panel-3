
function sendImage()
{
	var accounts = [];
	$('#accounts').find('input[type=checkbox]:checked').each(function(){
		if (this.value){
			accounts.push($(this).data('id'));
		}
	});
	$.ajax({
		url: '/actions/post/',
		data:  {'accounts' : accounts, 'image-id': $('.flex-active-slide').find('img').data('id')},
		method: 'post',
		dataType: 'json',
		beforeSend: function () {
			$('.add-link').addClass('hidden');
			$('.add-link-wait').removeClass('hidden');
			$('.action-result').removeClass('bg-warning').text('');
		},
		success: function (data) {
			if (data.error == undefined) {
				for (var id in data){
					if (data[id].error){
						$('#upload-message-'+id).removeClass('text-success').addClass('text-danger').text(data[id].error);
					} else {
						$('#action-checkbox-'+id).change();
						$('#upload-message-'+id).addClass('text-success').removeClass('text-danger').text(window.Zengram.t(data[id].result));
					}
				}
				if (data.text){
					alertModal(data.text);
					setTimeout(function(){
						window.location.href = '/';
					}, 5000);
				}
			} else {
				$('.action-result').addClass('bg-warning').text(window.Zengram.t(data.error));
			}
		},
		complete: function () {
			$('.add-link-wait').addClass('hidden');
			$('.add-link').removeClass('hidden');
		}
	});
}

function recount(){
	var accounts = [];
	$('#accounts').find('input[type=checkbox]:checked').each(function(){
		if (this.value){
			accounts.push($(this).data('id'));
		}
	});
	$.getJSON('/actions/count-price', {'accounts' : accounts}, function(data){
		if (data['price'] !== undefined){
			$('#free-sum').text(data['price']);
		}
	});
}

$(document).ready(function ()
{
	$('.flexslider').flexslider({
		animation: "fade",
		controlNav: false,
		touch: true,
		slideshow: false,
		animationSpeed: 700
	});
	$('.account-checkbox').on('change', recount);
});