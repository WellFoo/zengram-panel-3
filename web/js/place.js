function geocoderDataSource(query, process)
{
	$.ajax({
		url: 'https://geocode-maps.yandex.ru/1.x/',
		type: 'GET',
		data: {
			geocode:  query,
			kind:     'locality',
			format:   'json',
			lang: window.Zengram.t('en_US'),
			results:  20,
			key:  mapsApiKey
		},
		dataType: 'jsonp',
		success: function callback(response/*, status, xhr*/) {
			if (!response.response){
				return false;
			}
			/**
			 * @namespace response.response.GeoObjectCollection
			 * @namespace response.response.GeoObjectCollection.featureMember
			 */
			var points = $.map(response.response.GeoObjectCollection.featureMember, function(item/*, index*/) {
				/**
				 * @namespace item.GeoObject.*
				 * @namespace item.GeoObject.metaDataProperty
				 * @namespace item.GeoObject.metaDataProperty.GeocoderMetaData
				 */
				if (item.GeoObject.metaDataProperty.GeocoderMetaData.kind !== 'locality') {
					return null;
				}
				/**
				 * @namespace item.GeoObject.boundedBy
				 * @namespace item.GeoObject.boundedBy.Envelope
				 * @namespace item.GeoObject.boundedBy.Envelope.upperCorner
				 */
				/** @namespace item.GeoObject.boundedBy.Envelope.lowerCorner */
				/** @namespace item.GeoObject.metaDataProperty.GeocoderMetaData.AddressDetails.Country.CountryName */
				/** @namespace item.GeoObject.metaDataProperty.GeocoderMetaData.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName */
				return {
					name: item.GeoObject.name,
					fullName: item.GeoObject.name + ', ' + item.GeoObject.description,
					text: item.GeoObject.metaDataProperty.GeocoderMetaData.text,
					center: item.GeoObject.Point.pos,
					corners: {
						ne: item.GeoObject.boundedBy.Envelope.upperCorner,
						sw: item.GeoObject.boundedBy.Envelope.lowerCorner
					},
					country: item.GeoObject.metaDataProperty.GeocoderMetaData.AddressDetails.Country.CountryName,
					region: item.GeoObject.metaDataProperty.GeocoderMetaData.AddressDetails.Country.AdministrativeArea ?
						item.GeoObject.metaDataProperty.GeocoderMetaData.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName: false
				};
			});
			process(points);
		}
	});
}


function placeChanged(place, callback)
{
	var id = place.center.replace(/\./g, '').replace(' ', '_'),
		center = geoPoint(place.center),
		cornerNe = geoPoint(place.corners.ne),
		cornerSw = geoPoint(place.corners.sw);

	var dL = Math.abs((parseFloat(center.lat) + 90) - (parseFloat(cornerSw.lat) + 90)),
		dR = Math.abs((parseFloat(center.lat) + 90) - (parseFloat(cornerNe.lat) + 90)),
		dT = Math.abs((parseFloat(center.lng) + 180) - (parseFloat(cornerNe.lng) + 180)),
		dB = Math.abs((parseFloat(center.lng) + 180) - (parseFloat(cornerSw.lng) + 180));

	var corner = {
		lat: dL > dR ? cornerSw.lat : cornerNe.lat,
		lng: dB > dT ? cornerSw.lng : cornerNe.lng
	};

	callback({
		action: 'create',
		place: {
			googleId: id,
			name: place.name,
			country: place.country,
			region: place.region,
			text: place.text,
			center: {
				lat: center.lat,
				lng: center.lng
			},
			radius: sectionLength(center, corner),
			corners: {
				ne: {
					lat: cornerNe.lat,
					lng: cornerNe.lng
				},
				sw: {
					lat: cornerSw.lat,
					lng: cornerSw.lng
				}
			}
		}
	});
}

function sectionLength(from, to)
{
	var w = to.lng;
	var centerLng = from.lng;

	var centerLat = toRad(from.lat);

	var dlong = toRad(Math.abs(centerLng - w)); // This is the distance from the center to the left in radians

	//Calculating distance using the Haversine formula.
	var a = Math.pow(Math.cos(centerLat), 2) *
		Math.sin(dlong / 2) * Math.sin(dlong / 2);

	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

	return Math.round(6371000 * c);
}

function toRad(number)
{
	return number * Math.PI / 180;
}

function geoPoint(string, lngFirst)
{
	var parts = string.split(' ');
	return {
		lat: lngFirst ? parts[0] : parts[1],
		lng: lngFirst ? parts[1] : parts[0]
	};
}
