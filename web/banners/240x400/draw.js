(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 240,
	height: 400,
	fps: 30,
	color: "#FFFFFF",
	manifest: [
		{src:"/banners/240x400/logo.png", id:"logo"}
	]
};



// symbols:



(lib.logo = function() {
	this.initialize(img.logo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,217,50);


(lib.Symbol29 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#92FFDD").s().p("AgUB8QgFAAgDgDQgDgDAAgFIAAgXIAAjKIAAgBQAAgEADgDQADgDAFAAIA0AAIAAD3g");
	this.shape.setTransform(-123.8,171.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#86E4C2").s().p("AgIB8IAAj2IAAgBIAQAAIABD3g");
	this.shape_1.setTransform(-119.6,171.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFB095").s().p("Ag0CKIgCgBQgGAAgCgDQgHgDgBgGIgBgBIABAAIBlABQAFgBAGgEQAEgFAAgGIAAgMQAAgGgEgEQgGgFgFAAIhmAAIgBgFIAAAAIgBgBIgBgBIgFgBIAAgBIgCgBIgDAAIgBgGIAAgDICUAAQAIAAAFgEQAFgHAAgHIAAgSQAAgHgFgGQgFgFgIAAIiUAAIAAgEIAAgEIgBAAIgEgBQgEAAgBAAQgEgCgBgDIAAgCICjAAQAIAAAFgFQAFgFAAgIIAAgNQAAgHgFgGQgFgFgIAAIijAAIABgEIAAgCQADgEAGgBIABAAIADgBIABgFIAAgEICKgBQAHAAADgDQAFgFABgHIgBgpIAGAAQAKgCAHgDIABAAIABAyQAAAGgEAGQgFAFAAACQAAABAIAFQAJAGAAAJIAAAcQAAAIgEAGQgFAEgGACQgBAAAAAAQgBABAAAAQAAAAAAAAQgBAAAAAAQAAAAABAAQAAABAAAAQABABAAAAQABAAABAAQAGADAFAGQADAGAAAHIAAAjQAAAIgGAHQgGAGgJAAQgEAAgFADQgCAEAAAFIAAAbQAAAKgGAFQgGAGgJAAg");
	this.shape_2.setTransform(-160.2,164.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFD2B1").s().p("AgrESIgHgBIh1gnIBmABQAJABAGgHQAGgFAAgJIAAgbQAAgFACgEQAFgEAEAAQAJAAAGgFQAGgIAAgIIAAgjQAAgGgDgGQgFgGgGgDQgBAAgBgBQAAAAgBAAQAAgBAAAAQgBgBAAAAQAAAAABgBQAAAAAAgBQAAAAABAAQAAAAABgBQAGgCAFgDQAEgHAAgHIAAgcQAAgKgJgGQgIgEAAgBQAAgCAFgFQAEgHAAgGIgBgvIAGgFQAFgDADgGQACgGgCgFIghh/QgCgJADgHIAWgvQACgHAHgFQAGgEAGAAIAFAAQALAAAIAIQAIAHAAALIAAAmQgBAGADAGIBOCPIAgBkQABAEADADQADACAEAAIA5AAIAAABIAADNIgvgCIgHACIgiANIgHABgAi4DhIgBgFQABAGAHADQgEgBgDgDgAi5DbIgBgBIAAgoIBoAAQAFgBAGAFQAEAEAAAGIAAANQAAAGgEAFQgGAEgFAAgAi8CrIABABIAAAAIgBgBgAjICfIAAg2ICWAAQAIAAAFAEQAFAGAAAIIAAASQAAAHgFAGQgFAFgIAAgAjXBXIAAgEIAAguIClAAQAIAAAFAGQAFAFAAAHIAAAOQAAAIgFAEQgFAGgIAAgAjIgTQAAgJAGgFQAEgEAEgBIAGgBICHAAIABAoQgBAGgFAFQgDAEgHAAIiMABg");
	this.shape_3.setTransform(-148.7,154.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#517786").s().p("Ag3CeQgHgHgBgMIgBkVQAAgKAHgJQAGgIALAAIBpAAIAAFKIhkABQgLAAgJgIg");
	this.shape_4.setTransform(-112.1,172.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-170.3,127.2,64.8,61.6);


(lib.Symbol28 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAdA2IAAgsIgVAAIgfAsIgcAAIAkgvQgKgDgFgEQgFgEgCgGQgCgGAAgHQABgOAJgIQAJgIARAAIA3AAIAABrgAgLgfQgEAEAAAGQAAAIAEAEQAEAEAHAAIAdAAIAAgdIgdAAQgHAAgEADg");
	this.shape.setTransform(2.8,110.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAXA2IAAhNIAAAAIgnBNIgeAAIAAhrIAYAAIAABNIAAAAIAnhNIAeAAIAABrg");
	this.shape_1.setTransform(-8.3,110.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgKA2IAAhYIgkAAIAAgTIBdAAIAAATIgkAAIAABYg");
	this.shape_2.setTransform(-19.2,110.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgZAyQgNgIgFgMQgHgNAAgQQAAgQAHgNQAGgOAMgGQAMgIAPAAQAUAAAOALQAMAKACATIgXAAQgCgKgHgGQgGgFgKAAQgMAAgIAKQgHALgBAQQABASAHAKQAIAJALABQALAAAGgHQAIgGABgMIAYAAQgDAVgNALQgOAMgUAAQgPAAgLgHg");
	this.shape_3.setTransform(-29.8,110.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAaA1QgEgDgCgHQgHAHgKADQgIAEgLgBQgRAAgKgIQgJgJAAgPQAAgLAEgGQAFgGAHgCQAHgDAJgCIAQgCIALgCIAIgCIADgCIABgDIAAgFQAAgIgFgEQgGgEgJAAQgLgBgGAFQgGAEgBALIgXAAQACgUANgJQANgIAUAAQALAAAKADQAKAEAFAHQAGAHAAAMIAAA1IABAGQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIACAAIADgBIAAARIgIACIgHAAIgCABQgGAAgEgEgAAHAEIgLABQgGABgFACQgGABgDAEQgEADAAAHQAAAIAFAEQAFAEAKAAQAHAAAFgCQAGgCAEgEQAEgEABgFIAAgKIAAgMQgGACgGACg");
	this.shape_4.setTransform(-41.4,110.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAWA2IAAguIgJACIgMACIgMABQgQAAgJgIQgJgGAAgQIAAgkIAYAAIAAAcQAAAKAFAFQAFAFAKAAIAHgBIAJgCIAHgCIAAgrIAYAAIAABrg");
	this.shape_5.setTransform(-53.3,110.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AggBKIgKgBIAAgUIAIABIAFABQAGgBADgEQAEgEACgGIADgKIgphoIAaAAIAbBOIAbhOIAZAAIgoBpIgIAVQgEAJgFAGQgIAHgNAAIgHAAg");
	this.shape_6.setTransform(-64.2,112.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgbAyQgNgIgGgMQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGANAAAQQAAARgGANQgHAMgMAIQgMAGgRABQgPgBgMgGgAgVgbQgIALAAAQQAAARAIALQAIAKANAAQAOAAAIgKQAIgKAAgSQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape_7.setTransform(-81.7,110.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgkA2IAAhrIBJAAIAAATIgyAAIAABYg");
	this.shape_8.setTransform(-91.8,110.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgaAyQgLgIgHgNQgGgMAAgRQAAgOAGgNQAHgOAMgIQAMgHANAAQAQAAAMAHQALAIAHAOQAFANAAAQIAAADIhOAAQABARAIAIQAHAJANAAQAJAAAGgEQAHgFADgLIAXAAQgDAMgHAJQgHAIgKAFQgKAEgMAAQgPAAgMgHgAAcgJQgCgOgHgIQgHgHgMAAQgKAAgHAHQgIAIgBAOIA2AAIAAAAg");
	this.shape_9.setTransform(-102.6,110.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhHA2IAAhrIAXAAIAABYIAmAAIAAhYIAVAAIAABYIAlAAIAAhYIAYAAIAABrg");
	this.shape_10.setTransform(-117.1,110.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAaA1QgEgDgCgHQgHAHgKADQgIAEgLgBQgRAAgKgIQgJgJAAgPQAAgLAEgGQAFgGAHgCQAHgDAJgCIAQgCIALgCIAIgCIADgCIABgDIAAgFQAAgIgFgEQgGgEgJAAQgLgBgGAFQgGAEgBALIgXAAQACgUANgJQANgIAUAAQALAAAKADQAKAEAFAHQAGAHAAAMIAAA1IABAGQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIACAAIADgBIAAARIgIACIgHAAIgCABQgGAAgEgEgAAHAEIgLABQgGABgFACQgGABgDAEQgEADAAAHQAAAIAFAEQAFAEAKAAQAHAAAFgCQAGgCAEgEQAEgEABgFIAAgKIAAgMQgGACgGACg");
	this.shape_11.setTransform(-131.3,110.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("Ag8BKIAAiTIBCAAQAQAAALAFQALAEAFAJQAGAIAAALQgBALgFAJQgGAIgKAEQANADAIAHQAHAKAAAOQAAANgGAKQgGAKgKAGQgLAFgOAAgAgjA1IAmAAQAJAAAHgCQAHgCAFgFQAEgFAAgJQAAgLgFgFQgEgFgIgCQgHgBgJAAIglAAgAgjgLIAjAAQAOAAAHgFQAHgEABgMQgBgJgEgEQgEgFgGgBQgGgBgIAAIgjAAg");
	this.shape_12.setTransform(-144.4,108.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAmA2IAAhGIgBAAIgZBGIgWAAIgbhGIAAAAIAABGIgXAAIAAhrIAgAAIAcBUIAAAAIAehUIAfAAIAABrg");
	this.shape_13.setTransform(-54.2,85.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AggBKIgKgBIAAgUIAIABIAFABQAGgBADgEQAEgEACgGIADgKIgphoIAaAAIAbBOIAbhOIAZAAIgoBpIgIAUQgEAKgFAGQgIAHgNABIgHgBg");
	this.shape_14.setTransform(-67.1,87.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAmA2IAAhGIAAAAIgbBGIgWAAIgahGIAAAAIAABGIgXAAIAAhrIAfAAIAdBUIABAAIAchUIAgAAIAABrg");
	this.shape_15.setTransform(-79.9,85.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAXA2IAAhNIAAAAIgnBNIgeAAIAAhrIAYAAIAABNIAAAAIAnhNIAeAAIAABrg");
	this.shape_16.setTransform(-93.3,85.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAXA2IAAgvIgtAAIAAAvIgYAAIAAhrIAYAAIAAArIAtAAIAAgrIAYAAIAABrg");
	this.shape_17.setTransform(-105.3,85.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAXA2IAAhNIAAAAIgnBNIgeAAIAAhrIAYAAIAABNIAAAAIAnhNIAeAAIAABrg");
	this.shape_18.setTransform(-117.3,85.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAmA2IAAhGIgBAAIgZBGIgWAAIgbhGIAAAAIAABGIgXAAIAAhrIAgAAIAcBUIAAAAIAehUIAfAAIAABrg");
	this.shape_19.setTransform(-130.7,85.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgdALIAAgVIA7AAIAAAVg");
	this.shape_20.setTransform(-148.1,84.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgZAxQgNgHgGgMQgGgNAAgQQAAgQAHgNQAGgNAMgIQAMgHANAAQAQAAAMAHQALAIAHANQAFAOAAARIAAACIhOAAQABAQAIAJQAHAJANABQAKAAAGgGQAGgEADgKIAXAAQgDAMgHAIQgGAIgLAEQgKAFgMAAQgPAAgLgIgAAcgJQgBgNgIgIQgHgIgMAAQgKAAgHAIQgIAHgBAOIA2AAIAAAAg");
	this.shape_21.setTransform(-20.6,59.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAXA2IAAhNIAAAAIgnBNIgeAAIAAhrIAYAAIAABNIAAAAIAnhNIAeAAIAABrg");
	this.shape_22.setTransform(-32.5,59.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAXA2IAAgvIgtAAIAAAvIgYAAIAAhrIAYAAIAAArIAtAAIAAgrIAYAAIAABrg");
	this.shape_23.setTransform(-44.5,59.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgZAxQgMgHgHgMQgGgNgBgQQABgQAHgNQAGgNAMgIQAMgHANAAQAQAAALAHQAMAIAGANQAGAOABARIAAACIhOAAQAAAQAIAJQAIAJALABQAKAAAHgGQAFgEAEgKIAXAAQgDAMgHAIQgHAIgJAEQgLAFgMAAQgPAAgLgIgAAbgJQgBgNgHgIQgHgIgMAAQgKAAgHAIQgIAHgBAOIA1AAIAAAAg");
	this.shape_24.setTransform(-56.3,59.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AA2A2IgggvIgLALIAAAkIgVAAIAAgkIgKgLIghAvIgdAAIAug8IgogvIAdAAIAlAuIAAAAIAAguIAVAAIAAAuIABAAIAlguIAdAAIgqAvIAvA8g");
	this.shape_25.setTransform(-70.4,59.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAXA2IAAhNIAAAAIgnBNIgeAAIAAhrIAYAAIAABNIAAAAIAnhNIAeAAIAABrg");
	this.shape_26.setTransform(-84.6,59.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgvA2IAAhrIA6AAQAPAAAIAHQAJAHAAANQAAAJgEAFQgDAFgJAEIAAAAQALADAFAFQAFAHAAALQAAAPgLAIQgJAIgUAAgAgXAkIAfAAQAIAAAEgEQAEgEAAgHQAAgHgEgEQgEgEgIAAIgfAAgAgXgKIAdAAQAGAAAEgDQADgDAAgHQABgGgEgDQgEgDgHAAIgcAAg");
	this.shape_27.setTransform(-96.2,59.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAmBHIAAgiIhLAAIAAAiIgXAAIAAg2IAOAAQAEgIADgKQADgLABgOQACgOAAgRIAAgNIBPAAIAABXIAPAAIAAA2gAgKgsQAAANgCAMIgFAVIgEAPIAsAAIAAhEIghAAg");
	this.shape_28.setTransform(-108.6,61.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgbAxQgNgGgGgNQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGAMAAARQAAARgGANQgHANgMAGQgMAIgRAAQgPAAgMgIgAgVgbQgIAKAAARQAAARAIALQAIAKANAAQAOAAAIgKQAIgLAAgRQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape_29.setTransform(-121.1,59.9);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("Ag0BLIAAiSIAWAAIAAAOQAHgJAJgEQAJgEAJAAQAPAAALAHQALAHAGANQAGANAAASQAAAQgGALQgFANgMAHQgLAHgOAAQgJAAgJgEQgJgEgHgJIAAA2gAgVgtQgIAKAAASQAAARAIAJQAIAKANAAQANAAAIgKQAIgJAAgQQAAgSgIgLQgIgKgNAAQgOAAgHAKg");
	this.shape_30.setTransform(-133.5,61.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AAXA2IAAhYIgtAAIAABYIgYAAIAAhrIBdAAIAABrg");
	this.shape_31.setTransform(-146.1,59.9);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgZAyQgMgIgHgMQgGgNAAgRQAAgPAHgNQAGgNAMgHQAMgIANAAQAQAAAMAIQALAHAHANQAFAOAAARIAAACIhNAAQAAARAIAJQAIAIAMABQAJAAAHgGQAFgEAEgKIAXAAQgDALgHAJQgGAIgLAEQgKAFgMAAQgPAAgLgHgAAcgJQgBgOgIgHQgHgIgMAAQgKAAgHAIQgHAHgCAOIA2AAIAAAAg");
	this.shape_32.setTransform(8.3,34.5);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgbAyQgNgIgGgMQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGAMAAARQAAARgGANQgHAMgMAIQgMAGgRABQgPgBgMgGgAgVgbQgIAKAAARQAAASAIAKQAIAKANAAQAOAAAIgKQAIgLAAgRQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape_33.setTransform(-3.8,34.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AAVA2IgggxIgOAPIAAAiIgYAAIAAhrIAYAAIAAAwIApgwIAdAAIgqAsIAvA/g");
	this.shape_34.setTransform(-15,34.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgZAxQgNgGgFgNQgHgMAAgRQAAgQAHgNQAGgNAMgIQAMgGAPgBQAUAAAOALQAMAKACATIgXAAQgCgKgHgGQgGgFgKAAQgMAAgIAKQgHAKgBARQABARAHALQAIAKALAAQALAAAGgHQAIgGABgMIAYAAQgDAUgNAMQgOALgUAAQgPABgLgIg");
	this.shape_35.setTransform(-27.1,34.5);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgaAyQgMgIgGgMQgGgNAAgRQAAgPAGgNQAHgNAMgHQAMgIANAAQAQAAALAIQAMAHAGANQAHAOgBARIAAACIhOAAQABARAIAJQAHAIANABQAJAAAGgGQAHgEADgKIAXAAQgDALgGAJQgIAIgKAEQgKAFgMAAQgPAAgMgHgAAbgJQAAgOgIgHQgHgIgMAAQgKAAgHAIQgIAHgBAOIA1AAIAAAAg");
	this.shape_36.setTransform(-38.8,34.5);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AAWA2IAAguIgJACIgMACIgMABQgQAAgJgIQgJgGAAgQIAAgkIAYAAIAAAcQAAAKAFAFQAFAFAKAAIAHgBIAJgCIAHgCIAAgrIAYAAIAABrg");
	this.shape_37.setTransform(-50.6,34.5);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AAXA2IAAhNIAAAAIgnBNIgeAAIAAhrIAYAAIAABNIAAAAIAnhNIAeAAIAABrg");
	this.shape_38.setTransform(-62,34.5);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgKA2IAAhYIgkAAIAAgTIBdAAIAAATIgkAAIAABYg");
	this.shape_39.setTransform(-72.9,34.5);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AAaA1QgEgDgCgHQgHAHgKADQgIAEgLgBQgRAAgKgIQgJgJAAgPQAAgLAEgGQAFgGAHgCQAHgDAJgCIAQgCIALgCIAIgCIADgCIABgDIAAgFQAAgIgFgEQgGgEgJAAQgLgBgGAFQgGAEgBALIgXAAQACgUANgJQANgIAUAAQALAAAKADQAKAEAFAHQAGAHAAAMIAAA1IABAGQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIACAAIADgBIAAARIgIACIgHAAIgCABQgGAAgEgEgAAHAEIgLABQgGABgFACQgGABgDAEQgEADAAAHQAAAIAFAEQAFAEAKAAQAHAAAFgCQAGgCAEgEQAEgEABgFIAAgKIAAgMQgGACgGACg");
	this.shape_40.setTransform(-83.4,34.5);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AAmA2IAAhGIAAAAIgaBGIgXAAIgahGIAAAAIAABGIgXAAIAAhrIAfAAIAdBUIAAAAIAehUIAfAAIAABrg");
	this.shape_41.setTransform(-96.9,34.5);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgbAyQgNgIgGgMQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGAMAAARQAAARgGANQgHAMgMAIQgMAGgRABQgPgBgMgGgAgVgbQgIAKAAARQAAASAIAKQAIAKANAAQAOAAAIgKQAIgLAAgRQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape_42.setTransform(-110.5,34.5);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgKA2IAAhYIgkAAIAAgTIBdAAIAAATIgkAAIAABYg");
	this.shape_43.setTransform(-121.6,34.5);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgvA2IAAhrIA6AAQAPAAAJAHQAIAHAAANQAAAJgEAFQgEAFgHAEIAAAAQAKADAFAFQAFAHAAALQAAAPgLAIQgJAIgUAAgAgXAkIAfAAQAIAAAEgEQAEgEAAgHQAAgHgEgEQgEgEgIAAIgfAAgAgXgKIAcAAQAHAAAEgDQADgDAAgHQAAgGgDgDQgEgDgHAAIgcAAg");
	this.shape_44.setTransform(-132,34.5);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AArBKIgOgnIg6AAIgOAnIgbAAIA5iTIAaAAIA6CTgAAWAPIgWg9IgWA9IAsAAg");
	this.shape_45.setTransform(-145.1,32.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-154.1,19,176.3,103.6);


(lib.Symbol27 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#1C1C1B").s().p("AgKAzIAAgGIgGAAIAAgGIAGAAIAAAGIAoAAIAAAGgAAeAtIAAgGIASAAIAAAGgAAxAnIAAgGIALAAIAAgvIgMAAIAAgGIATAAIAAA7gAgXAnIAAgMIgGAAIAAgSIAYAAIAAAGIgSAAIAAAMIAXAAIAAAGIgRAAIAAAGgAgjAJIAAgLIgZAAIAAgGIgGAAIAAgMIAGAAIAAAMIA3AAIAAAGIgYAAIAAALgAAkgUIAAgGIgMAAIAAgGIAMAAIAAAGIAMAAIAAAGgAg8gUIAAgGIA4AAIAAgGIAEAAIAAAGIASAAIAAAGgAAMggIAAgGIgGAAIAAgGIAGAAIAAAGIAMAAIAAAGgAgKggIAAgSIAQAAIAAAGIgKAAIAAAMg");
	this.shape.setTransform(18.2,23.6,2.422,2.422,90);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1 copy
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhdCiIAAgrIABAAIgDgBIgOAAIAAgtIgPAAIAAhkIAPAAIAAgOIAOAAIACgCIAAgOIAbAAIAAgOIAtAAIAAgQIAdAAIAAg8IAOAAIAAgOIAdAAIAAANIAQAAIAACIIANAAIAAgPIAtAAIAAAqIgQAAIAAAPIgPAAIAAAcIgOAAIAAAeIgNAAIAAAdIgPAAIgBABIAAAsg");
	this.shape_1.setTransform(18.2,23.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(5.7,7.3,25,32.6);


(lib.Symbol23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2382AC").s().p("AhkCNIAAkYIBGAAQA/gBAiAlQAiAkAABDQAABCgiAlQgiAmg9AAgAhABvIAfAAQAvAAAageQAYgcAAg1QAAg1gYgcQgZgdgwAAIgfAAg");
	this.shape.setTransform(-1.4,22.2,0.364,0.364);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2382AC").s().p("AgbBnQAggQAAgeIgEAAQgHAAgHgGQgGgFAAgLQAAgJAHgGQAGgHAIABQAMgBAHALQAHAJAAARQAAAYgNATQgNARgVALgAgOhPQgGgHAAgKQAAgKAGgHQAHgGAHAAQAKAAAGAGQAHAHAAAKQAAAKgHAHQgGAGgKAAQgHAAgHgGg");
	this.shape_1.setTransform(-8.2,22.9,0.364,0.364);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Ah/EpQh7g1gxh9Qgyh7A1h7QA1h7B9gyQB6gxB7A1QB8A1AxB9QAyB6g1B8Qg1B7h9AxQg8AYg7AAQg/AAhAgbg");
	this.shape_2.setTransform(-3.7,22.1,0.364,0.364);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E7E7E7").s().p("AhhiiIEKDnIlRBeg");
	this.shape_3.setTransform(7,23.9,0.364,0.364);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FDE4D3").s().p("Ah7A4QgXgBgRgQQgQgRAAgWQAAgWAQgQQARgQAXAAID3AAQAXAAARAQQAQAQAAAWQAAAWgQARQgRAQgXABg");
	this.shape_4.setTransform(62.7,77.8,0.24,0.24);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FDE4D3").s().p("Ah7A4QgXgBgRgQQgQgRAAgWQAAgWAQgQQARgQAXAAID3AAQAXAAARAQQAQAQAAAWQAAAWgQARQgRAQgXABg");
	this.shape_5.setTransform(10.4,77.8,0.24,0.24);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#ACEEEB").s().p("AiyAwIAAhgIFlAAIAABgg");
	this.shape_6.setTransform(49.6,74.6,0.24,0.24);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#FEFFFF").p("AwoIxIAAxhMAhRAAAIAARhg");
	this.shape_7.setTransform(36.2,64.6,0.24,0.24);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#ACEEEB").s().p("AhgATIAAglIDBAAIAAAlg");
	this.shape_8.setTransform(31.8,61.9,0.24,0.24);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#ACEEEB").s().p("AiHAbIAAg1IEOAAIAAA1g");
	this.shape_9.setTransform(21.4,61.9,0.24,0.24);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#ACEEEB").s().p("AhgATIAAglIDBAAIAAAlg");
	this.shape_10.setTransform(51.7,61.9,0.24,0.24);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#ACEEEB").s().p("AhhATIAAglIDDAAIAAAlg");
	this.shape_11.setTransform(41.3,61.9,0.24,0.24);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#ACEEEB").s().p("AiHCIIAAkOIEPAAIAAEOg");
	this.shape_12.setTransform(31.8,66.7,0.24,0.24);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#ACEEEB").s().p("AiHCIIAAkOIEOAAIAAEOg");
	this.shape_13.setTransform(21.4,66.7,0.24,0.24);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#ACEEEB").s().p("AiGCIIAAkOIEOAAIAAEOg");
	this.shape_14.setTransform(51.8,66.7,0.24,0.24);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#ACEEEB").s().p("AiGCIIAAkOIENAAIAAEOg");
	this.shape_15.setTransform(41.3,66.7,0.24,0.24);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#6EC5C5").s().p("AtvCMIAAkXIbfAAIAAEXg");
	this.shape_16.setTransform(36.3,74.6,0.24,0.24);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#6EC5C5").s().p("AifAuIAAhbIE/AAIAABbg");
	this.shape_17.setTransform(36.6,58,0.24,0.24);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#2382AC").s().p("AhbIxIAAxhIC3AAIAARhg");
	this.shape_18.setTransform(59.7,64.6,0.24,0.24);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#2382AC").s().p("AhcIxIAAxhIC5AAIAARhg");
	this.shape_19.setTransform(12.9,64.6,0.24,0.24);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#6EC5C5").s().p("AhbBCIAAiDIC3AAIAACDg");
	this.shape_20.setTransform(55.4,53.6,0.24,0.24);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#ACEEEB").s().p("AtXBCIAAiDIavAAIAACDg");
	this.shape_21.setTransform(32.7,53.6,0.24,0.24);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CECECE").s().p("AxjA7QgyAAgigjQgkghAAgxMAm3AAAQAAAxgkAhQgiAjgyAAg");
	this.shape_22.setTransform(35.9,83.6,0.24,0.24);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#ADADAD").s().p("AzbAkIAAhHMAm3AAAIAABHg");
	this.shape_23.setTransform(35.9,81.3,0.24,0.24);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FEFFFF").s().p("AwoIxIAAxhMAhRAAAIAARhg");
	this.shape_24.setTransform(36.2,64.6,0.24,0.24);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#CECECE").s().p("AxAKTQgfAAgVgVQgXgWAAgfIAAyRQAAgfAXgWQAVgVAfAAMAiCAAAQAeAAAWAVQAVAWABAfIAASRQgBAfgVAWQgWAVgeAAg");
	this.shape_25.setTransform(36.2,64.6,0.24,0.24);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FDE4D3").s().p("Ai3C4QhNhMAAhsQAAhrBNhMQBMhNBrAAQBsAABMBNQBNBMAABrQAABshNBMQhMBNhsAAQhrAAhMhNg");
	this.shape_26.setTransform(64.2,80.4,0.24,0.24);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FDE4D3").s().p("Ai3C4QhNhMAAhsQAAhrBNhMQBMhNBrAAQBsAABMBNQBNBMAABrQAABshNBMQhMBNhsAAQhrAAhMhNg");
	this.shape_27.setTransform(8.9,80.4,0.24,0.24);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AiCAoQg3gwAAgvQAAglA3AAQAhgBBhAHQAzAABQgGQA3AAAAAlQAAAvg3AwQg7A1hIAAQhHAAg7g1g");
	this.shape_28.setTransform(36.4,34.9,0.24,0.24);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#D45954").s().p("AgTFGQi6gPiYhcQiehghJiUQAdhwBThRQBXhVB1gUQB1gUChBBQCfBBBKBcQAQAUAbAoQAbApAPATQA3BEBCAPQBvAcAhghQhdCBilBDQiJA4iWAAQggAAgfgDg");
	this.shape_29.setTransform(45,7.9,0.24,0.24);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#D45954").s().p("AgPCdQgOhCghhtQgjhSgRgvQgfhXAYgvQAfg9A+AUQAxAQArA3QAsA4AQBMQAPBGgMBIQgMBPgqBTQgiBDg3BMQAZgmgYiFg");
	this.shape_30.setTransform(50.6,18.8,0.24,0.24);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#D45954").s().p("AmyCMQAGjJBjipQA2heBMhDQBShJBfgfQBjgiBoAVQBtAVBHBIQAPAQATAEQAcAGAMAGQgOBShFBHQg6A7haAtQgZANiMA7QhjAqg6AmQh9BWhACAQhCCGAVCPQhYivAGjKg");
	this.shape_31.setTransform(30.1,15,0.24,0.24);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FDE4D3").s().p("AhcBdQgngmAAg3QAAg1AngnQAngnA1AAQA2AAAnAnQAnAnAAA1QAAA2gnAnQgnAng2AAQg1AAgngng");
	this.shape_32.setTransform(51.2,26.6,0.24,0.24);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FDE4D3").s().p("AhcBdQgngnAAg2QAAg1AngnQAngnA1AAQA2AAAnAnQAnAnAAA1QAAA3gnAmQgmAng3AAQg1AAgngng");
	this.shape_33.setTransform(21.6,26.6,0.24,0.24);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgYAZQgKgLAAgOQAAgNAKgLQALgKANAAQAOAAALAKQAKALAAANQAAAOgKALQgLAKgOAAQgNAAgLgKg");
	this.shape_34.setTransform(43.6,23.7,0.24,0.24);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#3D3D3D").s().p("AgPAlQgPgQAQgjIgPgCQAhAAAZgXQAFAQgUAiQgOAdgJAAQgDAAgDgDg");
	this.shape_35.setTransform(45.1,23.6,0.24,0.24);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#D45954").s().p("AAfgcQhFgdh0AqQAfgqAtgQQAtgQArAOQAuAPAnArQAmApAVA7QhGhZg1gWg");
	this.shape_36.setTransform(44.7,18.6,0.24,0.24);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#3D3D3D").s().p("AgNALQgUgiAFgQQAZAXAhAAIgQACQARAkgQAPQgCADgDAAQgKAAgNgdg");
	this.shape_37.setTransform(27.6,23.6,0.24,0.24);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#D45954").s().p("AhdgRQAmgrAugPQArgOAtAQQAsAQAgAqQh0gqhEAdQg2AWhHBZQAWg7Angpg");
	this.shape_38.setTransform(28,18.6,0.24,0.24);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FCD3B7").s().p("AhKBLQgfgfAAgsQAAgqAfggQAfgfArAAQAsAAAfAfQAfAgAAAqQAAAsgfAfQggAfgrAAQgrAAgfgfg");
	this.shape_39.setTransform(46.3,29.8,0.24,0.24);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FCD3B7").s().p("AhKBLQgfgfAAgsQAAgqAfggQAggfAqAAQAsAAAfAfQAfAgAAAqQAAAsgfAfQgfAfgsAAQgqAAgggfg");
	this.shape_40.setTransform(26.6,29.8,0.24,0.24);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#3D3D3D").s().p("AgzBFQgWgdAAgoQAAgnAWgdQAWgdAdAAQAeAAAWAdQAWAdAAAnQAAAogWAdQgWAdgeAAQgdAAgWgdg");
	this.shape_41.setTransform(43.2,24.6,0.24,0.24);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#3D3D3D").s().p("AAoAcQgSgPgWAAQgUAAgTAPQgSAQgJAaQgHgUAAgWQAAgnAWgcQAWgdAdgBQAfABAVAdQAWAcAAAnQAAAWgHAUQgJgagSgQg");
	this.shape_42.setTransform(29.5,23.9,0.24,0.24);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FDE4D3").s().p("AmFKbQgfgXgcgaQhFhCgbhfQhYkqgIijQgNkeBajBQCXlFGcAQQGdgQCXFFQBaDBgNEeQgICjhYEqQgbBfhFBCQgcAagfAXQiuB/jYAAQjXAAiuh/g");
	this.shape_43.setTransform(36.4,23,0.24,0.24);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FCC6A4").s().p("AjXBqQADiYgChiQAAgIAGAAIGhAAQAGAAAAAIQgCBjADCXQhpAvhvgBQhuABhpgvg");
	this.shape_44.setTransform(36.4,42.2,0.24,0.24);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FDE4D3").s().p("AraD/IGyiHQAZgIARgWQAOgVAFgcQALhCADijQAChdgBhaQAAgGAGAAIGtAAQAHAAgBAGIABC5QADCkALA/QAFAcAPAVQAQAWAZAIIGyCHQlcB7l/AAQl+AAlch7g");
	this.shape_45.setTransform(36.4,46.7,0.24,0.24);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#ACEEEB").s().p("AABI/IoCgCQoFgDAAgDQACgIgHg5QgIhAACgvQAIijB7haQA1gnBKgXIHpiXQAZgIARgWQAOgUAFgdQALhEADijQAChdgBhaQAAgGAGAAIGtAAQAHAAgBAGIABC6QADCkALBAQAFAdAPAUQAQAWAZAIIHoCWQBSAaA+AyQBzBbgECcQgBAsgMA/QgKA1ABAJIACAGQAAAAAAABQAAAAAAAAQAAABgBAAQAAAAgBAAg");
	this.shape_46.setTransform(36.4,51.4,0.24,0.24);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FDE4D3").s().p("AwDIuQgBAAAAAAQgBAAAAAAQAAgBAAAAQAAAAAAgBIACgFQABgFgCg6QgCg9AHgvQAWieB5hYQAzgmBHgWIHbiTQAZgHAPgVQAPgVAEgbQALhCADieQAChegBhUQAAgBAAAAQAAgBAAAAQAAgBABAAQAAgBABAAQAAAAABgBQAAAAABAAQAAgBABAAQAAAAABAAIGgAAQABAAAAAAQABAAAAABQABAAAAAAQABABAAAAQABAAAAABQAAAAABABQAAAAAAABQAAAAAAABQgBBZACBbQADCfALA/QAFAcAOAUQAPAVAZAHIHaCTQBOAXA+AyQBvBYALCXQADAtgGA7QgFA2ACAHQAAAEv7ADg");
	this.shape_47.setTransform(36.3,51.4,0.24,0.24);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#D45954").s().p("AqANoIAA7PIUBAAIAAbPg");
	this.shape_48.setTransform(36.4,36,0.24,0.24);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.6,0,86.1,86.7);


(lib.Symbol21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Isolation Mode
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3D494E").s().p("ABdAhQgHgNAAgUQAAgSAHgOQAJgOAKAAQALAAAHAOQAIAOAAASQAAAUgIANQgHAPgLAAQgKAAgJgPgAiCAhQgIgNAAgUQAAgSAIgOQAIgOALAAQALAAAHAOQAIAOAAASQAAAUgIANQgHAPgLAAQgLAAgIgPg");
	this.shape.setTransform(85,72.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#604839").s().p("ABUAoQgEgDAFgJQAJgQAXgFQAVgFATAIQAJAEgDACQgCADgGgCQgSgGgQAGQgQAGgMANQgEAFgDAAIgCgBgAhcgJQgMgOgPgFQgRgGgSAFQgGADgCgDQgCgCAIgEQATgIAVAFQAXAFAJAQQAFAIgDAEIgDABQgDAAgEgFg");
	this.shape_1.setTransform(84.6,62.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D9B293").s().p("AgzgSQAXAUAcAAQAdAAAYgUQgQAlglAAQgkAAgPglg");
	this.shape_2.setTransform(85,84.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 7
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#512A15").s().p("ABbAZIhbgUIhbAUIgSAAIBtgwIAAgBIAAABIAAgBIAAABIBvAwg");
	this.shape_3.setTransform(85.3,93.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("ABbBQIg2hDIgigpIgDgFIB8gyIACgBIAdAeIACABIg+CKgAicg1IACgBIAdgeIACABIB7AyIgCAFIgiApIg2BEIgDAEg");
	this.shape_4.setTransform(85.2,127.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#21282C").s().p("Ai0gRIBEgNIh9i6IACgBQAmgNArgJIgCACIA/CLIADgEIAyC7IAoCbIAoibIAzi7IADAEIA/iLIgCgCQAqAJApAOIAAAAIh9C6IBDANIi0EBg");
	this.shape_5.setTransform(85.2,145.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#A93239").s().p("AgnBKIAWjrIgTgUIAigrIACgEIADAEIAiArIgUAUIAYDrIgpCbg");
	this.shape_6.setTransform(85.2,147.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#B3C1CA").s().p("AARhTIAUgUIA2BDIgzC8gAhagjIA1hEIAUAUIgXDrgAAAiWIgCAEIACgFIADAFg");
	this.shape_7.setTransform(85.2,139.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#313F48").s().p("AEJDlIoTAAIAkigIANg2QAoimAUgUQAWgXB/gaIAmgIIgCABIB9C6IhDANIC1EBIgCAAg");
	this.shape_8.setTransform(58.4,147);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E3E4E5").s().p("ADpCnQgLgvAFhnQAZhHgrhcQgNgdgSgbIgQgVIATgKQBSgiAtB7QATA0AGBDQAFA8gHAqIgBABQglgMgLAlQgMAlAJBlQgjgdgLgtgAkZBnQgLglglAMIAAgBQgIgqAFg8QAGhCATg1QAth8BTAjIASAKQgaAfgVAuQgrBcAZBHQAFBmgLAwQgLAtgjAdQAJhlgMglg");
	this.shape_9.setTransform(85.1,57.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#EED2B6").s().p("Ah7F6IgDgBQhOgjgUgZQgdgmgYhMQgKgggHgjIgDABQgaADgWgZQgWgYgEgnQgEgnAQgbQAMgWARgIIAAABQAlgMAMAlQALAjgIBmQAigdALgtQALgugFhnQgZhJArhbQAVguAbggIgSgJQBLg6BoAAIAAAAQBoAABMA5IgTAKIAQAVQATAbANAeQArBbgaBJQgFBnAMAuQALAtAiAdQgIhmALgjQAMglAlAMIAAgBQARAIAMAWQARAbgEAnQgEAngWAYQgWAZgbgDIgCgBQgHAjgKAgQgZBNgdAlQgTAZhOAjIgDABQhQAkgsAAQgrAAhQgkgABcEJIATAAIhugxIAAgBIgBAAIAAAAIAAABIhtAxIATAAIBagTg");
	this.shape_10.setTransform(85.2,69.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#D19E84").s().p("Ah7AfIAAhvQBQAkArAAQAsAABQgkIAABvIh8Ayg");
	this.shape_11.setTransform(85.2,115.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#2B3A46").s().p("AkKDlIC1kCIhEgNIB+i6IAkAIQB/AaAXAWQATAVApClIALA0IAlCjg");
	this.shape_12.setTransform(111.9,147.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#658D2B").s().p("ApXKpQh2h2hBiYQhDieAAitQAAirBDieQBBiZB2h1QB1h2CZhAQCehDCrAAQCtAACdBDQCZBAB1B2QB2B1BACZQBDCeABCrQgBCthDCeQhACYh2B2QgvAvg2AnIgLgzQgpiogTgUQgXgXh/gaIgmgHIAAgBQgpgOgqgJIgdgdIgDAAIAAhwIAEgCQBOgjATgZQAdglAZhMQAKghAHgjIACABQAbAEAVgZQAXgZAEglQADgmgQgeQgMgWgRgIQAHgqgFg9QgGhDgSg1Qguh7hSAiQhMg5hpAAIgBAAQhnABhLA5QhTgjgtB7QgTA1gFBDQgGA+AIAqQgRAHgMAWQgQAeAEAmQAEAlAWAZQAVAZAbgEIACgBQAHAjALAhQAYBLAdAmQAUAZBNAjIAEACIAABwIgCAAIgeAdQgqAJgnANIgoAJQh/AagWAXQgUAUgoCoIgNA1Qg3gngwgxg");
	this.shape_13.setTransform(85,77);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,170,170);


(lib.Symbol20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Isolation Mode
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3D494E").s().p("ABbAfQgHgNAAgSQAAgRAHgOQAIgNALAAQAKAAAIANQAHAOAAARQAAASgHANQgIAOgKAAQgLAAgIgOgAh+AfQgIgNAAgSQAAgRAIgOQAIgNAKAAQAKAAAIANQAHAOAAARQAAASgHANQgIAOgKAAQgKAAgIgOg");
	this.shape.setTransform(84.5,78);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E08C30").s().p("ACiAQQgVgfgpAIQgJACgBgFQgBgFAJgDQASgEAUAJQATAIAJAPQAFAIgCABIgBABQgCAAgCgEgAimATQgBgBAEgIQAKgPATgIQATgJASAEQAKADgBAFQgBAFgJgCQgqgIgUAfQgDAEgCAAIgBgBg");
	this.shape_1.setTransform(84.5,69.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D9B293").s().p("AAAASQgjAAgPgjQAWASAcABQAdAAAWgTQgPAjgjAAIgBAAg");
	this.shape_2.setTransform(84.5,89.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 20
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#5B9E3D").s().p("AiiDuIAAj8QBOAAA4hBQA1hBAChcIAAgBIAwAUQATAIATAMIACABIATBGQARBCAIA8QAIA/gMBeQgFAvgIAig");
	this.shape_3.setTransform(101.3,146.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#8ABF43").s().p("AiNDuQgIgigGgvQgLheAIg/QAHg9AShBQARg5ACgNIACgBQATgMATgIIAjgPIANgFQACBdA1BBQA4BBBOAAIAAD8g");
	this.shape_4.setTransform(68.7,146.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#EED2B6").s().p("AAAFqQgqAAhOg2IgDgCQhBgsgYgfQgngygUguQgLgZgHgbIgIACQgaAEgWgZQgVgZgEgmQgFglAQgeQAIgNAJgIQALgKAPgCIAGgBIAAgBQAHAOAJANQAQh5BFgdQAZgLBOgOQA1gJA1gnIAGgFQAdgWASglQACAWAGAWQAsCmDIBIIAGgVIAGABQAPACALAKQAJAIAHANQARAegFAlQgDAmgWAZQgWAZgbgEIgIgCQgHAbgLAZQgUAugnAyQgYAehAAsIgEADQhOA2gqAAIAAAAgAg7DmQAZASAiABIAAAAQAjAAAZgTQAZgTAAgbIipAAQAAAbAZATgAkakIIAAABIAAAAIAAgBg");
	this.shape_5.setTransform(85.2,78.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E5C1A6").s().p("AEyExQAHgiAGgvQAMhegIhAQgIg+gShAIgShFIgCgBQBIAnAWAiQAYBEAOCaIADAgQAFA6ACAygAmiExQACgyAFg6IADggQAPiaAXhEQAWgjBJgnIgCACQgCAMgRA5QgSBAgHA+QgIBAALBeQAGAvAIAigAiEgNQg3hBgChdIgNAGQAqgTAngNIAAhqIADACQBOA1AoABIABAAQArgBBOg1IAEgDIAABtIBAAaQgCBbg3BBQg3A/hOAAQhMAAg4g/g");
	this.shape_6.setTransform(85,139.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#56392C").s().p("Ak2EpIAAABIgGAAQgPACgLAKQgdhCABhPQAAiaBthrQBBhBBRgaIgBgSQAAgwAjgjQAhgiAwAAIAAAAQAwAAAiAiQAjAjAAAwIgBASQBRAaBBBBQBtBrAACaQAABPgcBCQgLgKgPgCIgGAAIgGAUQjIhIgsimQgGgVgCgXQgSAlgdAXIgGAFQg1Ang1AJQhOAOgZAKQhFAegQB4QgJgNgHgNgAkaCHIAAAAIAAgCIAAACg");
	this.shape_7.setTransform(85.2,38.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#369F93").s().p("AGZL8QgPiagXhGQgWgihJgnQgSgMgUgIIgwgUIAAABIhBgaIAAhtQBBgsAXgeQAngyAVguQAKgZAIgbIAIACQAaAEAWgZQAWgZAEgkQAEgngQgeQgHgNgJgIQAchDAAhOQAAiahthtQhBhChSgaIACgRQAAgxgjgiQgigjgxAAIgBAAQguABgiAiQgjAiABAxIABARQhSAahBBCQhtBtAACaQAABOAcBDQgJAIgHANQgQAeAEAnQAEAkAVAZQAXAZAagEIAHgCQAIAbAKAZQAVAuAnAyQAXAfBCAsIAABrQgnANgqASIgjAPQgTAIgTAMQhJAngWAiQgXBGgPCaIgDAgQhng5hVhVQh2h2hBiYQhDieAAitQAAirBDieQBBiZB2h1QB1h2CZhAQCehDCrAAQCtAACdBDQCZBAB1B2QB2B1BACZQBDCeABCrQgBCthDCeQhACYh2B2QhVBVhnA5IgDggg");
	this.shape_8.setTransform(85,79.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAAAgQgigBgZgSQgZgRAAgbICpAAQAAAbgZARQgZATgjAAIAAAAg");
	this.shape_9.setTransform(85.2,100.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,170,170.2);


(lib.Symbol19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Isolation Mode
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E08C30").s().p("AguAYQgCgCAGgKQALgSAXgKQAWgLAWAFQAMADgBAHQgCAGgLgCQgwgLgZAmQgEAFgBAAIgCAAg");
	this.shape.setTransform(71.4,63.8,0.99,0.99);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E08C30").s().p("AAoATQgZgmgxALQgKACgCgGQgBgHALgDQAXgFAVALQAXAKAMASQAFAKgCACIgBAAQgCAAgDgFg");
	this.shape_1.setTransform(98.3,63.8,0.99,0.99);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3D494E").s().p("ABZAmQgIgQgBgWQABgVAIgQQAKgQAMAAQANAAAJAQQAJAQAAAVQAAAWgJAQQgJAQgNAAQgMAAgKgQgAiEAmQgJgQAAgWQAAgVAJgQQAKgQAMAAQANAAAIAQQAKAQgBAVQABAWgKAQQgIAQgNAAQgMAAgKgQg");
	this.shape_2.setTransform(84.8,73.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAjCDIAAgBIgbgmIgEgGIgEgHIB8g/IACgBIBBAmIhUBDIgzApgAiMBeIgsgkIgGgFIBBgmIACABIB7A/IgKARIgBABIgYAhIgWAfgAgaiSQgKgGgBgIIBLAAQAAAIgMAGQgKAGgQAAQgOAAgMgGg");
	this.shape_3.setTransform(85,117.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#353A44").s().p("AhQCyIgFhDIABgCIBKkVIBRhDQAZAHAXAIIh7C5IBBAOIizEBg");
	this.shape_4.setTransform(97,146.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#2A968C").s().p("AglClIAEgwIABgSIAAgDIATj1IgVgWIAAAAIgBgBIAYggIABgCIAKgQIAEAGIAEAHIAbAlIAAABIgLALIgFAFIgFAGIATD1IAAACIAFBDIgmA6g");
	this.shape_5.setTransform(85,147.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#B3C1CA").s().p("Ah7iiIBTBEIAXggIAAABIAAAAIATAWIgRD1IAAACIgBATgAAfhnIAFgGIAFgFIALgLIAVAfIAzgpIhKEVg");
	this.shape_6.setTransform(83.3,143.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#2B2F34").s().p("AkKDqIAkiiIANg0QAoimAUgUQAWgXB/gaQAtgKAsgIIAsAkIBrFFIgEAwIAnA6g");
	this.shape_7.setTransform(58.3,146.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#5D0206").s().p("Ah7G5IgDgBQhNgjgUgZQgUgagUgyQgXg7gNhEIgDgUIAHAUIACAFQAOAoATAfQAnBAAsAAQAeAAA4gaQA4gZAjAAQAlAAA4AZQA4AaAeAAQAsAAAnhBQASgeAOgnIACgGIAHgUIgDAUQgMBEgXA7QgUAzgUAZQgUAZhOAjIgDABQhQAkgsAAQgrAAhQgkgAgZFwQALAGAOAAQAQAAALgGQALgGAAgIIhKAAQAAAIALAGgADpBVQgLgwAFhkQAWhAgehNIAAAAQgbg2g/gIQgqgEhXATIAAAAQhvgbhCAkQgUALgOAQIgIAMQgdBNAWA/QAFBkgLAwQgLAtgjAeQAJhmgMglQgLgjglAMIAAgBQgIgqAFg9QAGhDATg1QAVg6AegYQBIhmBagpQAvgWAvgDQAmgCAmAJQBRATA+A+QA8A8ATBOIAAAAIAKAYQATA1AGBDQAFA9gIAqIAAABQglgMgLAjQgMAlAJBmQgjgegLgtg");
	this.shape_8.setTransform(85,65.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#EED2B6").s().p("ABcELQg4gagkAAQgkAAg3AaQg4AZgeAAQgtAAgnhAQgSgfgOgnIgCgGIAAAAIgHgTIADAUQgZADgVgYQgWgZgEgmQgEgnAQgeQAMgUARgHIAAAAQAlgMAMAmQALAjgIBlQAigdALgtQALgwgFhlQgVg/AdhMIAIgNQANgQAVgLQBBgkBwAbIAAAAQBWgTArAFQA+AHAcA2IAAABQAdBNgWA/QgFBlAMAwQALAtAiAdQgIhlALgjQAMgmAlAMIAAgBQARAIAMAUQARAegEAnQgEAmgWAZQgVAYgZgDIADgUIgHATIgBAAIgBAGQgOAogTAeQgmBAgtAAQgeAAg4gZg");
	this.shape_9.setTransform(85,68.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#D19E84").s().p("Ah7ANIAAhYQBQAjArAAQAsAABQgjIAABYIh8A/g");
	this.shape_10.setTransform(85,117.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#27292F").s().p("AkKDlIC1kCIhDgNIB9i6IAlAIQB/AaAVAWQAUAVAoClIAMA1IAlCig");
	this.shape_11.setTransform(111.8,147.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#716867").s().p("AAAgBIABAAIgBADIAAgDg");
	this.shape_12.setTransform(114.6,83.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#786F6E").s().p("ApXKpQh2h1hBiZQhDieAAitQAAirBDieQBBiYB2h2QB1h1CZhBQCehDCrAAQCtAACdBDQCZBBB1B1QB2B2BACYQBDCeABCrQgBCthDCeQhACZh2B1QgwAwg2AoIgMg1QgoiogUgUQgWgXh/gaIgmgHQgYgIgYgHIhBgmIgCABIAAhbIADgBQBOgjATgZQAVgZAUgzQAXg7AMhEQAZADAVgYQAWgYAEglQAEgmgRgeQgLgXgSgHQAIgqgGg9QgGhDgSg1IgKgYIAAAAQgThOg8g8Qg/g+hQgTQgngJglACQgvADgvAWQhaAphIBmQgeAYgWA6QgTA1gFBDQgFA9AIAqQgSAIgMAWQgPAeADAmQAEAlAWAYQAVAYAZgDQAMBEAXA7QAUAyAUAaQAVAZBNAjIADABIAABbIgCgBIhBAmIAGAFQgtAHguAKQiAAagWAXQgTAUgpCoIgMA1Qg2gogwgwgAknBBIABAAIABAFIgCgFg");
	this.shape_13.setTransform(85,76.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,170,170);


(lib.Symbol18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Isolation Mode
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D9B293").s().p("Ag+gVQAbAXAjAAQAiAAAcgXQgRAsgtAAQgrgBgTgrg");
	this.shape.setTransform(85.4,86.9,0.94,0.94);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#9F6D5C").s().p("ACzARQgUgRgZgDQgYgCgXAJQgNAGgDgHQgDgEANgJQAVgPAhAHQAfAHAUAUQAIAKgEACIgCAAQgEAAgFgEgAi9AVQgEgCAIgKQAUgUAfgHQAhgHAVAPQANAJgEAEQgDAHgMgGQgXgJgXACQgaADgUARQgFAEgEAAIgCAAg");
	this.shape_1.setTransform(84.1,63.6,0.94,0.94);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3D494E").s().p("ABwAoQgJgRAAgXQAAgWAJgRQAIgRANAAQANAAAKARQAIARABAWQgBAXgIARQgKARgNAAQgNAAgIgRgAiaAoQgKgRABgXQgBgWAKgRQAJgRAMAAQANAAAKARQAIARABAWQgBAXgIARQgKARgNAAQgMAAgJgRg");
	this.shape_2.setTransform(85.4,73.4,0.94,0.94);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#A0B6C1").s().p("AkKDsIAAgBIAwhaIgeksIAUgTIA5BIIA8iFIB5AWQB/AaAVAXQAUAUApCmIAMA0IAkCigAiWCFICcAAIAAgkIicAAg");
	this.shape_3.setTransform(111.7,146.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#2DA192").s().p("AguCLIAdksIgTgTIAkgwIAlAwIgTATIAdEsIgvBag");
	this.shape_4.setTransform(85,147);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#778E9A").s().p("ABzARIAAghICfAAIAAAhgAkRARIAAghICfAAIAAAhg");
	this.shape_5.setTransform(85,157.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#BACBD3").s().p("AkKDsIAkiiIAMg0QApimATgUQAWgXB/gaQA+gNA7gJIA8CFIA5hIIAUATIgeEsIAwBaIAAABgAgFCFICcAAIAAgkIicAAg");
	this.shape_6.setTransform(58.3,146.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#D19E84").s().p("Ah7AfIAAhvQBQAkArAAQAsAABQgkIAABvIh8Ayg");
	this.shape_7.setTransform(85,115.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAlCNIglgvIB8gzIACgBIAfAgIgDAGIg8CFgAiZBQIgDgGIAfggIACABIB7AzIgkAvIg5BIgAAABegAhHi3QgegMAAgRIDLAAQAAARgfAMQgdAMgqAAQgoAAgfgMg");
	this.shape_8.setTransform(85,114.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#EED2B6").s().p("Ah7E4IgDgCQhNgjgVgZQgcglgZhMQgKghgHgiIgDAAQgaAEgWgZQgWgYgEglQgDgmAPgfQARgeAagDIAKAAQAQACAKAIQAMAJABAuQAAAWgCAVIAWAAQgDgVABgZQADgxAYgRIAAg2QgDAAgEgJQgIgRAAgnQAAgyA6gdQA+geBzAAIACAAQBzAAA9AeQA7AdAAAyQAAAngHARQgEAJgEAAIAAA2QAYARADAxQACAZgDAVIAVAAQgCgVAAgWQACguAKgJQALgIAPgCIAIAAIAGAAQAaADAQAeQARAfgEAmQgFAlgVAYQgWAZgagEIgDAAQgHAigKAhQgYBMgeAlQgTAZhOAjIgDACQhQAjgsAAQgrAAhQgjgAhHDEQAfANAoAAQAqAAAdgNQAfgMAAgRIjLAAQAAARAeAMg");
	this.shape_9.setTransform(85,76.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#EF961C").s().p("AHmLMQgpiogTgUQgWgXh/gaIh7gWIADgGIgfggIgCABIAAhxIADgBQBOgjATgZQAegmAYhMQAKghAHgiIADAAQAaAEAWgZQAVgYAFglQAEgmgRgeQgQgegagEIgGAAIAHAAIAAigQgBiSighLQgygYg8gOIgvgIIAAgBIAAAAIgCAAIAAABQhOAIhPAmQihBLABCSIAACgIADAAQgaAEgRAeQgPAeADAmQAEAlAWAYQAWAZAagEIADAAQAHAiAKAhQAZBMAcAmQAVAZBNAjIADABIAABxIgCgBIgfAgIADAGQg7AJhAANQh/AagWAXQgUAUgoCoIgMA1Qg2gogwgwQh2h1hBiZQhDieAAitQAAirBDieQBBiYB2h2QB1h1CZhBQCehDCrAAQCtAACdBDQCZBBB1B1QB2B2BACYQBDCeABCrQgBCthDCeQhACZh2B1QgwAwg2AoIgMg1g");
	this.shape_10.setTransform(85,76.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#7F5538").s().p("AD3ELQAEgVgDgYQgDgxgYgRIAAg2QAFAAADgJQAHgRAAgnQAAgwg6gdQg9geh0gBIgBAAQh0ABg9AeQg7AdAAAwQAAAnAIARQAEAJADAAIAAA2QgYARgDAxQgBAYADAVIgVAAQACgUgBgXQAAgtgMgJQgLgJgPgBIgKAAIgEAAIAAigQAAiQCghMQBPglBPgJIAAAAIAAAAIABAAIAAAAIAvAJQA9AOAxAXQChBMAACQIAACgIgGgBIgIABQgPABgLAJQgLAJgBAtQAAAXACAUg");
	this.shape_11.setTransform(84.9,48.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,170,170);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgbAxQgNgGgGgNQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGANAAAQQAAARgGANQgHANgMAGQgMAIgRAAQgPAAgMgIgAgVgbQgIALAAAQQAAARAIALQAIAKANAAQAOAAAIgKQAIgKAAgSQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape.setTransform(247.6,22.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAXA2IAAgvIgtAAIAAAvIgYAAIAAhrIAYAAIAAArIAtAAIAAgrIAYAAIAABrg");
	this.shape_1.setTransform(235.4,22.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgKA2IAAhYIgkAAIAAgTIBdAAIAAATIgkAAIAABYg");
	this.shape_2.setTransform(224.5,22.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAaA1QgEgDgCgHQgHAHgKADQgIAEgLgBQgRAAgKgIQgJgJAAgPQAAgLAEgGQAFgGAHgCQAHgDAJgCIAQgCIALgCIAIgCIADgCIABgDIAAgFQAAgIgFgEQgGgEgJAAQgLgBgGAFQgGAEgBALIgXAAQACgUANgJQANgIAUAAQALAAAKADQAKAEAFAHQAGAHAAAMIAAA1IABAGQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIACAAIADgBIAAARIgIACIgHAAIgCABQgGAAgEgEgAAHAEIgLABQgGABgFACQgGABgDAEQgEADAAAHQAAAIAFAEQAFAEAKAAQAHAAAFgCQAGgCAEgEQAEgEABgFIAAgKIAAgMQgGACgGACg");
	this.shape_3.setTransform(214,22.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgtA2IgGgBIAAgTIAEABIAEAAQAEAAADgCQADgCADgGQACgGAAgLIACg+IBOAAIAABrIgYAAIAAhYIgfAAIgCAtQAAAWgJAMQgIALgPAAIgIgBg");
	this.shape_4.setTransform(201.5,22.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAXA2IAAhYIgtAAIAABYIgYAAIAAhrIBdAAIAABrg");
	this.shape_5.setTransform(190,22.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgZAyQgNgIgGgMQgGgNAAgQQAAgQAGgNQAHgOAMgGQAMgIAPAAQAUAAAOALQAMAKACATIgYAAQgCgKgGgGQgGgFgKAAQgMAAgIAKQgIAKAAARQAAASAIAJQAIAKALABQALAAAGgGQAIgHABgMIAYAAQgDAVgNALQgOAMgUAAQgPgBgLgGg");
	this.shape_6.setTransform(178.2,22.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgaAxQgLgHgHgNQgGgMAAgQQAAgPAGgNQAHgOAMgIQAMgHANAAQAQAAAMAHQALAIAHAOQAFANAAAQIAAADIhOAAQABARAIAIQAHAJANAAQAJAAAGgEQAHgFADgLIAXAAQgDAMgGAJQgIAIgKAFQgKAEgMAAQgPAAgMgIgAAcgJQgCgNgHgJQgHgHgMAAQgKAAgHAHQgIAIgBAOIA2AAIAAAAg");
	this.shape_7.setTransform(166.5,22.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgbBGQgNgHgGgPQgDgHgCgJIgBgSIABgVQACgLADgLQADgLAFgIQAHgLAKgFQALgGANgBIAPgBQAFgBACgEIAYAAQgCAKgEAFQgEAFgHADQgGACgIAAIgRACQgJABgHAFQgIAGgFAJQgFAIgCAMIAAAAQAHgLAJgGQAJgGAOAAQAOAAALAHQALAHAGAMQAHALAAARQAAARgHAMQgGANgMAHQgLAHgQAAQgQAAgMgIgAgUgDQgIAIAAARQAAARAIAKQAIAKAMAAQANAAAIgKQAIgKAAgRQAAgRgIgIQgIgKgNAAQgMAAgIAKg");
	this.shape_8.setTransform(154.5,20.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AguA2IAAhrIAYAAIAAAoIAgAAQASAAAJAJQAKAHAAAQQAAAQgKAJQgJAKgSAAgAgWAjIAdAAQAIAAAEgEQAEgFgBgGQAAgIgDgEQgFgFgIAAIgcAAg");
	this.shape_9.setTransform(137,22.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgKA2IAAhYIgkAAIAAgTIBdAAIAAATIgkAAIAABYg");
	this.shape_10.setTransform(126.1,22.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAaA1QgEgDgCgHQgHAHgKADQgIAEgLgBQgRAAgKgIQgJgJAAgPQAAgLAEgGQAFgGAHgCQAHgDAJgCIAQgCIALgCIAIgCIADgCIABgDIAAgFQAAgIgFgEQgGgEgJAAQgLgBgGAFQgGAEgBALIgXAAQACgUANgJQANgIAUAAQALAAAKADQAKAEAFAHQAGAHAAAMIAAA1IABAGQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIACAAIADgBIAAARIgIACIgHAAIgCABQgGAAgEgEgAAHAEIgLABQgGABgFACQgGABgDAEQgEADAAAHQAAAIAFAEQAFAEAKAAQAHAAAFgCQAGgCAEgEQAEgEABgFIAAgKIAAgMQgGACgGACg");
	this.shape_11.setTransform(115.6,22.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgvA2IAAhrIA6AAQAPAAAJAHQAIAHAAANQAAAJgEAFQgDAFgJAEIAAAAQALADAFAFQAFAHAAALQAAAPgLAIQgKAIgTAAgAgXAkIAfAAQAIAAAEgEQAEgEAAgHQAAgHgEgEQgEgEgIAAIgfAAgAgXgKIAdAAQAGAAAEgDQADgDAAgHQABgGgEgDQgEgDgHAAIgcAAg");
	this.shape_12.setTransform(104,22.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgbAxQgNgGgGgNQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGANAAAQQAAARgGANQgHANgMAGQgMAIgRAAQgPAAgMgIgAgVgbQgIALAAAQQAAARAIALQAIAKANAAQAOAAAIgKQAIgKAAgSQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape_13.setTransform(91.7,22.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgbBGQgNgHgGgPQgDgHgCgJIgBgSIABgVQACgLADgLQADgLAFgIQAHgLAKgFQALgGANgBIAPgBQAFgBACgEIAYAAQgCAKgEAFQgEAFgHADQgGACgIAAIgRACQgJABgHAFQgIAGgFAJQgFAIgCAMIAAAAQAHgLAJgGQAJgGAOAAQAOAAALAHQALAHAGAMQAHALAAARQAAARgHAMQgGANgMAHQgLAHgQAAQgQAAgMgIgAgUgDQgIAIAAARQAAARAIAKQAIAKAMAAQANAAAIgKQAIgKAAgRQAAgRgIgIQgIgKgNAAQgMAAgIAKg");
	this.shape_14.setTransform(79.4,20.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgbAxQgNgGgGgNQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGANAAAQQAAARgGANQgHANgMAGQgMAIgRAAQgPAAgMgIgAgVgbQgIALAAAQQAAARAIALQAIAKANAAQAOAAAIgKQAIgKAAgSQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape_15.setTransform(66.9,22.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("Ag0BLIAAiSIAXAAIAAAOQAGgJAIgEQAKgEAJAAQAPAAALAHQALAHAGANQAGANAAASQAAAQgGALQgGANgLAHQgKAHgPAAQgJAAgJgEQgJgEgGgJIAAA2gAgVgtQgIAKAAASQAAARAIAJQAIAKANAAQANAAAIgKQAIgJAAgQQAAgSgIgLQgIgKgNAAQgNAAgIAKg");
	this.shape_16.setTransform(54.5,24.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAXA2IAAhYIgtAAIAABYIgYAAIAAhrIBdAAIAABrg");
	this.shape_17.setTransform(41.8,22.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgbAxQgNgGgGgNQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGANAAAQQAAARgGANQgHANgMAGQgMAIgRAAQgPAAgMgIgAgVgbQgIALAAAQQAAARAIALQAIAKANAAQAOAAAIgKQAIgKAAgSQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape_18.setTransform(29.6,22.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAiBKIAAh9IhDAAIAAB9IgaAAIAAiTIB3AAIAACTg");
	this.shape_19.setTransform(15.9,20.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#55B553").ss(6.2,1,1).p("A0KlJMAoVAAAQByAAAAByIAAGvQAAByhyAAMgoVAAAQhyAAAAhyIAAmvQAAhyByAAg");
	this.shape_20.setTransform(132.1,21.5,0.94,0.65);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#55B553").s().p("A0KFKQhyAAABhyIAAmvQgBhxBygBMAoVAAAQBxABABBxIAAGvQgBByhxAAg");
	this.shape_21.setTransform(132.1,21.5,0.94,0.65);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.1,-3.1,270.3,49.1);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_44 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(44).call(this.frame_44).wait(1));

	// 8629
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#55B553").s().p("AhOB2QgbgoAAhOQAAhMAbgoQAbgoAzgBQA0AAAbAoQAbAoAABOQAABNgbAoQgbAog0AAQgzAAgbgogAgfhnQgLAMgFASQgFASgBAVQgCAUABARQgBA7AOAbQAOAbAbAAQAUgBAMgMQALgMAFgSQAFgTABgTQACgTgBgPQABgOgCgTQgBgUgFgTQgEgSgMgNQgMgMgUAAQgTAAgMALg");
	this.shape.setTransform(170.1,20.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#55B553").s().p("AALCaIAAjTIhLAAIAAglQARABATgEQATgEANgNQAPgNAFgaIApAAIAAEzg");
	this.shape_1.setTransform(143.5,20.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#55B553").s().p("AhOB2QgbgoAAhOQAAhMAbgoQAbgoAzgBQA0AAAbAoQAbAoAABOQAABNgbAoQgbAog0AAQgzAAgbgogAgfhnQgLAMgFASQgFASgBAVQgCAUABARQgBA7AOAbQAOAbAbAAQAUgBAMgMQALgMAFgSQAFgTABgTQACgTgBgPQABgOgCgTQgBgUgFgTQgEgSgMgNQgMgMgUAAQgTAAgMALg");
	this.shape_2.setTransform(121.2,20.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#55B553").s().p("AhOB2QgbgoAAhOQAAhMAbgoQAbgoAzgBQA0AAAbAoQAbAoAABOQAABNgbAoQgbAog0AAQgzAAgbgogAgfhnQgLAMgFASQgFASgBAVQgCAUABARQgBA7AOAbQAOAbAbAAQAUgBAMgMQALgMAFgSQAFgTABgTQACgTgBgPQABgOgCgTQgBgUgFgTQgEgSgMgNQgMgMgUAAQgTAAgMALg");
	this.shape_3.setTransform(96.7,20.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#55B553").s().p("AAVCbIAAhJIiBAAIAAgyICCi6IAwAAIAADBIAnAAIAAArIgnAAIAABJgAhCAnIBYAAIAAiDg");
	this.shape_4.setTransform(169.9,20.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#55B553").s().p("Ag8CZQABghAJgjQAKglAQgjQAQgiATgfQAVggAXgVIifAAIAAguIDQAAIAAApQgXAZgVAfQgTAfgPAhQgPAkgIAkQgIAjAAAkg");
	this.shape_5.setTransform(145.5,20.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#55B553").s().p("Ag4CSQgYgNgNgXQgNgWAAgfIAAgHIAwAAQABAhAPAQQAPAQAbAAQAaAAAOgPQAPgOAAgaQAAgTgIgLQgJgLgOgFQgOgGgOABIgIAAIgIAAIAAgkIAEAAQAQAAAMgDQAPgCAJgKQAJgJABgUQgBgUgMgMQgNgMgUgBQgYAAgNAQQgNAPgBAeIgwAAQACgwAagbQAagbAtAAQAcAAAVAKQAWALAMASQAMATAAAZQAAAWgMARQgLAQgUAHQAaAHAPARQAOATAAAcQAAAdgNAXQgOAWgZANQgYAMgfAAQggAAgYgMg");
	this.shape_6.setTransform(96.5,20.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#55B553").s().p("Ag3CQQgXgMgNgUQgNgVgCgdIAyAAQADAYAPAOQAOAOAYAAQAaAAAPgSQAOgRAAggQAAgegPgRQgPgPgaAAQgRAAgMAGQgMAHgKANIgsgCIAdijICdAAIAAArIh5AAIgOBPQAMgMAPgFQAOgFAPAAQAdABAWAMQAVAMAMAXQAMAVAAAeQAAAhgOAYQgOAZgZAOQgZAOgeAAQgfAAgXgLg");
	this.shape_7.setTransform(169.8,20.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#55B553").s().p("Ag3CSQgYgMgOgUQgOgWAAgcQAAgdAPgTQAPgTAbgIQgVgHgMgRQgLgRAAgXQAAgYAMgTQAMgTAWgJQAWgKAbgBQAcABAVAKQAVAJAMATQAMASAAAXQAAAYgLARQgLAQgWAJQAbAJAPAQQAPARAAAfQAAAcgOAXQgOAVgYAMQgYAMggAAQgfAAgYgMgAgoAWQgQAPAAAZQAAAaAQAPQAQAPAZAAQAaAAAPgOQAPgOAAgbQAAgagQgPQgPgPgaAAQgZAAgPAPgAgihpQgNANAAAUQAAAVANALQAOAMAUABQAWgBANgLQANgMAAgVQAAgUgNgNQgNgMgWABQgVgBgNAMg");
	this.shape_8.setTransform(145.6,20.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#55B553").s().p("AhoCbQABgjAMgXQAMgXAUgSQAVgRAagRIAdgVQAPgJALgPQALgPAAgVQgBgYgNgNQgMgPgYAAQgUAAgLAMQgMAKgEARQgEAQAAASIgxAAIAAgHQABgiALgYQAOgYAWgOQAXgNAfAAQAdAAAXAMQAWALAMAUQAMAWABAcQgBAcgMAUQgMASgSAOQgUAPgWAOIgXARQgNAKgLALQgLAMgFAMICUAAIAAAtg");
	this.shape_9.setTransform(120.5,20.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#55B553").s().p("AhNB2QgcgnAAhLQAAgyANgkQAOgkAagUQAZgTAiAAQAoAAAXAVQAZAWAFAnIgxAAQgEgWgLgKQgMgLgUAAQgSABgLALQgNALgGARQgGARgDASIgEAgQAMgSASgKQARgKAWAAQAcAAAVANQAWANALAWQAMAVAAAeQAAAggMAYQgOAZgXANQgXAOgfAAQg0AAgcgogAgkAGQgOAQAAAdQAAAeAPASQAPARAXAAQAYAAAOgRQAPgRABgeQgBgdgPgRQgOgPgZAAQgXAAgPAPg");
	this.shape_10.setTransform(170.2,20.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#55B553").s().p("Ag0CUQgVgLgMgSQgNgTgBgYIAwAAQACATAMALQANAMAVABQARgBAMgLQAMgLAGgRQAHgRACgSQADgSAAgPQgMAUgRAJQgQAKgVAAQgcAAgWgNQgVgNgMgWQgMgVgBgeQABggANgYQANgZAYgOQAXgNAfAAQAfAAAVANQAVAOAMAWQANAXAFAaQAFAbgBAbQABAagGAdQgFAdgNAYQgNAZgWAPQgWAPgfAAQgbAAgUgKgAgphhQgOARAAAeQAAAcAOARQAPAPAYAAQAXgBAOgOQAPgRAAgcQAAgegPgRQgOgSgYAAQgYAAgOASg");
	this.shape_11.setTransform(145.4,20.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#55B553").s().p("Ag9CZQACghAJgjQAKglAQgjQAQgiATgfQAVggAXgVIieAAIAAguIDPAAIAAApQgXAZgVAfQgTAfgQAhQgOAkgIAkQgHAjgBAkg");
	this.shape_12.setTransform(170,20.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#55B553").s().p("AhNB2QgcgnAAhLQAAgyANgkQAOgkAagUQAZgTAiAAQAoAAAXAVQAZAWAFAnIgxAAQgEgWgLgKQgMgLgUAAQgSABgLALQgNALgGARQgGARgDASIgEAgQAMgSASgKQARgKAWAAQAcAAAVANQAWANALAWQAMAVAAAeQAAAggNAYQgNAZgXANQgXAOgfAAQg0AAgcgogAgkAGQgOAQAAAdQAAAeAPASQAPARAXAAQAYAAAOgRQAPgRABgeQgBgdgPgRQgOgPgZAAQgXAAgPAPg");
	this.shape_13.setTransform(96.8,20.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#55B553").s().p("AhoCbQABgjAMgXQAMgXAVgSQATgRAbgRIAdgVQAPgJALgPQALgPAAgVQAAgYgOgNQgNgPgXAAQgUAAgMAMQgLAKgEARQgEAQAAASIgxAAIAAgHQAAgiAMgYQANgYAXgOQAYgNAdAAQAeAAAXAMQAWALANAUQAMAWAAAcQAAAcgMAUQgMASgUAOQgSAPgWAOIgYARQgMAKgMALQgLAMgFAMICTAAIAAAtg");
	this.shape_14.setTransform(145,20.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#55B553").s().p("AhNB2QgdgnAAhLQAAgyAOgkQANgkAagUQAagTAiAAQAnAAAYAVQAZAWAEAnIgwAAQgEgWgMgKQgLgLgTAAQgTABgMALQgMALgGARQgHARgCASIgEAgQAMgSASgKQARgKAWAAQAcAAAVANQAWANAMAWQAMAVgBAeQABAggNAYQgOAZgXANQgYAOgeAAQg0AAgcgogAgkAGQgOAQAAAdQABAeAOASQAOARAYAAQAZAAAOgRQAOgRABgeQAAgdgPgRQgPgPgaAAQgWAAgPAPg");
	this.shape_15.setTransform(121.3,20.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1,p:{x:143.5}},{t:this.shape,p:{x:170.1}}]}).to({state:[{t:this.shape_6,p:{x:96.5}},{t:this.shape_1,p:{x:119}},{t:this.shape_5},{t:this.shape_4,p:{x:169.9}}]},14).to({state:[{t:this.shape_4,p:{x:96.5}},{t:this.shape_9},{t:this.shape_8,p:{x:145.6}},{t:this.shape_7,p:{x:169.8}}]},5).to({state:[{t:this.shape_7,p:{x:96.4}},{t:this.shape_6,p:{x:120.9}},{t:this.shape_11,p:{x:145.4}},{t:this.shape_10}]},5).to({state:[{t:this.shape_13},{t:this.shape_4,p:{x:121}},{t:this.shape,p:{x:145.6}},{t:this.shape_12,p:{x:170}}]},5).to({state:[{t:this.shape_12,p:{x:96.6}},{t:this.shape_7,p:{x:120.9}},{t:this.shape_1,p:{x:143.5}},{t:this.shape_8,p:{x:170.1}}]},5).to({state:[{t:this.shape_8,p:{x:96.7}},{t:this.shape_15},{t:this.shape_14},{t:this.shape_11,p:{x:169.9}}]},5).to({state:[{t:this.shape_8,p:{x:96.7}},{t:this.shape_15},{t:this.shape_14},{t:this.shape_11,p:{x:169.9}}]},5).wait(1));

	// клиентов уже пользуются сервисом
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#55B553").s().p("AAbAmIAAgxIAAAAIgTAxIgPAAIgSgxIgBAAIAAAxIgRAAIAAhLIAXAAIAUA7IAAAAIAVg7IAXAAIAABLg");
	this.shape_16.setTransform(259.6,58.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#55B553").s().p("AgTAkQgJgGgFgJQgEgJAAgMQAAgLAEgKQAFgIAJgFQAJgGAKAAQAMAAAIAGQAJAFAFAIQAFAKgBALQABAMgFAJQgFAJgJAGQgIAEgMAAQgKAAgJgEgAgPgTQgFAHAAAMQAAAMAFAIQAGAHAJAAQAKAAAGgHQAFgIABgMQgBgMgFgHQgGgHgKAAQgJAAgGAHg");
	this.shape_17.setTransform(249.9,58.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#55B553").s().p("AgRAjQgJgFgEgIQgFgKAAgMQAAgLAFgIQAEgKAJgFQAJgFAJAAQAPAAAJAHQAJAHACAOIgRAAQgCgHgEgEQgEgEgHAAQgIAAgGAIQgGAGAAAMQAAANAGAGQAFAIAIAAQAIAAAEgEQAFgFACgJIAQAAQgBAPgKAIQgJAJgPAAQgKgBgIgFg");
	this.shape_18.setTransform(241.3,58.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#55B553").s().p("AAQAmIAAg2IAAAAIgbA2IgWAAIAAhLIARAAIAAA2IABAAIAbg2IAWAAIAABLg");
	this.shape_19.setTransform(232.8,58.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#55B553").s().p("AghAmIAAhLIApAAQAKAAAHAFQAFAFAAAJQAAAGgCAEQgDAEgGACQAIACADADQAEAGAAAHQAAALgHAGQgHAFgOAAgAgQAZIAWAAQAFAAAEgCQACgDAAgFQAAgFgCgDQgEgDgFAAIgWAAgAgQgGIAUAAQAEAAADgDQADgCAAgFQAAgEgDgCQgDgCgFAAIgTAAg");
	this.shape_20.setTransform(224.5,58.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#55B553").s().p("AglA1IAAhoIAQAAIAAALQAEgGAHgEQAHgDAFAAQALAAAIAGQAIAEAEAKQAFAJAAANQAAALgFAIQgEAJgIAFQgHAFgLAAQgGAAgGgDQgGgDgFgGIAAAmgAgPggQgGAIAAANQAAALAGAGQAGAHAJABQAJgBAGgHQAFgGAAgLQAAgNgFgIQgGgGgJgBQgJAAgGAHg");
	this.shape_21.setTransform(215.7,60);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#55B553").s().p("AgSAjQgJgFgEgJQgFgJAAgMQAAgKAFgKQAFgJAIgFQAJgFAJgBQALAAAIAGQAJAFAEAJQAEALABALIAAABIg4AAQABAMAFAGQAGAHAIAAQAHAAAEgEQAFgDACgHIARAAQgDAIgEAHQgFAFgIAEQgHACgJAAQgKABgIgGgAAUgGQgBgJgGgHQgFgFgIAAQgHAAgFAFQgFAGgBAKIAmAAIAAAAg");
	this.shape_22.setTransform(206.8,58.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#55B553").s().p("AgRAjQgJgFgEgIQgFgKAAgMQAAgLAFgIQAEgKAJgFQAJgFAJAAQAPAAAJAHQAJAHACAOIgRAAQgCgHgEgEQgEgEgHAAQgIAAgGAIQgGAGAAAMQAAANAGAGQAFAIAIAAQAIAAAEgEQAFgFACgJIAQAAQgBAPgKAIQgJAJgPAAQgKgBgIgFg");
	this.shape_23.setTransform(198.5,58.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#55B553").s().p("AAUAmIAAgfIgOAAIgWAfIgUAAIAZghQgGgCgEgDQgEgCgBgEQgBgFAAgFQAAgKAGgGQAHgFAMAAIAnAAIAABLgAgHgWQgDADAAAFQAAAFADADQADADAEAAIAUAAIAAgVIgUAAQgEAAgDACg");
	this.shape_24.setTransform(185.4,58.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#55B553").s().p("AgRAjQgJgFgEgIQgFgKAAgMQAAgLAFgIQAEgKAJgFQAJgFAJAAQAPAAAJAHQAJAHACAOIgRAAQgCgHgEgEQgEgEgHAAQgIAAgGAIQgGAGAAAMQAAANAGAGQAFAIAIAAQAIAAAEgEQAFgFACgJIAQAAQgBAPgKAIQgJAJgPAAQgKgBgIgFg");
	this.shape_25.setTransform(177.6,58.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#55B553").s().p("AgHAmIAAg+IgZAAIAAgNIBBAAIAAANIgZAAIAAA+g");
	this.shape_26.setTransform(170,58.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#55B553").s().p("AgJAeQgKgJgCgRIgOAAIAAAiIgQAAIAAhMIAQAAIAAAfIAPAAQACgPAKgJQAIgJAPAAQAMAAAIAGQAJAFAEAIQAFAKgBALQABAMgFAJQgEAJgJAGQgIAEgMAAQgPAAgJgKgAAAgTQgDAHAAAMQAAAMADAIQAFAHAKAAQAJAAAGgHQAFgIAAgMQAAgMgFgHQgGgHgJAAQgKAAgFAHg");
	this.shape_27.setTransform(160.6,58.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#55B553").s().p("AgXA1IgGgBIAAgOIAFAAIAEAAQAEAAACgCQACgDACgEIACgIIgdhJIASAAIATA3IAUg3IASAAIgdBKIgFAPQgEAGgDAFQgFAFgKAAIgFAAg");
	this.shape_28.setTransform(150.5,60.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#55B553").s().p("AgYAhQgIgHgBgMIARAAQACAFACADQADADADABIAGABQAIAAAEgDQAEgDAAgFQAAgFgDgEQgDgDgJAAIgIAAIAAgKIAHAAQAFAAAEgDQAEgCABgGQAAgEgEgDQgDgCgHAAQgFAAgDADQgEADgBAFIgSAAQACgJAEgGQAFgFAIgCQAIgCAFAAQAJAAAGACQAHACADAEQAEAFAAAIQAAAEgBADIgEAGQgDACgEABIAAAAIAIACIAFAFQACAEABAGQgBAJgFAFQgEAGgIADQgHACgJAAQgOAAgKgHg");
	this.shape_29.setTransform(142.7,58.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#55B553").s().p("AggAmIAAhLIARAAIAAAcIAWAAQANAAAGAGQAHAFAAALQAAAMgHAHQgGAGgNAAgAgPAZIAUAAQAGAAADgDQACgDAAgFQAAgFgDgEQgDgDgFAAIgUAAg");
	this.shape_30.setTransform(135,58.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#55B553").s().p("AggAmIgEgBIAAgNIADABIADAAIAFgCQACgBACgFQABgEABgHIABgsIA3AAIAABLIgRAAIAAg+IgWAAIgBAgQAAAPgGAJQgGAIgLAAIgGgBg");
	this.shape_31.setTransform(126.1,58.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#55B553").s().p("AgTAkQgJgGgEgJQgGgJAAgMQAAgLAGgKQAEgIAJgFQAIgGALAAQAMAAAJAGQAIAFAFAIQAFAKAAALQAAAMgFAJQgFAJgIAGQgJAEgMAAQgLAAgIgEgAgOgTQgHAHABAMQgBAMAHAIQAGAHAIAAQAKAAAFgHQAGgIABgMQgBgMgGgHQgFgHgKAAQgIAAgGAHg");
	this.shape_32.setTransform(117.8,58.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#55B553").s().p("AAQAmIAAg+IggAAIAAA+IgRAAIAAhLIBDAAIAABLg");
	this.shape_33.setTransform(109,58.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#55B553").s().p("AgSAjQgJgFgEgJQgFgJAAgMQAAgKAFgKQAFgJAIgFQAJgFAJgBQALAAAIAGQAJAFAEAJQAEALABALIAAABIg4AAQABAMAFAGQAGAHAIAAQAHAAAEgEQAFgDACgHIARAAQgDAIgEAHQgFAFgIAEQgHACgJAAQgKABgIgGgAAUgGQgBgJgGgHQgFgFgIAAQgHAAgFAFQgFAGgBAKIAmAAIAAAAg");
	this.shape_34.setTransform(96.4,58.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#55B553").s().p("AAnAmIgYghIgHAIIAAAZIgPAAIAAgZIgHgIIgYAhIgUAAIAhgqIgdghIAUAAIAbAhIAAAAIAAghIAPAAIAAAhIAAAAIAbghIAVAAIgeAhIAhAqg");
	this.shape_35.setTransform(86.4,58.6);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#55B553").s().p("AgXA1IgHgBIAAgOIAGAAIADAAQAFAAACgCQADgDABgEIADgIIgehJIASAAIAUA3IATg3IASAAIgdBKIgGAPQgDAGgDAFQgGAFgIAAIgGAAg");
	this.shape_36.setTransform(76.7,60.1);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#55B553").s().p("AghAmIAAhLIApAAQALAAAFAFQAHAFAAAJQgBAGgCAEQgDAEgFACQAHACAEADQADAGAAAHQAAALgHAGQgIAFgOAAgAgQAZIAVAAQAHAAACgCQADgDAAgFQAAgFgDgDQgCgDgHAAIgVAAgAgQgGIAUAAQAFAAACgDQADgCAAgFQAAgEgDgCQgDgCgEAAIgUAAg");
	this.shape_37.setTransform(248.8,30.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#55B553").s().p("AgTAkQgJgGgFgJQgEgJAAgMQAAgLAEgKQAFgIAJgFQAJgGAKAAQAMAAAJAGQAIAFAFAIQAFAKAAALQAAAMgFAJQgFAJgIAGQgJAEgMAAQgKAAgJgEgAgPgTQgFAHAAAMQAAAMAFAIQAGAHAJAAQAKAAAFgHQAGgIABgMQgBgMgGgHQgFgHgKAAQgJAAgGAHg");
	this.shape_38.setTransform(240.1,30.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#55B553").s().p("AgHAmIAAg+IgZAAIAAgNIBBAAIAAANIgZAAIAAA+g");
	this.shape_39.setTransform(232.2,30.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#55B553").s().p("AARAmIAAghIggAAIAAAhIgSAAIAAhLIASAAIAAAeIAgAAIAAgeIARAAIAABLg");
	this.shape_40.setTransform(224.4,30.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#55B553").s().p("AgSAjQgJgFgEgJQgFgJAAgMQAAgKAFgKQAFgJAIgFQAJgFAJgBQALAAAIAGQAJAFAEAJQAEALABALIAAABIg4AAQABAMAFAGQAGAHAIAAQAHAAAEgEQAFgDACgHIARAAQgDAIgEAHQgFAFgIAEQgHACgJAAQgKABgIgGgAAUgGQgBgJgGgHQgFgFgIAAQgHAAgFAFQgFAGgBAKIAmAAIAAAAg");
	this.shape_41.setTransform(215.9,30.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#55B553").s().p("AAQAmIAAg2IAAAAIgbA2IgVAAIAAhLIARAAIAAA2IAAAAIAbg2IAVAAIAABLg");
	this.shape_42.setTransform(207.4,30.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#55B553").s().p("AggAmIgEgBIAAgNIADABIADAAIAFgCQACgBACgFQABgEABgHIABgsIA3AAIAABLIgRAAIAAg+IgWAAIgBAgQAAAPgGAJQgGAIgLAAIgGgBg");
	this.shape_43.setTransform(198.5,30.4);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#55B553").s().p("AAPAmIgWgiIgLAKIAAAYIgRAAIAAhLIARAAIAAAiIAdgiIAVAAIgeAfIAiAsg");
	this.shape_44.setTransform(191,30.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16}]}).wait(45));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(70.7,-6,196.2,73.7);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#55B553").s().p("AAVAfIAAgoIgPAoIgLAAIgPgoIAAAoIgOAAIAAg9IASAAIAQAwIARgwIASAAIAAA9g");
	this.shape.setTransform(62.3,98.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#55B553").s().p("AAVAgQgEAAgCgBQgDgCgBgEQgEAEgGACQgDABgGAAQgKAAgGgFQgFgFAAgIQAAgGACgEQADgEAEAAIAJgCIAJgCIAGgBIAEgBIACgBIABgCIAAgDQAAgFgDgCQgDgCgFAAQgGAAgEACQgDADgBAGIgNAAQACgLAHgFQAHgFALAAQAGAAAGACQAFACAEAEQADAEAAAHIAAAdIABAEIACABIABAAIACgBIAAAKIgFABIgEAAgAAEACIgGABIgGABIgFADQgCACAAAEQAAAFADACQADADAFAAQAEAAACgCIAGgDQACgDAAgCIABgGIAAgHIgHACg");
	this.shape_1.setTransform(54.9,98);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#55B553").s().p("AgdArIAAhTIANAAIAAAIQADgFAFgDQAGgCAEAAQAJAAAGAEQAGAEADAIQAEAHAAAKQAAAJgEAGQgDAHgGAEQgGAEgIAAQgEAAgGgCQgFgCgEgFIAAAfgAgLgZQgFAGAAAKQAAAJAFAFQAEAFAHAAQAIAAAEgFQAEgFAAgJQAAgKgEgGQgFgGgHAAQgHAAgEAGg");
	this.shape_2.setTransform(47.9,99.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#55B553").s().p("AgUAfIAAg9IApAAIAAALIgbAAIAAAyg");
	this.shape_3.setTransform(41.9,98.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#55B553").s().p("AAVAgQgEAAgCgBQgDgCgBgEQgEAEgGACQgDABgGAAQgKAAgGgFQgFgFAAgIQAAgGACgEQADgEAEAAIAJgCIAJgCIAGgBIAEgBIACgBIABgCIAAgDQAAgFgDgCQgDgCgFAAQgGAAgEACQgDADgBAGIgNAAQACgLAHgFQAHgFALAAQAGAAAGACQAFACAEAEQADAEAAAHIAAAdIABAEIACABIABAAIACgBIAAAKIgFABIgEAAgAAEACIgGABIgGABIgFADQgCACAAAEQAAAFADACQADADAFAAQAEAAACgCIAGgDQACgDAAgCIABgGIAAgHIgHACg");
	this.shape_4.setTransform(35.8,98);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#55B553").s().p("AgFAfIAAgyIgVAAIAAgLIA1AAIAAALIgVAAIAAAyg");
	this.shape_5.setTransform(29.6,98.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#55B553").s().p("AgOAcQgGgEgEgHQgEgHAAgKQAAgIAEgHQAEgIAGgEQAIgEAHAAQAMAAAHAGQAHAGACALIgOAAQgBgGgEgDQgDgEgGAAQgGABgFAFQgEAGAAAJQAAAKAEAGQAEAFAHAAQAGAAAEgDQADgEABgHIAOAAQgCAMgHAHQgIAGgLAAQgIAAgHgEg");
	this.shape_6.setTransform(23.6,98);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#55B553").s().p("AANAfIAAgbIgZAAIAAAbIgOAAIAAg9IAOAAIAAAZIAZAAIAAgZIAOAAIAAA9g");
	this.shape_7.setTransform(16.7,98.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#55B553").s().p("AANAfIAAgrIAAAAIgVArIgSAAIAAg9IAOAAIAAArIAWgrIARAAIAAA9g");
	this.shape_8.setTransform(9.8,98.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#55B553").s().p("AgaAfIAAg9IAgAAQAJAAAFAEQAEAFABAHQAAAFgDADQgCADgEACQAFABADADQADAEAAAGQAAAJgGAEQgGAEgLABgAgMAUIAQAAQAFAAACgCQADgCAAgEQAAgFgDgCQgCgCgFAAIgQAAgAgMgFIAPAAQAEAAACgCQACgCAAgDQAAgEgCgCQgCgBgEAAIgPAAg");
	this.shape_9.setTransform(-0.2,98.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#55B553").s().p("AANAfIAAgrIAAAAIgVArIgSAAIAAg9IAOAAIAAArIAWgrIARAAIAAA9g");
	this.shape_10.setTransform(-10.5,98.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#55B553").s().p("AAMAfIgSgcIgIAJIAAATIgNAAIAAg9IANAAIAAAcIAXgcIARAAIgZAaIAbAjg");
	this.shape_11.setTransform(-16.7,98.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#55B553").s().p("AgFAfIAAgyIgVAAIAAgLIA1AAIAAALIgVAAIAAAyg");
	this.shape_12.setTransform(-23.1,98.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#55B553").s().p("AgSAqIgFAAIAAgMIAEABIADAAQADAAACgCIADgGIACgGIgXg6IAPAAIAOArIAQgrIAOAAIgXA7IgEALQgDAGgCAEQgEADgIAAIgEAAg");
	this.shape_13.setTransform(-29,99.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#55B553").s().p("AgdArIAAhTIANAAIAAAIQADgFAGgDQAFgCAEAAQAIAAAHAEQAGAEAEAIQADAHAAAKQAAAJgDAGQgDAHgHAEQgGAEgJAAQgEAAgFgCQgFgCgEgFIAAAfgAgMgZQgEAGAAAKQAAAJAEAFQAFAFAHAAQAHAAAFgFQAEgFABgJQAAgKgGgGQgEgGgHAAQgHAAgFAGg");
	this.shape_14.setTransform(-35.6,99.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#55B553").s().p("AAMAfIgSgcIgIAJIAAATIgNAAIAAg9IANAAIAAAcIAXgcIARAAIgZAaIAbAjg");
	this.shape_15.setTransform(-42.3,98.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#55B553").s().p("AgOAcQgHgEgEgHQgDgHAAgKQAAgIAEgHQAEgIAGgEQAIgEAHAAQAMAAAHAGQAHAGABALIgNAAQgBgGgEgDQgEgEgFAAQgGABgEAFQgFAGAAAJQAAAKAEAGQAEAFAGAAQAGAAAEgDQAEgEACgHIANAAQgCAMgHAHQgIAGgMAAQgHAAgHgEg");
	this.shape_16.setTransform(-49.1,98);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#55B553").s().p("AAVAgQgEAAgCgBQgDgCgBgEQgEAEgGACQgDABgGAAQgKAAgGgFQgFgFAAgIQAAgGACgEQADgEAEAAIAJgCIAJgCIAGgBIAEgBIACgBIABgCIAAgDQAAgFgDgCQgDgCgFAAQgGAAgEACQgDADgBAGIgNAAQACgLAHgFQAHgFALAAQAGAAAGACQAFACAEAEQADAEAAAHIAAAdIABAEIACABIABAAIACgBIAAAKIgFABIgEAAgAAEACIgGABIgGABIgFADQgCACAAAEQAAAFADACQADADAFAAQAEAAACgCIAGgDQACgDAAgCIABgGIAAgHIgHACg");
	this.shape_17.setTransform(-55.7,98);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#55B553").s().p("AgdArIAAhTIANAAIAAAIQADgFAGgDQAFgCAEAAQAIAAAHAEQAGAEAEAIQADAHAAAKQAAAJgDAGQgDAHgHAEQgGAEgJAAQgDAAgFgCQgGgCgDgFIAAAfgAgMgZQgEAGAAAKQAAAJAEAFQAFAFAHAAQAHAAAFgFQAFgFAAgJQgBgKgFgGQgEgGgHAAQgHAAgFAGg");
	this.shape_18.setTransform(-62.7,99.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#55B553").s().p("AgOAcQgGgEgFgHQgDgHAAgKQAAgIAEgHQAEgIAGgEQAIgEAHAAQAMAAAHAGQAHAGABALIgNAAQgCgGgDgDQgDgEgGAAQgGABgEAFQgFAGAAAJQAAAKAEAGQAFAFAFAAQAHAAADgDQAFgEABgHIANAAQgCAMgHAHQgIAGgMAAQgHAAgHgEg");
	this.shape_19.setTransform(66.3,82.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#55B553").s().p("AANAeIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8g");
	this.shape_20.setTransform(59.4,82.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#55B553").s().p("AgaAeIAAg8IAgAAQAJABAFAEQAEAEABAGQAAAFgDAEQgCADgEACQAFABADACQADAFAAAGQAAAJgGAEQgGAFgLgBgAgMAUIAQAAQAFAAACgCQADgCAAgEQAAgFgDgBQgCgDgFAAIgQAAgAgMgFIAPAAQAEAAACgCQACgBAAgFQAAgDgCgBQgCgCgEAAIgPAAg");
	this.shape_21.setTransform(52.8,82.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#55B553").s().p("AgdArIAAhTIANAAIAAAIQADgFAFgDQAGgCAEAAQAIAAAHAEQAGAEADAIQAEAHAAAKQAAAJgEAGQgDAHgGAEQgGAEgIAAQgFAAgFgCQgFgCgEgFIAAAfgAgLgZQgFAGAAAKQAAAJAFAFQAEAFAHAAQAIAAAEgFQAFgFgBgJQAAgKgEgGQgFgGgHAAQgHAAgEAGg");
	this.shape_22.setTransform(45.8,83.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#55B553").s().p("AgOAcQgHgEgEgHQgDgIAAgJQAAgIAEgHQAEgIAGgEQAHgEAHAAQAJAAAGAEQAHAEAEAIQADAHAAAJIAAACIgrAAQAAAJAEAFQAFAFAGAAQAFAAAEgCQADgDACgGIANAAQgBAHgEAEQgEAFgGADQgGACgHAAQgHAAgHgEgAAQgEQgBgIgEgFQgEgEgHAAQgFAAgEAEQgEAEgBAJIAeAAIAAAAg");
	this.shape_23.setTransform(38.7,82.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#55B553").s().p("AgOAcQgGgEgFgHQgDgHAAgKQAAgIAEgHQAEgIAGgEQAIgEAHAAQAMAAAHAGQAHAGABALIgNAAQgCgGgDgDQgDgEgGAAQgGABgEAFQgFAGAAAJQAAAKAEAGQAFAFAFAAQAHAAADgDQAFgEABgHIANAAQgCAMgHAHQgIAGgMAAQgHAAgHgEg");
	this.shape_24.setTransform(32.1,82.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#55B553").s().p("AANArIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8gAgMgdQgFgFgBgIIAHAAQABAEADADQADACAEAAQAFAAADgCQADgDABgEIAHAAQgBAIgEAFQgFAEgJAAQgHAAgFgEg");
	this.shape_25.setTransform(21.8,81.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#55B553").s().p("AANAeIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8g");
	this.shape_26.setTransform(14.9,82.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#55B553").s().p("AAMAeIgSgbIgIAIIAAATIgNAAIAAg8IANAAIAAAcIAXgcIAQAAIgYAZIAbAjg");
	this.shape_27.setTransform(8.7,82.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#55B553").s().p("AgOAcQgHgEgEgHQgDgHAAgKQAAgIAEgHQAEgIAGgEQAIgEAHAAQAMAAAHAGQAHAGABALIgNAAQgCgGgDgDQgEgEgFAAQgGABgEAFQgFAGAAAJQAAAKAEAGQAFAFAFAAQAHAAADgDQAFgEABgHIANAAQgCAMgHAHQgIAGgMAAQgHAAgHgEg");
	this.shape_28.setTransform(1.8,82.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#55B553").s().p("AgOAcQgHgEgEgHQgDgIAAgJQAAgIAEgHQAEgIAGgEQAHgEAHAAQAJAAAGAEQAHAEAEAIQADAHAAAJIAAACIgrAAQAAAJAEAFQAFAFAGAAQAFAAAEgCQADgDACgGIANAAQgBAHgEAEQgEAFgGADQgGACgHAAQgHAAgHgEgAAQgEQgBgIgEgFQgEgEgHAAQgFAAgEAEQgEAEgBAJIAeAAIAAAAg");
	this.shape_29.setTransform(-4.9,82.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#55B553").s().p("AANAeIAAgaIgGACIgHABIgGABQgJAAgFgGQgFgCAAgJIAAgVIAOAAIAAAQQAAAGACADQADADAGAAIADgBIAFgBIAFgBIAAgZIANAAIAAA8g");
	this.shape_30.setTransform(-11.6,82.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#55B553").s().p("AANAeIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8g");
	this.shape_31.setTransform(-18.2,82.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#55B553").s().p("AgFAeIAAgwIgVAAIAAgMIA1AAIAAAMIgVAAIAAAwg");
	this.shape_32.setTransform(-24.4,82.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#55B553").s().p("AAVAgQgEAAgCgBQgDgCgBgEQgEAEgGACQgDABgGAAQgKAAgGgFQgFgFAAgIQAAgGACgEQADgEAEAAIAJgCIAJgCIAGgBIAEgBIACgBIABgCIAAgDQAAgFgDgCQgDgCgFAAQgGAAgEACQgDADgBAGIgNAAQACgLAHgFQAHgFALAAQAGAAAGACQAFACAEAEQADAEAAAHIAAAdIABAEIACABIABAAIACgBIAAAKIgFABIgEAAgAAEACIgGABIgGABIgFADQgCACAAAEQAAAFADACQADADAFAAQAEAAACgCIAGgDQACgDAAgCIABgGIAAgHIgHACg");
	this.shape_33.setTransform(-30.3,82.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#55B553").s().p("AAVAeIAAgmIgPAmIgLAAIgPgmIAAAmIgOAAIAAg8IASAAIAQAwIARgwIASAAIAAA8g");
	this.shape_34.setTransform(-38,82.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#55B553").s().p("AgPAcQgHgEgEgHQgEgIAAgJQAAgJAEgHQAEgHAHgEQAHgEAIAAQAJAAAHAEQAHAEAEAHQAEAHAAAJQAAAJgEAIQgEAHgHAEQgHAEgJAAQgIAAgHgEgAgLgPQgFAGAAAJQAAAKAFAGQAEAFAHAAQAIAAAEgFQAFgGAAgKQAAgJgFgGQgEgGgIAAQgHAAgEAGg");
	this.shape_35.setTransform(-45.8,82.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#55B553").s().p("AgFAeIAAgwIgVAAIAAgMIA1AAIAAAMIgVAAIAAAwg");
	this.shape_36.setTransform(-52.1,82.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#55B553").s().p("AgaAeIAAg8IAgAAQAJABAFAEQAEAEABAGQAAAFgDAEQgCADgEACQAFABADACQADAFAAAGQAAAJgGAEQgGAFgLgBgAgMAUIAQAAQAFAAACgCQADgCAAgEQAAgFgDgBQgCgDgFAAIgQAAgAgMgFIAPAAQAEAAACgCQACgBAAgFQAAgDgCgBQgCgCgEAAIgPAAg");
	this.shape_37.setTransform(-58.1,82.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#55B553").s().p("AAYAqIgHgWIggAAIgJAWIgQAAIAihTIAOAAIAgBTgAAMAIIgMgiIgMAiIAYAAg");
	this.shape_38.setTransform(-65.5,81.5);

	this.instance = new lib.logo();
	this.instance.setTransform(-108.5,14);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-108.5,14,217,91.7);


(lib.Symbol5copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgqAwIAAhgIA0AAQAOABAHAFQAIAHAAALQAAAJgEAEQgDAFgHADIAAAAQAJADAFAEQAEAHAAAKQAAANgJAIQgJAGgSAAgAgVAgIAcAAQAHAAAEgDQAEgEAAgGQAAgGgEgEQgEgEgHAAIgcAAgAgVgIIAaAAQAGAAADgEQADgCAAgHQAAgFgDgCQgDgDgHAAIgZAAg");
	this.shape.setTransform(-11.7,309.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgZAtQgLgGgGgMQgGgLAAgQQAAgOAGgMQAGgMALgGQAMgGANgBQAPABALAGQALAGAGAMQAGAMAAAOQAAAQgGALQgGAMgLAGQgLAHgPAAQgNAAgMgHgAgTgYQgHAJgBAPQABAQAHAKQAIAIALABQANAAAGgKQAIgJAAgQQAAgPgIgJQgGgJgNgBQgLABgIAJg");
	this.shape_1.setTransform(-22.8,309.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgJAwIAAhPIggAAIAAgRIBTAAIAAARIgfAAIAABPg");
	this.shape_2.setTransform(-32.9,309.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAVAwIAAgqIgpAAIAAAqIgWAAIAAhgIAWAAIAAAnIApAAIAAgnIAWAAIAABgg");
	this.shape_3.setTransform(-42.7,309.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgdBDIgJgBIAAgSIAHABIAFAAQAFAAADgEQADgDACgGIADgJIglhdIAYAAIAYBGIAYhGIAXAAIgkBeIgHASQgEAJgFAGQgHAGgMAAIgGAAg");
	this.shape_4.setTransform(-53.1,311.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAYAwQgEgCgBgHQgHAHgJACQgHADgKAAQgPAAgJgIQgJgHAAgOQAAgKAEgGQAEgFAHgBQAGgDAIgCIAPgCIAKgCIAGgBIADgDIABgCIABgFQAAgHgFgEQgFgEgIAAQgLAAgFAEQgFAEgBAKIgVAAQACgSAMgIQALgHASAAQAKAAAJADQAJADAFAGQAFAHAAAKIAAAwIABAGQABAAAAAAQABABAAAAQABAAAAAAQABAAABAAIACAAIACAAIAAAPIgIACIgGAAQgGAAgEgDgAAGADIgJACIgKACQgFABgDAEQgEADAAAGQAAAHAFAEQAEAEAJAAQAHAAAEgCQAFgCAEgEQADgDABgFIABgJIAAgLQgGACgGABg");
	this.shape_5.setTransform(-63.1,309.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AATAwIgdgsIgNAOIAAAeIgVAAIAAhgIAVAAIAAAsIAlgsIAbAAIgmAoIAqA4g");
	this.shape_6.setTransform(-73.1,309.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AATAwIgdgsIgNAOIAAAeIgVAAIAAhgIAVAAIAAAsIAlgsIAbAAIgmAoIAqA4g");
	this.shape_7.setTransform(-83.3,309.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAYAwQgEgCgBgHQgHAHgJACQgHADgKAAQgPAAgJgIQgJgHAAgOQAAgKAEgGQAEgFAHgBQAGgDAIgCIAPgCIAKgCIAGgBIADgDIABgCIABgFQAAgHgFgEQgFgEgIAAQgLAAgFAEQgFAEgBAKIgVAAQACgSAMgIQALgHASAAQAKAAAJADQAJADAFAGQAFAHAAAKIAAAwIABAGQABAAAAAAQABABAAAAQABAAAAAAQABAAABAAIACAAIACAAIAAAPIgIACIgGAAQgGAAgEgDgAAGADIgJACIgKACQgFABgDAEQgEADAAAGQAAAHAFAEQAEAEAJAAQAHAAAEgCQAFgCAEgEQADgDABgFIABgJIAAgLQgGACgGABg");
	this.shape_8.setTransform(-94.1,309.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAYAwIgYgkIgXAkIgaAAIAlgxIgigvIAbAAIATAfIAVgfIAZAAIgiAuIAmAyg");
	this.shape_9.setTransform(88.4,286.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAkAwIAAhgIAVAAIAABggAg4AwIAAhgIAVAAIAAAkIAaAAQAPAAAIAIQAJAHAAAOQAAAPgJAIQgJAIgNAAgAgjAfIAYAAQAIAAADgDQABgFAAgFQAAgHgCgEQgEgEgGAAIgYAAg");
	this.shape_10.setTransform(76.5,286.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAVAwIAAgqIgpAAIAAAqIgVAAIAAhgIAVAAIAAAnIApAAIAAgnIAVAAIAABgg");
	this.shape_11.setTransform(64.2,286.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAVAwIAAgqIgpAAIAAAqIgWAAIAAhgIAWAAIAAAnIApAAIAAgnIAVAAIAABgg");
	this.shape_12.setTransform(53.3,286.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAYAwQgEgCgBgHQgHAHgJACQgHADgKAAQgPAAgJgIQgJgHAAgOQAAgKAEgGQAEgFAHgBQAGgDAIgCIAPgCIAKgCIAGgBIADgDIABgCIABgFQAAgHgFgEQgFgEgIAAQgLAAgFAEQgFAEgBAKIgVAAQACgSAMgIQALgHASAAQAKAAAJADQAJADAFAGQAFAHAAAKIAAAwIABAGQABAAAAAAQABABAAAAQABAAAAAAQABAAABAAIACAAIACAAIAAAPIgIACIgGAAQgGAAgEgDgAAGADIgJACIgKACQgFABgDAEQgEADAAAGQAAAHAFAEQAEAEAJAAQAHAAAEgCQAFgCAEgEQADgDABgFIABgJIAAgLQgGACgGABg");
	this.shape_13.setTransform(42.8,286.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgqAwIAAhgIA0AAQAOAAAHAHQAIAGAAAMQAAAHgEAFQgDAFgHADIAAAAQAJADAFAEQAEAHAAAKQAAANgJAHQgJAIgSgBgAgVAgIAcAAQAHAAAEgDQAEgDAAgHQAAgHgEgDQgEgDgHgBIgcAAgAgVgIIAaAAQAGgBADgDQADgCAAgHQAAgFgDgDQgDgCgHAAIgZAAg");
	this.shape_14.setTransform(32.3,286.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgZAtQgLgGgGgMQgGgMAAgPQAAgPAGgLQAGgLALgHQALgGAOAAQAPAAALAGQALAHAGALQAGALAAAPQAAAPgGAMQgGAMgLAGQgLAHgPgBQgOABgLgHgAgTgYQgHAJAAAPQAAAQAHAKQAIAIALABQANAAAGgKQAIgJAAgQQAAgPgIgJQgGgJgNAAQgLAAgIAJg");
	this.shape_15.setTransform(21.2,286.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgvBEIAAiEIAUAAIAAANQAGgIAIgEQAJgEAHAAQAOAAAKAGQAKAHAFALQAGAMAAAQQAAAPgFAKQgGAMgKAGQgJAGgOAAQgIAAgIgDQgIgEgGgIIAAAxgAgTgpQgHAKAAAQQAAAPAHAIQAHAJAMAAQAMAAAHgJQAHgIAAgPQAAgQgHgJQgHgJgMgBQgMAAgHAJg");
	this.shape_16.setTransform(9.9,288);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAVAwIAAhFIgBAAIgjBFIgbAAIAAhgIAWAAIAABGIAAAAIAkhGIAbAAIAABgg");
	this.shape_17.setTransform(-1.5,286.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AATAwIgdgsIgNAOIAAAeIgVAAIAAhgIAVAAIAAAsIAlgsIAbAAIgmAoIAqA4g");
	this.shape_18.setTransform(-11.5,286.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgZAtQgLgGgGgMQgGgMAAgPQAAgPAGgLQAGgLAMgHQALgGANAAQAPAAALAGQALAHAGALQAGALAAAPQAAAPgGAMQgGAMgLAGQgLAHgPgBQgOABgLgHgAgTgYQgHAJAAAPQAAAQAHAKQAHAIAMABQAMAAAIgKQAHgJAAgQQAAgPgHgJQgIgJgMAAQgMAAgHAJg");
	this.shape_19.setTransform(-22.8,286.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgoAxIgGgCIAAgRIADABIAFAAIAGgBQADgDACgFQACgFAAgJIACg5IBGAAIAABgIgVAAIAAhPIgcAAIgCAqQAAATgIAKQgIAKgNAAIgHAAg");
	this.shape_20.setTransform(-34.3,286.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgZBAQgLgHgGgOQgDgGgBgIIgBgRIABgSQABgKADgKQADgKAEgHQAHgKAJgFQAKgFAMgBIANgBQAFgBACgDIAVAAQgBAIgEAFQgEAFgGACQgGACgHABIgPABQgIABgHAFQgHAFgEAIQgEAIgDAKIABAAQAFgKAJgFQAIgFAMAAQANAAAKAGQAKAGAGALQAGAJAAAQQAAAPgGAMQgGALgLAGQgKAGgOAAQgOAAgMgGgAgSgCQgHAHAAAPQAAAPAHAJQAIAJAKAAQAMAAAHgJQAHgJAAgPQAAgPgHgHQgHgJgMAAQgKAAgIAJg");
	this.shape_21.setTransform(-44.9,284.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAYAwQgEgCgBgHQgHAHgJACQgHADgKAAQgPAAgJgIQgJgHAAgOQAAgKAEgGQAEgFAHgBQAGgDAIgCIAPgCIAKgCIAGgBIADgDIABgCIABgFQAAgHgFgEQgFgEgIAAQgLAAgFAEQgFAEgBAKIgVAAQACgSAMgIQALgHASAAQAKAAAJADQAJADAFAGQAFAHAAAKIAAAwIABAGQABAAAAAAQABABAAAAQABAAAAAAQABAAABAAIACAAIACAAIAAAPIgIACIgGAAQgGAAgEgDgAAGADIgJACIgKACQgFABgDAEQgEADAAAGQAAAHAFAEQAEAEAJAAQAHAAAEgCQAFgCAEgEQADgDABgFIABgJIAAgLQgGACgGABg");
	this.shape_22.setTransform(-55.7,286.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgdAqQgMgIgBgQIAXAAQAAAHAEADQADAEAFABQAFACACAAQAKAAAGgEQAFgEAAgHQAAgGgEgEQgEgEgMAAIgJAAIAAgOIAIAAQAHAAAFgDQAGgDAAgHQAAgGgEgDQgFgDgJgBQgHABgEAEQgEADgCAHIgVAAQAAgMAHgHQAGgHAJgCQAKgDAIAAQALAAAHADQAJACAFAGQAFAGAAAJQAAAGgCAEQgCAFgEACQgDADgEABIAAAAQAEABAFACQAEACACAFQAEAFAAAIQAAAKgHAHQgFAHgKAEQgKADgKAAQgUAAgKgJg");
	this.shape_23.setTransform(-66.1,286.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AghAzQgLgRAAgiQAAggALgRQALgSAWAAQAWAAAMARQAMASgBAgQAAAhgLASQgMARgWAAQgWAAgLgRgAgNgrQgEAEgCAIQgDAIgBAJIAAAPQAAAZAGAMQAGAMALAAQAJAAAEgGQAFgFACgIQACgIABgIIABgPIgBgNQgBgJgCgIQgCgIgFgFQgFgFgIgBQgHABgGAFg");
	this.shape_24.setTransform(-81.5,284.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgaAKIAAgTIA1AAIAAATg");
	this.shape_25.setTransform(-95.8,286);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgZAtQgLgGgGgMQgGgLAAgQQAAgOAGgMQAGgMALgGQAMgHANABQAPgBALAHQALAGAGAMQAGAMAAAOQAAAQgGALQgGAMgLAGQgLAGgPAAQgNAAgMgGgAgTgYQgHAJgBAPQABAQAHAJQAIAJALAAQANAAAGgJQAIgJAAgQQAAgPgIgJQgGgJgNAAQgLAAgIAJg");
	this.shape_26.setTransform(100,263.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAVAxIAAgrIgpAAIAAArIgWAAIAAhhIAWAAIAAAnIApAAIAAgnIAWAAIAABhg");
	this.shape_27.setTransform(88.9,263.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgXAtQgLgHgFgLQgGgLAAgPQAAgPAGgLQAGgMALgHQALgGANAAQATAAALAJQALAKACARIgVAAQgCgKgGgFQgFgFgJAAQgLABgHAJQgHAJAAAPQAAAQAHAJQAHAJAKAAQAKAAAGgGQAGgFACgLIAVAAQgCASgMAKQgMALgTAAQgNAAgLgGg");
	this.shape_28.setTransform(78.3,263.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAYAwQgEgCgBgHQgHAHgJACQgHADgKAAQgPAAgJgIQgJgHAAgOQAAgKAEgGQAEgFAHgBQAGgDAIgCIAPgCIAKgCIAGgBIADgDIABgCIABgFQAAgHgFgEQgFgEgIAAQgLAAgFAEQgFAEgBAKIgVAAQACgSAMgIQALgHASAAQAKAAAJADQAJADAFAGQAFAHAAAKIAAAwIABAGQABAAAAAAQABABAAAAQABAAAAAAQABAAABAAIACAAIACAAIAAAPIgIACIgGAAQgGAAgEgDgAAGADIgJACIgKACQgFABgDAEQgEADAAAGQAAAHAFAEQAEAEAJAAQAHAAAEgCQAFgCAEgEQADgDABgFIABgJIAAgLQgGACgGABg");
	this.shape_29.setTransform(67.8,263.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAVAxIAAhPIgpAAIAABPIgWAAIAAhhIBVAAIAABhg");
	this.shape_30.setTransform(56.9,263.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgZAtQgLgGgGgMQgGgLAAgQQAAgOAGgMQAGgMAMgGQAKgHAOABQAPgBALAHQALAGAGAMQAGAMAAAOQAAAQgGALQgGAMgLAGQgLAGgPAAQgNAAgMgGgAgTgYQgHAJgBAPQABAQAHAJQAHAJAMAAQAMAAAIgJQAHgJAAgQQAAgPgHgJQgIgJgMAAQgMAAgHAJg");
	this.shape_31.setTransform(45.9,263.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgdAqQgMgIgBgQIAXAAQABAHADADQADAEAFABQAFACACAAQAKAAAGgEQAFgEAAgHQAAgGgEgEQgEgEgMAAIgJAAIAAgOIAIAAQAHAAAGgDQAFgDAAgHQAAgGgEgDQgFgDgJgBQgGABgFAEQgEADgCAHIgVAAQABgMAGgHQAHgHAIgCQAKgDAIAAQALAAAHADQAJACAFAGQAFAGABAJQgBAGgCAEQgCAFgEACQgDADgEABIAAAAQAEABAFACQAEACADAFQADAFAAAIQAAAKgHAHQgFAHgKAEQgJADgLAAQgUAAgKgJg");
	this.shape_32.setTransform(35.2,263.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgXAtQgLgGgGgMQgFgLgBgPQAAgOAHgLQAFgMAMgIQAKgGAMAAQAOAAALAGQAKAHAGANQAGALAAAQIAAACIhHAAQABAPAGAIQAIAHAKAAQAJABAFgFQAGgEADgJIAVAAQgCAKgGAIQgHAHgJAFQgJADgLAAQgOAAgKgGgAAZgIQgCgMgGgHQgHgHgKAAQgJAAgGAGQgHAHgBANIAwAAIAAAAg");
	this.shape_33.setTransform(25.2,263.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgZBAQgLgHgGgOQgDgGgBgIIgBgRIABgSQABgKADgKQADgKAEgHQAHgKAJgFQAKgFAMgBIANgBQAFgBACgDIAVAAQgBAIgEAFQgEAFgGACQgGACgHABIgPABQgIABgHAFQgHAFgEAIQgEAIgDAKIABAAQAFgKAJgFQAIgFAMAAQANAAAKAGQAKAGAGALQAGAJAAAQQAAAPgGAMQgGALgLAGQgKAGgOAAQgOAAgMgGgAgSgCQgHAHAAAPQAAAPAHAJQAIAJAKAAQAMAAAHgJQAHgJAAgPQAAgPgHgHQgHgJgMAAQgKAAgIAJg");
	this.shape_34.setTransform(14.3,261.3);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgZAtQgLgGgGgMQgGgLAAgQQAAgOAGgMQAGgMALgGQAMgHANABQAPgBALAHQALAGAGAMQAGAMAAAOQAAAQgGALQgGAMgLAGQgLAGgPAAQgOAAgLgGgAgTgYQgHAJAAAPQAAAQAHAJQAIAJALAAQANAAAGgJQAIgJAAgQQAAgPgIgJQgGgJgNAAQgLAAgIAJg");
	this.shape_35.setTransform(-2.3,263.2);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AAVAxIAAgrIgpAAIAAArIgVAAIAAhhIAVAAIAAAnIApAAIAAgnIAVAAIAABhg");
	this.shape_36.setTransform(-13.4,263.2);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgJAxIAAhPIghAAIAAgSIBVAAIAAASIghAAIAABPg");
	this.shape_37.setTransform(-23.3,263.2);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgMAnQgNgMgBgWIgSAAIAAAsIgVAAIAAhhIAVAAIAAAnIASAAQADgUANgKQAKgLATAAQAPgBAKAHQALAGAGAMQAFAMAAAOQAAAQgFALQgGAMgLAGQgKAGgPAAQgTAAgMgMgAAAgYQgFAJAAAPQAAAQAFAJQAGAJANAAQAMAAAHgJQAGgJAAgQQAAgPgGgJQgHgJgMAAQgMAAgHAJg");
	this.shape_38.setTransform(-35.1,263.2);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgoAxIgGgBIAAgSIADABIAFAAIAGgBQADgDACgFQACgFAAgKIACg4IBGAAIAABhIgVAAIAAhPIgcAAIgCApQAAATgIAKQgIAKgNAAIgHAAg");
	this.shape_39.setTransform(-48.8,263.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgZAtQgLgGgGgMQgGgLAAgQQAAgOAGgMQAGgMAMgGQAKgHAOABQAPgBALAHQALAGAGAMQAGAMAAAOQAAAQgGALQgGAMgLAGQgLAGgPAAQgNAAgMgGgAgTgYQgHAJgBAPQABAQAHAJQAHAJAMAAQAMAAAIgJQAHgJAAgQQAAgPgHgJQgIgJgMAAQgMAAgHAJg");
	this.shape_40.setTransform(-59.4,263.2);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgXAtQgLgHgFgLQgGgLAAgPQAAgPAGgLQAGgMALgHQALgGANAAQATAAALAJQALAKACARIgVAAQgCgKgGgFQgFgFgJAAQgLABgHAJQgHAJAAAPQAAAQAHAJQAHAJAKAAQAKAAAGgGQAGgFACgLIAVAAQgCASgMAKQgMALgTAAQgNAAgLgGg");
	this.shape_41.setTransform(-70.3,263.2);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgZBAQgLgHgGgOQgDgGgBgIIgBgRIABgSQABgKADgKQADgKAEgHQAHgKAJgFQAKgFAMgBIANgBQAFgBACgDIAVAAQgBAIgEAFQgEAFgGACQgGACgHABIgPABQgIABgHAFQgHAFgEAIQgEAIgDAKIABAAQAFgKAJgFQAIgFAMAAQANAAAKAGQAKAGAGALQAGAJAAAQQAAAPgGAMQgGALgLAGQgKAGgOAAQgOAAgMgGgAgSgCQgHAHAAAPQAAAPAHAJQAIAJAKAAQAMAAAHgJQAHgJAAgPQAAgPgHgHQgHgJgMAAQgKAAgIAJg");
	this.shape_42.setTransform(-81.2,261.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AAnBDIgNgkIg0AAIgNAkIgZAAIA0iFIAYAAIA1CFgAAUANIgUg2IgUA2IAoAAg");
	this.shape_43.setTransform(-93.2,261.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-101.5,249,214.7,71.4);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAYAwQgEgCgBgHQgHAHgJACQgHADgKAAQgPAAgJgIQgJgHAAgOQAAgKAEgGQAEgFAHgBQAGgDAIgCIAPgCIAKgCIAGgBIADgDIABgCIABgFQAAgHgFgEQgFgEgIAAQgLAAgFAEQgFAEgBAKIgVAAQACgSAMgIQALgHASAAQAKAAAJADQAJADAFAGQAFAHAAAKIAAAwIABAGQABAAAAAAQABABAAAAQABAAAAAAQABAAABAAIACAAIACAAIAAAPIgIACIgGAAQgGAAgEgDgAAGADIgJACIgKACQgFABgDAEQgEADAAAGQAAAHAFAEQAEAEAJAAQAHAAAEgCQAFgCAEgEQADgDABgFIABgJIAAgLQgGACgGABg");
	this.shape.setTransform(97,124.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAiBAIAAgfIhDAAIAAAfIgVAAIAAgwIAMAAQAEgJADgIQACgJACgNQABgNAAgQIAAgLIBIAAIAABPIANAAIAAAwgAgJgoQAAAMgCALIgEATIgEAOIAnAAIAAg/IgdAAg");
	this.shape_1.setTransform(85.8,126.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgZAtQgLgGgGgMQgGgLAAgQQAAgOAGgMQAGgMALgGQAMgHANABQAPgBALAHQALAGAGAMQAGAMAAAOQAAAQgGALQgGAMgLAGQgLAGgPAAQgNAAgMgGgAgTgYQgHAJgBAPQABAQAHAJQAHAKAMgBQAMAAAIgJQAHgJAAgQQAAgPgHgJQgIgJgMgBQgMABgHAJg");
	this.shape_2.setTransform(74.6,124.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgvBEIAAiEIAUAAIAAANQAGgIAIgEQAJgEAHAAQAOAAAKAGQAKAHAFALQAGAMAAAQQAAAPgFAKQgGAMgKAGQgJAGgOAAQgIAAgIgDQgIgEgGgIIAAAxgAgTgpQgHAKAAAQQAAAPAHAIQAHAJAMAAQAMAAAHgJQAHgIAAgPQAAgQgHgJQgHgJgMgBQgMAAgHAJg");
	this.shape_3.setTransform(63.3,126.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgZAtQgLgGgGgMQgGgLAAgQQAAgOAGgMQAGgMAMgGQAKgHAOABQAPgBALAHQALAGAGAMQAGAMAAAOQAAAQgGALQgGAMgLAGQgLAGgPAAQgNAAgMgGgAgTgYQgHAJgBAPQABAQAHAJQAHAKAMgBQAMAAAIgJQAHgJAAgQQAAgPgHgJQgIgJgMgBQgMABgHAJg");
	this.shape_4.setTransform(51.7,124.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AghAxIAAhgIBDAAIAAARIgtAAIAABPg");
	this.shape_5.setTransform(42.5,124.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgZAtQgLgGgGgMQgGgLAAgQQAAgOAGgMQAGgMAMgGQAKgHAOABQAPgBALAHQALAGAGAMQAGAMAAAOQAAAQgGALQgGAMgLAGQgLAGgPAAQgNAAgMgGgAgTgYQgHAJgBAPQABAQAHAJQAHAKAMgBQAMAAAIgJQAHgJAAgQQAAgPgHgJQgIgJgMgBQgMABgHAJg");
	this.shape_6.setTransform(27.1,124.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AghAxIAAhgIBDAAIAAARIgtAAIAABPg");
	this.shape_7.setTransform(17.9,124.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgXAtQgLgHgGgLQgGgLABgQQAAgNAFgMQAHgLAKgIQALgGAMAAQAPAAAKAGQAKAHAGAMQAGAMgBAQIAAACIhGAAQAAAPAHAHQAIAIAKAAQAJABAGgEQAFgFAEgJIAUAAQgDAKgGAIQgGAHgJAFQgJADgMAAQgMAAgLgGgAAZgIQgBgMgHgHQgHgHgKAAQgJAAgHAGQgGAIgCAMIAxAAIAAAAg");
	this.shape_8.setTransform(8.1,124.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AhAAxIAAhgIAVAAIAABPIAiAAIAAhPIATAAIAABPIAiAAIAAhPIAVAAIAABgg");
	this.shape_9.setTransform(-5,124.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAYAwQgEgCgBgHQgHAHgJACQgHADgKAAQgPAAgJgIQgJgHAAgOQAAgKAEgGQAEgFAHgBQAGgDAIgCIAPgCIAKgCIAGgBIADgDIABgCIABgFQAAgHgFgEQgFgEgIAAQgLAAgFAEQgFAEgBAKIgVAAQACgSAMgIQALgHASAAQAKAAAJADQAJADAFAGQAFAHAAAKIAAAwIABAGQABAAAAAAQABABAAAAQABAAAAAAQABAAABAAIACAAIACAAIAAAPIgIACIgGAAQgGAAgEgDgAAGADIgJACIgKACQgFABgDAEQgEADAAAGQAAAHAFAEQAEAEAJAAQAHAAAEgCQAFgCAEgEQADgDABgFIABgJIAAgLQgGACgGABg");
	this.shape_10.setTransform(-17.8,124.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("Ag3BDIAAiFIA8AAQAPAAAKAEQAJAFAGAHQAEAIAAAKQAAAKgFAHQgFAIgJADQALADAHAHQAHAIABANQgBAMgFAJQgGAJgJAFQgKAFgNAAgAgfAwIAhAAQAJAAAGgCQAGgBAFgFQADgFAAgIQAAgJgDgFQgFgFgHgBQgGgCgIAAIghAAgAgfgKIAfAAQANAAAGgEQAHgEgBgKQAAgJgDgEQgEgEgGgBIgMgBIgfAAg");
	this.shape_11.setTransform(-29.6,122.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgXAtQgLgHgFgLQgGgLAAgPQAAgPAGgLQAGgMALgHQALgGANAAQATAAALAJQALAKACARIgVAAQgCgKgGgFQgFgFgJAAQgLABgHAJQgHAJAAAPQAAAQAHAJQAHAJAKAAQAKAAAGgGQAGgFACgLIAVAAQgCASgMAKQgMALgTAAQgNAAgLgGg");
	this.shape_12.setTransform(-47.1,124.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAVAxIAAhGIAAAAIgkBGIgbAAIAAhgIAWAAIAABFIABAAIAihFIAcAAIAABgg");
	this.shape_13.setTransform(50.2,101.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AATAxIgdgsIgNANIAAAfIgVAAIAAhgIAVAAIAAArIAlgrIAbAAIgmAnIAqA5g");
	this.shape_14.setTransform(40.2,101.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAVAxIAAhGIgBAAIgjBGIgbAAIAAhgIAWAAIAABFIAAAAIAkhFIAbAAIAABgg");
	this.shape_15.setTransform(29.1,101.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAUAxIAAgqIgIACIgMACIgKABQgPAAgIgHQgIgGAAgPIAAgfIAWAAIAAAYQAAAKAEAEQAFAFAJAAIAGgBIAIgCIAHgCIAAgmIAWAAIAABgg");
	this.shape_16.setTransform(18.3,101.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgXAtQgLgHgFgLQgGgLAAgPQAAgPAGgLQAGgMALgHQALgGANAAQATAAALAJQALAKACARIgVAAQgCgKgGgFQgFgFgJAAQgLABgHAJQgHAJAAAPQAAAQAHAJQAHAJAKAAQAKAAAGgGQAGgFACgLIAVAAQgCASgMAKQgMALgTAAQgNAAgLgGg");
	this.shape_17.setTransform(8.2,101.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAVAxIAAhGIgBAAIgjBGIgbAAIAAhgIAWAAIAABFIABAAIAjhFIAbAAIAABgg");
	this.shape_18.setTransform(-2.6,101.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAVAxIAAhPIgpAAIAABPIgWAAIAAhgIBVAAIAABgg");
	this.shape_19.setTransform(-13.5,101.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAiBAIAAgfIhDAAIAAAfIgVAAIAAgwIAMAAQAEgIADgJIAEgWQABgNAAgQIAAgLIBIAAIAABPIANAAIAAAwgAgJgnQAAALgCALIgEATIgEAOIAnAAIAAg/IgdAAg");
	this.shape_20.setTransform(-24.7,102.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgZAtQgLgGgGgMQgGgMAAgPQAAgPAGgLQAGgMAMgGQAKgHAOAAQAPAAALAHQALAGAGAMQAGALAAAPQAAAPgGAMQgGAMgLAGQgLAGgPABQgNgBgMgGgAgTgYQgHAJgBAPQABAQAHAJQAHAKAMgBQAMAAAIgJQAHgJAAgQQAAgPgHgJQgIgJgMgBQgMABgHAJg");
	this.shape_21.setTransform(-35.9,101.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAVAxIAAhPIgpAAIAABPIgWAAIAAhgIBUAAIAABgg");
	this.shape_22.setTransform(-47,101.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgXAtQgLgGgGgMQgGgLABgQQAAgNAFgMQAHgMAKgGQALgIAMAAQAOAAALAIQAKAGAGAMQAFANAAAOIAAADIhGAAQAAAOAIAIQAHAJAKAAQAJAAAGgFQAFgEAEgJIAUAAQgDALgFAHQgHAIgJADQgJAFgLAAQgOAAgKgHgAAZgHQgBgNgHgHQgGgHgLAAQgJAAgGAHQgHAGgBAOIAwAAIAAAAg");
	this.shape_23.setTransform(73.2,78.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AAkAwIAAhgIAWAAIAABggAg4AwIAAhgIAVAAIAAAkIAaAAQAPAAAIAIQAJAHAAAOQAAAPgJAIQgIAJgPgBgAgjAfIAYAAQAIAAACgDQACgFAAgFQAAgHgCgEQgEgEgGAAIgYAAg");
	this.shape_24.setTransform(61,78.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgqAwIAAhgIA0AAQAOABAHAFQAIAHAAALQAAAJgEAEQgDAFgHADIAAAAQAJADAFAEQAEAHAAAKQAAANgJAIQgJAGgSAAgAgVAgIAcAAQAHAAAEgDQAEgEAAgGQAAgGgEgEQgEgEgHAAIgcAAgAgVgIIAaAAQAGAAADgEQADgCAAgHQAAgFgDgCQgDgDgHAAIgZAAg");
	this.shape_25.setTransform(49.1,78.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAVAwIAAhFIAAAAIgjBFIgbAAIAAhgIAVAAIAABGIABAAIAihGIAbAAIAABgg");
	this.shape_26.setTransform(38.1,78.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAxAwIgegqIgJAKIAAAgIgTAAIAAggIgJgKIgeAqIgaAAIAqg2IglgqIAaAAIAhAqIABAAIAAgqIATAAIAAAqIABAAIAhgqIAaAAIglAqIAqA2g");
	this.shape_27.setTransform(25.3,78.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgZAtQgLgGgGgMQgGgLAAgQQAAgOAGgMQAGgMALgGQAMgGANgBQAPABALAGQALAGAGAMQAGAMAAAOQAAAQgGALQgGAMgLAGQgLAHgPAAQgNAAgMgHgAgTgYQgHAJgBAPQABAQAHAKQAHAIAMABQANAAAGgKQAIgJAAgQQAAgPgIgJQgGgJgNgBQgMABgHAJg");
	this.shape_28.setTransform(7,78.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AATAwIgdgsIgNAOIAAAeIgVAAIAAhgIAVAAIAAAsIAlgsIAbAAIgmAoIAqA4g");
	this.shape_29.setTransform(-3.2,78.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgpAwIAAhgIAVAAIAAAkIAdAAQAPAAAJAIQAJAHAAAOQAAAPgJAIQgJAJgPgBgAgUAfIAaAAQAIAAADgDQADgFAAgFQAAgHgDgEQgEgEgIAAIgZAAg");
	this.shape_30.setTransform(-13.7,78.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgoAxIgGgCIAAgRIADACIAFAAIAGgCQADgCACgFQACgGAAgJIACg5IBGAAIAABgIgVAAIAAhPIgcAAIgCApQAAAUgIAKQgIALgNAAIgHgBg");
	this.shape_31.setTransform(-25,78.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgZAtQgLgGgGgMQgGgLAAgQQAAgOAGgMQAGgMAMgGQAKgGAOgBQAPABALAGQALAGAGAMQAGAMAAAOQAAAQgGALQgGAMgLAGQgLAHgPAAQgNAAgMgHgAgTgYQgHAJgBAPQABAQAHAKQAHAIAMABQAMAAAIgKQAHgJAAgQQAAgPgHgJQgIgJgMgBQgMABgHAJg");
	this.shape_32.setTransform(-35.6,78.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgLBDIAAhxIgrAAIAAgUIBtAAIAAAUIgsAAIAABxg");
	this.shape_33.setTransform(-46.8,76.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-54.5,64,158.8,71.4);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Isolation Mode
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#122A33").s().p("A/DHSMAnTgmFIW0XjMgnUAmEg");
	this.shape.setTransform(-26.3,352.3,0.346,0.346);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DBA577").s().p("At6OHIB4h0QAqgsAfg7QAvhaAEhmIgiu8QAAhMAXhNQAmh6BbhYIGbmOIAWgSQAdgUAjgRQBwg3CMACIIuAAIAZADQAdAGAVAPQBBAugxB0IgjBTQgtBahSAvQg3AghCAIImpAVQgnAGgaAaQgmAlgBBLIADBbIAQBFQAZBSAqBAIBgCTQAUAlAMAxQApCegrDaIgkDRQgaCghGCDQguBXg6A9In7Htg");
	this.shape_1.setTransform(24.3,257.8,0.346,0.346);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#DBA57D").s().p("AhEEkQhfAAhDhEQhChEADhhQADheBFhEIB+h4QBGhEBdAAQBgAABCBFQBCBEgCBgQgDBehGBDIh9B6QhFBDhdAAIgCAAg");
	this.shape_2.setTransform(102.5,284.2,0.346,0.346);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#DBA57D").s().p("AkdEUQhBhEAEhhQADhhBIhDIDijcQBGhFBigBQBgAABCBEQBCBEgDBhQgEBhhHBDIjkDcQhFBFhhABIgCAAQhgAAhChEg");
	this.shape_3.setTransform(100.3,263.4,0.346,0.346);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#DBA57D").s().p("Ai4GVQhdgEhChEQhChEgBhdQgBhcBBg/IF2lqQBBg/BdAEQBcADBCBFQBCBEABBcQABBdhBA/Il2FqQg+A7hWAAIgJAAg");
	this.shape_4.setTransform(100.3,240.4,0.346,0.346);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#DBA57D").s().p("AiGFkQhgAAhChEQhChEAChgQADhgBGhCIECj6QBEhEBgABQBgAABCBEQBCBEgDBgQgCBghGBCIkDD6QhDBDhfAAIgBAAg");
	this.shape_5.setTransform(96.8,219.9,0.346,0.346);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#302825").s().p("AhYgUICxAAIixApg");
	this.shape_6.setTransform(69.2,230.9,0.346,0.346);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#4A403B").s().p("AhYgUICxAAIAAApg");
	this.shape_7.setTransform(58.4,230.9,0.346,0.346);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#C6C6C6").s().p("Ahvg2IAGAAQA3gEAyggQBUg4Ach5IAAAAQAAASgFAnQgJBDgSBEQg7DRiECGg");
	this.shape_8.setTransform(67.5,266.1,0.346,0.346);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E3E3E3").s().p("AhPhPQgThEgIhDIgGg1QAdB5BUA4QAzAgA2AEIAGAAIAAFCQiFiHg6jUg");
	this.shape_9.setTransform(59.7,266.1,0.346,0.346);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#302825").s().p("AB1BjQglishQgIQg1gGgvATQg6AXgwADIAAlaQBzAABUA2QBpBFBKClQAZA4AIBaQAEAtgCAjIgBAAQgSAKgOAoQgXBGAICGIgRAJQAAipgZh5g");
	this.shape_10.setTransform(70.8,225,0.346,0.346);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#4A403B").s().p("AieF8QAJiGgYhGQgOgpgSgJIgBAAQgBgjADgtQAIhaAZg4QBIijBxhHQBXg2BqAAIAAFaQgwgDg5gXQgwgTg2AGQhPAIglCsQgZB5AACpg");
	this.shape_11.setTransform(56.4,225,0.346,0.346);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#DDAE8A").s().p("Aj6G/IAAtVQAwgDA6gXQAvgTA3AGQBOAIAlCuQAZB5AACnIARgJQgIiEAXhGQAOgoASgKQA8gaAUAxQAQAmgKBKQgIA1gfApQgfAlgqANQgQBmgWBIQgbBUgeAaQgoAkhcApQhmArg4AAgAiyitICygqIiyAAg");
	this.shape_12.setTransform(72.3,237.6,0.346,0.346);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#ECC29E").s().p("AD7G/Qg5AAhlgrQhdgpgogkQgegagahUQgXhIgQhmQgqgNgfglQgfgpgHg1QgLhKAQgmQAUgxA8AaQATAJANApQAYBGgICEIARAJQAAinAZh5QAkiuBOgIQA3gGAwATQA5AXAxADIAANVgAC/itIAAgqIizAAg");
	this.shape_13.setTransform(54.9,237.6,0.346,0.346);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#454545").s().p("AscGAQANhMAXhqQAtjRAqiVIALgRIAKgMIAEgGIADgCIAJgJIATgQIAygiIB5g6IAhgMIABgBQBSgfBpgdIAGA2QAIBDATBDQA6DVCGCHQCFiGA8jSQAShDAJhDQAFgoAAgRIAAgBQBcAaBOAcIACAAIAOAGIAEABIB/A4IArAZIAsAgIAZAYIACADIAFAFIAIAMIAFAJQA2CIAoDWQAUBtAKBRg");
	this.shape_14.setTransform(63.6,270.1,0.346,0.346);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#CEA082").s().p("AhviJIABAAQA5AABjgsIAACGIBCARQgcB4hUA3QgyAhg3ADIgGABg");
	this.shape_15.setTransform(67.5,257.9,0.346,0.346);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E2BB9C").s().p("ABqC1Qg2gDgzghQhUg3gdh4IBEgRIAAiGQBiAsA5AAIABAAIAAE/g");
	this.shape_16.setTransform(59.7,257.9,0.346,0.346);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFCF8").s().p("AgDBgIAAi/IAHAAIAAC/g");
	this.shape_17.setTransform(37.2,184.2,0.346,0.346);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E3E3E3").s().p("AhbAbIAAg1IC3AAIAAA1g");
	this.shape_18.setTransform(64.5,196.2,0.346,0.346);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E3E3E3").s().p("AgzAbIAAg1IBnAAIAAA1g");
	this.shape_19.setTransform(57.7,196.2,0.346,0.346);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E3E3E3").s().p("AivAbIAAg1IFfAAIAAA1g");
	this.shape_20.setTransform(47.5,196.2,0.346,0.346);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFCF8").s().p("AhRAAIA3g1IATATIgVAVIBuAAIAAAaIhuAAIAVAWIgTAUg");
	this.shape_21.setTransform(31,184.3,0.346,0.346);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#454545").p("AAVAAQAAAIgGAGQgHAGgIAAQgHAAgHgGQgGgGAAgIQAAgHAGgHQAHgGAHAAQAIAAAHAGQAGAHAAAHg");
	this.shape_22.setTransform(84.9,291.2,0.346,0.346);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#454545").p("AAVAAQAAAIgGAHQgHAGgIAAQgHAAgHgGQgGgHAAgIQAAgHAGgHQAHgFAHAAQAIAAAHAFQAGAHAAAHg");
	this.shape_23.setTransform(84.9,289.1,0.346,0.346);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#454545").p("AAVAAQAAAIgGAGQgHAGgIAAQgHAAgHgGQgGgGAAgIQAAgHAGgHQAHgGAHAAQAIAAAHAGQAGAHAAAHg");
	this.shape_24.setTransform(84.9,286.8,0.346,0.346);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#F0F0F0").s().p("AgIAHIAAgNIARAAIAAANg");
	this.shape_25.setTransform(76.3,177.2,0.346,0.346);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#F0F0F0").s().p("AgIAOIAAgbIARAAIAAAbg");
	this.shape_26.setTransform(77.3,177,0.346,0.346);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#F0F0F0").s().p("AgIATIAAglIARAAIAAAlg");
	this.shape_27.setTransform(78.4,176.8,0.346,0.346);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#F0F0F0").s().p("AgIAZIAAgxIARAAIAAAxg");
	this.shape_28.setTransform(79.4,176.6,0.346,0.346);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#F0F0F0").s().p("AgIAfIAAg9IARAAIAAA9g");
	this.shape_29.setTransform(80.4,176.4,0.346,0.346);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#F0F0F0").s().p("AgoASIAAgjIBRAAIAAAjg");
	this.shape_30.setTransform(86.3,176.6,0.346,0.346);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#F0F0F0").ss(0.8).p("Ag4gZIAAANIgKAAIAAAZIAKAAIAAANIB7AAIAAgzg");
	this.shape_31.setTransform(85.7,176.6,0.346,0.346);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#F0F0F0").s().p("AgEAHQgBgDAAgEQAAgEABgCQACgCACAAQACAAACACQACADAAADQAAAEgCADQgCACgCAAQgCAAgCgCgAgBgEIgBAEIABAFQAAAAABAAQAAABAAAAQAAAAAAAAQAAAAAAAAQAAAAAAAAQABAAAAAAQAAAAAAgBQABAAAAAAIABgFIgBgEQAAgBgBAAQAAAAAAAAQAAgBgBAAQAAAAAAAAQAAAAAAAAQAAAAAAABQAAAAAAAAQgBAAAAABg");
	this.shape_32.setTransform(94.2,175.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#F0F0F0").s().p("AgFAIIAAgDIADABIACABIACgBIABgDIgBgCIgCgBIgCAAIAAAAIACAAQACAAAAgDIAAgCIgCgBIgBABIgCABIgCgCQADgCACAAIAEABQAAAAAAABQABAAAAAAQAAABAAAAQAAABAAAAIgBADIgDABIAEAAIABADQAAABAAABQAAAAgBABQAAAAAAABQAAAAgBABQgCABgCAAQgDAAgCgBg");
	this.shape_33.setTransform(92.7,175.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#F0F0F0").s().p("AAAAGIgBgBIABgCIAAAAIABAAIAAACIAAABIgBABIAAgBgAAAgDIgBgBQAAgBABAAQAAAAAAgBQAAAAAAAAQAAAAAAAAIABABIAAABIAAABIgBABIAAgBg");
	this.shape_34.setTransform(91.6,176);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#F0F0F0").s().p("AAAAJIAAgKIAAgEIAAABIgBACIgBgCIADgEIADAAIAAARg");
	this.shape_35.setTransform(90.3,175.8);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#F0F0F0").s().p("AABAJIAAgKIAAgEIgBABIgBACIgCgCIAEgEIACAAIAAARg");
	this.shape_36.setTransform(88.8,175.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#5D5D5D").ss(4).p("AxmAAMAjNAAA");
	this.shape_37.setTransform(63.4,173.1,0.346,0.346);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#5D5D5D").ss(4).p("AxmAAMAjNAAA");
	this.shape_38.setTransform(63.4,294.6,0.346,0.346);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#EF6E4E").s().p("AhKBMQggggAAgsQAAgrAggfQAfggArAAQAsAAAgAgQAfAfAAArQAAAsgfAgQggAfgsAAQgrAAgfgfg");
	this.shape_39.setTransform(32.6,196,0.346,0.346);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#454545").p("ABYBUIgNgyQAHgPAAgTQAAgjgZgZQgagZgiAAQgjAAgaAZQgZAZAAAjQAAAjAZAaQAaAZAjAAQAZAAAWgPg");
	this.shape_40.setTransform(43.8,288.7,0.346,0.346,0,0,0,-0.1,0);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#454545").p("AAqhWQgZAAgRASQgPgSgZAAQgYAAgRARQgRARAAAXQAAAKAFAPIAEAJQAPAfAoAcQATAOAPAIQAQgIAUgOQAngcAPgfIAFgJQAEgNAAgMQAAgXgRgRQgRgRgXAAg");
	this.shape_41.setTransform(32.9,289,0.346,0.346);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#C1ECF7").s().p("AxmSSMAAAgkjMAjNAAAMAAAAkjg");
	this.shape_42.setTransform(63.4,242.9,0.346,0.346);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#71A1CC").s().p("AxmCYIAAkvMAjNAAAIAAEvg");
	this.shape_43.setTransform(63.4,184.2,0.346,0.346);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#5B87B9").s().p("AxmBdIAAi5MAjNAAAIAAC5g");
	this.shape_44.setTransform(63.4,176.4,0.346,0.346);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFCF8").s().p("AxmbTMAAAg2lMAjNAAAMAAAA2lg");
	this.shape_45.setTransform(63.4,233.5,0.346,0.346);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#E3E3E3").p("ACSAAQAAA8grArQgrArg8AAQg7AAgrgrQgrgqAAg9QAAg7ArgrQArgrA7AAQA8AAArArQArArAAA7g");
	this.shape_46.setTransform(63.4,305.4,0.346,0.346);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#E3E3E3").p("ACzAAQAABKg1A0Qg0A1hKAAQhIAAg1g1Qg1g0AAhKQAAhJA1g0QA0g1BJAAQBKAAA0A1QA1A0AABJg");
	this.shape_47.setTransform(63.4,305.5,0.346,0.346);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#1A1A1A").s().p("EgO/AkdQhFAAgzgyQgygyABhGMAAAhDlQgBhGAygyQAzgyBFAAId/AAQBFAAAyAyQAzAyAABGMAAABDlQAABGgzAyQgyAyhFAAg");
	this.shape_48.setTransform(63.4,234.2,0.346,0.346);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#BF8358").s().p("AymOCMAlNgr2MgAGAmfI10VKg");
	this.shape_49.setTransform(23.8,276.3,0.346,0.346);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95,153.6,209.6,267);


(lib.Symbol2copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgSvAtKMAAAhaTMAlfAcIMAAAA+Lg");
	this.shape.setTransform(120,229);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-60,240,578);


(lib.Symbol2copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#A1B400").s().p("EgSvAtUMAAAhanMAlfAcIMAAAA+fg");
	this.shape.setTransform(120,230);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-60,240,580);


(lib.Symbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#30AFB8").s().p("EgSvAtUMAAAhanMAlfAcIMAAAA+fg");
	this.shape.setTransform(120,230);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-60,240,580);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF5151").s().p("EgSvAxOMAAAhiaMAlfAjtMAAAA+tg");
	this.shape.setTransform(120,87);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-228,240,630);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMBKIAAgaIAZAAIAAAagAgFAhIgGg/IAAgrIAXAAIAAArIgGA/g");
	this.shape.setTransform(-7.8,64.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgbAxQgNgHgGgMQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGAMAAARQAAARgGANQgHAMgMAHQgMAIgRAAQgPAAgMgIgAgVgbQgIAKAAARQAAASAIAKQAIAKANAAQAOAAAIgKQAIgKAAgSQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape_1.setTransform(-16.9,66.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAXA2IAAgvIgtAAIAAAvIgYAAIAAhrIAYAAIAAArIAtAAIAAgrIAYAAIAABrg");
	this.shape_2.setTransform(-29.1,66.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AguA2IAAhrIAYAAIAAAoIAgAAQASAAAJAJQAKAHAAAQQAAAQgKAJQgJAKgSAAgAgWAjIAdAAQAIAAAEgEQADgFAAgGQAAgIgDgEQgFgFgIAAIgcAAg");
	this.shape_3.setTransform(-40.4,66.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgtA2IgGgBIAAgTIAEABIAEAAQAEAAADgCQADgCADgGQACgGAAgLIACg+IBOAAIAABrIgYAAIAAhYIgfAAIgCAtQAAAWgJAMQgIALgPAAIgIgBg");
	this.shape_4.setTransform(-52.8,66.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgZAxQgMgHgHgNQgGgMAAgQQAAgPAHgOQAGgNAMgIQAMgHANAAQAQAAAMAHQALAIAHANQAFAOAAAQIAAADIhNAAQAAAQAIAJQAIAJAMAAQAJAAAHgEQAFgFAEgKIAXAAQgDAMgHAIQgGAIgLAFQgKAEgMAAQgPAAgLgIgAAcgJQgBgNgIgJQgHgHgMAAQgKAAgHAHQgHAIgCAOIA2AAIAAAAg");
	this.shape_5.setTransform(-64.2,66.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgKA2IAAhYIgkAAIAAgTIBdAAIAAATIgkAAIAABYg");
	this.shape_6.setTransform(-75,66.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAdA2IAAgsIgVAAIgfAsIgcAAIAkgvQgKgDgFgEQgFgEgCgGQgCgGAAgHQABgOAJgIQAJgIARAAIA3AAIAABrgAgLgfQgEAEAAAGQAAAIAEAEQAEAEAHAAIAdAAIAAgdIgdAAQgHAAgEADg");
	this.shape_7.setTransform(-86.4,66.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgbAxQgNgHgGgMQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGAMAAARQAAARgGANQgHAMgMAHQgMAIgRAAQgPAAgMgIgAgVgbQgIAKAAARQAAASAIAKQAIAKANAAQAOAAAIgKQAIgKAAgSQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape_8.setTransform(-97.7,66.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgKA2IAAhYIgkAAIAAgTIBdAAIAAATIgkAAIAABYg");
	this.shape_9.setTransform(-108.8,66.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgZAyQgNgIgFgMQgHgNAAgQQAAgQAHgNQAGgOAMgGQAMgIAPABQAUAAAOAKQAMAKACATIgXAAQgCgLgHgFQgGgFgKAAQgMAAgIAKQgHALgBAQQABARAHAKQAIALALAAQALAAAHgGQAGgHADgMIAXAAQgDAVgNALQgNAMgVgBQgOAAgMgGg");
	this.shape_10.setTransform(-119.4,66.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgbAxQgNgHgGgMQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGAMAAARQAAARgGANQgHAMgMAHQgMAIgRAAQgPAAgMgIgAgVgbQgIAKAAARQAAASAIAKQAIAKANAAQAOAAAIgKQAIgKAAgSQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape_11.setTransform(-131.6,66.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAmA2IAAhGIAAAAIgbBGIgWAAIgahGIAAAAIAABGIgXAAIAAhrIAfAAIAdBUIABAAIAchUIAgAAIAABrg");
	this.shape_12.setTransform(-145.2,66.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAaA1QgEgDgCgHQgHAHgKADQgIAEgLgBQgRAAgKgIQgJgJAAgPQAAgLAEgGQAFgGAHgCQAHgDAJgCIAQgCIALgCIAIgCIADgCIABgDIAAgFQAAgIgFgEQgGgEgJAAQgLgBgGAFQgGAEgBALIgXAAQACgUANgJQANgIAUAAQALAAAKADQAKAEAFAHQAGAHAAAMIAAA1IABAGQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIACAAIADgBIAAARIgIACIgHAAIgCABQgGAAgEgEgAAHAEIgLABQgGABgFACQgGABgDAEQgEADAAAHQAAAIAFAEQAFAEAKAAQAHAAAFgCQAGgCAEgEQAEgEABgFIAAgKIAAgMQgGACgGACg");
	this.shape_13.setTransform(-158.2,66.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgZAyQgMgIgGgMQgHgNAAgQQAAgQAHgNQAGgOAMgGQAMgIAPABQAVAAANAKQAMAKACATIgXAAQgDgLgGgFQgGgFgKAAQgMAAgIAKQgIALAAAQQAAARAIAKQAHALAMAAQALAAAHgGQAHgHACgMIAXAAQgDAVgNALQgNAMgVgBQgPAAgLgGg");
	this.shape_14.setTransform(-170,66.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAmA2IAAhGIgBAAIgaBGIgWAAIgZhGIgBAAIAABGIgXAAIAAhrIAfAAIAdBUIABAAIAchUIAgAAIAABrg");
	this.shape_15.setTransform(24.1,40.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAaA1QgEgDgCgHQgHAHgKADQgIAEgLgBQgRAAgKgIQgJgJAAgPQAAgLAEgGQAFgGAHgCQAHgDAJgCIAQgCIALgCIAIgCIADgCIABgDIAAgFQAAgIgFgEQgGgEgJAAQgLgBgGAFQgGAEgBALIgXAAQACgUANgJQANgIAUAAQALAAAKADQAKAEAFAHQAGAHAAAMIAAA1IABAGQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIACAAIADgBIAAARIgIACIgHAAIgCABQgGAAgEgEgAAHAEIgLABQgGABgFACQgGABgDAEQgEADAAAHQAAAIAFAEQAFAEAKAAQAHAAAFgCQAGgCAEgEQAEgEABgFIAAgKIAAgMQgGACgGACg");
	this.shape_16.setTransform(11.1,40.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("Ag0BLIAAiSIAWAAIAAAOQAHgJAJgEQAJgEAJAAQAPAAALAHQALAHAGANQAGANAAASQAAAQgGALQgFANgMAHQgLAHgOAAQgJAAgJgEQgJgEgHgJIAAA2gAgVgtQgIAKAAASQAAARAIAJQAIAKANAAQANAAAIgKQAIgJAAgQQAAgSgIgLQgIgKgNAAQgOAAgHAKg");
	this.shape_17.setTransform(-1.2,42.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AglA2IAAhrIBLAAIAAATIgyAAIAABYg");
	this.shape_18.setTransform(-11.7,40.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAaA1QgEgDgCgHQgHAHgKADQgIAEgLgBQgRAAgKgIQgJgJAAgPQAAgLAEgGQAFgGAHgCQAHgDAJgCIAQgCIALgCIAIgCIADgCIABgDIAAgFQAAgIgFgEQgGgEgJAAQgLgBgGAFQgGAEgBALIgXAAQACgUANgJQANgIAUAAQALAAAKADQAKAEAFAHQAGAHAAAMIAAA1IABAGQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIACAAIADgBIAAARIgIACIgHAAIgCABQgGAAgEgEgAAHAEIgLABQgGABgFACQgGABgDAEQgEADAAAHQAAAIAFAEQAFAEAKAAQAHAAAFgCQAGgCAEgEQAEgEABgFIAAgKIAAgMQgGACgGACg");
	this.shape_19.setTransform(-22.4,40.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgKA2IAAhYIgkAAIAAgTIBdAAIAAATIgkAAIAABYg");
	this.shape_20.setTransform(-33.4,40.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgaAxQgLgGgHgNQgGgMAAgRQAAgQAGgNQAHgNAMgIQAMgGAPAAQAUgBANALQANAKACATIgYAAQgCgKgGgGQgGgFgKAAQgMAAgIAKQgHALgBAQQABARAHAKQAIALALAAQALAAAGgHQAIgGABgMIAYAAQgDAVgNALQgOALgUAAQgOABgNgIg");
	this.shape_21.setTransform(-44,40.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAXA2IAAgvIgtAAIAAAvIgYAAIAAhrIAYAAIAAArIAtAAIAAgrIAYAAIAABrg");
	this.shape_22.setTransform(-55.9,40.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAXA2IAAhNIAAAAIgnBNIgeAAIAAhrIAYAAIAABNIAAAAIAnhNIAeAAIAABrg");
	this.shape_23.setTransform(-67.9,40.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgvA2IAAhrIA6AAQAPAAAIAHQAJAHAAANQAAAJgEAFQgDAFgJAEIAAAAQALADAFAFQAFAHAAALQAAAPgLAIQgJAIgUAAgAgXAkIAfAAQAIAAAEgEQAEgEAAgHQAAgHgEgEQgEgEgIAAIgfAAgAgXgKIAdAAQAGAAAEgDQADgDAAgHQABgGgEgDQgEgDgHAAIgcAAg");
	this.shape_24.setTransform(-85.4,40.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgKA2IAAhYIgkAAIAAgTIBdAAIAAATIgkAAIAABYg");
	this.shape_25.setTransform(-102.2,40.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAXA2IAAgvIgtAAIAAAvIgYAAIAAhrIAYAAIAAArIAtAAIAAgrIAYAAIAABrg");
	this.shape_26.setTransform(-113.1,40.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AggBKIgKgBIAAgUIAIABIAFAAQAGAAADgEQAEgEACgFIADgLIgphnIAaAAIAbBOIAbhOIAZAAIgoBoIgIAUQgEAKgFAHQgIAGgNABIgHgBg");
	this.shape_27.setTransform(-124.6,43);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAaA1QgEgDgCgHQgHAHgKADQgIAEgLgBQgRAAgKgIQgJgJAAgPQAAgLAEgGQAFgGAHgCQAHgDAJgCIAQgCIALgCIAIgCIADgCIABgDIAAgFQAAgIgFgEQgGgEgJAAQgLgBgGAFQgGAEgBALIgXAAQACgUANgJQANgIAUAAQALAAAKADQAKAEAFAHQAGAHAAAMIAAA1IABAGQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIACAAIADgBIAAARIgIACIgHAAIgCABQgGAAgEgEgAAHAEIgLABQgGABgFACQgGABgDAEQgEADAAAHQAAAIAFAEQAFAEAKAAQAHAAAFgCQAGgCAEgEQAEgEABgFIAAgKIAAgMQgGACgGACg");
	this.shape_28.setTransform(-135.6,40.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAVA2IgggxIgOAPIAAAiIgYAAIAAhrIAYAAIAAAwIApgwIAdAAIgqAsIAvA/g");
	this.shape_29.setTransform(-146.7,40.9);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAVA2IgggxIgOAPIAAAiIgYAAIAAhrIAYAAIAAAwIApgwIAdAAIgqAsIAvA/g");
	this.shape_30.setTransform(-158,40.9);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AAaA1QgEgDgCgHQgHAHgKADQgIAEgLgBQgRAAgKgIQgJgJAAgPQAAgLAEgGQAFgGAHgCQAHgDAJgCIAQgCIALgCIAIgCIADgCIABgDIAAgFQAAgIgFgEQgGgEgJAAQgLgBgGAFQgGAEgBALIgXAAQACgUANgJQANgIAUAAQALAAAKADQAKAEAFAHQAGAHAAAMIAAA1IABAGQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIACAAIADgBIAAARIgIACIgHAAIgCABQgGAAgEgEgAAHAEIgLABQgGABgFACQgGABgDAEQgEADAAAHQAAAIAFAEQAFAEAKAAQAHAAAFgCQAGgCAEgEQAEgEABgFIAAgKIAAgMQgGACgGACg");
	this.shape_31.setTransform(-169.9,40.9);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AAXBMIAAhNIAAAAIgnBNIgeAAIAAhqIAYAAIAABMIAAAAIAnhMIAeAAIAABqgAgXg1QgIgHgCgPIAMAAQACAIAGAEQAFAEAIAAQAJAAAFgEQAGgEACgIIAMAAQgCAPgIAHQgIAIgQAAQgPAAgIgIg");
	this.shape_32.setTransform(-34.1,13.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgbAyQgNgIgGgMQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGANAAAQQAAARgGANQgHAMgMAIQgMAGgRABQgPgBgMgGgAgVgbQgIAKAAARQAAASAIAKQAIAKANAAQAOAAAIgKQAIgLAAgRQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape_33.setTransform(-46.3,15.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgvA2IAAhrIA6AAQAPAAAJAHQAIAHAAANQAAAJgEAFQgDAFgJAEIAAAAQALADAFAFQAFAHAAALQAAAPgLAIQgKAIgTAAgAgXAkIAfAAQAIAAAEgEQAEgEAAgHQAAgHgEgEQgEgEgIAAIgfAAgAgXgKIAdAAQAGAAAEgDQADgDAAgHQAAgGgDgDQgEgDgHAAIgcAAg");
	this.shape_34.setTransform(-58.1,15.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgaAxQgLgHgHgMQgGgMAAgRQAAgQAGgNQAHgNAMgIQAMgGAPgBQAVAAAMALQANAKACATIgYAAQgCgLgGgFQgGgFgKAAQgMAAgIAKQgHAKgBARQABARAHALQAHAKAMAAQALAAAGgHQAIgGABgMIAYAAQgDAUgNAMQgOALgUAAQgOABgNgIg");
	this.shape_35.setTransform(-70,15.5);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AAXA2IAAhNIAAAAIgnBNIgeAAIAAhrIAYAAIAABNIAAAAIAnhNIAeAAIAABrg");
	this.shape_36.setTransform(-87.8,15.5);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgKA2IAAhYIgkAAIAAgTIBdAAIAAATIgkAAIAABYg");
	this.shape_37.setTransform(-98.7,15.5);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AggBKIgKgBIAAgUIAIABIAFAAQAGAAADgEQAEgEACgFIADgLIgphnIAaAAIAbBOIAbhOIAZAAIgoBoIgIAVQgEAJgFAHQgIAGgNAAIgHAAg");
	this.shape_38.setTransform(-109,17.6);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("Ag0BLIAAiSIAWAAIAAAOQAGgJAKgEQAJgEAJAAQAPAAALAHQALAHAGANQAGANAAASQAAAQgGALQgFANgLAHQgLAHgPAAQgJAAgJgEQgJgEgHgJIAAA2gAgVgtQgIAKAAASQAAARAIAJQAIAKANAAQANAAAIgKQAIgJAAgQQAAgSgIgLQgIgKgNAAQgNAAgIAKg");
	this.shape_39.setTransform(-120.6,17.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AAVA2IgggxIgOAPIAAAiIgYAAIAAhrIAYAAIAAAwIApgwIAdAAIgqAsIAvA/g");
	this.shape_40.setTransform(-132.3,15.5);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgaAxQgLgHgHgMQgGgMAAgRQAAgQAGgNQAHgNAMgIQAMgGAPgBQAUAAANALQANAKACATIgYAAQgCgLgGgFQgGgFgKAAQgMAAgIAKQgHAKgBARQABARAHALQAIAKALAAQALAAAGgHQAIgGABgMIAYAAQgDAUgNAMQgOALgUAAQgOABgNgIg");
	this.shape_41.setTransform(-144.3,15.5);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AAaA1QgEgDgCgHQgHAHgKADQgIAEgLgBQgRAAgKgIQgJgJAAgPQAAgLAEgGQAFgGAHgCQAHgDAJgCIAQgCIALgCIAIgCIADgCIABgDIAAgFQAAgIgFgEQgGgEgJAAQgLgBgGAFQgGAEgBALIgXAAQACgUANgJQANgIAUAAQALAAAKADQAKAEAFAHQAGAHAAAMIAAA1IABAGQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIACAAIADgBIAAARIgIACIgHAAIgCABQgGAAgEgEgAAHAEIgLABQgGABgFACQgGABgDAEQgEADAAAHQAAAIAFAEQAFAEAKAAQAHAAAFgCQAGgCAEgEQAEgEABgFIAAgKIAAgMQgGACgGACg");
	this.shape_42.setTransform(-155.9,15.5);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("Ag5BKIAAiTIA/AAQAQAAALAFQAMAEAGALQAHAKAAAPQAAAWgNAKQgNANgXAAIgnAAIAAA5gAgegCIAiAAQANAAAHgGQAJgGAAgNQAAgKgFgFQgEgFgHgCQgHgDgJABIgfAAg");
	this.shape_43.setTransform(-168.5,13.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-178,0,217.6,78.2);


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_35 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(35).call(this.frame_35).wait(1));

	// Layer 3
	this.instance = new lib.Symbol21("synched",0);
	this.instance.setTransform(385.2,-121.9,0.575,0.575,0,0,0,85.1,84.9);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(21).to({_off:false},0).to({y:-6.2},9,cjs.Ease.get(1)).to({regY:85,y:-17.7},5,cjs.Ease.get(1)).wait(1));

	// Layer 2
	this.instance_1 = new lib.Symbol20("synched",0);
	this.instance_1.setTransform(303.1,-121.8,0.575,0.575,0,0,0,85,85);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(14).to({_off:false},0).to({regY:85.1,y:-6.1},9,cjs.Ease.get(1)).to({y:-17.7},5,cjs.Ease.get(1)).wait(8));

	// Layer 4
	this.instance_2 = new lib.Symbol19("synched",0);
	this.instance_2.setTransform(221,-121.8,0.575,0.575,0,0,0,84.9,85);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(7).to({_off:false},0).to({regY:85.1,y:-6.1},9,cjs.Ease.get(1)).to({y:-17.7},5,cjs.Ease.get(1)).wait(15));

	// Layer 5
	this.instance_3 = new lib.Symbol18("synched",0);
	this.instance_3.setTransform(138.9,-110.2,0.575,0.575,0,0,0,84.9,85.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({y:-6.1},9,cjs.Ease.get(1)).to({y:-17.7},5,cjs.Ease.get(1)).wait(22));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(90,-159.2,97.8,97.8);


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 15
	this.instance = new lib.Symbol15("synched",0);
	this.instance.setTransform(132.1,65.4,0.91,0.91,0,0,0,132.1,21.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Бесплатно 3 дня
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#55B553").s().p("AAXAsIAAgkIgRAAIgZAkIgYAAIAfgmQgIgDgFgDQgEgCgBgGQgCgEABgHQgBgLAIgHQAIgGAOgBIAtAAIAABYgAgJgaQgDAEAAAFQAAAHADACQAEAEAFAAIAXAAIAAgYIgXAAQgGAAgDACg");
	this.shape.setTransform(196.1,18);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#55B553").s().p("AATAsIAAgnIglAAIAAAnIgUAAIAAhYIAUAAIAAAkIAlAAIAAgkIAUAAIAABYg");
	this.shape_1.setTransform(186.9,18);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#55B553").s().p("AAgA7IAAgdIg+AAIAAAdIgTAAIAAgsIALAAQAEgIACgHIAEgVIABgaIAAgLIBBAAIAABJIAMAAIAAAsgAgIgkQAAAKgCALIgDARIgEANIAkAAIAAg5IgbAAg");
	this.shape_2.setTransform(176.6,19.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#55B553").s().p("AgVA5QgKgFgFgJQgFgJAAgMIAAgCIATAAQAAAMAGAHQAGAGAKAAQAKAAAGgGQAGgFAAgLQAAgHgEgEQgDgFgGgCQgFgCgFAAIgDAAIgDAAIAAgNIACAAIAKgBQAFgBAEgEQAEgDAAgIQAAgIgFgFQgFgFgIAAQgIAAgGAGQgFAGAAAMIgTAAQABgTAKgKQAKgLARAAQALAAAIAEQAJAEAFAHQAEAIAAAKQAAAIgEAHQgFAHgIACQALADAFAFQAGAIAAALQAAAMgFAJQgGAIgJAFQgKAFgMAAQgMAAgJgFg");
	this.shape_3.setTransform(161.6,16.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#55B553").s().p("AgXApQgKgFgFgLQgGgKAAgPQAAgNAGgKQAFgLALgGQAKgGAMAAQAOAAAKAGQAKAGAFALQAGAKAAANQAAAPgGAKQgFALgKAFQgKAGgOAAQgMAAgLgGgAgRgVQgGAHgBAOQABAPAGAIQAHAJAKAAQALgBAIgIQAGgIAAgPQAAgOgHgIQgGgIgMAAQgKAAgHAJg");
	this.shape_4.setTransform(147,18);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#55B553").s().p("AATAsIAAgnIgmAAIAAAnIgTAAIAAhYIATAAIAAAkIAmAAIAAgkIAUAAIAABYg");
	this.shape_5.setTransform(136.9,18);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#55B553").s().p("AgJAsIAAhIIgdAAIAAgQIBNAAIAAAQIgdAAIAABIg");
	this.shape_6.setTransform(127.9,18);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#55B553").s().p("AAWAsQgEgCgBgGQgGAGgJACQgGADgJAAQgOAAgIgHQgIgHAAgNQAAgJAEgFQADgFAGgBIANgEIAOgCIAJgCIAGgBIADgCIABgDIAAgEQAAgHgEgDQgFgDgHAAQgKAAgFADQgEAEgBAJIgTAAQACgRAKgHQALgHAQAAQAJAAAIADQAIADAFAGQAFAGAAAJIAAAsQAAAEABABQAAABABAAQAAAAAAAAQABAAABAAQAAAAABAAIACAAIACAAIAAAOIgHACIgGAAIgBAAQgFAAgDgDgAAGADIgJACIgJABQgFACgDADQgDADAAAGQAAAGAFADQAEAEAIAAQAGAAADgCQAFgCAEgDQADgDABgFIAAgIIAAgKQgFACgFABg");
	this.shape_7.setTransform(119.1,17.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#55B553").s().p("AglAtIgFgCIAAgPIADABIAEAAQADAAACgBQADgCACgFQACgFAAgKIACgzIBAAAIAABYIgUAAIAAhIIgZAAIgCAmQAAARgHAKQgHAKgMAAIgHgBg");
	this.shape_8.setTransform(108.7,18.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#55B553").s().p("AATAsIAAhIIgmAAIAABIIgTAAIAAhYIBNAAIAABYg");
	this.shape_9.setTransform(99.2,18);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#55B553").s().p("AgVApQgKgGgFgKQgFgLAAgNQAAgNAFgLQAGgLAKgGQAKgGALAAQASAAAKAJQALAIABAQIgTAAQgCgIgFgFQgFgEgIAAQgKAAgGAIQgHAJAAANQAAAPAGAIQAHAIAJAAQAJAAAFgFQAGgFACgKIATAAQgCARgLAJQgLAKgRAAQgMAAgKgGg");
	this.shape_10.setTransform(89.4,17.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#55B553").s().p("AgVApQgKgGgFgLQgGgKAAgOQABgMAFgLQAGgLAKgGQAJgGALAAQANAAAKAGQAJAGAFALQAGAMAAANIAAACIhBAAQAAAOAHAHQAGAHAKABQAIAAAGgEQAEgEAEgJIATAAQgDAKgFAHQgHAHgIADQgIAEgLAAQgLAAgKgGgAAXgHQgCgLgFgHQgHgGgJAAQgIAAgGAGQgGAGgCAMIAtAAIAAAAg");
	this.shape_11.setTransform(79.7,18);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#55B553").s().p("AgxA9IAAh5IBZAAIAAATIhDAAIAAAcIAiAAQAVAAALAKQALAHAAATQAAATgLAJQgLAKgVAAgAgbArIAiAAQALAAAFgEQAFgFAAgLQAAgKgFgFQgGgFgKAAIgiAAg");
	this.shape_12.setTransform(69,16.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(9.1,4.5,246,83.3);


// stage content:
(lib._image = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.instance = new lib.Symbol27("synched",0);
	this.instance.setTransform(120.1,390.1,1,1,0,0,0,18.2,23.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(546).to({_off:false},0).to({y:330.1},11,cjs.Ease.get(1)).to({regX:18.1,rotation:-27,x:120},6).to({regX:18.2,rotation:0,x:120.1},6).to({regX:18.1,rotation:27,x:120},6).to({regX:18.2,rotation:0,x:120.1},5).wait(1));

	// Бесплатно 3 дня
	this.instance_1 = new lib.Symbol16();
	this.instance_1.setTransform(120,375.9,0.82,0.82,0,0,0,132.1,33.9);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(420).to({_off:false},0).to({y:310.3},12,cjs.Ease.get(1)).to({y:318.5},5,cjs.Ease.get(1)).wait(144));

	// Symbol 14
	this.instance_2 = new lib.Symbol14();
	this.instance_2.setTransform(84.5,275.9,1,1,0,0,0,133.3,35.6);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(403).to({_off:false},0).to({y:195.9},12,cjs.Ease.get(1)).to({y:205.9},5,cjs.Ease.get(1)).wait(161));

	// Symbol 13
	this.instance_3 = new lib.Symbol13("synched",0);
	this.instance_3.setTransform(259.8,130.9,1,1,0,0,0,139.8,35.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(386).to({_off:false},0).to({y:50.9},12,cjs.Ease.get(1)).to({y:60.9},5,cjs.Ease.get(1)).wait(178));

	// Layer 8
	this.instance_4 = new lib.Symbol2copy3("synched",0);
	this.instance_4.setTransform(527.5,608.6,1,1,0,0,0,527.5,157.6);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(366).to({_off:false},0).to({y:37.6},20,cjs.Ease.get(1)).wait(195));

	// Layer 4
	this.instance_5 = new lib.Symbol23("synched",0);
	this.instance_5.setTransform(122.4,379.4,1.29,1.29,0,0,0,36.7,43.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(280).to({_off:false},0).to({y:250.4},9,cjs.Ease.get(1)).to({y:263.3},4,cjs.Ease.get(1)).wait(45).to({startPosition:0},0).to({scaleX:1.68,scaleY:1.68},6,cjs.Ease.get(1)).to({scaleX:1.29,scaleY:1.29},6,cjs.Ease.get(1)).to({regY:43.2,scaleX:1.68,scaleY:1.68,y:263.2},6,cjs.Ease.get(1)).to({regY:43.3,scaleX:1.29,scaleY:1.29,y:263.3},6,cjs.Ease.get(1)).to({_off:true},25).wait(194));

	// 4.Автоматическое продвижение - минимум Вашего участия
	this.instance_6 = new lib.Symbol28("synched",0);
	this.instance_6.setTransform(592.1,114,1,1,0,0,0,407.9,18.2);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(275).to({_off:false},0).to({y:44},11,cjs.Ease.get(1)).to({_off:true},101).wait(194));

	// Layer 5
	this.instance_7 = new lib.Symbol2copy2("synched",0);
	this.instance_7.setTransform(527.5,608.6,1,1,0,0,0,527.5,157.6);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(255).to({_off:false},0).to({y:37.6},20,cjs.Ease.get(1)).to({_off:true},112).wait(194));

	// Layer 3
	this.instance_8 = new lib.Symbol29("synched",0);
	this.instance_8.setTransform(310,120.4);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(136).to({_off:false},0).to({y:60.4},10,cjs.Ease.get(1)).to({_off:true},129).wait(306));

	// Layer 1
	this.instance_9 = new lib.Symbol17();
	this.instance_9.setTransform(194.1,106.5,0.6,0.6,0,0,0,389.6,90.4);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(129).to({_off:false},0).to({_off:true},146).wait(306));

	// 2.Только живые подписчики с Вашего города. copy
	this.instance_10 = new lib.Symbol5copy("synched",0);
	this.instance_10.setTransform(452,105.1,1,1,0,0,0,336.1,19.3);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(127).to({_off:false},0).to({y:49.3},11,cjs.Ease.get(1)).to({y:45.1},4).to({_off:true},133).wait(306));

	// 2.Только живые подписчики с Вашего города.
	this.instance_11 = new lib.Symbol5("synched",0);
	this.instance_11.setTransform(452,105.1,1,1,0,0,0,336.1,19.3);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(122).to({_off:false},0).to({y:49.3},11,cjs.Ease.get(1)).to({y:45.1},4).to({_off:true},138).wait(306));

	// Layer 2
	this.instance_12 = new lib.Symbol2copy("synched",0);
	this.instance_12.setTransform(527.5,608.6,1,1,0,0,0,527.5,157.6);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(107).to({_off:false},0).to({y:37.6},20,cjs.Ease.get(1)).to({_off:true},259).wait(195));

	// instagram.png
	this.instance_13 = new lib.Symbol3("synched",0);
	this.instance_13.setTransform(-136.6,-44.2,2.43,2.43,0,0,0,53,40);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(28).to({_off:false},0).to({x:93.4,y:-104.2},10,cjs.Ease.get(1)).to({scaleX:1,scaleY:1,x:106.4,y:45},7,cjs.Ease.get(1)).wait(42).to({startPosition:0},0).to({regX:53.1,scaleX:2,scaleY:2,x:96.5,y:-125},11,cjs.Ease.get(1)).to({regX:53,scaleX:1,scaleY:1,x:106.4,y:45},9,cjs.Ease.get(1)).to({_off:true},20).wait(454));

	// 1.Раскрути свой аккаунт в инстаграм самостоятельно!
	this.instance_14 = new lib.Symbol1("synched",0);
	this.instance_14.setTransform(592.3,115.1,1,1,0,0,0,399.4,19.3);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(16).to({_off:false},0).to({y:45.1},12,cjs.Ease.get(1)).to({_off:true},99).wait(454));

	// Layer 10
	this.instance_15 = new lib.Symbol2("synched",0);
	this.instance_15.setTransform(527.5,789.6,1,1,0,0,0,527.5,157.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).to({y:157.6},20,cjs.Ease.get(1)).to({_off:true},107).wait(454));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(120,604,240,630);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;