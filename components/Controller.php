<?php namespace app\components;

use app\models\Users;
use app\models\UserTokens;
use Yii;

class Controller extends \yii\web\Controller
{
	public function beforeAction($action)
	{
		if (!parent::beforeAction($action)) {
			return false;
		}

		if (YII_DEBUG === true && YII_ENV === 'dev') {
			$lang = Yii::$app->request->post(
				'switch_language_for_test',
				Yii::$app->session->get('language_for_test', Yii::$app->language)
			);
			Yii::$app->session->set('language_for_test', $lang);
			Yii::$app->language = $lang;
			if (isset($_POST['switch_language_for_test'])) {
				Yii::$app->user->logout(false);
				Yii::$app->end();
			}
		}

		Yii::setAlias('@img', '@web/img/'.Yii::$app->language);

		if (!Yii::$app->user->isGuest) {
			if (!$user = \Yii::$app->user->identity) {
				$user = Users::findOne(['id' => Yii::$app->user->id]);
			}

			if ($user === null) {
				Yii::$app->user->logout();
				$this->redirect(['/']);
			}
		} else {
			if (isset($_GET['access_key'])) {
				/* @var $token UserTokens */
				$token = UserTokens::findOne(['token' => $_GET['access_key']]);
				if ($token !== null && $token->user !== null) {
					Yii::$app->user->login(Users::findByMail($token->user->mail), 3600 * 24 * 30);
				}
			} else {
				Users::find()->count();
			}
		}

		return true;
	}
}