<!-- 0. Шаблон сообщения -->
--main--

<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Zen-promo</title>
	<style type="text/css">
		#outlook a {padding:0;}
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
		.ExternalClass {width:100%;}
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
		a img {border:none;}
		.image_fix {display:block;}
		p {margin: 0px 0px !important;}

		table td {border-collapse: collapse;}
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
		table[class=full] { width: 100%; clear: both; }

		/*################################################*/
		/*IPAD STYLES*/
		/*################################################*/
		@media only screen and (max-width: 640px) {
			a[href^="tel"], a[href^="sms"] {
				text-decoration: none;
				color: #ffffff; /* or whatever your want */
				pointer-events: none;
				cursor: default;
			}
			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
				text-decoration: default;
				color: #ffffff !important;
				pointer-events: auto;
				cursor: default;
			}
			table[class=devicewidth] {width: 440px!important;text-align:center!important;}
			table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
			table[class="sthide"]{display: none!important;}
			img[class="bigimage"]{width: 420px!important;height:219px!important;}
			img[class="col2img"]{width: 420px!important;height:258px!important;}
			img[class="image-banner"]{width: 440px!important;height:106px!important;}
			td[class="menu"]{text-align:center !important; padding: 0 0 10px 0 !important;}
			td[class="logo"]{padding:10px 0 5px 0!important;margin: 0 auto !important;}
			img[class="logo"]{padding:0!important;margin: 0 auto !important;}

		}
		/*##############################################*/
		/*IPHONE STYLES*/
		/*##############################################*/
		@media only screen and (max-width: 480px) {
			a[href^="tel"], a[href^="sms"] {
				text-decoration: none;
				color: #ffffff; /* or whatever your want */
				pointer-events: none;
				cursor: default;
			}
			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
				text-decoration: default;
				color: #ffffff !important;
				pointer-events: auto;
				cursor: default;
			}
			table[class=devicewidth] {width: 280px!important;text-align:center!important;}
			table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
			table[class="sthide"]{display: none!important;}
			img[class="bigimage"]{width: 260px!important;height:136px!important;}
			img[class="col2img"]{width: 260px!important;height:160px!important;}
			img[class="image-banner"]{width: 280px!important;height:68px!important;}

		}
	</style>

	<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="block">
	<!-- start of header -->
	<table width="100%" background="https://cp.unisender.com/ru/user_file?resource=att&user_id=1123264&name=40970090%2F7_fon.jpg" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
		<tbody>
		<tr>
			<td>
				<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" hlitebg="edit" shadow="edit">
					<tbody>
					<tr>
						<td>
							<!-- logo -->
							<table width="100%" cellpadding="0" cellspacing="0" border="0" align="left" class="devicewidth">
								<tbody>
								<tr>
									<td width="30%" height="40">&nbsp;</td>
									<td width="40%" height="40" class="logo">
										<center><img src="http://f.imgsurl.com/303365/logos.png" alt="&nbsp;" border="0" style="display:block; border:none; outline:none; text-decoration:none; margin-left: auto; margin-right: auto; margin: 30px auto!important;" st-image="edit" class="logo"></center>
									</td>
									<td width="30%">&nbsp;</td>
								</tr>
								</tbody>
							</table>
							<!-- End of logo -->
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
	<!-- end of header -->
</div><div class="block">
	<!-- image + text -->
	<table width="100%" background="https://cp.unisender.com/ru/user_file?resource=att&user_id=1123264&name=40970090%2F7_fon.jpg" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="bigimage">
		<tbody>
		<tr>
			<td>
				<table bgcolor="#ffffff" width="580" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" modulebg="edit">
					<tbody>
					<tr>
						<td width="100%" height="0"></td>
					</tr>
					<tr>
						<td>
							<table width="500" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner">
								<tbody>

								<tr>
									<td width="100%" height="20"></td>
								</tr>

								<tr>
									<td width="100%" height="20"></td>
								</tr>
								<tr>
									<td style="font-family: 'PT Sans', Arial, sans-serif; font-size: 14px; color: #5b5b5b; text-align: left; line-height: 20px;" st-content="rightimage-paragraph">
<span style="font-size:14px;">

	%CONTENT%

</span></td></tr>

								<tr>
									<td style="text-align: center; line-height: 20px;">

										%FOOTER%

									</td>
								</tr>



								</td>
								</tr>
								<!-- end of content -->
								<!-- Spacing -->
								<tr>
									<td width="100%" height="10"></td>

									<!-- Spacing -->
								<tr>
									<td width="100%" height="20"></td>
								</tr>
								<!-- Spacing -->
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
</div>

<div class="block">
	<!-- start of left image -->
	<table width="100%" background="https://cp.unisender.com/ru/user_file?resource=att&user_id=1123264&name=40970090%2F7_fon.jpg" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="leftimage">
		<tbody>
		<tr>
			<td>
				<table bgcolor="#ffffff" width="580" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" modulebg="edit">
					<tbody>
					<!-- Spacing -->
					<tr>
						<td height="20"></td>
					</tr>
					<!-- Spacing -->
					<tr>
						<td>
							<table width="540" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
								<tbody>
								<tr>
									<td>
										<!-- start of text content table -->
										<table width="130" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
											<tbody>
											<!-- image -->
											<tr>
												<td width="135" height="10" align="center" style="font-family: 'PT Sans', Arial, sans-serif; font-size: 14px; color: #5b5b5b; padding-left: 0px; text-align: left;">
													<div style="border-top:1px solid grey; width:135px;"></div>
													Team zen-promo.com<br/>
													<img src="http://f.imgsurl.com/303365/logos2.png">
												</td>
											</tr>
											</tbody>
										</table>
										<!-- mobile spacing -->
										<table align="left" border="0" cellpadding="0" cellspacing="0" class="mobilespacing">
											<tbody>
											<tr>
												<td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
											</tr>
											</tbody>
										</table>
										<!-- mobile spacing -->

									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<!-- Spacing -->
					<tr>
						<td height="20"></td>
					</tr>
					<!-- Spacing -->
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
	<!-- end of left image -->
</div>



<div class="block">
	<!-- Start of preheader -->
	<table width="100%" background="https://cp.unisender.com/ru/user_file?resource=att&user_id=1123264&name=40970090%2F7_fon.jpg" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="postfooter">
		<tbody>
		<tr>
			<td width="100%">
				<table width="580" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
					<tbody>
					<!-- Spacing -->
					<tr>
						<td width="100%" height="5"></td>
					</tr>
					<!-- Spacing -->
					<tr>
						<td align="left" valign="middle" style="font-family: 'PT Sans', Arial, sans-serif; text-align: center; font-size: 13px; color: #5b5b5b; padding: 10px;" st-content="preheader">
							<span style="color:#518ed4;">Click here</span> to update your email preferences or unsubscribe.<br>
							Have questions? We're here to help! <span style="color:#518ed4;">Contact us</span> anytime.
						</td>
					</tr>
					<!-- Spacing -->
					<tr>
						<td width="100%" height="5"></td>
					</tr>
					<!-- Spacing -->
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
	<!-- End of preheader -->
</div>

</body></html>

--SEPARATOR--

<!-- 1. Шаблон напоминания о сервисе -->
--service-remind-3--

Hello!<br/><br/>
A few days ago you signed up Zen-promo - when automatic promotion in Instagram.<br/>
I hope you will appreciate the features of our service, because it will be able to see the results already in the first hours after the start of their project in Zen-promo.<br/>
Add your own projects right now and get 3 days for promotion when Zen-promo free!<br/><br/><br/>


--SEPARATOR--
<!-- 1. Шаблон напоминания о сервисе -->
--service-remind-5--

Hello!<br/><br/>
We noticed that five days ago, you signed up for Zen-promo - service to attract subscribers in Instagram.<br/>
You have to add to your account on the button below.<br/>
And you will be able to appreciate the features of our service, seeing new subscribers in your account already in the first hours after the addition of the project.<br/><br/>
Add your own projects right now and get 3 days for promotion when Zen-promo free!
<br/><br/><br/>


--SEPARATOR--
<!-- 1.1 Шаблон напоминания о сервисе - футер -->
--service-remind-footer--

<a href="https://zen-promo.com?access_key=%ATOKEN%"><img src="http://f.imgsurl.com/303365/button2.png"/></a>



--SEPARATOR--
<!-- 2. Шаблон напоминания окончания тестового периода -->
--test-period-end-5--


Hello!<br/><br/>
Thank you very much for having used our service.<br/>
A few days ago, you have ended the test period.<br/><br/>
I have great news for you:<br/>
Now you can get a lot more new podpischkov in Instagram.<br/>
and if you make a payment within 3 days,<br/>
we give you as a new customer a discount of 10% for the first month.

<br/><br/><br/>


--SEPARATOR--
<!-- 2. Шаблон напоминания окончания тестового периода -->
--test-period-end-10--


Hello!<br/><br/>
Thank you very much for having used our service.
A few days ago, you have ended the test period.<br/><br/>
Remember that you can get a lot more new podpischkov in Instagram.<br/>
If you make a payment within 3 days,
we give you a discount of 20% for the first month.
<br/><br/><br/>


--SEPARATOR--
<!-- 2.1 Шаблон напоминания окончания тестового периода - футер -->
--test-period-end-footer-5--


<a href="https://zen-promo.com/page/price?discount=24g628b992&access_key=%ATOKEN%"><img src="http://f.imgsurl.com/303365/button4.png"/></a>

--SEPARATOR--
<!-- 2.1 Шаблон напоминания окончания тестового периода - футер -->
--test-period-end-footer-10--


<a href="https://zen-promo.com/page/price?discount=vb68uyhnkf45&access_key=%ATOKEN%"><img src="http://f.imgsurl.com/303365/button5.png"/></a>



--SEPARATOR--
<!-- 3. Шаблон напоминания оплаты -->
--no-money-no-honey-20--


Hello!<br/><br/>
We noticed that you are more than 20 days are not getting new subscribers to Zen-promo service.<br/>
You can now add credit and resume your account Instagram.Podpischiki waiting for you!
<br/><br/><br/>


--SEPARATOR--
<!-- 3. Шаблон напоминания оплаты -->
--no-money-no-honey-40--


Hello!<br/><br/>
Thank you for that use our service.<br/>
We've noticed that lately you do not get new followers in Instagram.<br/>
Today, we want to make you a gift - a 10% discount when to effectively promote to Instagram!
<br/><br/><br/>


--SEPARATOR--
<!-- 3.1 Шаблон напоминания оплаты - футер -->
--no-money-no-honey-footer-20--



<a href="https://zen-promo.com/page/price?access_key=%ATOKEN%"><img src="http://f.imgsurl.com/303365/button8.png"/></a>


--SEPARATOR--
<!-- 3.1 Шаблон напоминания оплаты - футер -->
--no-money-no-honey-footer-40--

<a href="https://zen-promo.com/page/price?discount=24g628b992&access_key=%ATOKEN%"><img src="https://web.pechkin-mail.ru/editor/userfiles/303365/button9.png"/></a>


--SEPARATOR--
<!-- 4. Шаблон напоминания об остановленных проектах -->
--no-active-projects-40--



Hello!<br/><br/>
I recall that in Zen-promo when you have a positive balance, and you can now get new subscribers.<br/>
Take advantage of our service and start to work your projects!
<br/><br/><br/>


--SEPARATOR--
<!-- 4. Шаблон напоминания об остановленных проектах -->
--no-active-projects-60--



Hello!<br/><br/>
I remind you that in Zen-promo when you have a positive balance, and you can now get new followers.<br/>
Take advantage of our service and start to work your projects! New followers!

<br/><br/><br/>


--SEPARATOR--
<!-- 4.1 Шаблон напоминания об остановленных проектах - футер -->
--no-active-projects-footer--


<a href="https://zen-promo.com?access_key=%ATOKEN%"><img src="http://f.imgsurl.com/303365/button6.png"/></a>


