<?php namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string  $code
 * @property string  $date_start
 * @property string  $date_end
 * @property integer $size
 */

class Discounts extends ActiveRecord
{
	private $_in_range;

	public static function tableName()
	{
		return 'discounts';
	}

	public function rules()
	{
		return [
			[['code', 'date_start', 'date_end', 'size'], 'required'],
			['code', 'string'],
			[['date_start', 'date_end'], 'date', 'format' => 'yyyy-MM-dd'],
			['size', 'integer', 'min' => 0, 'max' => 100],
			['code', 'unique'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id'         => 'ID',
			'code'       => 'Код активации',
			'date_start' => 'Дата начала',
			'date_end'   => 'Дата окончания',
			'size'       => 'Размер скидки (%)'
		];
	}

	public function checkDate()
	{
		if (is_null($this->_in_range)) {
			$time = time();
			$this->_in_range = (strtotime($this->date_start) < $time && $time < strtotime($this->date_end));
		}
		return $this->_in_range;
	}

	public function calc($val)
	{
		$result = round($val * $this->size / 100);
		return $result > 1 ? $result : 1;
	}
}