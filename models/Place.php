<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "place".
 *
 * @property integer $id
 * @property string  $googleId
 * @property string  $lat
 * @property string  $lng
 * @property integer $radius
 * @property float   $northEastLat
 * @property float   $northEastLng
 * @property float   $southWestLat
 * @property float   $southWestLng
 * @property string  $name
 * @property string  $country
 * @property string  $region
 *
 * @property Cover[] $covers
 */
class Place extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'place';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['googleId', 'name', 'country', 'region'], 'string', 'max' => 255],
			[['lat', 'lng'], 'string', 'max' => 20],
			[['googleId'], 'unique']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'       => 'ID',
			'googleId' => 'Google ID',
			'lat'      => 'Lat',
			'lng'      => 'Lng',
			'radius'   => 'Radius',
			'name'     => 'Name',
		];
	}

	public function generateCovers()
	{

		/*for (var i = northEastLat; i > southWestLat; i = i - 0.04) {
			for(var r = northEastLng; r > southWestLng; r = r - 0.04) {
				console.log('step:' + r);
			}
		}*/

		//обходим квадрат, генерируем точки
		$rows = [];
		for ($i = $this->northEastLat; $i > $this->southWestLat; $i = $i - 0.04) {
			for ($j = $this->northEastLng; $j > $this->southWestLng; $j = $j - 0.04) {
				$lat   = $i;
				$lng   = $j;

				/*$cover = new Cover([
					'place_id' => $this->id,
					'lat' => strval($lat),
					'lng' => strval($lng),
					'sec' => $this->getPhotosTime($lat, $lng)
				]);*/
				$rows[] = [
					'place_id' => $this->id,
					'lat' => strval($lat),
					'lng' => strval($lng),
					'sec' => $this->getPhotosTime($lat, $lng)
				];
				//$cover->save();
			}
		}

		Yii::$app->db->createCommand()->batchInsert(Cover::tableName(), ['place_id', 'lat', 'lng', 'sec'], $rows)->execute();

		/*//вычисляем широту и долготу в километрах
		$maxLat          = 111.3; //const
		$maxLng          = $maxLat * cos(deg2rad($this->lat));
		$zoomCoefficient = 6;
		$mapRadius = floatval($this->radius) / 2500;

		//по соотношению процентов узнаем сколько это будет в градусах
		$currentLatStep = round($zoomCoefficient / $maxLat, 15);
		$currentLngStep = round($zoomCoefficient / $maxLng, 15);

		//считаем сколько у нас отступа от текущей точки для прорисовки границ основного квадрата
		$zoomRadius = round(((2 * $mapRadius) * sqrt(2)) / 2, 15);
		$squareSide = floor((2 * $mapRadius) / $zoomCoefficient);
		//из км в радианы
		$currentLatStepRadians = round($zoomRadius / $maxLat, 15);
		$currentLngStepRadians = round($zoomRadius / $maxLng, 15);

		//идем в верхний левый угол
		$beginPointLat = floatval($this->lat) + $currentLatStepRadians;
		$beginPointLng = floatval($this->lng) - $currentLngStepRadians;

		//обходим квадрат, генерируем точки
		for ($i = 0; $i < $squareSide; $i++) {
			for ($j = 0; $j < $squareSide; $j++) {
				$lat   = $beginPointLat - $currentLatStep * $i;
				$lng   = $beginPointLng + $currentLngStep * $j;
				$cover = new Cover([
					'place_id' => $this->id, 'lat' => strval($lat), 'lng' => strval($lng),
					'sec'      => $this->getPhotosTime($lat, $lng)
				]);
				$cover->save();
			}
		}*/
	}

	public function getPhotosTime($lat, $lng, $radius = 3000)
	{
		return 0;
	}

	/*
	public function afterSave($insert, $changedAttributes)
	{
		if ($insert) {
			$this->generateCovers();
		}
		parent::afterSave($insert, $changedAttributes);
	}
	*/

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCovers()
	{
		return $this->hasMany(Cover::className(), ['place_id' => 'id']);
	}

	public function containsPoint($lat, $lng)
	{
		if ($lat < $this->southWestLat || $lat > $this->northEastLat) {
			return false;
		}
		if ($lng < $this->southWestLng || $lng > $this->northEastLng) {
			return false;
		}
		return true;
	}

	public function toOptionsArray()
	{
		return [
			'id'     => $this->id,
			'name'   => $this->name,
			'radius' => $this->radius,
			'center' => [
				'lat'  => $this->lat,
				'lng'  => $this->lng,
			],
			'sw' => [
				'lat' => $this->southWestLat,
				'lng' => $this->southWestLng,
			],
			'ne' => [
				'lat' => $this->northEastLat,
				'lng' => $this->northEastLng,
			],
		];
	}
}
