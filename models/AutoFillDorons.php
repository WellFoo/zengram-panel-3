<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $account_id
 * @property string  $donor_login
 * @property string  $donor_insta_id
 * @property boolean $complete
 *
 * @property Account $account
 *
 * This is the model class for table "account_competitors".
 */


class AutoFillDorons extends ActiveRecord
{
	public $account;

	public static function tableName()
	{
		return 'auto_fill_dorons';
	}

	public function rules()
	{
		return [
			[['user_id', 'account_id', 'donor_login'], 'required'],
			[['user_id', 'account_id', 'donor_insta_id'], 'number'],
			['donor_login', 'checkLogin'],
		];
	}

	public function checkLogin($attribute, $value)
	{
		if ($this->hasErrors()) {
			return;
		}
		
		/*if ($this->account === null || !$this->account->findUser($this->donor_login)) {
			$this->addError($attribute, Yii::t('app', 'Unable to check account, please try again later'));
			return;
		}*/

		//$result = $this->account->findUserResult;
		$result = \app\components\RPCHelper::findUser($this->account, $this->donor_login);


		if ($result['status'] !== 'ok') {
			$this->addError($attribute, Yii::t('app', 'Unable to check account, please try again later'));
			return;
		}

		if (empty($result['data'])) {
			$this->addError($attribute, Yii::t('app', 'Account @{login} not found', ['login' => $this->donor_login]));
			return;
		}

		$userData = null;

		if (!empty($result['data']['users'][0])){
			$userData = $result['data']['users'][0];
		} else {
			$userData = $result['data'];
		}
		if ($userData['is_private']) {
			$this->addError($attribute, Yii::t('app', 'Account @{login} is private', ['login' => $this->donor_login]));
			return;
		}

		$competitor = self::findOne(['account_id' => $this->account_id, 'donor_insta_id' => $userData['pk']]);
		if (!is_null($competitor)) {
			$this->addError($attribute, Yii::t('app', 'Account @{login} already exists', ['login' => $this->donor_login]));
			return;
		}

		$this->donor_insta_id = $userData['pk'];
	}
}