<?php

namespace app\models;

use app\components\TreeTrait;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "faq_cats".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string  $name
 *
 * @property FAQCats $parent
 * @property FAQCats[] $childrens
 */
class FAQCats extends ActiveRecord
{
	use TreeTrait;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'faq_cats';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['name', 'required'],
			['parent_id', 'integer'],
			['name', 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'parent_id' => Yii::t('app', 'Parent ID'),
			'name' => Yii::t('app', 'Catalog name'),
		];
	}

	public function getParent()
	{
		return self::hasOne(self::className(), ['id' => 'parent_id']);
	}

	public function getChildrens()
	{
		return self::hasMany(self::className(), ['parent_id' => 'id']);
	}
}
