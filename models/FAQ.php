<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "faq".
 *
 * @property integer $id
 * @property integer $cat_id
 * @property string  $question
 * @property string  $answer
 * @property string  $answer_short
 * @property boolean $on_top
 * @property integer $rating
 * @property FAQCats $cat
 */
class FAQ extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'faq';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['cat_id', 'question', 'answer'], 'required'],
			[['cat_id', 'rating'], 'integer'],
			[['rating'], 'default', 'value' => 0],
			[['answer', 'answer_short'], 'string'],
			['on_top', 'boolean'],
			['question', 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'           => Yii::t('app', 'ID'),
			'cat_id'       => Yii::t('app', 'Cat ID'),
			'question'     => Yii::t('app', 'Question'),
			'answer_short' => Yii::t('app', 'Short answer'),
			'answer'       => Yii::t('app', 'Answer'),
			'on_top'       => Yii::t('app', 'On top'),
			'rating'       => Yii::t('app', 'Rating'),
		];
	}

	public function getCat()
	{
		return self::hasOne(FAQCats::className(), ['id' => 'cat_id']);
	}

	public function getRatingSum()
	{
		return intval(FAQRates::find()->where(['faq_id' => $this->id])->sum('rating'));
	}

	public function doRate($rating)
	{
		/** @var FAQRates $model */
		$model = FAQRates::findOne(['faq_id' => $this->id, 'user_id' => Yii::$app->user->id]);
		if (is_null($model)) {
			$model = new FAQRates();
			$model->faq_id = $this->id;
			$model->user_id = Yii::$app->user->id;
		}
		$model->rating = $rating === 'no' ? -1 : 1;
		$model->save();

		$this->rating = $this->getRatingSum();
		$this->save();
	}
}
