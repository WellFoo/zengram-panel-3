<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%payments_history}}".
 *
 * @property integer $id
 * @property integer $date
 * @property string $operation_id
 * @property string $payment_system
 * @property string $service
 * @property double $amount
 * @property string $currency
 */
class PaymentHistoryItem extends ActiveRecord
{
	public static $services = [
		'zengram.ru',
		'zengram.net',
		'fast-unfollow.com',
		'new.fast-unfollow.com',
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%payments_history}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['date', 'operation_id', 'service', 'amount', 'currency', 'payer_email'], 'required'],
			['date', 'integer'],
			[['service', 'currency', 'operation_id'], 'string'],
			['amount', 'number'],
			['payment_system', 'string', 'max' => 20],
			['operation_id', 'string', 'max' => 255],
			['payer_email', 'string', 'max' => 320],
			['operation_id', 'unique'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'             => 'ID',
			'date'           => 'Дата',
			'payment_system' => 'Платежная система',
			'payment_id'     => 'ID платежа',
			'service'        => 'Сайт',
			'amount'         => 'Сумма',
			'currency'       => 'Валюта',
			'operation_id'   => 'ID операции',
		];
	}

	/**
	 * @param $transaction \PayPal\EBLBaseComponents\PaymentTransactionType
	 * @return $this
	 */
	public static function fromPayPalPayment($transaction)
	{
		$item = new self([
			'date'           => strtotime($transaction->PaymentInfo->PaymentDate),
			'payment_system' => 'paypal',
			'operation_id'   => $transaction->PaymentInfo->TransactionID,
			'service'        => self::parseServiceName($transaction->PaymentItemInfo->Memo),
			'amount'         => $transaction->PaymentInfo->GrossAmount->value,
			'currency'       => $transaction->PaymentInfo->GrossAmount->currencyID,
			'payer_email'    => $transaction->PayerInfo->Payer
		]);
		$item->save();
		return $item;
	}

	private static function parseServiceName($string)
	{
		$string = mb_strtolower($string);
		$matches = [];
		mb_eregi('(unfollows)', $string, $matches);
		mb_eregi('(zengram\.(?:ru|net)|(?:new\.)?fast-unfollow\.com)', $string, $matches);

		if (empty($matches)) {
			return 'unknown';
		}

		$site = $matches[1];
		if ($site === 'unfollows') {
			$site = 'fast-unfollow.com';
		}

		return $site;
	}
}
