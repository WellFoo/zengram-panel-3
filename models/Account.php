<?php

namespace app\models;

use app\components\RPCHelper;
use app\components\GearmanClientSingleton;
use app\components\GearManTrait;
use app\components\UHelper;
use core\instagram\exceptions\CheckpointReqiredException;
use core\storage\MemoryStorage;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

/**
 * This is the model class for table "accounts".
 *
 * @property integer $id
 * @property string $instagram_id
 * @property string $login
 * @property string $password Encripted pass
 * @property int $proxy_id
 * @property int $device_id
 * @property string $ip
 * @property int|string user_id
 * @property Users $user
 * @property string $apiKey
 * @property string $server
 * @property string $account_avatar
 * @property string $message
 * @property int $likes
 * @property int $comments
 * @property int $follow
 * @property int $unfollow
 * @property int $password_resets
 * @property int $lastfollows - num follows, when account added
 * @property int $lastfollowers - num followers, when account added
 * @property int $newcomments
 * @property int $posted_photos
 * @property int $manual_comments
 *
 * @property int $account_media
 * @property int $account_follows
 * @property int $account_followers
 * @property int $timer
 * @property int $comment_status
 * @property int $follow_status
 * @property int $unfollow_status
 *
 * @property int $comments_block_date
 * @property int $comments_block_expire
 * @property int $show_follow_block
 * @property int $follow_block_date
 * @property int $follow_block_expire
 * @property int $show_unfollow_block
 * @property int $unfollow_block_date
 * @property int $unfollow_block_expire
 * @property int $pause_until
 * @property bool $is_paused
 * @property bool $use_trusted
 * @property bool $whitelist_added
 * @property bool $is_new
 * @property bool $auth_block
 *
 * @property string $added
 * @property string	$monitoring_status
 *
 * @property Options $options
 * @property Place[] $places
 * @property Regions[] $regions
 * @property AccountHashtags[] $hashtags
 * @property AccountCompetitors[] $competitors
 * @property AccountComments[] $commentsList
 * @property Device $device
 * @property Proxy  $proxy
 * @property bool actionAvaible
 */
class Account extends ActiveRecord
{
	use GearManTrait;

	const DEMO_ID = 'DEMO';
	public $demo = false;
	
	const STATUS_WORK  = 'work';
	const STATUS_PAUSE = 'pause';
	const STATUS_IDLE  = 'idle';

	const UNFOLLOW_STATUS_DAILY_LIMIT  = 1;
	const UNFOLLOW_STATUS_BLOCK        = 2;
	const UNFOLLOW_STATUS_SWITCH_PAUSE = 3;

	const FOLLOW_STATUS_BLOCK        = 1;
	const FOLLOW_STATUS_SWITCH_PAUSE = 2;

	const FOLLOWS_LIMIT = 7500;

	const IMAGE_UPLOAD_COST = 18000; // пять часов

	const MAX_PASSWORD_LENGTH = 60;

	const FOLLOWS_LIMIT_MESSAGE         = 'followsLimitExceeded';
	const FOLLOWS_BLOCK                 = 'followsBlocked';
	const FOLLOWS_SWITCH_PAUSE          = 'followsSwitchPause';
	
	const UNFOLLOWS_LIMIT_MESSAGE       = 'unfollowersLimitExceeded';
	const UNFOLLOWS_DAILY_LIMIT_MESSAGE = 'unfollowsDailyLimitExceeded';
	const UNFOLLOWS_BLOCK               = 'unfollowsBlocked';
	const UNFOLLOWS_SWITCH_PAUSE        = 'unfollowsSwitchPause';
	
	const CHECKPOINT_ERROR              = 'checkpointError';
	const PASSWORD_ERROR                = 'passwordError';
	const EMPTY_GEO                     = 'emptyGeo';
	const BAD_GEO                       = 'badGeo';
	const EMPTY_HASHTAGS                = 'emptyHashtags';
	const EMPTY_COMPETITORS             = 'emptyCompetitors';
	const COMPLETE_COMPETITORS          = 'completeCompetitors';
	const BAD_HASHTAGS                  = 'badHashtags';
	const EMPTY_COMMENTS                = 'emptyComments';
	const SPAM_COMMENTS                 = 'spamComments';
	const GEO_NOT_MEDIA                 = 'geoNotMedia';
	const PROBLEM_REQUEST               = 'problemRequest';

	private $unfollow_pause;
	private $follow_pause;

	public $new_comments;

	public function getMessages() {
		return [
			self::CHECKPOINT_ERROR              => Yii::t('app', 'It is necessary to pass verification.'),
			self::PASSWORD_ERROR                => Yii::t('app', 'Wrong username or password.'),
			self::FOLLOWS_LIMIT_MESSAGE         => Yii::t('app', 'The limit of followings is exceeded. It is necessary to unfollow.'),
			self::UNFOLLOWS_LIMIT_MESSAGE       => Yii::t('app', 'Work of unfollowings is complete'),
			self::EMPTY_GEO                     => Yii::t('app', 'There are no points for work. It is necessary to adjust geography of spread.'),
			self::BAD_GEO                       => Yii::t('app', 'The chosen by you points contain few publications. It is necessary to choose other geopoint, or to expand the promoting area.'),
			self::EMPTY_HASHTAGS                => Yii::t('app', 'There are no hashtags for work. It is necessary to add hashtags.'),
			self::EMPTY_COMPETITORS             => Yii::t('app', 'There are no accounts for work. It is necessary to add accounts.'),
			self::BAD_HASHTAGS                  => Yii::t('app', 'The chosen by you hashtags contain few publications. It is necessary to choose other hashtags.'),
			self::EMPTY_COMMENTS                => Yii::t('app', 'There is no list of comments for additing. It is necessary to add comments, or to disconnect commenting.'),
			self::COMPLETE_COMPETITORS          => Yii::t('app', 'All accounts have been processed. Please add new accounts or select another type of targeting.'),
			self::SPAM_COMMENTS                 => Yii::t('app', 'Your comments are marked as Spam. Please add another comments.'),
			self::PROBLEM_REQUEST               => Yii::t('app', 'Problems of accounts authorization. Please try again later.'),

			self::FOLLOWS_BLOCK               => Yii::t('app', 'At the moment on the action imposed limit of 24 hours by instagram. You can run other actions. This message will disappear when the lock expiring. When activated, they begin follows automatically after {time} hours', [
				'time' => $this->getFollowsPauseExpire()
			]),
			self::FOLLOWS_SWITCH_PAUSE        => Yii::t('app', 'The limit of unfollowings is exceeded. Follow will start automatically after {time} hours.', [
				'time' => $this->getFollowsPauseExpire()
			]),
			
			self::UNFOLLOWS_DAILY_LIMIT_MESSAGE => Yii::t('app', 'Reached daily limit unfollows that was installed by instagramom since {date}. Works will continued automatically after {time} hours', [
				'date' => Yii::$app->formatter->asDate('14-04-2016'),
				'time' => $this->getUnfollowsPauseExpire()
			]),
			self::UNFOLLOWS_BLOCK               => Yii::t('app', 'At the moment on the action imposed limit of 24 hours by instagram. You can run other actions. This message will disappear when the lock expiring. When activated, they begin unfollows automatically after {time} hours', [
				'time' => $this->getUnfollowsPauseExpire()
			]),
			self::UNFOLLOWS_SWITCH_PAUSE        => Yii::t('app', 'The limit of followings is exceeded. Unfollow will start automatically after {time} hours.', [
				'time' => $this->getUnfollowsPauseExpire()
			]),
			self::FOLLOWS_SWITCH_PAUSE          => Yii::t('app', 'Work of unfollowings is complete. Follows will start automatically after {time} hours.', [
				'time' => $this->getFollowsPauseExpire()
			]),
			self::GEO_NOT_MEDIA                 => Yii::t('app', 'In these cities there is no media. Please change the target or add new geo tags.'),
		];
	}

	public function getUnfollowsPauseExpire()
	{
		if (!is_null($this->unfollow_pause)) {
			return $this->unfollow_pause;
		}

		$expire = ($this->unfollow_block_date + $this->unfollow_block_expire * 3600) - time();
		if ($expire < 0) {
			if ($this->message === self::UNFOLLOWS_DAILY_LIMIT_MESSAGE || $this->message === self::UNFOLLOWS_BLOCK) {
				$this->message = '';
			}
			$this->unfollow_status = 0;
			$this->save(false, [
				'message',
				'unfollow_status'
			]);
			$this->unfollow_pause = 0;
		} else {
			$this->unfollow_pause = ceil($expire / 3600);
		}
		return $this->unfollow_pause;
	}

	public function getFollowsStatusMessage($follow_status = null)
	{
		$messages = $this->getMessages();
		
		if (is_null($follow_status)) {
			$follow_status = $this->follow_status;
		}
		
		switch ($follow_status) {
			case self::FOLLOW_STATUS_BLOCK:
				return $messages[self::FOLLOWS_BLOCK];

			case self::FOLLOW_STATUS_SWITCH_PAUSE:
				return $messages[self::FOLLOWS_SWITCH_PAUSE];
		}

		return '';
	}

	public function getUnfollowsStatusMessage()
	{
		$messages = $this->getMessages();
		switch ($this->unfollow_status) {
			case self::UNFOLLOW_STATUS_DAILY_LIMIT:
				return $messages[self::UNFOLLOWS_DAILY_LIMIT_MESSAGE];

			case self::UNFOLLOW_STATUS_BLOCK:
				return $messages[self::UNFOLLOWS_BLOCK];

			case self::UNFOLLOW_STATUS_SWITCH_PAUSE:
				return $messages[self::UNFOLLOWS_SWITCH_PAUSE];
		}

		return '';
	}

	public function getFollowsPauseExpire()
	{
		if (!is_null($this->follow_pause)) {
			return $this->follow_pause;
		}

		$expire = ($this->follow_block_date + $this->follow_block_expire * 3600) - time();
		if ($expire < 0) {
			if ($this->message === self::FOLLOWS_SWITCH_PAUSE) {
				$this->message = '';
			}
			$this->follow_status = 0;
			$this->save(false, [
				'message',
				'follow_status'
			]);
			$this->follow_pause = 0;
		} else {
			$this->follow_pause = ceil($expire / 3600);
		}
		return $this->follow_pause;
	}

/*public function __construct($config = [])
	{
		$this->client = GearmanClientSingleton::getInstance();
		parent::__construct($config);
	}*/

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'accounts';
	}

	/**
	 * Получение статусов для списка insta_id
	 * возвращает массив вида [insta_id => status] или null в случае ошибки
	 *
	 * @param array $list
	 *
	 * @return array|null
	 */
	public static function getStatusesByInstagramIdsList($list)
	{
		if (!is_array($list)) {
			return null;
		}
		$insta_ids = implode(',', $list);
		$result = [];
		foreach (Yii::$app->params['workers'] as $worker) {
			try {
				$curlResponse = Yii::$app->curl->get('http://' . $worker . '/zengramManage.php',
					[
						'action'    => 'status-multiple-insta',
						'insta_ids' => $insta_ids,
					]
				);
			} catch (HttpException $e){
				Yii::error($e->getMessage());
				continue;
			}

			$response = json_decode($curlResponse, true);
			if ($response === false) {
				continue;
			}
			$data = ArrayHelper::getValue($response, 'data', []);
			foreach ($data as $id => $status) {
				$result[$id] = !empty($result[$id]) || $status;
			}
		}

		return $result;
	}

	public static function getActiveAccounts()
	{
		$return = [];
		foreach (Yii::$app->params['workers'] as $worker) {
			try {
				$result = Yii::$app->curl->get('http://' . $worker . '/zengramManage.php', [
					'action' => 'started-projects-insta',
				]);
			} catch (HttpException $e){
				Yii::error($e->getMessage());
				$result = '';
			}

			$response = json_decode($result, true);
			if ($response === false) {
				continue;
			}

			$return = array_merge($return, ArrayHelper::getValue($response, 'data', []));
		}

		return $return;
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['login', 'filter', 'filter' => [self::className(), 'loginFilter']],
			[['login', 'password'], 'required'],
			['login', 'string', 'max' => 50],
			[['id', 'posted_photos', 'manual_comments', 'unfollow_status', 'comment_status', 'follow_status', 'status', 'show_follow_block', 'use_trusted', 'is_new', 'auth_block'], 'integer'],
			[['apiKey', 'message', 'server', 'ip'], 'default', 'value' => ''],
			[['is_new'], 'default', 'value' => 1],
			[['auth_block'], 'default', 'value' => 0],
			[[
				'unfollow_status', 'lastfollows', 'device_id', 'comments_block_date', 'comments_block_expire',
				'posted_photos', 'manual_comments', 'follow_block_date', 'follow_block_expire', 'pause_until', 'test', 'is_paused'
			], 'default', 'value' => 0],
			['login', 'filter', 'filter' => 'trim'],
			['login', 'unique'],
			[['id', 'posted_photos', 'manual_comments'], 'number']
		];
	}

	public static function loginFilter($input)
	{
		$output = preg_replace('/(https|http)?:\/\/(?:www\.)?instagram.com\/?/', '', trim($input));
		return trim($output, '/@\\');
	}

	public static function log($message) {
		Yii::info($message);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'       => Yii::t('app', 'ID'),
			'login'    => Yii::t('app', 'Login'),
			'password' => Yii::t('app', 'Password'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser()
	{
		return $this->hasOne(Users::className(), ['id' => 'user_id']);
	}

	public function getActionAvaible(){
		return ActionsLog::find()->where(['and', ['instagram_id' => $this->instagram_id, 'deleted' => 0], ['>=', 'added',
			new Expression("(NOW() - '15 day'::interval)")]])->count() === 0;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOptions()
	{
		return $this->hasOne(Options::className(), ['account_id' => 'id']);
	}

	public function getHashtags()
	{
		return $this->hasMany(AccountHashtags::className(), ['account_id' => 'id']);
	}

	public function getCompetitors()
	{
		return $this->hasMany(AccountCompetitors::className(), ['account_id' => 'id']);
	}

	public function getCompetitorsCount()
	{
		return intval($this->hasMany(AccountCompetitors::className(), ['account_id' => 'id'])->count());
	}

	public function getCommentsList()
	{
		return $this->hasMany(AccountComments::className(), ['account_id' => 'id']);
	}

	public function hasFirmComments()
	{
		return $this->hasMany(AccountComments::className(), ['account_id' => 'id'])->andWhere(['is_firm' => '1'])->count() > 0;
	}

	public function fillFirmComments()
	{
		Yii::$app->db->createCommand(sprintf(
			'INSERT INTO %s (account_id, text, is_firm) SELECT %d, text, 1 FROM %s',
			AccountComments::tableName(),
			$this->id,
			'commentsbase'
		))->execute();
	}

	public function clearFirmComments()
	{
		Yii::$app->db->createCommand(sprintf(
			'DELETE FROM %s WHERE account_id = %d AND is_firm = 1',
			AccountComments::tableName(),
			$this->id
		))->execute();
	}

	public function getRegions()
	{
		return $this->hasMany(Regions::className(), ['id' => 'region_id'])
			->viaTable('account_regions', ['account_id' => 'id']);
	}

	public function getPlaces()
	{
		return $this->hasMany(Place::className(), ['id' => 'place_id'])
			->viaTable('account_places', ['account_id' => 'id']);
	}

	public function getDevice()
	{
		return $this->hasOne(Device::className(), ['id' => 'device_id']);
	}
	
	public function getDeviceData() {
		if (!empty($this->device_id)) {
			return Device::findOne($this->device_id)->toArray();
		} else {
			$device = Device::getRandom();

			$this->device_id = $device['id'];
			$this->save();

			return $device;
		}
	}

	public function getProxy()
	{
		return $this->hasOne(Proxy::className(), ['id' => 'proxy_id']);
	}

	public function getProxyData() 
	{
		if (!empty($this->proxy_id)) {
			return Proxy::findOne($this->proxy_id)->toArray();
		} else {
			$proxy = Proxy::getRandom();

			$this->proxy_id = $proxy['id'];
			$this->save();

			return $proxy;
		}
	}

	public function getProxyDataString()
	{
		return $this->getProxyData()['proxy'];
	}

	public function getRegionsID()
	{
		$result = (new Query())
			->select('*')
			->from('account_regions')
			->where(['account_id' => $this->id])
			->all();
		return ArrayHelper::getColumn($result, 'region_id');
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if ($insert){
				$this->password = call_user_func(MCRYPT_ENCODE, $this->password);
			} elseif (strlen($this->password) <= self::MAX_PASSWORD_LENGTH) {
				$this->password = call_user_func(MCRYPT_ENCODE, $this->password);
			}
			return true;
		}
		return false;
	}

	private function retrieveUserInfo($param, $default)
	{
		if (Yii::$app->getModule('user') === null) {
			return $default;
		}
		return Yii::$app->user->$param;
	}

	public function pauseProject()
	{

	}

	public function accountStart($admin = false)
	{
		if (!$this->options->unfollow) {
			if (($this->options->isSearchByPlaces() && $this->getPlacesCount() === 0) ||
				($this->options->isSearchByRegions() && $this->getRegionsCount() === 0)
			) {
				$this->message = self::EMPTY_GEO;
				$this->save();
				return $this->message;
			}

			if ($this->options->isSearchByHashtags() &&
				$this->getHashtagsCount() === 0
			) {
				$this->message = self::EMPTY_HASHTAGS;
				$this->save();
				return $this->message;
			}

			if ($this->options->isSearchByCompetitors() &&
				$this->getIncompleteCompetitorsCount() === 0
			) {
				$this->message = self::EMPTY_COMPETITORS;
				$this->save();
				return $this->message;
			}
		}

		if ($this->options->comment && $this->getCommentsCount() === 0) {
			$this->message = self::EMPTY_COMMENTS;
			$this->save();
			return $this->message;
		}

		/** @var Users $user */
		$user = Users::findIdentity($this->user_id);

		if (!$admin) {
			if (!$user->is_payed) {
				/** @var Demo $demo */
				$demo = Demo::find()->where(
					['login' => $this->instagram_id])->one();
				if ($demo === null) {
					$demo = new Demo(['user_id' => $user->id, 'login' => $this->instagram_id]);
					$demo->save();
				} else {
					if ($demo->user_id !== $user->id) {
						$demo->tryed = new Expression('CURRENT_TIMESTAMP()');
						$demo->save();
						return Yii::t('app', 'Free works for account {login} already done', ['login' => $this->login]);
					}
				}
			}
		}
		return $this->addTask('addProccess', [
			'admin'     => $admin,
			'sameMade'  => $user->sameMade,
			'sameBlack' => $user->sameBlack,
			'exactGeo'  => $user->exactGeo,
			'antispam' => $user->antispam,
			'unModeMulti' => $user->unfollow_mode_multiactions,
			'followForPrivate' => $user->follow_for_private
		]);
	}

	public function getPlacesCount()
	{
		return intval(
			(new Query())
				->from('account_places')
				->where(['account_id' => $this->id])
				->count()
		);
	}

	public function getRegionsCount()
	{
		return intval(
			(new Query())
				->from('account_regions')
				->where(['account_id' => $this->id])
				->count()
		);
	}

	public function getHashtagsCount()
	{
		return intval($this->hasMany(AccountHashtags::className(), ['account_id' => 'id'])->count());
	}

	public function getIncompleteCompetitorsCount()
	{
		return intval($this->hasMany(AccountCompetitors::className(), ['account_id' => 'id'])->where(['complete' => '0'])->count());
	}

	public function getCommentsCount()
	{
		return intval($this->hasMany(AccountComments::className(), ['account_id' => 'id'])->count());
	}

	public function accountStop($admin = false)
	{
		/** @var Users $user */
		$user = Users::findOne(['id' => $this->user_id]);
		if ($user->balance_flow > 60) {
			$bf = new BalanceFlow([
				'user_id'     => $user->id,
				'value'       => $user->balance_flow*60,
				'description' => 'Автоматическое снятие по планировщику после остановки. Пользователь '.$this->login,
				'type'        => 'sub'
			]);
			$bf->save();
		}
		$user->balance_flow = 0;
		$user->save();

		return $this->addTask('killProccess_' . ($this->server ? $this->server : Yii::$app->params['serverIP']), [
			'user_id' => $this->instagram_id,
			'admin'   => $admin,
		]) ? 'ok' : 'error';
	}

	public static function getTimerValues()
	{
		return [
			-1 => Yii::t('views', 'Off'),
			1  * 3600  => Yii::t('views', '1 hour'),
			2  * 3600  => Yii::t('views', '2 hours'),
			3  * 3600  => Yii::t('views', '3 hours'),
			4  * 3600  => Yii::t('views', '4 hours'),
			5  * 3600  => Yii::t('views', '5 hours'),
			6  * 3600  => Yii::t('views', '6 hours'),
			7  * 3600  => Yii::t('views', '7 hours'),
			8  * 3600  => Yii::t('views', '8 hours'),
			9  * 3600  => Yii::t('views', '9 hours'),
			10 * 3600  => Yii::t('views', '10 hours'),
			11 * 3600  => Yii::t('views', '11 hours'),
			12 * 3600  => Yii::t('views', '12 hours'),
			13 * 3600  => Yii::t('views', '13 hours'),
			14 * 3600  => Yii::t('views', '14 hours'),
			15 * 3600  => Yii::t('views', '15 hours'),
			16 * 3600  => Yii::t('views', '16 hours'),
			17 * 3600  => Yii::t('views', '17 hours'),
			18 * 3600  => Yii::t('views', '18 hours'),
			19 * 3600  => Yii::t('views', '19 hours'),
			20 * 3600  => Yii::t('views', '20 hours'),
			21 * 3600  => Yii::t('views', '21 hours'),
			22 * 3600  => Yii::t('views', '22 hours'),
			23 * 3600  => Yii::t('views', '23 hours'),
			1  * 86400 => Yii::t('views', '1 day'),
			2  * 86400 => Yii::t('views', '2 days'),
			3  * 86400 => Yii::t('views', '3 days'),
			5  * 86400 => Yii::t('views', '5 days'),
			10 * 86400 => Yii::t('views', '10 days'),
			20 * 86400 => Yii::t('views', '20 days'),
			30 * 86400 => Yii::t('views', '30 days'),
			60 * 86400 => Yii::t('views', '60 days'),
		];
	}

	public function getTimer($interval = 60)
	{
		if ($this->timer == -86400) {
			$timer = 86400;
		} else {
			$this->timer -= $interval;
			$timer = $this->timer;
		}
		$this->save();

		return $timer;
	}

	public function resetTimer()
	{
		$this->timer = -86400;
		$this->save();
	}

	public function Options()
	{
		//$options = Options::find()->where(['account_id' => $this->id])->one();
		return $this->options;
	}

	public function add($user_id = null)
	{
		if ($this->validate()) {
			if ($user_id === null){
				$user = Yii::$app->user->identity;
			} else {
				$user = Users::findOne(['id' => $user_id]);
			}
			/** @var Users $user */
			if ($user->is_service){
				return $this->checkCredentials(false, $user_id);
			} else {
				return $this->checkLogin(false, $user_id);
			}
		} else {
			return false;
		}
	}

	public function checkLogin($checkOnly = false, $user_id = null)
	{
		return $this->checkCredentials($checkOnly, $user_id);
	}

	public function updateStats()
	{
		return $this->updateStatsPrivateApi();
	}

	public function generateHttps($account_id, $image)
	{
		if(strpos($image, 'https') === 0){
			return $image;
		}
		if (preg_match('#http://(.*)?cdninstagram\.com.*#ui', $image, $found)){
			return str_replace('http', 'https', $image);
		}
		$newfile = '/userdata/images/' . $account_id . '.jpg';
		if (copy($image, Yii::getAlias('@app') . $newfile)) {
			return $newfile;
		} else {
			return $image;
		}
	}

	public function createOptions() {
		$options = new Options();

		$options->account_id = $this->id;
		$options->skip_popular_update = $this->user->skip_popular_update;
		$options->comment_popular = $this->user->comment_popular;

		$options->save();
	}

	public function revision() {
		$self = new self();
		$self->login = $this->login;
		$self->password = $this->password;

		return $self->getPostInfo('revision', $this->user_id);
	}

	public function checkCredentials($checkOnly = false, $user_id = null)
	{
		\Yii::error('Не получен getPostInfo от пользователя '. Yii::$app->user->id);
		$result = $this->getPostInfo('password', $user_id);


		\Yii::info($result);


		if (empty($result)) {
//			Yii::$app->session->setFlash('AccountLoginError', Yii::t('app', 'Unable to login to Instagram <label>{login}</label>. Please, try again later.', ['login' => $this->login]));
			$this->addError('AccountLoginError', Yii::t('app', 'Unable to login to Instagram. Please, try again later.'));
			return false;
		}

		if (!empty($result['error']) && $result['error'] != 'Sorry, there was a problem with your request.') {
			if ($result['error'] == 'Update your app to log in with two-factor authentication.'){
				$tw = new Twofactor(['login' => $this->login, 'password' => $this->password]);
				$tw->save();
			}
			if ($result['error'] == 'checkpoint_required' || (!empty($result['exception']['text']) && $result['exception']['text'] == 'checkpoint_required')){
				Yii::$app->session->setFlash('AccountCheckpoint', true);
				Yii::$app->session->setFlash('AccountLogin', $this->login);
				$this->addError('AccountLogin', Yii::t('app', 'You need to pass verification'));
				$this->addError('error', Yii::t('app', 'checkpoint_required'));
				/** @var UsersInfo $modelInfo */
				$modelInfo = UsersInfo::findOne(['user_id' => Yii::$app->user->id]);
				if ($modelInfo === null) {
					$modelInfo = new UsersInfo(['user_id' => Yii::$app->user->id]);
				}
				$modelInfo->was_checkpoint = 1;
				$modelInfo->save();
				return false;
			} else {
				//TODO: доделать обработку ошибок при неверном пароле
				Yii::$app->session->setFlash('AccountIncorrect', $result['error']);
//				$this->addError('AccountIncorrect', $result['error']);

				if (stripos($result['error'], 'incorrect') !== false) {
					$this->addError('password', Yii::t('app', 'Incorrect login or password'));
				} else {
					$this->addError('password', Yii::t('app', $result['error']));
				}
				if (strpos($result['error'], 'password') !== false) {
					/** @var UsersInfo $modelInfo */
					$modelInfo = UsersInfo::findOne(['user_id' => Yii::$app->user->id]);
					if ($modelInfo === null) {
						$modelInfo = new UsersInfo(['user_id' => Yii::$app->user->id]);
					}
					$modelInfo->was_wrongpass = 1;
					$modelInfo->save();
					return false;
				}
				return false;
			}
		}



		if (!empty($result['device_id'])){
			$this->device_id = $result['device_id'];
		}

		$account = $result['user'];

		/** @var Users $user */
		$user = !is_null($user_id) ? Users::findOne($user_id) : Yii::$app->user->identity;

		if ($account['is_private'] && !$user->comment_popular) {
			$this->addError('AccountLoginError', Yii::t('app', 'Unable to make account <label>{login}</label> public.', ['login' => $this->login]));
			return false;
		}

		$same = self::findOne(['instagram_id' => $account['pk']]);
		if ($same !== null && $same->id != $this->id) {
			$this->addError('login', Yii::t('app', 'Account with same Instagram ID already exists'));
			return false;
		}

		if ($checkOnly) {
			if ($this->message == 'passwordError') {
				$this->message = '';
			}
			return true;
		}

		$this->instagram_id = $account['pk'];
		//$this->server = Yii::$app->params['serverIP'];
		$this->server = RPCHelper::getIdleServer();
		if (empty($this->server)){
			$this->server = Yii::$app->params['serverIP'];
		}
		$this->user_id = $user_id !== null ? $user_id : Yii::$app->user->id;
		$this->save();
		$options = new Options();
		$options->account_id = $this->id;
		$options->skip_popular_update = $user->skip_popular_update;
		$options->comment_popular = $user->comment_popular;
		$options->save();

		$this->updateStatsPrivateApi($result);

		$this->writeStartLog();

		return true;
	}


//	public function checkCredentials($checkOnly = false, $user_id = null)
//	{
//		if (is_null($user_id)) {
//			$user_id = $this->user_id;
//		}
//		$this->server = RPCHelper::getIdleServer();
//		$result = $this->getPostInfo('password', $user_id);
//		if (empty($result)) {
////			Yii::$app->session->setFlash('AccountLoginError', Yii::t('core', 'Unable to login to Instagram <label>{login}</label>. Please, try again later.', ['login' => $this->login]));
//			$this->addError('AccountLoginError', Yii::t('core', 'Unable to login to Instagram. Please, try again later.'));
//			return false;
//		}
//		if (!empty($result['error'])){
//			if ($result['error'] == 'Update your app to log in with two-factor authentication.'){
//				$tw = new Twofactor(['login' => $this->login, 'password' => $this->password]);
//				$tw->save();
//			}
//			if ($result['error'] == 'checkpoint_required' || (!empty($result['exception']['text']) && $result['exception']['text'] == 'checkpoint_required')){
//				Yii::$app->session->setFlash('AccountCheckpoint', true);
//				Yii::$app->session->setFlash('AccountLogin', $this->login);
//				$this->addError('AccountLogin', Yii::t('core', 'You need to pass verification'));
//				$this->addError('error', Yii::t('core', 'checkpoint_required'));
//				/** @var UsersInfo $modelInfo */
//				$modelInfo = UsersInfo::findOne(['user_id' => Yii::$app->user->id]);
//				if ($modelInfo === null) {
//					$modelInfo = new UsersInfo(['user_id' => Yii::$app->user->id]);
//				}
//				$modelInfo->was_checkpoint = 1;
//				$modelInfo->save();
//				return false;
//			} else {
//				if (stripos($result['error'], 'incorrect') !== false) {
//					$this->addError('password', Yii::t('core', 'Incorrect login or password'));
//				} else {
//					$this->addError('password', Yii::t('core', $result['error']));
//				}
//				if (strpos($result['error'], 'password') !== false) {
//					/** @var UsersInfo $modelInfo */
//					$modelInfo = UsersInfo::findOne(['user_id' => Yii::$app->user->id]);
//					if ($modelInfo === null) {
//						$modelInfo = new UsersInfo(['user_id' => Yii::$app->user->id]);
//					}
//					$modelInfo->was_wrongpass = 1;
//					$modelInfo->save();
//				}
//				return false;
//			}
//		}
//
//		if (!empty($result['device_id'])){
//			$this->device_id = $result['device_id'];
//		}
//
//		$account = $result['user'];
//
//		/** @var Users $user */
//		$user = Users::findOne($user_id);
//
//		if ($account['is_private'] && !$user->comment_popular) {
//			$this->addError('AccountLoginError', Yii::t('core', 'Unable to make account <label>{login}</label> public.', ['login' => $this->login]));
//			return false;
//		}
//
//		$same = self::findOne(['instagram_id' => $account['pk']]);
//		if ($same !== null && $same->id != $this->id) {
//			$this->addError('login', Yii::t('core', 'Account with same Instagram ID already exists'));
//			return false;
//		}
//
//		if ($checkOnly) {
//			return true;
//		}
//
//		$this->instagram_id = $account['pk'];
//		$this->server = RPCHelper::getIdleServer();
//		$this->save();
//		$options = new Options();
//		$options->account_id = $this->id;
//		$options->skip_popular_update = $user->skip_popular_update;
//		$options->comment_popular = $user->comment_popular;
//		$options->save();
//
//		$this->updateStatsPrivateApi($result);
//
//		$this->writeStartLog();
//
//		return true;
//	}



	public function updateStatsPrivateApi($result = null)
	{
		if (is_null($result)) {
			$result = $this->getPostInfo('info', $this->user_id);
		}
		if (empty($result['user'])){
			return false;
		}
		return $this->updateStatsFromResult($result['user']);
	}
	
	public function updateStatsFromResult($fields){

		$this->account_media = $fields['media_count'];
		$this->account_follows = $fields['following_count'];
		$this->account_followers = $fields['follower_count'];
		$this->account_avatar = $this->generateHttps($fields['pk'], $fields['profile_pic_url']);

		if ($this->lastfollows == null && $this->lastfollowers == null) {
			$this->lastfollows = $this->account_follows;
			$this->lastfollowers = $this->account_followers;
		}
		if (!$this->save()) {
			return false;
		}
		return true;
	}

	public function getProxyByUserId($user_id = null){

		if (empty($this->proxy_id)) {
			$continent = null;
			$proxy_cat = null;
			if (!empty(Yii::$app->params['use_continents'])) {
				if (!empty(Yii::$app->user->identity->continent)) {
					$continent = Yii::$app->user->identity->continent;
					$missingCodes = [
						'AF' => 'EU',
						'AN' => 'EU',
						'OC' => 'EU',
						'AS' => 'EU',
						'SA' => 'NA',
					];
					if (array_key_exists($continent, $missingCodes)) {
						$continent = $missingCodes[$continent];
					}
				} else {
					$continent = 'EU';
				}
			} else {
				if ($user_id === null) {
					$proxy_cat = Yii::$app->user->identity->proxy_cat_id;
					$log_user = Yii::$app->user->id;
				} else {
					/** @var Users $user */
					$user = Users::findOne($user_id);
					if ($user !== null) {
						$proxy_cat = $user->proxy_cat_id;
						unset($user);
					} else {
						$proxy_cat = null;
					}
					$log_user = $user_id;
				}
				if (!empty($log_user)) {
					$logEntry = new UsersLog(['user_id' => $log_user, 'text' => 'Попытка добавить аккаунт ' . $this->login]);
					/** @var UsersInfo $modelInfo */
					$modelInfo = UsersInfo::findOne(['user_id' => $log_user]);
					if ($modelInfo === null) {
						$modelInfo = new UsersInfo(['user_id' => $log_user]);
					}
					$modelInfo->try_add = 1;
					$modelInfo->save();
					$logEntry->save();
				}
			}
			$freeProxy = Proxy::getFree($proxy_cat, $continent);
			

			$this->proxy_id = $freeProxy->id;
			return $freeProxy->proxy;
		}
		return $this->getProxyString($this->proxy_id);
	}

	public function getPostInfoFields($user_id = null, $additional_params = [])
	{
		if (empty($this->ip)) {
			if ($ip = $this->generateIp($user_id)){
				$this->ip = $ip;
				if ($this->id) {
					$this->save();
				}
			} else {
				$this->ip = sprintf('192.168.%d.%d', rand(0, 255), rand(1, 254));
			}
		}

		$password = $this->password;

		if (strlen($this->password) >= self::MAX_PASSWORD_LENGTH) {
			$password = UHelper::unpack($this->password);
		}

		$data = [
			'login'      => $this->login,
			'password'   => $password,
			'insta_id'   => $this->instagram_id,
			'device_id'  => $this->device_id,
			'ip'         => $this->ip,
			'proxy'      => $this->getProxyByUserId($user_id)
		];
		if (!empty($additional_params)){
			$data = array_merge($data, $additional_params);
		}
		return $data;
	}

	public function getPostInfo($action, $user_id = null, $additional_params = [])
	{
		$curl = clone Yii::$app->curl;
		$curl->options['timeout'] = 600;

		$data = $this->getPostInfoFields($user_id, $additional_params);
		$test = print_r($data, true);

		//file_put_contents('geotest.txt', 'Наши данные' . $test);

		// Пишем информацию о переменных в логи
		//$file = fopen('geotest.txt', 'a');
		//$log = 'Просто проврека';
		//fwrite($file, $log);
		//fclose($file);

		Yii::info('http://' . $this->server . '/zengramInfo.php?action='.$action);
		Yii::info($test);


		// >=- TODO костыль
		if (empty($this->server)) {
			foreach (Yii::$app->params['workers'] as $worker){
				$this->server = $worker;
			}
		}

		$result = $curl->post('http://' . $this->server . '/zengramInfo.php?action='.$action, $data);

		preg_match("#{.*}#", $result, $matches);
		if ($matches) {
			$result = $matches[0];
		}
		return json_decode($result, true);
	}
	
	public function generateIp($user_id = null){
		/** @var Users $user */
		$user = null;
		if (!is_null($user_id)) {
			$user = Users::findOne($user_id);
		}
		if (is_null($user)) {
			if (is_null($this->user)) {
				$user = $this->getUser()->one();
			} else {
				$user = $this->user;
			}
		}
		if (is_null($user)) {
			$user = $this->retrieveUserInfo('identity', null);
		}
		if (is_null($user)) {
			if (php_sapi_name() !== 'cli') {
				return Yii::$app->request->getUserIP();
			}
		} else {
			$current_ip = $user->registerIP;

			$parts = explode('.', $current_ip);
			$parts[3] = rand(1, 255);

			return implode('.', $parts);
		}
		return false;
	}

	public function getStatus()
	{
		if (!$this->instagram_id) {
			return false;
		}
		$urls = [];
		if (!empty($this->server)) {
			$urls[] = 'http://'.$this->server.'/zengramManage.php';
		} else {
			foreach (Yii::$app->params['workers'] as $worker){
				$urls[] = 'http://'.$worker.'/zengramManage.php';
			}
		}
		$active = false;
		foreach ($urls as $url) {
			try {
				$result = Yii::$app->curl->get($url,
					[
						'action'   => 'status-insta',
						'insta_id' => $this->instagram_id,
					]);
			} catch (HttpException $e){
				Yii::error($e->getMessage());
				continue;
			}
			$active = $active || $result == 'true';
		}
		return $active;
	}

	public function getFreeProxyId()
	{
		$started = self::getActiveAccounts();
		$andWhere = ($started) ? 'AND a.instagram_id IN ('.implode(',', $started).') ' : ' AND false';
		$where = 'WHERE p.status=\'ok\'';
		$sql = "SELECT p.*, count(a.id) AS c
		    FROM proxy AS p 
		    LEFT JOIN accounts AS a 
		    ON a.proxy_id = p.id ".$andWhere . $where ."
		    GROUP BY p.id 
		    ORDER BY c ASC LIMIT 1";

		$row = Yii::$app->db->createCommand($sql)->queryOne();
		//var_dump($row);
		return $row['id'];
	}

	/**
	 * Проверка на привышение лимита подписок
	 *
	 * @return boolean
	 */
	public function isFollowsLimitExceeded()
	{
		if ($this->account_follows < self::FOLLOWS_LIMIT) {
			return false;
		}
		if ($this->options->unfollow
			|| $this->options->comment
			|| !($this->options->follow && $this->options->likes)
		) {
			return false;
		}
		return true;
	}

	/**
	 * Возвращает кол-во новых фоловеров
	 *
	 * @return int
	 */
	public function getLastFollowers()
	{
		$makeFollowers = $this->account_followers - $this->lastfollowers;
		if ($makeFollowers < 0) {
			$makeFollowers = 0;
		}
		return $makeFollowers;
	}

	public function getTimerText()
	{
		return self::getStaticTimerText($this->timer);
	}

	public static function getStaticTimerText($timer, $full = false)
	{
		if ($timer <= 0) {
			return Yii::t('views', 'Off');
		} else {
			if ($timer < 86400) {
				$hours = ceil($timer / 3600);
				return \Yii::t('app', '{n, plural, =0{ # hours} one{ # hour} few{ # hours} many{ # hours} other{ # hours}}', ['n' => $hours]);
			} else {
				$days = ceil($timer / 86400);
				$hours = ceil(fmod($timer, 86400) / 3600);
				if ($hours) {
					if ($full){
						return \Yii::t('app', '{n, plural, =0{ # days} one{ # day} few{ # days} many{ # days} other{ # days}}', ['n' => $days]).
						' '.
						\Yii::t('app', '{n, plural, =0{ # hours} one{ # hour} few{ # hours} many{ # hours} other{ # hours}}', ['n' => $hours]);
					} else {
						return \Yii::t('app','{days}d. {hours}h.', ['days' => $days, 'hours' => $hours]);
					}
				} else {
					return \Yii::t('app', '{n, plural, =0{ # days} one{ # day} few{ # days} many{ # days} other{ # days}}', ['n' => $days]);
				}
			}
		}
	}

	public function getErrorMessage()
	{
		if (!$this->message) {
			return '';
		}
		$messages = $this->getMessages();
		if (!array_key_exists($this->message, $messages)) {
			return $this->message;
		} else {
			return $messages[$this->message];
		}
	}

	public function getRelationsCount($type)
	{
		return self::getStaticRelationsCount($this->id, $type);
	}

	public static function getStaticRelationsCount($id, $type)
	{
		$count = (new Query())
			->from('{{relations}}')
			->where(['account_id' => $id, 'type' => $type])
			->count();
		return intval($count);
	}


	public function getNewComments()
	{
		return self::countNewComments($this->id);
	}

	public static function countNewComments($id)
	{
		$row = Yii::$app->db->createCommand('SELECT sum(new_comments) AS sum FROM media_count WHERE account_id=' . $id)->queryOne();
		//var_dump($row);
		return $row['sum'] ? $row['sum'] : '0';
	}

	public function clearNewComments()
	{
		self::unmarkNewComments($this->id);
	}

	public static function unmarkNewComments($id)
	{
		Yii::$app->db->createCommand("UPDATE media_count SET new_comments = 0 WHERE account_id=" . $id)->execute();
	}

	public static function ipInfo($purpose = "location", $deep_detect = true, $ip = null)
	{
		$output = null;
		if (filter_var($ip, FILTER_VALIDATE_IP) === false) {
			$ip = $_SERVER["REMOTE_ADDR"];
			if ($deep_detect) {
				if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)) {
					$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				}
				if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)) {
					$ip = $_SERVER['HTTP_CLIENT_IP'];
				}
			}
		}
		$purpose    = str_replace(["name", "\n", "\t", " ", "-", "_"], null, strtolower(trim($purpose)));
		$support    = ["country", "countrycode", "state", "region", "city", "location", "address", "continentcode"];
		$continents = [
			"AF" => "Africa",
			"AN" => "Antarctica",
			"AS" => "Asia",
			"EU" => "Europe",
			"OC" => "Australia (Oceania)",
			"NA" => "North America",
			"SA" => "South America",
		];
		if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
			$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
			if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
				switch ($purpose) {
					case "location":
						$output = [
							"city"           => @$ipdat->geoplugin_city,
							"state"          => @$ipdat->geoplugin_regionName,
							"country"        => @$ipdat->geoplugin_countryName,
							"country_code"   => @$ipdat->geoplugin_countryCode,
							"continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
							"continent_code" => @$ipdat->geoplugin_continentCode,
						];
						break;
					case "address":
						$address = [$ipdat->geoplugin_countryName];
						if (@strlen($ipdat->geoplugin_regionName) >= 1) {
							$address[] = $ipdat->geoplugin_regionName;
						}
						if (@strlen($ipdat->geoplugin_city) >= 1) {
							$address[] = $ipdat->geoplugin_city;
						}
						$output = implode(", ", array_reverse($address));
						break;
					case 'continentcode':
						$output = @$ipdat->geoplugin_continentCode;
						break;
					case "city":
						$output = @$ipdat->geoplugin_city;
						break;
					case "state":
						$output = @$ipdat->geoplugin_regionName;
						break;
					case "region":
						$output = @$ipdat->geoplugin_regionName;
						break;
					case "country":
						$output = @$ipdat->geoplugin_countryName;
						break;
					case "countrycode":
						$output = @$ipdat->geoplugin_countryCode;
						break;
				}
			}
		}
		return $output;
	}

	public function fillWhitelist()
	{
		$follows = $this->getAllFollows();

		if (!is_array($follows)) {
			return false;
		}

		if (count($follows) == 0) {
			return false;
		}

		$whitelist = [];
		foreach ($follows as $follow => $login) {
			$whitelist[] = [
				(string) $this->instagram_id,
				(string) $follow,
				$login
			];
		}

		Yii::$app->db->createCommand()->batchInsert(
			Whitelist::tableName(), [
				'account_id',
				'white_id',
				'login'
			], $whitelist
		)->execute();

		return true;
	}

	public function getFollows($max_id = null)
	{
		if (empty($max_id)) {
			$result = $this->getPostInfo('follows', $this->user_id);
		} else {
			$result = $this->getPostInfo('follows', $this->user_id, [
				'max_id' => $max_id
			]);
		}

		return !empty($result['data']) ? $result['data'] : false;
	}

	public function getAllFollows($list = false)
	{
		$follows = [];
		$max_id = null;

		do {
			if (empty($max_id)) {
				$result = $this->getPostInfo('follows', $this->user_id);
			} else {
				$result = $this->getPostInfo('follows', $this->user_id, [
					'max_id' => $max_id
				]);
			}

			$max_id = $result['max_id'];

			if (!is_array($result['data'])) {
				break;
			}

			if ($list) {
				$follows = array_merge($follows, array_keys($result['data']));
			} else {
				$follows = $follows + $result['data'];
			}
		} while (!empty($result['max_id']));

		return $follows;
	}

	public function getTopUnfollows($count, $mutual)
	{
		$result = $this->getPostInfo('unfollows', $this->user_id, [
			'count' => $count,
			'mutual' => $mutual,
			'account' => $this->id,
			'user' => $this->user_id
		]);

		if (!is_array($result['data'])) {
			return [];
		}
				
		return $result['data'];
	}

	private function writeStartLog()
	{
		$result = $this->getPostInfo('info', $this->user_id);
		if (empty($result)){
			\Yii::error('Не получен getPostInfo от пользователя '. $this->user_id);
			return;
		}
		$accountLog = new AccountsLog([
			'instagram_id' => $this->instagram_id,
			'clientmail' => Yii::$app->user->identity->mail,
			'datetime' => date("Y-m-d H:i:s"),
			'data' => json_encode($result),
		]);

		$accountLog->save();
	}
	public function checkParams($options = null)
	{
		if ($options === null){
			$options = $this->options;
		}
		if ($options->use_actions) {
			if (!$options->unfollow && $this->checkIncorrectGeoParam() && !$this->geo_adjusted) {
				$place = Yii::$app->request->isPost ? Yii::$app->request->post('place', '[]') : Yii::$app->request->get('place', '[]');
//				$place = json_decode($place_json, true);

				if (!empty($place['place'])) {
					OptionsController::placeCreate($this, $place['place']);
				}
				$this->message = self::EMPTY_GEO_UNFOLLOWS;
				$this->geo_adjusted = true;
				$this->save();
				return false;

			}

			if ($options->isSearchByHashtags() &&
				$this->getHashtagsCount() === 0
			) {
				$this->message = self::EMPTY_HASHTAGS;
				$this->save();
				return false;
			}

			if ($options->isSearchByCompetitors() &&
				$this->getIncompleteCompetitorsCount() === 0
			) {
				$this->message = self::EMPTY_COMPETITORS;
				$this->save();
				return false;
			}

			if ($options->comment && $this->getCommentsCount() === 0) {
				$this->message = self::EMPTY_COMMENTS;
				$this->save();
				return false;
			}
			if (in_array($this->message, [self::EMPTY_GEO_UNFOLLOWS, self::EMPTY_COMPETITORS, self::EMPTY_COMMENTS])){
				$this->message = false;
				$this->save();
			}
		}

		return true;
	}
	public function isDemo()
	{
		return $this->id === self::DEMO_ID;
	}


	static function check($login, $password, $ip = null, $proxy = null, $device = null)
	{
		if ($ip === null) {
			$ip = Yii::$app->request->getUserIP();
		}

		\Core::init([
			'app' => [
				'storage' => [
					'class' => '\core\storage\MemoryStorage',
					'base_dir' => ''
				],
				'api' => [
					'class'              => '\core\instagram\Api',
					'cookiesStorageName' => 'storage',
					'verbose'            => 0,
				],
				'captcha_apikey' => 'e963a8aab389d293b9982e42ef4cacba',
				'server_type'    => 'statistic',
			]
		]);

		\Core::setLogCallback(['Account', 'log']);

		\Core::$app->api->checkpointCallback = function ($url) {
			throw new CheckpointReqiredException('checkpoint');
		};

		if ($device === null) {
			$device = Device::getRandom();
		}

		if ($proxy === null) {
			$proxy = Proxy::getRandom()['proxy'];
		}

		Yii::info('Proxy: ' . $proxy);
		Yii::info('Auth: ' . $login . ' : ' . $password);
		Yii::info('Client Ip: ' . $ip);

		\Core::$app->api->load([
			'proxy' => $proxy,
			'ip' => $ip,
			'username' => $login,
			'password' => $password,
			'device' => [
				'os' => [
					'class' => '\core\instagram\device\OS',
					'api_level' => $device['android_api'],
					'version' => $device['android_version'],
					'language' => 'en_US',
				],
				'screen' => [
					'class' => '\core\instagram\device\Screen',
					'width' => $device['screen_x'],
					'height' => $device['screen_y'],
					'dpi' => $device['dpi'],
				],
				'vendor' => $device['vendor'],
				'model' => $device['model'],
				'platform' => $device['platform'],
				'cpu' => $device['cpu'],
			]
		]);

		return \Core::$app->api->login($login, $password);
	}
}
