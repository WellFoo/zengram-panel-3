<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "media_count".
 *
 * @property integer $media_id
 * @property integer $comments
 * @property integer $new_comments
 * @property integer $account_id
 */
class MediaCount extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'media_count';
	}

	/**
	 * @inheritdoc
	 */
	public static function primaryKey()
	{
		return ['account_id', 'media_id'];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['media_id', 'comments', 'account_id'], 'required'],
			[['comments', 'account_id'], 'integer'],
			[['media_id'], 'string'],
			[['media_id'], 'unique']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'media_id' => Yii::t('app', 'Media ID'),
			'comments' => Yii::t('app', 'Comments'),
		];
	}
}
