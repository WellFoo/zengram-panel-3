<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sniffer_log".
 *
 * @property integer $id
 * @property integer $server_id
 * @property integer $time
 * @property integer $ping
 * @property string  $disk_drive
 * @property float   $memory
 * @property float   $processor
 * @property string  $parameters
 */
class SnifferLog extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'sniffer_log';
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
		];
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {

			$this->time = time();

			return true;
		}
		return false;
	}

	public static function log($data)
	{
		$log = new SnifferLog($data);
		$log->save();
		return $log;
	}
}
