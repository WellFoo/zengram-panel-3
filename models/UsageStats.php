<?php namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%usage_stats}}".
 *
 * @property string $date
 * @property integer $usage_avg
 * @property integer $usage_sum
 * @property integer $regs
 * @property integer $trial
 * @property integer $payed
 */
class UsageStats extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%usage_stats}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['date'], 'safe'],
			[['usage_avg', 'usage_sum', 'regs', 'trial', 'payed'], 'integer'],
			[['date'], 'unique'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'date' => 'Date',
			'usage_avg' => 'Usage Avg',
			'usage_sum' => 'Usage Sum',
			'regs' => 'Regs',
			'trial' => 'Trial',
			'payed' => 'Payed',
		];
	}

	public function getUsageAvg()
	{
		return self::format($this->usage_avg);
	}

	public function getUsageSum()
	{
		return self::format($this->usage_sum);
	}

	public static function format($data)
	{
		$days = floor($data / 86400);
		$data = $data % 86400;
		$hours = floor($data / 3600);
		$data = $data % 3600;
		$minutes = floor($data / 60);

		return sprintf(
			'%d д. %d ч. %d м.',
			$days,
			$hours,
			$minutes
		);
	}
}
