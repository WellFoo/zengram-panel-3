<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property integer $time
 * @property integer $status
 * @property string  $executor
 * @property string  $name
 *
 */
class Task extends ActiveRecord
{

	const STATUS_FREE = 1;
	const STATUS_WORK = 2;
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'task';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'time', 'status'], 'integer'],
			[['executor', 'name'], 'string']
		];
	}

	public static function check($task, $forward)
	{
		return self::find()->where([
			'name' => $task
		])->count() >= $forward;
	}

	public static function count($task)
	{
		return self::find()->where([
			'name' => $task
		])->count();
	}

	public static function create($data)
	{
		$task = Task::findOne([
			'name' => $data['name'],
			'time' => $data['time'],
		]);

		if (!is_null($task)) {
			return $task;
		}

		$task = new Task($data);

		if ($task->save()) {
			return $task;
		}

		return false;
	}
	
	public static function time($task)
	{
		/** @var Task $task */
		$task = self::find()
			->where([
				'name' => $task
			])
			->orderBy(['time' => SORT_DESC])
			->one();

		if (is_null($task)) {
			return time();
		}
		
		return $task->time;
	}

	public static function getTask($name)
	{
		/** @var Task $task */
		$task = self::find()
			->where([
				'name' => $name
			])
			->andWhere(['<', 'time', time()])
			->orderBy(['time' => SORT_ASC])
			->one();

		if (is_null($task)) {
			return false;
		}

		if ($task->delete() != 1) {
			return false;
		}
		
		return $task;
	}

	public static function getNewTasks()
	{
		/** @var Task $task */
		$tasks = self::find()
			->where(['<', 'time', time()])
			->all();

		return $tasks;
		
	}
}
