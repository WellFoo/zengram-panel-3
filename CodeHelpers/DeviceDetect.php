<?php namespace alexandernst\devicedetect;

/**
 * @method isMobile()
 * @method isTablet()
 * @method isDesktop()
 */
class DeviceDetect extends \yii\base\Component {}