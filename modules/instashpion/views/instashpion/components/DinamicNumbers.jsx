var DinamicNumber = React.createClass({
    interval: null,

    getInitialState() {
        return {
            number: 0.0,
            part: 0.0
        }
    },

    componentDidMount() {
        this.setState({
            number: this.props.sync
        });

        this.reInterval();
    },

    /*
     * Переинициализация таймера
     */
    reInterval() {
        if (this.interval != null) {
            clearInterval(this.interval);
        }

        this.setState({
            part: this.getSpeed() / this.props.fps
        });

        this.interval = setInterval(this.tick, 1000 / this.props.fps);
    },

    componentWillUnmount() {
        clearInterval(this.interval);

    },

    /*
     * Обработка новых входных данных
     */
    shouldComponentUpdate(nextProps, nextState) {
        // console.log('Новый прогрес' + nextProps.sync + ' / ' + this.state.number);
        if (nextProps.sync != this.props.sync || nextProps.right != this.props.right) {

            if (nextProps.sync > this.state.number) {
                this.setState({
                    number: nextProps.sync
                });
            }

            this.reInterval();
        }

        return true;
    },

    /*
     * Рассчет скорости обновления значения
     */
    getSpeed() {
        var rightPer = (100 - this.props.sync);
        var rightTime = this.props.right;

        if (this.state.number >= 80 && this.state.number > this.props.sync) {
            rightTime = rightTime + (this.state.number - this.props.sync);
        }

        if (this.state.number >= 80 && this.state.number > this.props.sync) {
            rightTime = rightTime + (this.state.number - this.props.sync);
        }
        
        return parseFloat(rightPer / rightTime);
    },

    /*
     * Обработчик для каждой итерации интервала
     */
    tick() {
        if (this.state.number >= 100) {
            this.setState({
                number: 100
            });

            this.props.onComplete();

            clearInterval(this.interval);
            return true;
        }

        var value = this.state.number + this.state.part;

        if (this.props.sync >= value) {
            value = this.props.sync;
        }

        this.setState({
            number: value
        });
    },

    /*
     * Вывод значения
     */
    render() {
        return (
            <span>
                {this.state.number.toFixed(this.props.formatNumbers || 2)}
            </span>
        );
    }
});
