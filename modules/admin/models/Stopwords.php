<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "stopwords".
 *
 * @property integer $id
 * @property string  $word
 * @property string  $text_word
 * @property integer $pattern
 */
class Stopwords extends \yii\db\ActiveRecord
{

	const PATTERN_EVERYPLACE = 0;
	const PATTERN_BEGINING   = 1;
	const PATTERN_ENDING     = 2;
	const PATTERN_WHOLE_WORD = 3;

	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stopwords';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text_word', 'pattern'], 'required'],
            [['word'], 'string'],
            [['text_word'], 'string', 'max' => 255],
	        ['pattern', 'in', 'range' => [
		        self::PATTERN_EVERYPLACE,
		        self::PATTERN_BEGINING,
		        self::PATTERN_ENDING,
		        self::PATTERN_WHOLE_WORD,
	        ]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text_word' => 'Стоп-Слово',
            'word' => 'Стоп-Слово',
            'pattern' => 'Положение в тексте',
        ];
    }

	public static function patternDescriptions()
	{

		return [
			self::PATTERN_EVERYPLACE => 'Любое место',
			self::PATTERN_BEGINING   => 'В начале слова',
			self::PATTERN_ENDING     => 'В конце слова',
			self::PATTERN_WHOLE_WORD => 'Слово целиком',
		];

	}

	public static function getPatternDescription($pattern)
	{
		return self::patternDescriptions()[$pattern];
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {

			$this->word = self::patternWrap($this->text_word, $this->pattern);

			return true;
		}

		return false;

	}

	public static function patternWrap($word, $pattern)
	{

		switch ($pattern) {
			case self::PATTERN_EVERYPLACE:
			default:
				return $word;
				break;
			case self::PATTERN_BEGINING:
				return '\s'.$word;
				break;
			case self::PATTERN_ENDING:
				return $word.'\s';
				break;
			case self::PATTERN_WHOLE_WORD:
				return '\s'.$word.'\s';
				break;
		}

	}
}
