<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * HashtagsSearch represents the model behind the search form about `app\models\Hashtags`.
 */
class HashtagsSearch extends Hashtags
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'counter'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $ids = null)
    {
        $query = Hashtags::find();
        $query->where(['id'=>$ids]);
        $query->orderBy(['counter' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        /*
        $query->andFilterWhere([
            'id' => $this->id,
            'counter' => $this->counter,
            'sort' => [
                'default' => ['counter' => SORT_DESC]
            ]
        ]);
        */

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
