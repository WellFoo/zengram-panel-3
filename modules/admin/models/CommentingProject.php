<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "commenting_project".
 *
 * @property integer $id
 * @property integer $popular_category
 * @property string  $name
 * @property integer $expired_time
 * @property integer $post_count
 * @property integer $cron_status
 * @property integer $time_from
 * @property integer $time_until
 * @property integer $count_per_iteration
 * @property string  $comment_account
 * @property integer $max_random_image
 * @property boolean $obfuscation
 */

class CommentingProject extends ActiveRecord
{

	const STATUS_ACTIVE = 1;
	const STATUS_PAUSE  = 2;
	const STATUS_STOP   = 0;

	const CATEGORY_30   = 1;
	const CATEGORY_100  = 2;
	const CATEGORY_500  = 3;
	const CATEGORY_1000 = 4;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'commenting_project';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['expired_time'], 'integer', 'min' => 10],
			[['comment_account', 'name'], 'string'],
			[['cron_status'], 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_STOP, self::STATUS_PAUSE]],
			[['time_from', 'time_until'], 'integer', 'min' => 0, 'max' => 24],
			/*['popular_category', 'in', 'range' => [
					self::CATEGORY_30,
					self::CATEGORY_100,
					self::CATEGORY_500,
					self::CATEGORY_1000
				]
			],*/
			['popular_category', 'safe'],
			[['post_count', 'count_per_iteration'], 'integer', 'min' => 1],
			[['max_random_image'], 'integer', 'min' => 0],
			[['id'], 'integer'],
			[['obfuscation'], 'boolean'],
		];
	}

	public function attributeLabels()
	{
		return [
			'popular_category'    => 'Категория звёзд',
			'expired_time'        => 'Время жизни комментария (минут)',
			'post_count'          => 'Количество постов от одного аккаунта',
			'cron_status'         => 'Комментирование запущено',
			'time_from'           => 'Время начала комментирования',
			'time_until'          => 'Время окончания комментирования',
			'count_per_iteration' => 'Комментариев за 10 минут',
			'comment_account'     => 'Аккаунт для выборки комментариев',
			'name'                => 'Наименование проекта',
			'max_random_image'    => 'Выбор из последних изображений',
			'obfuscation'         => 'Обфускация комментариев',
		];
	}

	public static function getAccountQuery($categorys)
	{
		$query = PopularAccounts::find();

		$conditions = [];

		foreach ($categorys as $category) {

			switch ($category) {
				case self::CATEGORY_1000:

					$conditions[] = ['>=', 'follower_count', 1000000];

					break;
				case self::CATEGORY_500:

					$conditions[] = ['and',
						['>=', 'follower_count', 500000],
						['<', 'follower_count', 1000000]
					];

					break;
				case self::CATEGORY_100:

					$conditions[] = ['and',
						['>=', 'follower_count', 100000],
						['<', 'follower_count', 500000]
					];

					break;
				case self::CATEGORY_30:
				default:

					$conditions[] = ['and',
						['>=', 'follower_count', 30000],
						['<', 'follower_count', 100000]
					];

					break;
			}

		}

		if (count($conditions) === 1) {
			$where = $conditions[0];
		} else {

			$where = [
				'or'
			];

			foreach ($conditions as $condition) {

				$where[] = $condition;

			}

		}

		$query->where($where);
		
		return $query;
	}

	public static function createDefault()
	{

		$project = new self([

			'popular_category' => self::CATEGORY_30,
			'expired_time' => 30,
			'post_count' => 1,
			'cron_status' => self::STATUS_STOP,
			'time_from' => 0,
			'time_until' => 24,
			'count_per_iteration' => 1,
			'comment_account' => '',
			'max_random_image' => 3,
			'obfuscation' => 0,

		]);

		$project->save();

		return $project->id;

	}

	public function afterFind ()
	{

		$this->popular_category = explode(',', $this->popular_category);

	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {

			if (is_array($this->popular_category)) {

				$this->popular_category = implode(',', $this->popular_category);

			}

			return true;
		}

		return false;

	}

	public function afterSave($insert, $changedAttributes) {

		if ($insert) return true;

		if (isset($changedAttributes['cron_status'])) {

			if ($this->cron_status === self::STATUS_STOP && $changedAttributes['cron_status'] !== self::STATUS_STOP) {
				PopularAccountsComments::deleteAll('project_id = '.$this->id.' AND comment_id IS NULL');
				return true;
			}

		}

		if (isset($changedAttributes['popular_category'])) {

			if ($this->popular_category != $changedAttributes['popular_category']) {
				PopularAccountsComments::deleteAll('project_id = '.$this->id.' AND comment_id IS NULL');
				return true;
			}

		}

		return true;
	}

	public function beforeDelete()
	{
		if (parent::beforeDelete()) {

			PopularAccountsComments::deleteAll('project_id = '.$this->id.' AND comment_id IS NULL');

			return true;

		} else {

			return false;

		}
	}

}
