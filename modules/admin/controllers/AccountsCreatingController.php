<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\AccountsCampaigns;
use app\modules\admin\models\Donor;
use app\modules\admin\models\DonorForm;
use app\modules\admin\models\CampaignCreatingForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class AccountsCreatingController extends Controller
{
	public function actionIndex()
	{
		$modelForm = new CampaignCreatingForm;

		$dataProvider = new ActiveDataProvider([
			'query' => AccountsCampaigns::find(),
			'pagination' => [
				'pageSize' => 20,
			],
		]);

		return $this->render('index',['modelForm' => $modelForm,'dataProvider' => $dataProvider]);
	}
	public function actionCampaignCreate()
	{
		$modelForm = new CampaignCreatingForm;
		if ($modelForm->load(Yii::$app->request->post()) && $modelForm->validate()) {
			print_r(Yii::$app->request->post());
			die();
			$campaign  = new AccountsCampaigns();
			$campaign->name = $modelForm->name;

			$campaign->accountIdBeforeTrig = $modelForm->accountIdBeforeTrig;
			$campaign->frequencyOfPosting = $modelForm->frequencyOfPosting;
			$campaign->limitFollow = $modelForm->limitFollow;
			$campaign->limitLike = $modelForm->limitLike;
			$campaign->tags = $modelForm->tags;
			$campaign->fpopularFrom = $modelForm->fpopularFrom;
			$campaign->fpopularTo = $modelForm->fpopularTo;
			$campaign->frecomendedFrom = $modelForm->frecomendedFrom;
			$campaign->frecomendedTo = $modelForm->frecomendedTo;
			$campaign->followersTrigger = $modelForm->followersTrigger;
			$campaign->daysTrigger = $modelForm->daysTrigger;
			$campaign->hourPassTrigger = $modelForm->hourPassTrigger;
			$campaign->hourBanTrigger = $modelForm->hourBanTrigger ;
			if (!$campaign->save()){
				var_dump($campaign->getErrors());
			}

			//public $location;
			//$campaign->profilesOfDonors = $modelForm->profilesOfDonors;
			//var_dump($campaign);
		}
		return $this->render('campaigns_create',['modelForm' => $modelForm]);
	}

	public function actionDonors()
	{
		$donors = new ActiveDataProvider([
			'query' => Donor::find()->where(['completed' => 1])
		]);

		return $this->render('donors', [
			'donors' => $donors
		]);
	}

	public function actionDonorCreate()
	{
		return $this->actionDonorEdit();
	}

	public function actionDonorEdit($id = null)
	{
		if (is_null($id) && $this->action->id === 'donor-edit') {
			throw new BadRequestHttpException('Отсутствует обязательный параметр id');
		}

		$donor = new DonorForm();
		if (!is_null($id) && !$donor->find($id)) {
			throw new NotFoundHttpException('Донор не найден');
		}

		$step = Yii::$app->request->get('step', 1);
		if ($step < 1 || $step > 2) {
			$step = 1;
		}

		$donor->scenario = 'step_'.$step;
		if ($donor->load(Yii::$app->request->post()) && $donor->save()) {
			if ($step === 1) {
				return $this->redirect(['donor-edit', 'id' => $donor->id, 'step' => 2]);
			}
			return $this->redirect(['donors']);
		}

		return $this->render('donor_edit_s'.$step, [
			'donor' => $donor
		]);
	}

	public function actionDonorValidation()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		$donor = new DonorForm();
		$donor->load(Yii::$app->request->post());
		return ActiveForm::validate($donor);
	}
}