<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Stopwords;
use app\modules\admin\models\StopwordsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AntispamSettingsController implements the CRUD actions for Stopwords model.
 */
class AntispamSettingsController extends Controller
{
    private $viewsFolder = '@app/modules/admin/views/antispam/settings/';
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Stopwords models.
     * @return mixed
     */
    public function actionIndex()
    {
        //var_dump(Yii::$app->request->post());
        if ($trigger = Yii::$app->request->post('antispamTrigger')){
            if ($trigger == '1'){
                //var_dump('Включаем антиспам для всех');
                Yii::$app->db->createCommand('UPDATE users SET antispam = 1')->execute();
            }elseif ($trigger == '2'){
                //var_dump('Выключаем антиспам для всех');
                Yii::$app->db->createCommand('UPDATE users SET antispam = 0')->execute();
            }
        }
        $searchModel = new StopwordsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render($this->viewsFolder.'index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Stopwords model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render($this->viewsFolder.'view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Stopwords model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Stopwords();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render($this->viewsFolder.'create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Stopwords model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render($this->viewsFolder.'update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Stopwords model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Stopwords model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Stopwords the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Stopwords::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
