<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Посты блога';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-admin">

	<?php if (Yii::$app->session->hasFlash('PostDeleted')): ?>
		<div class="alert alert-success">
			Пост успешно удален
		</div>
	<?php endif; ?>

	<div class="row">
		<div class="col-xs-12 col-md-2 col-sm-2">
			<a href="<?= Url::toRoute('/admin/blog/create'); ?>" class="btn btn-primary">Добавить пост</a>
		</div>
		<div class="col-xs-12 col-md-10 col-sm-10">

		</div>
	</div>

	<table class="table">
		<thead>
		<tr>
			<th>ID</th>
			<th>Заголовок</th>
			<th>Текст</th>
			<th>Дата</th>
			<th></th>
			<th></th>
		</tr>
		</thead>
		<tbody>
		<?php
		/** @var $posts /app/models/Post[] */
		foreach ($posts as $post): ?>
			<tr>
				<td><?php echo $post['id']; ?></td>
				<td><?php echo $post['title']; ?></td>
				<td><?php echo $post['text']; ?></td>
				<td><?php echo $post['date']; ?></td>
				<td><?php echo Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', array(
						'/admin/blog/view/?id=' . $post['id']
					), array('class' => '')); ?></td>
				<td><?php echo Html::a('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', array(
						'/admin/blog/delete/?id=' . $post['id']
					), array('class' => '')); ?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

</div>