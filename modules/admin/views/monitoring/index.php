<?php

use app\models\Monitoring;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\MonitoringSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $categoriesCount array */
/* @var string $date */
/* @var string $dateFrom */
/* @var string $dateTo */

$this->title = 'Отслеживания';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monitoring-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a('Добавить отслеживание', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<p>
		<?= Html::a('Лог серверов', ['servers-log'], ['class' => 'btn btn-default']) ?>
	</p>
	<p>
		<?= Html::a('Лог перегрузок', ['overload-log'], ['class' => 'btn btn-default']) ?>
	</p>
	<p>
		<?= Html::a('Незачисленные платежи', ['bad-invoices'], ['class' => 'btn btn-default']) ?>
	</p>
	<?php
	Pjax::begin();
	ActiveForm::begin([
		'method'  => 'get',
		'options' => [
			'class' => 'form-horizontal',
			'style' => 'margin: 20px 0;',
		],
	]); ?>
	<h3 class="text-danger">Показатели "среднемесячно факт" и "среднеквартально факт" будут корректно отображены начиная с 01.05.2016 и 01.07.2016 соответственно</h3>
	<div class="row">
		<div class="col-xs-3">
			<div class="btn-group" role="group" aria-label="...">
				<button type="submit" name="date" value="day"
				        class="btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
				</button>
				<button type="submit" name="date" value="week"
				        class="btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
				</button>
				<button type="submit" name="date" value="month"
				        class="btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
				</button>
			</div>

		</div>

		<div class="col-xs-9">
			<label class="control-label col-xs-2">Диапазон</label>

			<div class="input-group col-xs-6 col-xs-offset-1">
				<label for="date-from" class="input-group-addon">с</label>
				<input name="from" value="<?= $dateFrom ? date('Y-m-d', $dateFrom) : '' ?>" id="date-from"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">
				<label for="date-to" class="input-group-addon">по</label>
				<input name="to" value="<?= $dateTo ? date('Y-m-d', $dateTo) : '' ?>" id="date-to"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">

					<span class="input-group-btn">
						<button type="submit" name="date" value="range"
						        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
						</button>
					</span>
			</div>
		</div>
	</div>
	<?php
	ActiveForm::end();
	//	print_r($searchModel->$categoriesCount);
	echo GridView::widget([
		'id'           => 'monitoring',
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'rowOptions'   => function ($item) {
			/* @var $item Monitoring */
			if ($item['incorrect']) {
				return ['class' => 'danger'];
			}
			return [];
		},
		'columns'      => [
			[
				'attribute'      => 'category',
				'filter'         => $searchModel->categoriesCount,
				'contentOptions' => function ($item, $key, $index, $column) {
					/* @var $item Monitoring */
					if (isset($column->grid->filterModel->categoriesCount[$item['category']])) {
						$count = $column->grid->filterModel->categoriesCount[$item['category']];
						unset($column->grid->filterModel->categoriesCount[$item['category']]);
						return ['rowspan' => $count];
					}
					return ['class' => 'hidden'];
				},
			],
			'title',
			[
				'attribute' => 'comparation',
				'filter'    => Monitoring::comparationLabels(),
				'format'    => 'html',
				'value'     => function ($item) {
					return Monitoring::comparationLabels()[$item['comparation']] . (YII_ENV_DEV ? ', <br>' . Monitoring::periodLabels()[$item['period']] : '');
				},
			],
			'value',
			'result',
			[
				'attribute' => 'monthAverage',
				'header'    => 'среднемесячно факт',
				'format'    => ['decimal', 0],
				'value'     => function($item){
					/* @var $item Monitoring */
					return $item->getMonthLog();
				}
			],
			[
				'attribute' => 'quarterAverage',
				'header'    => 'среднеквартально факт',
				'format'    => ['decimal', 0],
				'value'     => function($item){
					/* @var $item Monitoring */
					return $item->getQuarterLog();
				}
			],
			[
				'attribute' => 'count',
				'header' => 'Количество проишествий',
				'value' => function($item){
					/* @var $item app\models\Monitoring */
					return $item->getFails();
				}
			],
//			[
//				'attribute' => 'period',
//				'filter'    => Monitoring::periodLabels(),
//				'value'     => function ($item) {
//					/* @var $item Monitoring */
//					return $item->periodLabels()[$item->period];
//				},
//			],
			[
				'attribute' => 'email',
				'value' => function($item){
					return str_replace(',', ', ',$item->email);
				}
			],
			[
				'attribute' => 'sms',
				'value' => function($item){
					return str_replace(',', ', +',$item->sms);
				}
			],
			[
				'format' => 'raw',
				'value'  => function ($item) {
					return '<div class="btn-group-vertical">' .
					Html::a('Log', ['log', 'id' => $item['id']], ['class' => 'btn btn-warning']) .
					Html::a('Edit', ['update', 'id' => $item['id']], ['class' => 'btn btn-primary']) . '</div>' .
//					'<div class="btn-group">'.
//						Html::a('Delete', ['delete', 'id' => $item->id], ['class' => 'btn btn-danger', 'data' => [
//							'confirm' => 'Are you sure you want to delete this item?',
//							'method'  => 'post',
//						],
//						]).
					'</div>';
				}],
		],
	]);
	Pjax::end();
	?>
	<script>
		var gridview_id = "monitoring"; // specific gridview
		var columns = [2]; // index column that will grouping, start 1

		var column_data = [];
		column_start = [];
		rowspan = [];

		for (var i = 0; i < columns.length; i++) {
			var column = columns[i];
			column_data[column] = "";
			column_start[column] = null;
			rowspan[column] = 1;
		}

		var row = 1;
		$(gridview_id + " table > tbody  > tr").each(function ()
		{
			var col = 1;
			$(this).find("td").each(function ()
			{
				for (var i = 0; i < columns.length; i++) {
					if (col == columns[i]) {
						if (column_data[columns[i]] == $(this).html()) {
							$(this).remove();
							rowspan[columns[i]]++;
							$(column_start[columns[i]]).attr("rowspan", rowspan[columns[i]]);
						}
						else {
							column_data[columns[i]] = $(this).html();
							rowspan[columns[i]] = 1;
							column_start[columns[i]] = $(this);
						}
					}
				}
				col++;
			});
			row++;
		});
//		$('.table-striped td').html($('.table-striped td').html().replace(/,/g , ',&#8203;'));
	</script>
</div>
