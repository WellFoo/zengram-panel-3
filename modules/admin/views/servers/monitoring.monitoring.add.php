<?php
/**
 * @var $this \yii\web\View
 * @var $monitoring \app\models\SnifferMonitoringParameters
 * @var $user \app\models\SnifferUsers
 */

use app\models\SnifferServers;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

$this->title = $user->name . ', добавление отслеживания';

echo '<h2>' . $this->title . '</h2>';

echo $this->render('monitoring.monitoring.form.php', [
	'monitoring' => $monitoring
]);