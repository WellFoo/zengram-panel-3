<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;

$this->title = 'Добавить пользователя для тестовой рассылки';
//$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('@web/js/translit.js', ['position' => View::POS_BEGIN]);
?>
<div class="site-admin">

	<?php
	$form = ActiveForm::begin([
		'options'     => ['class' => 'form-horizontal', 'role' => 'form'],
		'fieldConfig' => [
			'template'     => "{label}\n<div class=\"col-sm-11\">{input}</div>\n<div class=\"col-sm-1\"></div><div class=\"col-sm-11\">{error}</div>",
			'labelOptions' => ['class' => 'col-sm-1 control-label'],
		],
	]);

	echo $form->field($model, 'email')->textInput([]);
	echo $form->field($model, 'instagram_id')->textInput([]);

	echo Html::submitButton('Добавить', ['class' => 'btn btn-primary']);

	ActiveForm::end();
	?>
</div>