<?php
/** @var $hashtags Array */
/** @var $place_id String */
//print_r($hashtags);
?>

<table class="table table-striped">
	<tr>
		<th>Город</th>
		<th>Количество хэштегов</th>
		<th>Просмотреть</th>
	</tr>
<?php
foreach($hashtags as $hashtag):
?>
<tr>
	<td><?= $hashtag['name'] ?></td>
	<td><?= $hashtag['total'] ?></td>
	<td><a href="/admin/hash-tags/in-region/<?= $hashtag['place_id']  ?>"><span class="glyphicon glyphicon-th-list"></span></a></td>
</tr>
<?php
endforeach;
?>
</table>


