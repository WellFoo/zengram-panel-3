<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Options;
use yii\web\View;
use app\modules\admin\models\Donor;
use app\modules\admin\models\CampaignCreatingForm;

/* TODO: Выпилить ненужны скрипты */
$this->registerJsFile('@web/js/lib/can.custom.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/lib/bs3paginator.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/lib/bs3radio.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/lib/bootstrap3-typeahead.min.js', ['position' => View::POS_BEGIN]);
//$this->registerJsFile('@web/js/account.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/options-new.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.autosize.js', ['position' => View::POS_BEGIN]);

// настройки лайкинга фолловинга по тегам и географии???
/*
настройка кампании
1) название кампании
2) локация. выбираем возможные локации для профилей. как сейчас в зенграм. по умолчанию выбирает все города. можно выбрать галочкой “брать по очереди” ___ города. тогда рэндом присваивает нужное количество.
язык русский\английский\испанский. выбираем возможные языки для профилей
3) Профили доноров. отмечаем профили, которые можно использовать. (по умолчанию берем рэндом из доступного для данной учетной записи профиля.
сколько создать аккаунтов для данной кампании)
4) аккаунт. на какой аккаунт зенграма передать после завершения триггеров. если не указано, то размещаются в текущем аккаунте.
5) частота постинга.
    - 1 фото каждый
    - 2-3,
    - 3-5,
    - 6-10.
можно выбрать несколько диапазонов. если выбрано два или более, то после каждого постинга рэндом выбирается следующий
диапазон постинга на ближайшее фото и включается счетчик.

6) настройки лайкинга фолловинга по тегам и географии

7) устанавливаем лимит действий по фоллоу и лайкам в сутки. начинать надо от 20% лимита и каждый день прибавлять по 10% до достижения 100%.

8) теги кампании. вводим теги при необходимости для поиска медиа

9) на сколько популярных подписываться в день от и до
10) на сколько рекомендаций подписываться в день от и до

11) Условия окончания кампании (триггеры). после достижения указанных параметров аккаунт передается на указанный аккаунт зенграм. если ничего в параметре не указано, значит триггер отключен. каждому триггеру назначается параметр и\или в случае “и” должны быть выполнены все условия. в случае или одно из
- сколько подписчиков надо набрать
- сколько времени должны идти работы в днях
- параметры остановки кампании.
после срабатывания триггеров все аккаунты кампании останавливаются и приходит письмо.
включить можно вручную.
    - в течении часа слет более __ паролей
    - в течении часа блок более ___ аккаунтов
*/
$this->title = 'Настройки кампании';
$this->registerCssFile('/css/bootstrap-toggle.min.css');
$this->registerCssFile('/css/site.css');

?>
<style>
    .form-control {
        max-width: 500px !important;
        */
    }

    .num {
        width: 40px;
        margin-left: 20px;
    }
</style>

<style type="text/css" media="all" scoped>
    /* TODO: Выпилить ненужны стили */
    .account-controls .btn {
        width: 180px;
    }

    .regions-list-wrap {
        position: relative;
    }

    .regions-list-wrap:before,
    .regions-list-wrap:after {
        background: linear-gradient(to top, rgba(247, 247, 247, 0), #F7F7F7);
        content: '';
        display: block;
        height: 5px;
        position: absolute;
        top: 0;
        left: 0;
        right: 7px;
    }

    .regions-list-wrap:after {
        background: linear-gradient(to bottom, rgba(247, 247, 247, 0), #F7F7F7);
        top: inherit;
        bottom: 0;
    }

    .clearfix.divide {
        border-top: 1px solid #E2E2E1;
        height: 0;
        margin: 10px -10px;
    }

    .btn-success.active {
        background-color: #419041;
    }

    .disable {
        /*opacity: 0.5;
        pointer-events: none;*/
    }

    /* Стили для ресайзера текстового поля Комментарий */
    #commentText {
        border: 2px solid #ccc;
        vertical-align: top;
        width: 100%;
        padding: 0;
        height: 0;
    }

    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    #commentsList {
        margin-top: 10px;
    }

    .panel.panel-settings {
        background: #F7F7F7;
        border: 1px solid #E2E2E1;
        border-radius: 7px;
    }

    .panel.panel-settings > .panel-header > .row > div {
        background: #A6A6A4;
        color: #FFF;
        font-size: 16px;
        height: 42px;
        line-height: 42px;
    }

    .panel.panel-settings > .panel-header > .row > div:first-child {
        background: #00BF51;
        border-top-left-radius: 6px;
        font-weight: bold;
        padding-left: 30px;
        width: 160px;
    }

    .panel.panel-settings > .panel-header > .row > div:last-child {
        border-top-right-radius: 6px;
        padding: auto 20px auto 50px;
        margin-left: 160px;
        float: inherit;
        width: inherit;
    }

    .panel.panel-settings > .panel-header .fa {
        line-height: inherit;
        vertical-align: middle;
    }

    .panel.panel-settings > .panel-body {
        padding: 10px;
    }

    .panel.panel-sub {
        border: 1px solid #E2E2E1;
        box-shadow: 0 2px 7px 1px rgba(0, 0, 0, 0.2);
        margin-bottom: 0;
        transition-duration: .3s;
    }

    .panel.panel-sub.disable {
        box-shadow: none;
    }

    .panel.panel-sub > .panel-header {
        background: #F7F7F7;
    }

    .panel.panel-sub > .panel-header > .row > div {
        background: #E1E1E1;
        font-size: 16px;
        height: 42px;
        line-height: 40px;
        transition-duration: .3s;
        white-space: nowrap;
    }

    .panel.panel-sub > .panel-header > .row > div:first-child {
        background: #00BF51;
        border-top-left-radius: 3px;
        font-weight: bold;
        text-align: center;
    }

    .panel.panel-sub > .panel-header > .row > div:last-child {
        border-top-right-radius: 3px;
        padding: auto 20px auto 50px;
    }

    .panel.panel-sub > .panel-header {
        border-bottom: none;
        border-radius: 5px 5px 0 0;
    }

    .panel.panel-sub.disable > .panel-header {
        color: #A6A6A4;
    }

    .panel.panel-sub.disable > .panel-header > .row > div:first-child {
        background: #A6A6A4;
    }

    .panel.panel-sub > .panel-header:last-child > .row > div:first-child {
        border-bottom-left-radius: 3px;
    }

    .panel.panel-sub > .panel-header:last-child > .row > div:last-child {
        border-bottom-right-radius: 3px;
    }

    .panel.panel-sub .btn-success {
        background: #00BF51;
    }

    .panel.panel-sub.disable .btn-success {
        background: #A6A6A4;
        border-color: #999;
    }

    .panel.panel-sub .option-switch {
        float: left;
        width: 90px;
    }

    .panel.panel-sub .option-label {
        margin-left: 90px;
        padding: 0 10px;
    }

    .panel.panel-sub .toggle .fa {
        color: #FFF;
        font-size: 115%;
        vertical-align: top;
    }

    .panel.panel-sub .fa.description {
        color: #A1A19F;
    }

    .panel.panel-sub .toggle .fa.fa-slack {
        transform: rotate(18deg);
    }

    .panel.panel-sub > .panel-header .toggle {
        box-shadow: 0 0 7px 1px rgba(0, 0, 0, .15);
    }

    .panel.panel-sub > .panel-header .toggle .toggle-handle {
        box-shadow: inset 1px 1px 7px 1px rgba(0, 0, 0, .15);
    }

    .panel.panel-sub > .panel-body {
        background: #F7F7F7;
    }

    .panel-sub .panel-items-list {
        height: 238px;
        overflow-y: auto;
        margin-right: -7px;
        padding-right: 7px;
    }

    .panel-sub .panel-items-list.panel-items-list-short {
        height: 194px;
    }

    .panel-sub .panel-items-list::-webkit-scrollbar {
        width: 6px;
    }

    .panel-sub .panel-items-list::-webkit-scrollbar-track {
        border-radius: 10px;
        background: #eee;
    }

    .panel-sub .panel-items-list::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background: #888;
    }

    .panel-sub .show-all {
        font-size: 14px;
        margin: 5px 0 -5px;
        text-align: right;
    }

    #actions .panel-body .form-group {
        margin: 5px 0;
    }

    #actions .panel-body .form-group > * {
        font-size: 14px;
        line-height: 20px;
        vertical-align: top;
    }

    .toggle.btn-setting-on {
        border-color: #FFF;
    }

    .toggle.btn-setting-on .toggle-on {
        background: #FFF;
        color: #00BF51;
    }

    .toggle.btn-setting-on .toggle-handle {
        background: #00BF51;
    }

    .toggle.btn-setting-on .fa {
        color: #00BF51 !important;
    }

    .panel-settings .btn-group .btn-success {
        background: #00BF51;
    }

    .panel-sub-header {
        padding-left: 35px;
    }

    .error-wrapper {
        margin: 0;
        padding: 0;
        position: relative;
    }

    .error-wrapper .error {
        background-color: #FFEEEE;
        border: 1px solid #FF433C;
        border-radius: 3px;
        color: #D43F3A;
        display: none;
        padding: 5px 10px;
        position: absolute;
        top: 3px;
        left: 0;
        right: 0;
        z-index: 2000;
    }

    .balloon {
        padding: 6px 70px 6px 15px;
        cursor: default;
    }

    .balloon * {
        line-height: 20px;
    }

    .balloon.balloon-float {
        float: left;
        margin: 0 10px 10px 0;
        padding-right: 35px;
        width: inherit;
    }

    .balloon .button-holder {
        top: 0;
    }

    .panel-header .pull-left {
        margin-right: 5px;
    }

    #settings-modal .modal-content {
        border-radius: 0;
        padding: 0 15px;
    }

    #settings-modal .modal-title {
        text-align: center;
        color: #ff6666;
        margin-bottom: 15px;
        padding-top: 10px;
        margin-top: 3px;
        font: 24px HelveticaNeueCyr-Bold;
        text-transform: uppercase;
    }

    #settings-modal .modal-inner-html {
        min-height: 200px;
        max-height: calc(100vh - 150px);
        overflow-x: hidden;
        overflow-y: auto;
    }

    #settings-modal .modal-inner-html::-webkit-scrollbar {
        width: 6px;
    }

    #settings-modal .modal-inner-html::-webkit-scrollbar-track {
        border-radius: 10px;
        background: #eee;
    }

    #settings-modal .modal-inner-html::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background: #888;
    }

    .simple-tree-view {
        font-size: 12pt;
        list-style: none;
        padding: 0;
    }

    .simple-tree-view ul {
        list-style: none;
        padding-left: 30px;
    }

    .simple-tree-view > ul {
        padding-left: 0;
    }

    .simple-tree-view li {
        padding: 4px 10px;
        border-radius: 3px;
    }

    .simple-tree-view div {
        display: none;
    }

    .simple-tree-view div.open {
        display: inherit;
    }

    .simple-tree-view li:hover {
        background: rgba(76, 175, 80, .15);
    }

    .simple-tree-view li a.toggler i {
        transition-duration: .2s;
    }

    .simple-tree-view li a.toggler.open i {
        transform: rotate(90deg);
    }

    .simple-tree-view a {
        color: #00BF51;
        cursor: pointer;
        float: left;
    }

    .simple-tree-view label {
        display: block;
        margin-left: 10px;
        margin-bottom: 0;
        overflow: hidden;
        white-space: nowrap;
    }

    .simple-tree-view a + label {
        margin-left: 20px;
    }

    @media (max-width: 992px) {
        .acc-pane-inside {
            margin-bottom: 20px;
        }

        .panel.panel-sub > .panel-header > .row > div {
            font-size: 14px;
        }
    }

    @media (max-width: 768px) {
        .panel.panel-sub {
            margin-bottom: 10px;
        }
    }

    .modal-body .popover {
        z-index: 2000;
    }

    #fslider {
        width: 150px;
        margin-left: 6px;
        margin-top: 8px;
    }

    h2 {
        font-size: 20px !important;
        margin-top: 30px;
    }

    #lfollow, #llike {
        width: 800px;
        margin-left: 6px;
        margin-top: 8px;
    }

    #campaigncreatingform-languages .checkbox {
        display: inline-block;
        margin-right: 15px;
    }
    .ilimits .form-group{
        width:100px;
        display:inline-block;
    }

</style>


<?php
$form = ActiveForm::begin([
    'id' => 'company-form',
]) ?>

<div class="panel panel-settings" id="targeting-panel">
    <div class="panel-header container-fluid">
        <div class="row">
            <div class="col-xs-3 col-sm-2">Создание кампании</div>
            <div class="col-xs-9 col-sm-10">
						<span class="visible-md visible-lg">
							Укажите название вашей кампании
						</span>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-5">
                <?= $form->field($modelForm, 'name')->label('Название кампании') ?>
            </div>

        </div>
    </div>
</div>


<?php

/* пока недопилино - закомментим:
<div class="container-fluid">
    <div class="btn-group row" data-toggle="buttons" id="search-by-toggler" style="display: block">
        <label class="btn col-xs-6 btn-default">
            <input type="radio" autocomplete="off" name="search-by"
                   value="<?= Options::SEARCH_PLACE ?>">
            <i class="fa fa-map-marker"></i>
            <?= Yii::t('views', 'cities') ?>
        </label>
        <label class="btn col-xs-6 btn-default">
            <input type="radio" autocomplete="off" name="search-by"
                   value="<?= Options::SEARCH_REGION ?>">
            <i class="fa fa-globe"></i>
            <?= Yii::t('views', 'regions') ?>
        </label>
    </div>
</div>
*/
?>
<?= '' // $form->field($modelForm, 'location')->label('Регион (здесь будет прикручен виджет с выбором городов и регионов)') ?>
<!-- Регионы либо теги -->


<div class="panel panel-settings"
     id="targeting-panel" <?php /* data-url="<?=Url::to(['options/search-by/' . $account->id])?>" */ ?>>
    <div class="panel-header container-fluid">
        <div class="row">
            <div class="col-xs-3 col-sm-2"><?= Yii::t('views', 'Targeting') ?></div>
            <div class="col-xs-9 col-sm-10">
						<span class="visible-md visible-lg">
							<?= Yii::t('views', 'Select how and where zengram will search new followers') ?>
						</span>
						<span class="hidden-md hidden-lg">
							<?= Yii::t('views', 'Followers search') ?>
                            <i class="fa fa-question-circle description" data-toggle="popover"
                               data-title="<?= Yii::t('views', 'Targeting') ?>"
                               data-content="<?= Yii::t('views', 'Select how and where zengram will search new followers') ?>"></i>
						</span>
            </div>
        </div>
    </div>

    <div class="panel-body">
        <div class="row">

            <div class="col-sm-6">
                <!-- Search settings panel -->
                <div class="panel panel-sub active">

                    <div class="panel-header container-fluid">
                        <div class="row">
                            <div class="option-switch">
                                <input type="checkbox" id="options-search-place" class="projectSearchBy"
                                    <?= ''// $account->options->isSearchByGeo() ? 'checked' : ''       ?>
                                       value="<?= Options::SEARCH_PLACE ?>"
                                       data-toggle="toggle" data-style="ios"
                                       data-on="<i class=&quot;fa fa-map-marker&quot;></i>"
                                       data-off="<i class=&quot;fa fa-map-marker&quot;></i>" data-onstyle="setting-on">
                            </div>
                            <div class="option-label">
                                <label for="options-search-place">
                                    <div class="pull-left">
                                        <span class="hidden-sm"><?= Yii::t('views', 'Search by cities') ?></span>
                                        <span class="visible-sm"><?= Yii::t('views', 'By cities') ?></span>
                                    </div>
                                    <i class="fa fa-question-circle description" data-toggle="popover"
                                       data-title="<?= Yii::t('views', 'Search by cities') ?>"
                                       data-content="<?= Yii::t('views', 'If this option is enabled zengram will search accounts in selected cities') ?>"></i>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        <?php if (!empty(Yii::$app->params['use_regions'])) : ?>
                            <div class="container-fluid">
                                <div class="btn-group row" data-toggle="buttons" id="search-by-toggler"
                                     style="display: block">
                                    <label class="btn col-xs-6 btn-default<?= '' /*
                                    (!$account->options->isSearchByGeo() || $account->options->isSearchByPlaces() ? 'success active' : 'default')
                                    ?> <?= $account->options->isSearchByGeo() ? '' : 'disabled' */ ?>">
                                        <input type="radio" autocomplete="off" name="search-by"
                                            <?= '' // (!$account->options->isSearchByGeo() || $account->options->isSearchByPlaces() ? 'checked' : '')       ?>
                                               value="<?= Options::SEARCH_PLACE ?>">
                                        <i class="fa fa-map-marker"></i>
                                        <?= Yii::t('views', 'cities') ?>
                                    </label>
                                    <label class="btn col-xs-6 btn-success active<?= ''/*
                                    ($account->options->isSearchByRegions() ? 'success active' : 'default')
                                    ?> <?= $account->options->isSearchByGeo() ? '' : 'disabled' */ ?>">
                                        <input type="radio" autocomplete="off" name="search-by"
                                            <?= '' /* ($account->options->isSearchByRegions() ? 'checked' : '') */ ?>
                                               value="<?= Options::SEARCH_REGION ?>">
                                        <i class="fa fa-globe"></i>
                                        <?= Yii::t('views', 'regions') ?>
                                    </label>
                                </div>
                            </div>

                            <div class="clearfix divide"></div>
                        <?php endif; ?>

                        <!-- Cities block -->
                        <div
                            style="display: none;" <?= ''//($account->options->isSearchByGeo() && !$account->options->isSearchByPlaces() ? 'style="display: none;"' : '')       ?>
                            id="places-panel">

                            <div id="places-list" <?php // data-url="<?=Url::to(['options/places/' . $account->id])?>"
                            data-api-key="<?= Yii::$app->params['yandex_maps_api_key'] ?>">
                        </div>

                        <script type="text/mustache" id="places-list-template">
										<input type="search" class="form-control" placeholder="<?= Yii::t('views', 'Enter the city') ?>"
										       autocomplete="off" style="margin-bottom: 10px;" {{#pending}}disabled{{/pending}}>
										<div class="error-wrapper"><div class="error"></div></div>
										<?php if (empty(Yii::$app->params['use_regions'])) : ?>
										<div class="clearfix divide"></div>
										<?php endif; ?>
										<div class="panel-items-list<?= (!empty(Yii::$app->params['use_regions']) ? ' panel-items-list-short' : '') ?>">
											{{#each items}}
											<div class="balloon balloon-float" {{data 'item'}}>
												<span class="button-holder">
													<span class="delete glyphicon glyphicon-trash" title="<?= Yii::t('app', 'Remove') ?>"></span>
												</span>
												<span class="content">{{name}}</span>
											</div>
											{{/each}}
										</div>
                        </script>
                    </div>
                    <!-- Cities block end -->

                    <?php if (!empty(Yii::$app->params['use_regions'])) : ?>
                        <!-- Regions block -->
                        <div
                            class="panel-items-list" <?= ''//(!$account->options->isSearchByRegions() ? 'style="display: none;"' : '')       ?>
                            id="regions-panel">

                            <div
                                id="regions-list" <?php // data-url="<?= Url::to(['options/regions', 'id' => $account->id]) "?>></div>

                            <script id="regions-initial-data" type="application/json"><?=
                                json_encode(\app\models\Regions::getTree(/*$account->getRegionsID()*/))
                                ?></script>
                            <script id="regions-list-template" type="text/mustache">
										<div class="simple-tree-view">
											{{>regions-tree-template}}
										</div>
                            </script>
                            <script id="regions-tree-template" type="text/mustache">
										<ul>
										{{#each items}}
										{{#prune_context}}
											<li {{data 'this'}}>
												{{#if items}}
													<a class="toggler{{#collapsed}} open{{/collapsed}}">
														<i class="glyphicon glyphicon-play"></i>
													</a>
												{{/if}}
												<label title="{{label}}">
													{{#if disabled}}
														<input type="checkbox" name="region[]" value="{{id}}" disabled>
													{{else}}
														<input type="checkbox" name="region[]" value="{{id}}" {{#selected}}checked{{/selected}}>
													{{/if}}
													<span>{{label}}</span>
												</label>
											</li>
											{{#if items}}
											<div class="{{#collapsed}}open{{/collapsed}}">
												{{>regions-tree-template}}
											</div>
											{{/if}}
										{{/prune_context}}
										{{/each}}
										</ul>
                            </script>
                        </div>
                        <!-- Regions block end -->
                    <?php endif; ?>

                    <div class="show-all">
                        <a href="#" data-type="geo" data-caption="<?= Yii::t('views', 'Cities') ?>">
                            <?= Yii::t('views', 'Show all') ?>
                        </a>
                    </div>
                </div>
            </div>
            <!-- Search settings panel end -->
        </div>

        <div class="col-sm-6">
            <!-- Hash settings panel -->
            <div
                class="panel panel-sub active<?= ''//$account->options->isSearchByHashtags() ? 'active' : 'disable'       ?>">

                <div class="panel-header container-fluid">
                    <div class="row">
                        <div class="option-switch">
                            <input type="checkbox" id="options-search-hashtag" class="projectSearchBy" checked
                                <?= '' // $account->options->isSearchByHashtags() ? 'checked' : ''       ?>
                                   value="<?= Options::SEARCH_HASHTAG ?>"
                                   data-toggle="toggle" data-style="ios"
                                   data-on="<i class=&quot;fa fa-slack&quot;></i>"
                                   data-off="<i class=&quot;fa fa-slack&quot;></i>" data-onstyle="setting-on">
                        </div>
                        <div class="option-label">
                            <label for="options-search-hashtag">
                                <div class="pull-left">
                                    <span class="hidden-sm"><?= Yii::t('views', 'Search by hashtags') ?></span>
                                    <span class="visible-sm"><?= Yii::t('views', 'By hashtags') ?></span>
                                </div>
                                <i class="fa fa-question-circle description" data-toggle="popover"
                                   data-title="<?= Yii::t('views', 'Search by hashtags') ?>"
                                   data-content="<?= Yii::t('views', 'If this option is enabled zengram will search accounts by selected hashtags') ?>"></i>
                            </label>
                        </div>
                    </div>
                </div>

                <!-- Hash block -->
                <div class="panel-body">

                    <div id="hashtags-list" <?php /* data-url="<?=Url::to([
                            'options/hashtags/' . $account->id
                        ])?>" */ ?>></div>

                    <script type="text/mustaches" id="hashtags-list-template">
									<div class="input-group">
										<input type="search" class="form-control" placeholder="<?= Yii::t('views', 'Enter the hashtag') ?>" {{#pending}}disabled{{/pending}}>
										<span class="input-group-btn">
											<button type="button" class="btn btn-success" {{#pending}}disabled{{/pending}}>
												{{#if pending}}
													<i class="fa fa-spinner fa-pulse"></i>
												{{else}}
													<span class="glyphicon glyphicon-plus" title="<?= Yii::t('app', 'Add') ?>"></span>
												{{/if}}
											</button>
										</span>
									</div>
									<div class="error-wrapper"><div class="error"></div></div>

									<div class="clearfix divide"></div>

									<div class="panel-items-list">
										{{#each items}}
										<div class="balloon balloon-float" {{data 'item'}}>
											<span class="button-holder">
												<span class="delete glyphicon glyphicon-trash" title="<?= Yii::t('app', 'Remove') ?>"></span>
											</span>
											<span class="content">#{{name}}</span>
										</div>
										{{/each}}
									</div>

                    </script>

                    <div class="show-all">
                        <a href="#" data-type="hashtags" data-caption="<?= Yii::t('views', 'Hashtags') ?>">
                            <?= Yii::t('views', 'Show all') ?>
                        </a>
                    </div>
                </div>
            </div>
            <!-- Hash settings panel end -->
        </div>
    </div>
</div>
</div><!-- End of .panel-settings -->





<?= ''//$form->field($modelForm, 'profilesOfDonors')->label('Профили доноров (здесь будет чеклист с профилями доноров)') ?>

<div class="panel panel-settings" id="targeting-panel">
    <div class="panel-header container-fluid">
        <div class="row">
            <div class="col-xs-3 col-sm-2">Языки</div>
            <div class="col-xs-9 col-sm-10">
						<span class="visible-md visible-lg">
							Выбирите возможные языки для профилей
						</span>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-5">
                <?= $form->field($modelForm, 'languages')->checkboxList(Donor::languagesLabels(), [
                    'item' => function ($index, $label, $name, $checked, $value) {
                        $result = Html::beginTag('div', ['class' => 'checkbox']);
                        $result .= Html::beginTag('label');
                        $result .= Html::input('checkbox', $name, $value, [
                            //'onchange' => 'switchDescriptions(this);',
                            'checked' => 1
                        ]);
                        $result .= $label;
                        $result .= Html::endTag('label');
                        $result .= Html::endTag('div');
                        return $result;
                    }
                ]); ?>
            </div>

        </div>
    </div>
</div>


<div class="panel panel-settings" id="targeting-panel">
    <div class="panel-header container-fluid">
        <div class="row">
            <div class="col-xs-3 col-sm-2">Профили</div>
            <div class="col-xs-9 col-sm-10">
						<span class="visible-md visible-lg">
							Отметьте профили, которые будут использоваться в текущей кампании
						</span>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-5">
                <?= $form->field($modelForm, 'accounts')->checkboxList(CampaignCreatingForm::donorsAccounts(), [
                    'item' => function ($index, $label, $name, $checked, $value) {
                        $result = Html::beginTag('div', ['class' => 'checkbox']);
                        $result .= Html::beginTag('label');
                        $result .= Html::input('checkbox', $name, $value, [
                            //'onchange' => 'switchDescriptions(this);',
                            //'checked' => $checked
                        ]);
                        $result .= $label;
                        $result .= Html::endTag('label');
                        $result .= Html::endTag('div');
                        return $result;
                    }
                ]); ?>
            </div>

        </div>
    </div>
</div>



<div class="panel panel-settings" id="targeting-panel">
    <div class="panel-header container-fluid">
        <div class="row">
            <div class="col-xs-3 col-sm-2">Аккаунт</div>
            <div class="col-xs-9 col-sm-10">
						<span class="visible-md visible-lg">
							На какой аккаунт передать после завершения триггеров
						</span>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-5">
                <strong><?= Yii::$app->user->identity->mail ?></strong><br><br>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">
                    Изменить аккаунт
                </button>
                <br><br>
            </div>
        </div>
    </div>
</div>


<div class="panel panel-settings" id="targeting-panel">
    <div class="panel-header container-fluid">
        <div class="row">
            <div class="col-xs-3 col-sm-2">Лимиты</div>
            <div class="col-xs-9 col-sm-10">
						<span class="visible-md visible-lg">
							Ограничения на количества размещений
						</span>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <h4>Частота постинга</h4>
                <div class="scale">
                    <span>1</span>
                    <span style="margin-left:30px;">2-3</span>
                    <span style="margin-left:28px;">3-5</span>
                    <span style="margin-left:20px;">6-10</span>

                </div>
                <?= yii\jui\Slider::widget([
                    'id' => 'fslider',
                    'clientOptions' => [
                        'min' => 1,
                        'max' => 4,
                    ],]) ?>
                <br>

                <?= $form->field($modelForm, 'limitFollow')->hiddenInput(['class' => 'num', 'value' => 50])->label(false)  ?>

                <h4>Лимит Follow в сутки</h4>
                <div class="scale">
                    <span>50</span>
                    <span style="margin-left:16px;">100</span>
                    <span style="margin-left:14px;">150</span>
                    <span style="margin-left:16px;">200</span>
                    <span style="margin-left:16px;">250</span>
                    <span style="margin-left:14px;">300</span>
                    <span style="margin-left:16px;">350</span>
                    <span style="margin-left:14px;">400</span>
                    <span style="margin-left:14px;">450</span>
                    <span style="margin-left:14px;">500</span>
                    <span style="margin-left:16px;">550</span>
                    <span style="margin-left:14px;">600</span>
                    <span style="margin-left:14px;">650</span>
                    <span style="margin-left:14px;">700</span>
                    <span style="margin-left:16px;">750</span>
                    <span style="margin-left:16px;">800</span>
                    <span style="margin-left:16px;">850</span>
                    <span style="margin-left:14px;">900</span>
                    <span style="margin-left:16px;">950</span>
                    <span style="margin-left:16px;">1000</span>
                </div>
                <?= yii\jui\Slider::widget([
                    'id' => 'lfollow',
                    'clientOptions' => [
                        'min' => 50,
                        'max' => 1000,
                        'step' => 50,
                    ],]) ?>


                <?= $form->field($modelForm, 'limitLike')->hiddenInput(['class' => 'num', 'value' => 50])->label(false) ?>

                <h4>Лимит Like в сутки</h4>
                <div class="scale">
                    <span>50</span>
                    <span style="margin-left:16px;">100</span>
                    <span style="margin-left:14px;">150</span>
                    <span style="margin-left:16px;">200</span>
                    <span style="margin-left:16px;">250</span>
                    <span style="margin-left:14px;">300</span>
                    <span style="margin-left:16px;">350</span>
                    <span style="margin-left:14px;">400</span>
                    <span style="margin-left:14px;">450</span>
                    <span style="margin-left:14px;">500</span>
                    <span style="margin-left:16px;">550</span>
                    <span style="margin-left:14px;">600</span>
                    <span style="margin-left:14px;">650</span>
                    <span style="margin-left:14px;">700</span>
                    <span style="margin-left:16px;">750</span>
                    <span style="margin-left:16px;">800</span>
                    <span style="margin-left:16px;">850</span>
                    <span style="margin-left:14px;">900</span>
                    <span style="margin-left:16px;">950</span>
                    <span style="margin-left:16px;">1000</span>
                </div>
                <?= yii\jui\Slider::widget([
                    'id' => 'llike',
                    'clientOptions' => [
                        'min' => 50,
                        'max' => 1000,
                        'step' => 50,
                    ],]) ?>

                <?= '' //$form->field($modelForm, 'tags')->label('Тэги')       ?>

                <div class="ilimits">
                    <h2>На сколько популярных подписываться в день</h2>
                    <?= $form->field($modelForm, 'fpopularFrom')->textInput(['class' => 'num'])->label('от') ?>
                    <?= $form->field($modelForm, 'fpopularTo')->textInput(['class' => 'num'])->label('до') ?>

                    <h2>На сколько рекомендаций подписываться в день</h2>
                    <?= $form->field($modelForm, 'frecomendedFrom')->textInput(['class' => 'num'])->label('от') ?>
                    <?= $form->field($modelForm, 'frecomendedTo')->textInput(['class' => 'num'])->label('до') ?>
                </div>
            </div>
        </div>
    </div>
</div>


<?= '' //$form->field($modelForm, 'accountIdBeforeTrig')->label('На какой аккаунт зенграма передать после завершения триггеров. Если не указано, то размещаются в текущем аккаунте. (здесь будет селект с аккаунтами)')       ?>

<?= $form->field($modelForm, 'frequencyOfPosting')->hiddenInput(['class' => 'num', 'value' => 1])->label(false)       ?>

<!--
    - 1 фото каждый
    - 2-3,
    - 3-5,
    - 6-10.
 -->

<div class="panel panel-settings" id="targeting-panel">
    <div class="panel-header container-fluid">
        <div class="row">
            <div class="col-xs-3 col-sm-2">Триггеры</div>
            <div class="col-xs-9 col-sm-10">
						<span class="visible-md visible-lg">
							Условия окончания кампании
						</span>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-8">
                <input type="checkbox" checked id="ft_chek" class="pull-left" style="margin:7px 10px 0 0;"><?= $form->field($modelForm, 'followersTrigger')->textInput(['class' => 'num'])->label('Сколько подписчиков надо набрать') ?>
                <input type="checkbox" checked id="dt_chek" class="pull-left" style="margin:7px 10px 0 0;"><?= $form->field($modelForm, 'daysTrigger')->textInput(['class' => 'num'])->label('Сколько времени должны идти работы в днях') ?>
                <input type="checkbox" checked id="wt_chek" class="pull-left" style="margin:7px 10px 0 0;"><?= $form->field($modelForm, 'wait')->textInput(['class' => 'num'])->label('Отлёжка аккаунта в днях (дать отлежаться или сразу передавать)') ?>
            </div>
        </div>
    </div>
</div>


<div class="panel panel-settings" id="targeting-panel">
    <div class="panel-header container-fluid">
        <div class="row">
            <div class="col-xs-3 col-sm-2">Параметры остановки кампании</div>
            <div class="col-xs-9 col-sm-10">
						<span class="visible-md visible-lg">
							Технические причины для остановки кампании
						</span>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-8">
                <input type="checkbox" checked id="spass_chek" class="pull-left" style="margin:7px 10px 0 0;"><?= $form->field($modelForm, 'hourPassTrigger')->textInput(['class' => 'num'])->label('Количество слётов паролей в течение часа') ?>
                <input type="checkbox" checked id="hb_chek" class="pull-left" style="margin:7px 10px 0 0;"><?= $form->field($modelForm, 'hourBanTrigger')->textInput(['class' => 'num'])->label('Количество заблокированных в течение часа аккаунтов') ?>
            </div>
        </div>
    </div>
</div>



<button type="submit" class="btn btn-info">Сохранить кампанию</button>
<?php ActiveForm::end(); ?>
<br><br>
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Выбор аккаунта</h4>
            </div>
            <div class="modal-body">
                Введите логин и пароль
                <form>
                    <input type="text" placeholder="Введите логин">
                    <input type="text" type="password" placeholder="Введите пароль">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary">Сохранить изменения</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('input[name="Options[unfollow]"]').change(function () {
        $('.function-status').toggleClass('disable');
    });
    $('#change_akk').click(function () {
        alert('Выбор аккаунтов');
    });
</script>
<script>
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover(
            {
                placement: 'top',
                trigger: 'hover',
                container: 'body'
            }
        );

        $('.helper').popover({placement: 'top'}).unbind('click').on('click', function (e) {
            $(this).popover('toggle');
        });
        $('input[name="CampaignCreatingForm[languages][]"]').change(function() {
            //alert(this.checked);
            //alert($('input[name="CampaignCreatingForm[languages][]"]:checkbox').first().value);
            //$('.' + this.value).hide();
            //var sitevalue = $('#campaigncreatingform-languages input:checkbox:checked').val();
            //alert(sitevalue);
            $('.ru').parent().hide();
            $('.en').parent().hide();
            $('.es').parent().hide();
            $('#campaigncreatingform-languages input:checkbox:checked').each(function(){
                //alert($(this).val());
                $('.' + $(this).val()).parent().show();
            });
        });

    });
    $('#lfollow').slider({
        change: function(event, ui) {
            $('#campaigncreatingform-limitfollow').val(ui.value);
        }
    });
    $('#llike').slider({
        change: function(event, ui) {
            $('#campaigncreatingform-limitlike').val(ui.value);
        }
    });

    $('#llike').slider({
        change: function(event, ui) {
            $('#campaigncreatingform-limitlike').val(ui.value);
        }
    });

    $('#fslider').slider({
        change: function(event, ui) {
            $('#campaigncreatingform-frequencyofposting').val(ui.value);
        }
    });

    $('#ft_chek').change(function() {
        $('#campaigncreatingform-followerstrigger').prop('disabled', !this.checked);
    });

    $('#dt_chek').change(function() {
        $('#campaigncreatingform-daystrigger').prop('disabled', !this.checked);
    });

    $('#wt_chek').change(function() {
        $('#campaigncreatingform-wait').prop('disabled', !this.checked);
    });

    $('#spass_chek').change(function() {
        $('#campaigncreatingform-hourpasstrigger').prop('disabled', !this.checked);
    });
    $('#hb_chek').change(function() {
        $('#campaigncreatingform-hourbantrigger').prop('disabled', !this.checked);
    });



</script>