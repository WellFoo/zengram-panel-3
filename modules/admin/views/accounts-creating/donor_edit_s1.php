<?php
use app\modules\admin\models\Donor;
use app\modules\admin\models\DonorForm;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var \yii\web\View $this
 * @var DonorForm $donor
 */

$this->title = $donor->isNew ? 'Новый донор' : 'Донор '.$donor->login;
?>
<div class="donor-edit-s1">
	<h2><?= $this->title ?></h2>

	<?php
	$form = ActiveForm::begin([
		'enableAjaxValidation' => true,
		'validateOnChange' => true,
		'validationUrl' => ['donor-validation']
	]);

	echo $form->field($donor, 'login');

	echo $form->field($donor, 'sex')->radioList(Donor::sexesLabels());

	echo $form->field($donor, 'languages')->checkboxList(Donor::languagesLabels(), [
		'item' => function ($index, $label, $name, $checked, $value) {
			$result = Html::beginTag('div', ['class' => 'checkbox']);
			$result .= Html::beginTag('label');
			$result .= Html::input('checkbox', $name, $value, [
				'onchange' => 'switchDescriptions(this);',
				'checked' => $checked
			]);
			$result .= $label;
			$result .= Html::endTag('label');
			$result .= Html::endTag('div');
			return $result;
		}
	]);

	echo $form->field($donor, 'descriptions')->begin();
	echo Html::activeLabel($donor, 'descriptions', ['class' => 'control-label']);
	echo Html::activeInput('hidden', $donor, 'descriptions', ['value' => '']);
	foreach (Donor::languagesLabels() as $code => $label) {
		echo Html::beginTag('div', [
			'class' => 'input-group',
			'id'    => 'description-'.$code,
			'style' => 'margin-bottom: 5px;'.(!in_array($code, $donor->languages) ? 'display: none;' : '')
		]);
		echo Html::tag('span', ucfirst($code), ['class' => 'input-group-addon']);
		echo Html::activeTextarea($donor, 'descriptions['.$code.']', [
				'class' => 'form-control',
				'disabled' => !in_array($code, $donor->languages)
		]);
		echo Html::endTag('div');
	}
	echo Html::error($donor,'descriptions', ['class' => 'help-block help-block-error']);
	echo $form->field($donor, 'descriptions')->end();

	echo $form->field($donor, 'links')->textarea();
	?>

	<div class="form-group">
		<?= Html::submitButton('Далее', ['class' => 'btn btn-primary']) ?>
		<?= Html::a('Отмена', ['donors'], ['class' => 'btn btn-default']) ?>
	</div>

	<?php $form::end() ?>
</div>

<script>
function switchDescriptions(el)
{
	var $cont = $('#description-' + el.value);
	$cont.toggle(el.checked);
	$cont.find('textarea').prop('disabled', !el.checked);
}
</script>