<?php namespace app\modules\partnership\models;

use app\modules\partnership\Module;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%partnership_partners}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $parent_id
 * @property string  $name
 * @property string  $ref_key
 * @property string  $walet
 * @property string  $referal_link
 * @property User    $user
 * @property Partner   $parent
 * @property Partner[] $childs
 * @property integer   $childsCount
 * @property Referal[] $referals
 * @property integer   $referalsCount
 * @property Refund[]  $refunds
 * @property integer   $refundsCount
 * @property float     $refundsSum
 * @property Payment[] $referalsPayments
 * @property Payment[] $referalsPaymentsUnrefunded
 * @property integer   $referalsPaymentsCount
 * @property integer   $referalsPaymentsSum
 * @property float     $balance
 */
class Partner extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%partnership_partners}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['user_id', 'ref_key'], 'required'],
			[['user_id', 'parent_id'], 'integer'],
			['walet', 'match', 'pattern' => '/^[BEGKRUXYZ]\d{12}$/i'],
			['ref_key', 'string', 'max' => 32],
			['name', 'string'],
			['user_id', 'unique'],
			['ref_key', 'unique'],
			['reg_date', 'safe'],
			['balance', 'number'],
			[['walet', 'name'], 'default', 'value' => ''],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'           => Module::t('module', 'ID'),
			'user_id'      => Module::t('module', 'User ID'),
			'ref_key'      => Module::t('module', 'Ref Key'),
			'parent_id'    => Module::t('module', 'Parent ID'),
			'reg_date'     => Module::t('module', 'Registration date'),
			'name'         => Module::t('module', 'Name'),
			'walet'        => Module::t('module', 'Walet ({type})', ['type' => 'WMR']),
			'balance'      => Module::t('module', 'Balance'),
			'referal_link' => Module::t('module', 'Referal link'),
		];
	}

	public function getUser()
	{
		return self::hasOne(User::className(), ['id' => 'user_id']);
	}

	public function getParent()
	{
		return self::hasOne(self::className(), ['id' => 'parent_id']);
	}

	public function getChilds()
	{
		return self::hasMany(self::className(), ['parent_id' => 'id']);
	}

	public function getChildsCount()
	{
		return (int) $this->getChilds()->count();
	}

	public function getReferals()
	{
		return self::hasMany(Referal::className(), ['partner_id' => 'id']);
	}

	public function getReferalsCount()
	{
		return (int) $this->getReferals()->count();
	}

	public function getRefunds()
	{
		return self::hasMany(Refund::className(), ['partner_id' => 'id']);
	}

	public function getRefundsCount()
	{
		return (int) $this->getRefunds()->count();
	}

	public function getRefundsSum()
	{
		return (double) $this->getRefunds()->sum('amount');
	}

	public function getReferalsPayments()
	{
		return self::hasMany(Payment::className(), ['partner_id' => 'id']);
	}

	public function getReferalsPaymentsUnrefunded()
	{
		return $this->getReferalsPayments()->where('bonus > refunded');
	}

	public function getReferalsPaymentsCount()
	{
		return (int) $this->getReferalsPayments()->count();
	}

	public function getReferalsPaymentsSum()
	{
		return (double) $this->getReferalsPayments()->sum('amount');
	}

	public function getBalance()
	{
		return $this->getReferalsPayments()
			->where(['is_refunded' => false])
			->sum('amount');
	}

	/**
	 * @param integer $id
	 * @return self|null
	 */
	public static function findByUserId($id)
	{
		return self::find()->where(['user_id' => intval($id)])->one();
	}

	public function generateReferalKey()
	{
		do {
			$key = md5(Yii::$app->security->generateRandomString());
		} while (self::find()->where(['ref_key' => $key])->count() > 0);
		$this->ref_key = $key;
	}

	public function getReferal_link()
	{
		return Url::to(['/', 'p' => $this->ref_key], true);
	}

	public function addBalance($data /*$amount, $ref_id, $ref_level*/)
	{

		$percent = $data['referral_level'] === 1 ? Module::$settings->percent_1lvl : Module::$settings->percent_2lvl;
		$payment = new Payment();
		$payment->referal_id = $data['referral'];
		$payment->partner_id = $this->id;
		$payment->amount = (double) $data['amount'];
		$payment->bonus = (double) $data['amount'] * $percent;
		$payment->referral_level = $data['referral_level'];
		$payment->description = $data['description'];
		$payment->save();

		$this->balance += $payment->bonus;
		$this->save();
	}

	public function subBalance($amount)
	{
		$this->balance -= (double) $amount;
	}
}
