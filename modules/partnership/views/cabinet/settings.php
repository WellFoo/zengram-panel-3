<?php
/**
 * @var \yii\web\View $this
 * @var app\modules\partnership\forms\PartnerForm $model
 */
use app\modules\partnership\Module;
use yii\helpers\Html;

$this->title = Module::t('module', 'Profile settings');
$this->registerCssFile('/css/partner.css');
?>

<a class="back_btn" href="/partnership/">Вернуться в кабинет партнера</a>

<h1><?= $this->title ?></h1>

<?php
$form = \yii\widgets\ActiveForm::begin([

]);
//echo $form->field($model, 'name');
echo $form->field($model, 'walet')->textInput(['placeholder' => 'WMR000000000000']);

echo Html::submitButton(Module::t('module', 'Save'), ['class' => 'btn btn-success']);

$form::end();
?>
