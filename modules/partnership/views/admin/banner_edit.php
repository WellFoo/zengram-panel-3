<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\partnership\models\Banner $model
 */

use app\modules\partnership\Module;
use yii\helpers\Html;

$this->title = Module::t('module', $model->isNewRecord ? 'Banner create' : 'Banner edit');
?>

<h1><?= $this->title ?></h1>

<?php
$form = \yii\widgets\ActiveForm::begin();

echo $form->field($model, 'code')->textarea()->hint(Module::t('module', 'Place {{URL}} in a place where the referral link should be placed.'));

echo Html::submitButton(Module::t('module', 'Save'), ['class' => 'btn btn-success']);
echo Html::a(Module::t('module', 'Cancel'), ['banners'], ['class' => 'btn btn-default']);

$form::end();
?>