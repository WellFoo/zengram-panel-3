<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var string $date_from
 * @var string $date_to
 * @var integer $status
 * @var integer $sum
 *
 */


use app\modules\partnership\Module;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use app\modules\partnership\models\Partner;
use app\modules\partnership\models\Refund;
use app\modules\partnership\models\User;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;


$date_to = Yii::$app->request->get('date_to');
$date_from = Yii::$app->request->get('date_from');

if (empty($date_from) || empty($date_to)) {
	$now = new \DateTime();
	$now->setDate($now->format('Y'), (int)$now->format('m') + 1, 1);
	$date_to = $now->format('Y-m-d');
	$now->sub(new \DateInterval('P1M'));
	$date_from = $now->format('Y-m-d');
}

$status = (int)Yii::$app->request->get('status', 0);
$sum = (int)Yii::$app->request->get('sum', 0);

$query = Refund::find()->select([
	Refund::tableName() . '.*',
	User::tableName() . '.mail',
	Partner::tableName() . '.walet',
]);

$query->leftJoin(Partner::tableName(),
	Partner::tableName() . '.id = ' . Refund::tableName() . '.partner_id'
);

$query->leftJoin(User::tableName(),
	User::tableName() . '.id = ' . Partner::tableName() . '.user_id'
);

$query->andWhere(['>=', 'date', $date_from]);
$query->andWhere(['<', 'date', $date_to]);

if ($status !== 0) {
	$query->andWhere([
		'is_refunded' => $status - 1 // Супер костыль
	]);
}

if ($sum === 1) {
	$query->andWhere(['>=', 'amount', Module::$settings->min_refund]);
} else if ($sum === 2) {
	$query->andWhere(['<', 'amount', Module::$settings->min_refund]);
}

$dataProvider = new ActiveDataProvider([
	'query' => $query->asArray(),
	'sort' => [
		'defaultOrder' => [
			'date' => SORT_ASC
		]
	]
]);

?>

<style>

	.refunds .form-control {
		height: 34px !important;
		border-radius: 0 !important;
		border: 1px solid #ccc !important;
	}

	.refunds [type=submit] {
		border-radius: 0 4px 4px 0 !important;
	}

	thead .form-control {
		cursor: pointer;
	}

	thead .form-control:hover {
		border-color: #006600;
	}

	.nav-tabs {
		margin-bottom: 35px;
	}

	.refund-status-update {
		border-radius: 20px 0 0 20px !important;
	}

	.refund-status-decline {
		border-radius: 0 20px 20px 0 !important;
	}

</style>

<form class="form-horizontal refunds">
	<div class="form-group">
		<label class="control-label col-xs-2">За период</label>
		<div class="col-xs-5">
			<div class="input-group">
				<span class="input-group-addon">С</span>
				<?= Html::input('date', 'date_from', $date_from, ['class' => 'form-control']) ?>
				<span class="input-group-addon">По</span>
				<?= Html::input('date', 'date_to', $date_to, ['class' => 'form-control']) ?>
				<span class="input-group-btn">
					<button type="submit" class="btn btn-success">Применить</button>
				</span>
				<?= Html::input('hidden', 'status', $status) ?>
				<?= Html::input('hidden', 'sum', $sum) ?>
			</div>
		</div>
	</div>
</form>

<?php

$form = ActiveForm::begin([
	'id' => 'refund-form',
]);

?>

<input type="hidden" name = "refund-id"/>
<input type="hidden" name = "action"/>
<input type="hidden" name = "message"/>

<?php

$form->end();

?>

<!-- Площадки -->
<div class="modal fade" id="decline">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">

				<?php

				echo Html::dropDownList(
					'default_message', 0,
					[
						1 => 'Недостаточно средств на счету.',
						100 => 'Иное'
					],
					[
						'prompt' => 'Выберите причину...',
						'class' => 'form-control',
					]
				).'<br/>';

				echo Html::textarea('message', '',
					['class' => 'form-control top-margin hide']
				);

				?>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default right" data-dismiss="modal">Отмена</button>
				<button type="button" class="btn btn-danger right send-decline">Отклонить</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<?= \yii\grid\GridView::widget([
	'dataProvider' => $dataProvider,
	'columns' => [
		[
			'attribute' => 'partner_id',
			'label' => 'Партнёр',
			'format' => 'raw',
			'value' => function($item){

				$result = $item['mail'];

				if ($item['is_refunded'] == 1) {
					return $result;
				} else {
					$result .= '<br/>Текущий баланс: ';
				}

				/* @var $partner \app\modules\partnership\models\Partner */
				$partner = \app\modules\partnership\models\Partner::findOne($item['partner_id']);

				$result .= '<strong>'.$partner->balance.'</strong> руб.';

				return $result;
			}
		], [
			'label' => 'Кошелёк',
			'value' => function($item) {

				if ($item['is_refunded'] == 1) {
					if ($item['target'] === \app\modules\partnership\models\Refund::TYPE_INNER) {
						return 'Внутренний счёт';
					} else {
						return $item['wallet'];
					}

				} else {
					return $item['walet'];
				}

			}
		], [
			'attribute' => 'date',
			'label' => 'Дата',
			'value' => function($item) {
				return date('d-m-Y', strtotime($item['date']));
			}
		], [
			'attribute' => 'amount',
			'format' => 'currency',
			'label' => 'Сумма',
			'header' => Html::dropDownList(
				'sum',
				$sum,
				[
					1 => '>= '.Module::$settings->min_refund,
					2 => '< '.Module::$settings->min_refund
				],
				[
					'prompt' => '',
					'class' => 'form-control filter',
				]
			),
		], [
			'label' => 'Статус',
			'format' => 'raw',
			'attribute' => 'is_refunded',
			'header' => Html::dropDownList(
				'status',
				$status,
				[
					1 => 'Не выведено',
					2 => 'Выведено',
				],
				[
					'prompt' => '',
					'class' => 'form-control filter',
				]
			),
			'value' => function($item) {

				if ($item['is_refunded'] == 1) {
					return 'Выведено';
				} else {

					return Html::submitButton(
						'Подтвердить',
						[
							'class' => 'btn btn-success refund-status-update',
							'data-id' => $item['id']
						]
					) . Html::submitButton(
						'Отклонить',
						[
							'class' => 'btn btn-danger refund-status-decline',
							'data' => [
								'id' => $item['id'],
								'target' => '#decline',
								'toggle' => 'modal'
							]
						]
					);

				}

			}
		],
	]
]) ?>

<script>

	var $refundForm = $('#refund-form'),
		$action = $refundForm.find('[name=action]'),
		$id = $refundForm.find('[name=refund-id]'),
		$message = $refundForm.find('[name=message]'),
		$filterForm = $('.refunds'),
		$declineModal = $('#decline'),
		$modalDefault = $declineModal.find('[name=default_message]'),
		$modalMessage = $declineModal.find('[name=message]');

	$('.refund-status-update').click(function(){

		var $this = $(this);

		$action.val('update');
		$id.val($this.attr('data-id'));
		$refundForm.submit();

	});

	$declineModal.find('[name = default_message]').change(function(){

		var textarea = $declineModal.find('[name = message]');

		if ($(this).val() == 100) {
			textarea.removeClass('hide');
		} else {
			textarea.addClass('hide');
		}

	});

	$('.refund-status-decline').click(function(){

		var $this = $(this);

		$action.val('decline');
		$id.val($this.attr('data-id'));


	});

	$('.send-decline').click(function(){

		var message = false,
			value = $modalDefault.val();

		if (value != 100 && value != 0) {
			message = $modalDefault.find(':selected').text();
		} else {
			message = $modalMessage.val();
		}

		if (message == '') {
			return false;
		}

		$message.val(message);
		$refundForm.submit();

	});

	$('.filter').change(function(){

		var $this = $(this),
			value = $this.val(),
			name = this.name;

		$filterForm.find('[name = ' + name + ']').val(value);
		$filterForm.submit();

	});

</script>


