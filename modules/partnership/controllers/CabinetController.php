<?php namespace app\modules\partnership\controllers;

use app\modules\partnership\components\Controller;
use app\modules\partnership\forms\RefundForm;
use app\modules\partnership\models\Partner;
use app\modules\partnership\forms\PartnerForm;
use app\modules\partnership\models\Payment;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use app\modules\partnership\models\Refund;
use Yii;

class CabinetController extends Controller
{
	public function actionIndex()
	{
		$partner = $this->getPartner();

		$refund_form = new RefundForm();
		$refund_form->partner = $partner;
		$refund_form->walet = $partner->walet;
		if ($refund_form->load(\Yii::$app->request->post()) && $refund_form->refund()) {
			if ($refund_form->target == Refund::TYPE_WALLET){
				if ($refund_form->amount > 1000){
					return $this->redirect('/partnership/cabinet/index/?cashback=1');
				} else{
					return $this->redirect('/partnership/cabinet/index/?cashback=2');
				}

			}
			if ($refund_form->target == Refund::TYPE_INNER){
				return $this->redirect('/partnership/cabinet/index/?cashback=3');
			}

		}

		$data_provider = new ActiveDataProvider([
			'query' => $partner->getReferalsPaymentsUnrefunded(),
			'sort' => [
				'defaultOrder' => ['date' => SORT_DESC],
			]
		]);

		$query = Payment::find()
			->where(['partner_id' => $partner->id])
			->andWhere('bonus > refunded');

		$sum = (double) $query->sum('bonus');
		$sum_refunded = (double) $query
			->andWhere(['>', 'refunded', 0])
			->sum('refunded');
		$model = new PartnerForm();

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$partner->walet = $model->walet;
			$partner->save();
		}


		return $this->render('index', [
			'partner'       => $partner,
			'data_provider' => $data_provider,
			'sum'           => $sum - $sum_refunded,
			'refund_form'   => $refund_form,
			'model' => $model,
		]);
	}

	public function actionSettings()
	{
		$partner = $this->getPartner();

		$model = new PartnerForm();
		$model->setPartner($partner);

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['index']);
		}

		return $this->render('settings', [
			'model' => $model
		]);
	}

	public function actionBanners()
	{
		$partner = $this->getPartner();

		return $this->render('banners', [
			'partner' => $partner
		]);
	}

	public function actionValidateRefund()
	{
		\Yii::$app->response->format = Response::FORMAT_JSON;
		$partner = $this->getPartner();
		$model = new RefundForm();
		$model->partner = $partner;
		$model->load(\Yii::$app->request->post());
		return ActiveForm::validate($model);
	}

	public function actionHistory()
	{
		$partner = $this->getPartner();

		$data_provider = new ActiveDataProvider([
			'query' => $partner->getRefunds(),
			'sort' => [
				'defaultOrder' => ['date' => SORT_DESC]
			]
		]);

		return $this->render('history', [
			'data_provider' => $data_provider,
		]);
	}

	/**
	 * @return Partner|null
	 * @throws ForbiddenHttpException
	 */
	private function getPartner()
	{
		$partner = Partner::findByUserId(\Yii::$app->user->id);
		if (is_null($partner)) {
			throw new ForbiddenHttpException();
		}
		return $partner;
	}
}