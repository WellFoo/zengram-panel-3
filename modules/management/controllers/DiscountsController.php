<?php

namespace app\modules\management\controllers;

use app\models\Discounts;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class DiscountsController extends Controller
{
	public function actionIndex()
	{
		$discounts = new ActiveDataProvider([
			'query' => Discounts::find()
		]);

		return $this->render('index', [
			'discounts' => $discounts
		]);
	}

	public function actionCreate()
	{
		return $this->actionUpdate();
	}

	public function actionUpdate($id = null)
	{
		if (!is_null($id)) {
			$model = Discounts::findOne($id);

			if (is_null($model)) {
				throw new NotFoundHttpException('Скидка с таким ID не найдена');
			}
		} else {
			$model = new Discounts();
		}

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['index']);
		}

		return $this->render('edit', [
			'model' => $model
		]);
	}

	public function actionRemove($id)
	{
		/** @var Discounts $model */
		$model = Discounts::findOne($id);
		if (is_null($model)) {
			throw new NotFoundHttpException('Скидка с таким ID не найдена');
		}
		$model->delete();
		return $this->redirect(['index']);
	}
}