<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $campaigns
 */
use yii\helpers\Html;

$this->title = 'Рассылки';
?>
<div class="campaigns">
	<h2><?= $this->title ?></h2>

	<p>
		<?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= \yii\grid\GridView::widget([
		'dataProvider' => $campaigns,
		'columns' => [
			[
				'attribute' => 'name',
				'format' => 'raw',
				'value' => function($item) {
					return Html::a(
						$item->name,
						'http://cp.unisender.ru/'.Yii::$app->language.'/v5/bursts/view/'.$item->unisender_id,
						['target' => '_blank']
					);
				}
			],
			'create_date:date'
		]
	]) ?>
</div>

