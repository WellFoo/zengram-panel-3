<?php return [
	//перевод для карт
	'en_US' => 'ru_RU',
	'Default city' => 'Москва',
	'/img/telefon.jpg' => Yii::getAlias('@img/telefon.jpg'),

	// account.js
	'On 10/07/2016 there are some problems of accounts authorization from instagram (5% of cases). Currently instagram denied authorization for your account @{account}. This can happen on any device or service that you are using. We found out that the problem is solved by itself during the day. We offer you is to set the settings of your project and get familiar with our service. Once your account is authorized, the project will automatically start, which we will inform you by email @{email}.' => 'С 10.07.2016 наблюдаются некоторые проблемы авторизации аккаунтов со стороны инстаграм (в 5% случаев). В данный момент инстаграм отказал в авторизации вашего аккаунта @{account}. Такое может произойти на любом устройстве или сервисе, которым вы пользуетесь. Мы выяснили что проблема решается сама собой в течении суток. Мы предлагаем вам пока выставить настройки вашего проекта и ознакомиться с нашим сервисом. Как только ваш аккаунт пройдет авторизацию, проект автоматически запуститься, о чем мы вас проинформируем на электронную почту @{email}.',

	'Dear Customer! The tinctures of the project you have chosen to target {target}, but does not have data to work with. Enter information or select a different target and run the project'
	=> 'Уважаемый клиент! В настойках проекта вы выбрали работу по {target}, но не указали их. Укажите правильные настройки и запустите проект.',

	'Search by cities' => 'городам',
	'Search by hashtags' => 'хештегам',
	'Search by accounts' => 'аккаунтам',

	'No points for work' => 'Нет точек для работы',

	'New comments' => 'Новые комментарии',
	'Account @{account} have incorrect username or password.' => 'У аккаунта @{account} неправильные логин или пароль.',
	'Wrong username or password.' => 'Неправильный логин или пароль',
	'It is necessary to verify @{account} Instagram account. Make this action on <a target="_blank" href="https://instagram.com">Instagram.com</a> or in your Instagram app and we will start work.' => 'Требуется подтвердить @{account} Instagram аккаунт. Выполните это действие на сайте <a target="_blank" href="https://instagram.com">Instagram.com</a> или в вашем Instagram приложение на смартфоне и мы начнем работать.',
	'It is necessary to pass verification.' => 'Необходимо пройти верификацию.',
	'A limit of followings of the account @{account} is exceeded. It is necessary to unfollow.' => 'У аккаунта @{account} превышен лимит подписок. Необходимо включить отписки.',
	'The limit of followings is exceeded. It is necessary to unfollow.' => 'Превышен лимит подписок. Необходимо включить отписки.',
	'A limit of unfollowings of the account @{account} is exceeded. It is necessary to follow.' => 'У аккаунта @{account} лимит отписок. Необходимо включить подписки.',
	'Work of unfollowings is complete' => 'Достигнут лимит отписок. Необходимо включить подписки.',
	'The account @{account} has no points for work. It is necessary to adjust geography of spread.' => 'У аккаунта @{account} нет точек для работы. Необходимо настроить географию распрострения.',
	'There are no points for work. It is necessary to adjust geography of spread.' => 'Нет точек для работы. Необходимо настроить географию распрострения.',
	'The chosen points of the account @{account} contain few publications. It is necessary to choose other geopoint, or to expand the promoting area.' => 'Выбранные точки у аккаунта @{account} содержат очень мало публикаций. Просим выбрать другую геоточку, или расширить область продвижения.',
	'The chosen by you points contain few publications. It is necessary to choose other geopoint, or to expand the promoting area.' => 'Выбранные вами точки содержат очень мало публикаций. Просим выбрать другую геоточку, или расширить область продвижения.',
	'The account @{account} has no hashtags for work. It is necessary to add hashtags.' => 'У аккаунта @{account} нет хештегов для работы. Необходимо добавить хештеги.',
	'There are no hashtags for work. It is necessary to add hashtags.' => 'Нет хештегов для работы. Необходимо добавить хештеги.',
	'The chosen hashtags of the account @{account} contain few publications. It is necessary to choose other hashtags.' => 'Выбранные хештеги у аккаунта @{account} содержат очень мало публикаций. Просим выбрать другие хештеги.',
	'The account @{account} has no accounts for work. It is necessary to add accounts.' => 'У аккаунта @{account} нет пользователей для работы. Необходимо добавить пользователей.',
	'There are no accounts for work. It is necessary to add accounts.' => 'Нет пользователей для работы. Необходимо добавить пользователей.',
	'The chosen by you hashtags contain few publications. It is necessary to choose other hashtags.' => 'Выбранные вами хештеги очень мало публикаций. Просим выбрать другие хештеги.',
	'The account @{account} has no list of comments for adding. It is necessary to add comments, or to disconnect commenting.' => 'У аккаунта @{account} нет списка комментариев для добавления. Необходимо добавить комментарии, или отключить комментирование.',
	'There is no list of comments for additing. It is necessary to add comments, or to disconnect commenting.' => 'Нет списка комментариев для добавления. Необходимо добавить комментарии, или отключить комментирование.',
	'There were error while uploading media to account @{account}. Try uploading another one after making some actions.' => 'Произошла ошибка загрузки медиа для аккаунта @{account}. Попробуйте загрузить другое медиа после выполнения нескольких действий',
	'days' => 'дней',

	'Action paused' => 'Действие находится на паузе',

	// content.js
	'Image uploaded' => 'Изображение добавлено',
	'Instagram error.' => 'Ошибка instagram.',
	'Error' => 'Ошибка',
	'Are you sure want to remove comment?' => 'Вы уверены, что хотите удалить комментарий?',

	// global.js
	'Error sending request' => 'Ошибка отправки запроса',

	// options-new.js
	'Are you sure want to remove this account?' => 'Вы уверены, что хотите удалить выбранный аккаунт?',
	'Are you sure want to remove this city?' => 'Вы уверены, что хотите удалить выбранный город?',
	'Add a city first' => 'Добавьте город',

	// pubimages.js
	'Image added to queue' => 'Изображение добавлено в очередь',

	//actions.js
	'success' => 'успешно',
];