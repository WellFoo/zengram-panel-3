<?php return [
	'Buy Real Instagram Followers, Followers On Instagram, Get Instagram Followers, Get More Instagram Followers, Real Instagram Followers' => 'Купить реальных подписчиков Instagram, подписчики Instagram, Получить подписчиков Instagram, Получить больше подписчиков Instagram, Реальные подписчики Instagram',

	// account/_account
	'You have' => 'У вас',
	'Experimental' => 'Тестовый',
	'photos' => 'фото',
	'Photo' => 'Фото',
	'Number of account&#39;s photos' => 'Количество фотографий вашего аккаунта',
	'Add photo' => 'Добавить фото',
	'Comment' => 'Комментировать',
	'Change password' => 'Сменить пароль',
	'New comments: {count}' => 'Новые комментарии: {count}',
	'Clear' => 'Очистить',
	'Clear number of new comments' => 'Очищает количество новых комментариев',
	'Followers' => 'Подписчики',
	'New followers' => 'Новые подписчики',
	'Followings' => 'Подписки',
	'Likes' => 'Лайки',
	'Comments' => 'Комментарии',
	'Follows' => 'Подписываться',
	'Unfollows' => 'Отписываться',
	'Reset activity counter' => 'Сбросить счётчик действий',
	'Action' => 'Действие',
	'Here you can start or stop work with project' => 'Здесь вы можете запустить или остановить работу сервиса',
	'Project stopped' => 'Проект остановлен',
	'Start' => 'Запустить',
	'Stop' => 'Остановить',
	'Speed of work' => 'Скорость работы',
	'Service work speed. We are highly recommending to choose Slow speed for first days and to increase speed with time' => 'Скорость работы сервиса. Настоятельно рекомендуем в первые дни запуска выбирать низкую, и постепенно повышать',
	'speed of work' => 'скорость<br/>работы',
	'Speed' => 'Скорость',
	'Slow' => 'Медленная',
	'Medium' => 'Средняя',
	'Fast' => 'Быстрая',
	'Timer of days' => 'Таймер дней',
	'Time is counting down only when your activity is started. When time ends, work will be stopped' => 'Через указанное количество дней сервис автоматически остановит работу с вашим аккаунтом',
	'timer of days' => 'таймер<br/>дней',
	'Timer' => 'Таймер',
	'Stop after' => 'Остановить через',
	'Unavailable' => 'Недоступно',
	'You must stop works in account to change timer' => 'Для использования функции таймера необходимо остановить проект',
	'Off' => 'Выкл',
	'1 hour'   => '1 час',
	'2 hours'  => '2 часа',
	'3 hours'  => '3 часа',
	'4 hours'  => '4 часа',
	'5 hours'  => '5 часов',
	'6 hours'  => '6 часов',
	'7 hours'  => '7 часов',
	'8 hours'  => '8 часов',
	'9 hours'  => '9 часов',
	'10 hours' => '10 часов',
	'11 hours' => '11 часов',
	'12 hours' => '12 часов',
	'13 hours' => '13 часов',
	'14 hours' => '14 часов',
	'15 hours' => '15 часов',
	'16 hours' => '16 часов',
	'17 hours' => '17 часов',
	'18 hours' => '18 часов',
	'19 hours' => '19 часов',
	'20 hours' => '20 часов',
	'21 hours' => '21 час',
	'22 hours' => '22 часа',
	'23 hours' => '23 часа',

	'1 day' => '1 день',
	'2 days' => '2 дня',
	'3 days' => '3 дня',
	'5 days' => '5 дней',
	'10 days' => '10 дней',
	'20 days' => '20 дней',
	'30 days' => '30 дней',
	'60 days' => '60 дней',
	'Settings' => 'Настройки',
	'Here you can go to the settings and change action&#39;s configuration, locations, comments and other settings' => 'Здесь вы можете перейти в настройки проекта и указать настройки действий, регион продвижения, комментарии и иные настройки',

	// account/add
	'It remains only to verify Instagram Account.<br> Do it online on <a target="_blank" href="https://instagram.com">Instagram.com</a><br>or open the Instagram app right now<br>on your smartphone to end the<br>activation and we’ll start working with your account.' => 'Остальсо только подтвердить аккаунт Instagram.<br>Это можно сделать на сайте <a target="_blank" href="https://instagram.com">Instagram.com</a><br>или открыв приложение<br>в смартфоне, чтобы завершить<br>активацию, и мы начнем работать с вашим аккаунтом.',
	'Add account' => 'Добавить аккаунт',
	'<strong style="font-weight: bold;">Dear client.</strong><br>We remind that withdrawal of time from your account for direct using of service supposed to be multiplied by number of accounts. For example, if you have 2 Instagram accounts, withdrawal for one working hour would be 2 hours. And if you have one Instagram account, withdrawal for one working hour would be one hour.' => '<strong style="font-weight: bold;">Уважаемый&nbsp;клиент!</strong><br>Напоминаем, что списание времени с Вашего баланса за фактическое использование сервиса умножается на количество добавленных Вами аккаунтов. Если у Вас в работе 2 аккаунта Инстаграм, то за один фактический час работы сервиса с баланса будет списано 2 часа; если 1 аккаунт в работе, то за 1 час работы сервиса будет списываться 1 час с Вашего баланса.',

	// account/change
	'Login and password settings' => 'Настройка логина и пароля',
	'Enter new password' => 'Введите новый пароль',
	'Save settings' => 'Сохранить настройки',

	// account/index
	'There was an error deleting your account' => 'Ошибка при удалении аккаунта',
	'Your account has successfully been deleted' => 'Аккаунт успешно удален',
	'Start all' => 'Запустить все',
	'Stop all' => 'Остановить все',
	'Time limit:' => 'Оставшийся лимит времени:',
	'Time:' => 'Лимит времени:',
	'days' => 'дней',
	'hours' => 'часов',
	'minutes' => 'минут',
	'Buy time package' => 'Купить время',
	'Buy' => 'Купить',
	'Demo project' => 'Ознакомительный проект',
	'<h3>Dear friends!</h3><p>Our project will have first birthday soon. We want to become better for you, so there are next promotion:</p><p>Report, what in your opinion we should make better, or founded imperfection in our service. We will consider it in our work and present you free time.</p>' => '<h3>Дорогие друзья!</h3><p>Нашему проекту скоро исполняется год. Мы хотим стать еще лучше для Вас и проводим следующую АКЦИЮ:</p><p>Напишите, что по Вашему мнению нам стоит улучшить. Или найденные недочеты в нашем сервисе. А мы их учтем в своей работе и подарим Вам бесплатное время.</p>',
	'Thanks!' => 'Спасибо!',
	'Your feedback is important for us!' => 'Нам важно Ваше мнение!',
	'Zengram&nbsp;recommends you to check your first account before start:' => 'Zengram&nbsp;рекомендует проверить Вам свой первый аккаунт перед запуском:',
	'Age of your account is more than 2 weeks' => 'Возраст Вашего аккаунта более 2 недель',
	'You already have some followers' => 'У Вас уже есть несколько подписчиков',
	'The account connected with your Facebook' => 'Аккаунт имеет привязку к Вашему фэйсбуку',
	'The account contains more than 10 publications' => 'Аккаунт содержит более 10 публикаций',
	'You don&#39;t use other services of Instagram promotion' => 'Вы не пользуетесь другими сервисами раскрутки Инстаграм',
	'According our experience, completing all these requirements makes promotion faster and more reliable.' => 'По нашему опыту, выполнение данных требований делает процесс раскрутки более быстрым и надежным.',
	'Press OK, and the project will start. By adding other accounts you won&#39;t see this message again.' => 'Нажмите "OK", и проект запустится. При добавлении других аккаунтов Вы больше не увидите данное сообщение.',
	'Add Instagram account' => 'Добавьте Instagram аккаунт',
	'Your privacy is very important for us!<br>That&#39;s why we don&#39;t store your password,<br>it will be used only for getting necessary API data for Instagram' => 'Ваша конфиденциальность очень важна<br>для нас! Поэтому мы не храним Ваш пароль,<br>он будет использован только для получения<br>необходимых для Instagram API данных.',
	'Log' => 'Лог',
	'1. Select image for upload' => '1. Выберите фото для загрузки',
	'2. Change zoom settings (if necessary)' => '2. Произведите настройки масштаба (при необходимости).',
	'3. Enter description (if necessary). Spintax allowed' => '3. Введите описание фото (при необходимости). Поддерживается spintax',
	'4. Enter pauses' => '4. Введите паузы',
	'From' => 'От',
	'to' => 'до',
	'5. Select projects to proceed' => '5. Выберите проекты для работы',
	'Select all' => 'Выбрать всех',
	'Incorrect account&#39;s username or password' => 'Неправильный логин или пароль у аккаунта',
	'Limit of followings is exceeded' => 'Превышен лимит подписчиков',
	'A limit of followings of the account <span id="{span_id}"></span> is exceeded. It is necessary to unfollow.' => 'У аккаунта <span id="{span_id}"></span> превышен лимит подписчиков. Необходимо включить отписки.',
	'No points for work' => 'Нет рабочих точек',
	'The account <span id="{span_id}"></span> has no points for work. It is necessary to adjust geography of spread.' => 'У аккаунта <span id="{span_id}"></span> нет точек для работы. Необходимо настроить географию распрострения.',
	'There are not enough funds for this operation on your account. Please, <a href="{url}">recharge balance</a>, and retry operation.' => 'В настоящий момент проекты не могут быть запущены по причине отрицательного баланса. Чтобы продолжить работу, нужно пополнить баланс. <div class="text-center"><a href="/{url}"><button type="button" class="btn btn-success btn-lg">OK</button></a></div>',
	'For each upload from your account will be subtracted 5 hours.' => 'За каждое размещение фотографии с вашего баланса будет списано 5 часов.',
	'3. Add description' => '3. Добавить подпись',
	'You can include up to 30 hashtags to description' => 'Вы можете включить в подпись до 30 хэштегов',
	'Upload photo' => 'Загрузить фото',
	'slow' => 'медл',
	'medium' => 'cредн',
	'fast' => 'быстр',
	'Action paused' => 'Действие находится на паузе',
	'There are some restrictions on using of the action by Instagram and it is paused for {hours} hours. The actions will start automatically when pause will be ended.' =>'На данную функцию временно наложено Инстаграмом ограничение на использование и она поставлена на паузу на {hours} часов. По истечении паузы она включится автоматически.',
	'There are some temporary restrictions by Instagram on using follows and comments and they are paused on {hours} hours. The actions will start automatically when pause will be ended.' => 'На функции комментирования и фолловинга временно наложено ограничение Инстаграмом на использование и они поставлены на паузу на {hours} часов. По истечении паузы они включатся автоматически.',
	'Time is not subs on that period.' => 'Списание времени в этот период не осуществляется.',

	// account/pubimages
	'Status' => 'Статус',
	'Description' => 'Подпись',
	'Pauses' => 'Паузы',
	'Add date' => 'Дата добавления',
	'Number of projects' => 'Всего проектов',
	'Error projects' => 'Ошибки в проектах',
	'Success projects' => 'Успешные проекты',

	// account/pubimageslog
	'Account' => 'Аккаунт',
	'Text' => 'Текст',
	'Error?' => 'Ошибка?',
	'Yes' => 'Да',
	'No' => 'Нет',
	'Back' => 'Назад',

	// content/comments
	'Load more' => 'Загрузить ещё',
	'Remove comment' => 'Удалить комментарий',
	'Server error.' => 'Произошла ошибка сервера.',

	// content/index
	'Account {login} content' => 'Контент аккаунта {login}',
	'My publications' => 'Мои публикации',
	'With new comments' => 'С новыми комментариями',

	// content/medias
	'Likes:' => 'Лайков:',
	'Comments:' => 'Комментариев:',
	'Show more' => 'Показать ещё',
	'Server error. Try again later' => 'Произошла ошибка сервера. Попробуйте позже',

	// faq/_item
	'Rating:' => 'Рейтинг:',
	'Is article helped?' => 'Помогла статья?',

	'Dear Customer! The tinctures of the project you have chosen to target {target}, but does not have data to work with. Enter information or select a different target and run the project'
		=> 'Уважаемый клиент! В настойках проекта Вы выбрали таргетинг {target}, но не внесли данные для работы. Внесите данные либо выберите другой таргетинг и запустите проект',


	//'No points for work' => 'Не заполнены данные для работы по таргетингу',
	// faq/index
	'How to Get More Followers and Likes on Instagram | Zengram' => 'Основные вопросы',
	'How to Get More Followers and Likes on Instagram? If you have any query related to Zengram app or functioning, prices etc, feel free to contact us!!' => 'Как получить подписчиков в инстаграм? Если у вас есть любые вопросы к Zengram, не стесняйтесь обращаться!!!',
	'How to Get Followers on Instagram, How To Get Many Likes On Instagram, How To Get More Followers On Instagram, How To Get More Likes On Instagram' => 'Купить реальных подписчиков Instagram, подписчики Instagram, Получить подписчиков Instagram, Получить больше подписчиков Instagram, Реальные подписчики Instagram',
	'Help' => 'Основные вопросы',
	'All questions' => 'Все вопросы',
	'Frequently asked questions' => 'Часто задаваемые вопросы',
	'Topics' => 'Вопросы по теме',
	'Show by' => 'Отображать по',
	'All' => 'Все',

	'The page remained inactive projects.' => 'На странице есть не запущенные проекты.',
	'do not forget to turn them on before you go.' => 'Не забудьте включить их, перед выходом.',
	'Do not show again' => 'Не показывать снова',
	
	// options/index
	'Account {login} options' => 'Настройки аккаунта {login}',
	'Delete account' => 'Удалить аккаунт',
	'Reset settings' => 'Сбросить настройки',
	'Main settings' => 'Основные настройки',
	'Actions' => 'Действия',
	'Select the actions to be performed by Zengram' => 'Выберите действия, которые zengram будет делать за вас',
	'Possible actions' => 'Возможные действия',
	'Put likes' => 'Ставить лайки',
	'If this option is enabled zengram will put likes for accounts that matches the search criteria' => 'Если эта опция включена, zengram будет ставить лайки аккаунтам, соответствующим критериям поиска',
	'Mutual likes' => 'Взаимные лайки',
	'If someone likes your account zengram will put like back one of the last photo of that account' => 'Если вам поставят лайк, zengram лайкнет в ответ одну из последних фотографий этого пользователя',
	'Like own followers' => 'Лайкать своих',
	'Zengram will put likes to the new photos of your followers' => 'Zengram будет лайкать новые фотографии ваших подписчиков',
	'If this option is enabled zengram will follows accounts that matches the search criteria' => 'Если эта опция включена, zengram будет подписываться на аккаунты, соответствующие критериям поиска',
	'Put comments' => 'Комментировать',
	'If this option is enabled zengram will put comments to accounts that matches the search criteria. You can edit list of available comments below.' => 'Если эта опция включена, zengram будет оставлять комментарии для аккаунтов, соответствующих критериям поиска. Список и тексты комментариев можно отредактировать на этой странице ниже.',
	'If this option is enabled zengram will unfollow from accounts that you follow' => 'Если эта опция включена, zengram будет отписываться от аккаунтов, на которые вы подписаны',
	'Keep mutual' => 'Взаимные отписки',
	'Autounfollows' => 'Автовключение',
	'Targeting' => 'Таргетинг',
	'Select how and where zengram will search new followers' => 'Укажите, как и где zengram будет искать для вас новых подписчиков',
	'Followers search' => 'Поиск подписчиков',
	'Search by cities' => 'Работать по городам',
	'Search by locations' => 'Поиск по месту',
	'By cities' => 'По городам',
	'If this option is enabled zengram will search accounts in selected cities' => 'При выборе этой опции Zengram будет искать пользователей в выбранных городах или регионах',
	'cities' => 'города',
	'regions' => 'регионы',
	'Enter the city' => 'Введите город',
	'Show all' => 'Показать все',
	'Search by hashtags' => 'Работать по хештегам',
	'By hashtags' => 'По хештегам',
	'If this option is enabled zengram will search accounts by selected hashtags' => 'При выборе этой опции Zengram будет искать пользователей по выбранным хештегам',
	'Enter the hashtag' => 'Введите хештег',
	'Search by accounts' => 'Работать по аккаунтам',
	'By accounts' => 'По аккаунтам',
	'If this option is enabled zengram will search accounts in followers of selected accounts' => 'При выборе этой опции Zengram будет работать по подписчикам выбранных аккаунтов',
	'Enter the account' => 'Введите аккаунт',
	'All followers of this account were processed' => 'Все подписчики данного аккаунта обработаны',
	'Select comments that zengram will put, or add your own' => 'Выберите комментарии, которые zengram будет оставлять, либо добавьте свои',
	'Comments texts' => 'Тексты комментариев',
	'Leave the desired<br/>or add your own:' => 'Оставьте нужные<br/>или добавьте свои:',
	'Show next 3' => 'Показать следующие 3',
	'Cities' => 'География',
	'Hashtags' => 'Хештеги',
	'Accounts' => 'Аккаунты',
	'Propagate comments' => 'Распространить комментарии',

	// page/about
	'About Zengram App | Get Free and Real Instagram Followers and Likes' => 'О нас',
	'Get Likes on Instagram App, Instagram Followers App, App To Get Followers On Instagram, Followers App, Free Followers On Instagram App'
		=> 'Купить реальных подписчиков Instagram, подписчики Instagram, Получить подписчиков Instagram, Получить больше подписчиков Instagram, Реальные подписчики Instagram',
	'Zengram is designed to attract attention to your Instagram account. Basically, it&#39;s an app aimed at getting you more followers on Instagram. In even simpler words, it&#39;s made so you can forget the question «How to Get Followers on Instagram?». You will get more Instagram comments, likes and, of course, real Instagram followers.'
		=> 'Zengram предназначен для привлечения внимания к вашей учетной записи instagram. Вы получаете больше подписчиков, лайков и комментариев.',

	'About us' => 'О нас',
	'Zengram is not just an app to get followers on Instagram, its main purpose is to drive as much attention as possible to your account using all the available means. You will not only get more real followers, likes and comments in the most natural way possible, you will be able to use professional tools to reach your target audience. Wonder how to get likes on Instagram? App is the answer.'
//		=> 'Зенграм это не просто приложение накручивающее подписчиков, его основная цель - привлечь к вашему аккаунту как можно больше внимания используя все доступные средства. Вы не только больше реальных подписчиков, лайков и комментариев в максимально естественном виде, вы получите профессиональные инструменты для поиска целевой аудитории. Задаетесь вопросом, как получить больше лайков на Инстаграм? Приложение - ответ.',
		=> 'Zengram предназначен для привлечения внимания к вашей учетной записи instagram. Вы получаете больше подписчиков, лайков и комментариев.',
	'The service is older than you might think: it has grown from a limited-access project for a certain group of professional influencers and promoters. After the promotion algorithms were tested and approved, we have polished the interface making it easy and safe for the wide audience to work with, added a few innovative features and rolled out this ultimate Instagram automation tool.'
//		=> 'Сервис старше, чем мы могли подумать: он вырос из проекта, доступ к которому был ограничен группой профессиональных инфлюенсеров и промоутеров. После того, как алгоритмы продвижения были проверены и испытаны, мы отшлифовали интерфейс чтобы сделать работу с ним легкой и безопасной для широкой аудитории, добавили несколько инновационных функций и запустили это совершенный инструмент для автоматизации Инстаграма.',
		=> 'Когда мы начинали Zengram, мы делали это для себя. Мы хотели получить больше подписчиков в Инстаграм, не тратя много времени вручную. Мы разработали специальные инструменты для того, чтобы сделать этот процесс простым и эффективным.<br>С помощью Zengram вы получаете настоящих подписчиков. Которым реально нравятся ваши фото и то, что вы делаете в Инстаграм.<br>И это будут жители вашего города!<br>Мы работаем с людьми, с брендами, с агентствами... со всеми, кому нужны подписчики в инстаграм. С Zengram вы быстро станете популярны в Инстаграм!',
	'Unlike some Instagram followers apps, we don’t offer you free followers on Instagram or thousands of free likes that would only drive up empty numbers. Zengram is aimed at finding real people interested in the same things as you, so they can truly appreciate your work. Flexible settings allow you to choose what kind of users you’re looking for, as the universal nature of the service makes it perfect to promote any type of accounts. It does not matter if you post memes or fitness techniques, with our service to get likes on Instagram is easy as pie. Just sign up and get yourself a flow of Free Followers On Instagram. App has a free tree day trial, don’t miss the deal!'
		=> 'В отличие от некоторых приложений для поиска подписчиков мы не предлагаем вам толпы бесплатных подписчиков или тысячи бесплатных лайков, лишь накручивающих пустые цифры. Зенграм ориентирован на нахождение реальных людей, заинтересованных в тех же вещах, что и вы, чтобы они смогли по достоинству оценить вашу работу. Гибкие настройки позволяют вам выбрать, какой тип пользователей вы ищете, так как универсальная природа нашего сервиса делает его идеальным для раскрутки аккаунта любого рода. Просто зарегистрируйся и получит поток бесплатных подписчиков на Инстаграм. Приложение имеет 3 дневный пробный период, не пропустите предложение! ',

	'Our advantages' => 'Наши преимущества',
	'Minimalism and efficiency - the widest set of tools and features without superfluous settings. Promotion made easy and effective.'
		=> 'Минимализм и эффективноть - широчайший набор функций без излишних настроек. Продвижение легко и эффективно.',
	'Sophisticated search algorithms - find potentially interested users via hashtags, geographic location and competitors.'
		=> 'Передовые поисковые алгоритмы - находите потенциально заинтересованных пользователей по хэштегам, геопозиции и конкурентам.',
	'Advanced geolocation - don’t stick to specific places on the city map, cover the entire town.  Target audience is closer than you think.'
		=> 'Улучшенная геолокация - не привязывайтесь к конкретным местам на карте, охватите весь город. Целевая аудитория ближе, чем вы думаете.',
	'Compete efficiently - attract your competitors’ followers and show them your advantages.'
		=> 'Конкурируйте эффективно - привлеките подписчиков конкурентов и продемонстрируйте им свои преимущества.',
	'Fully-automated basic Instagram interactions - start liking, commenting, following and unfollowing target and existing users with one click.'
		=> 'Полностью автоматизированные основные виды взаимодействия - начните подписываться, комментировать и лайкать целевых и существующих пользователей в один клик.',
	'Track new comments on your pics and comment on others’ on-the-fly - use our enormous base of unique phrases and add yours.'
		=> 'Отслеживайте новые комментарии на своих фото и комментируйте фото других на лету - используйте нашу огромный базу комментариев или добавьте свои.',
	'Don’t fritter away your resources - get rid of non-followers quickly and effortlessly with our branded unfollowing system as soon as you hit the limit of 7 500 followings.'
		=> 'Не тратьте ресурсы понапрасну - избавьтесь от тех, кто отписался от вас быстро и без усилий с помощью нашей фирменной системы отписок как только достигните лимита в 7 500 подписок.',
	'Functional web-interface - post pictures directly from your PC.'
		=> 'Функциональный веб-интерфейс - публикуйте фотографии напрямую с компьютера.',
	'Set up your goals and forget about them until they’re reached - web-based nature of the service allows usage from any stationary or mobile device and protects your safety.'
		=> 'Поставьте цель и забудьте о ней, пока она не будет достигнута - онлайновая природа сервиса позволяет использовать его с любого стационарного или мобильного устройства, а также заботится о вашей безопасности.',
	'Quality over quantity is our motto - we won’t drive up the numbers in your bio with bots, we’ll bring you real, active target audience that you’ll be able to convert into loyal followers and customers.'
		=> 'Наш девиз - качество важнее количества. Мы не будет накручивать цифры в вашем профиле ботами, мы приведем вам настоящую, активную целевую аудиторию, которую вы сможете конвертировать в верных подписчиков и клиентов.',

	// page/blog
	'Details' => 'Подробнее',

	// page/price
	'Our Prices | Zengram' => 'Цены',
	'Here you can buy the necessary amount of time for Zengram work. The bigger package you choose the less time costs for you.' => 'Здесь вы можете купить нужное количество времени для работы zengram. Чем больший пакет вы выбираете, тем ниже стоит для вас время.',
	'Prices' => 'Цены',
	'Congratulations!' => 'Поздравляем!',
	'Action available between {from, date, dd.MM.yyyy} and {to, date, dd.MM.yyyy}' => 'Срок действия акции с {from, date, dd.MM.yyyy} до {to, date, dd.MM.yyyy}.',
	'Under the terms of the stock when making payment you receive a +{size}% free package' => 'По условиям акции, при совершении оплаты пакета, Вы получите бесплатно +{size}%',
	'+{size}% free!' => '+{size}% бесплатно!',
	'Especially for you a special offer:' => 'Специально для Вас действует акция:',
	'pay any amount of time and get a {size}% free!' => 'оплатите любое количество времени и получите {size}% в подарок!',
	'Offer valid until {date, date, dd.MM.yyyy}.' => 'Предложение действует до {date, date, dd.MM.yyyy}.',
	'Here you can buy the necessary amount of time for zengram work. The bigger package you choose, the cheaper time costs for you.' => 'Здесь вы можете купить нужное количество времени для работы zengram.<br>Чем больший пакет вы выбираете, тем меньше стоит для вас время.',
	'The prices are valid till {date}.' => 'Срок действия данных цен продлен до {date}.',
	'Quantity of days' => 'количество дней',
	'Price' => 'стоимость',
	'Your bonus' => 'экономия',
	'free' => 'бесплатно',
	'fa-usd' => 'fa-rub',
	'For payment proceed, select payment and click necessary button' => 'Для оплаты выберете способ платежа и нажмите соответствующую ему кнопку',

	// page/price
	'Sitemap' => 'Карта сайта',
	'Home' => 'Главная',

	// page/sitemap
	'Sitemap | Zengram' => 'Карта сайта',
	'Visit Zengram Sitemap to find all links and pages available on website!!' => 'Посетите карту сайта Zengram, чтобы найти все ссылки и страницы, доступные на сайте!!!',

	// payment/invoice
	'Invoice' => 'Счет по оплате',
	'Purchase info:' => 'Информация о товаре:',

	// payment/success
	'Thank you for your purchase!' => 'Cпасибо за покупку!',
	'Your order' => 'Ваш заказ',
	'Date' => 'Дата',
	'Total' => 'Сумма',
	'You will be redirected to dashboard in a few seconds' => 'Через несколько секунд Вы будете перенаправлены на страницу дашборда',

	// site/blog
	'Read more...' => 'Подробнее...',

	// site/error
	'The above error occurred while the Web server was processing your request.' => 'Произошла ошибка при обработке вашего запроса.',
	'Please contact us if you think this is a server error. Thank you.' => 'Пожалуйста свяжитесь с нами, если вы считаете, что это ошибка сервера. Спасибо.',

	'Contacts' => 'Контакты',
	'Use geotags' => 'Уточнить',
	'When this option is activated, you will access additional geotags (attractions, cultural and public places, streets).' => 'При включении данной опции, Вам станут доступны дополнительные геотеги (достопримечательности, культурные и общественные места, улицы).'
];