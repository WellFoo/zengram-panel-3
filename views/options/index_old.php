<?php
/**
 * @var $this yii\web\View
 * @var $account app\models\Account
 * @var $form yii\bootstrap\ActiveForm
 */

use app\models\Options;
use yii\helpers\Url;
use yii\web\View;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->registerJsFile('@web/js/lib/can.custom.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/lib/bs3paginator.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/lib/bs3radio.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/lib/bootstrap3-typeahead.min.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/account.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/options.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/place.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.autosize.js', ['position' => View::POS_BEGIN]);

$this->title = Yii::t('views', 'Account {login} options', ['login' => $account->login]);
//$this->params['breadcrumbs'][] = $this->title;

$status = $account->getStatus();

$speeds = [Yii::t('views', 'slow'), Yii::t('views', 'medium'), Yii::t('views', 'fast')];
?>
<script type="text/javascript">
	var mapsApiKey = '<?= Yii::$app->params['yandex_maps_api_key'] ?>';
	$(function(){
		$('.animated').autosize();
	});
</script>
<style type="text/css" media="all" scoped>
	.account-controls .btn {
		width: 180px;
	}

	.regions-list-wrap {
		position: relative;
	}
	.regions-list-wrap:before,
	.regions-list-wrap:after {
		background: linear-gradient(to top, rgba(247, 247, 247, 0), #F7F7F7);
		content: '';
		display: block;
		height: 5px;
		position: absolute;
		top: 0;
		left: 0;
		right: 7px;
	}
	.regions-list-wrap:after {
		background: linear-gradient(to bottom, rgba(247, 247, 247, 0), #F7F7F7);
		top: inherit;
		bottom: 0;
	}

	.clearfix.divide {
		border-top: 1px solid #E2E2E1;
		height: 0;
		margin: 10px -10px;
	}

	.btn-success.active {
		background-color: #419041;
	}
	.disable {
		/*opacity: 0.5;
		pointer-events: none;*/
	}

	/* Р РЋРЎвЂљР С‘Р В»Р С‘ Р Т‘Р В»РЎРЏ РЎР‚Р ВµРЎРѓР В°Р в„–Р В·Р ВµРЎР‚Р В° РЎвЂљР ВµР С”РЎРѓРЎвЂљР С•Р Р†Р С•Р С–Р С• Р С—Р С•Р В»РЎРЏ Р С™Р С•Р СР СР ВµР Р…РЎвЂљР В°РЎР‚Р С‘Р в„– */
	#commentText {
		border:2px solid #ccc;
		vertical-align: top;
		width:100%
	padding:0;
		height: 0;
	}

	.animated {
		-webkit-transition: height 0.2s;
		-moz-transition: height 0.2s;
		transition: height 0.2s;
	}
	#commentsList{
		margin-top:10px;
	}

	.panel.panel-settings {
		background: #F7F7F7;
		border: 1px solid #E2E2E1;
		border-radius: 7px;
	}

	.panel.panel-settings > .panel-header > .row > div {
		background: #A6A6A4;
		color: #FFF;
		font-size: 16px;
		height: 42px;
		line-height: 42px;
	}
	.panel.panel-settings > .panel-header > .row > div:first-child {
		background: #00BF51;
		border-top-left-radius: 6px;
		font-weight: bold;
		padding-left: 30px;
		width: 160px;
	}
	.panel.panel-settings > .panel-header > .row > div:last-child {
		border-top-right-radius: 6px;
		padding: auto 20px auto 50px;
		margin-left: 160px;
		float: inherit;
		width: inherit;
	}
	.panel.panel-settings > .panel-header .fa {
		line-height: inherit;
		vertical-align: middle;
	}

	.panel.panel-settings > .panel-body {
		padding: 10px;
	}

	.panel.panel-sub {
		border: 1px solid #E2E2E1;
		box-shadow: 0 2px 7px 1px rgba(0,0,0,0.2);
		margin-bottom: 0;
		transition-duration: .3s;
	}
	.panel.panel-sub.disable {
		box-shadow: none;
	}

	.panel.panel-sub > .panel-header {
		background: #F7F7F7;
	}
	.panel.panel-sub > .panel-header > .row > div {
		background: #E1E1E1;
		font-size: 16px;
		height: 42px;
		line-height: 40px;
		transition-duration: .3s;
		white-space: nowrap;
	}
	.panel.panel-sub > .panel-header > .row > div:first-child {
		background: #00BF51;
		border-top-left-radius: 3px;
		font-weight: bold;
		text-align: center;
	}
	.panel.panel-sub > .panel-header > .row > div:last-child {
		border-top-right-radius: 3px;
		padding: auto 20px auto 50px;
	}
	.panel.panel-sub > .panel-header {
		border-bottom: none;
		border-radius: 5px 5px 0 0;
	}
	.panel.panel-sub.disable > .panel-header {
		color: #A6A6A4;
	}
	.panel.panel-sub.disable > .panel-header > .row > div:first-child {
		background: #A6A6A4;
	}
	.panel.panel-sub > .panel-header:last-child > .row > div:first-child {
		border-bottom-left-radius: 3px;
	}
	.panel.panel-sub > .panel-header:last-child > .row > div:last-child {
		border-bottom-right-radius: 3px;
	}

	.panel.panel-sub .btn-success {
		background: #00BF51;
	}
	.panel.panel-sub.disable .btn-success {
		background: #A6A6A4;
		border-color: #999;
	}

	.panel.panel-sub .option-switch {
		float: left;
		width: 90px;
	}
	.panel.panel-sub .option-label {
		margin-left: 90px;
		padding: 0 10px;
	}

	.panel.panel-sub .toggle .fa {
		color: #FFF;
		font-size: 115%;
		vertical-align: top;
	}
	.panel.panel-sub .fa.description {
		color: #A1A19F;
	}
	.panel.panel-sub .toggle .fa.fa-slack {
		transform: rotate(18deg);
	}
	.panel.panel-sub >  .panel-header .toggle {
		box-shadow: 0 0 7px 1px rgba(0,0,0,.15);
	}
	.panel.panel-sub >  .panel-header .toggle .toggle-handle {
		box-shadow: inset 1px 1px 7px 1px rgba(0,0,0,.15);
	}

	.panel.panel-sub > .panel-body {
		background: #F7F7F7;
	}

	.panel-sub .panel-items-list {
		height: 238px;
		overflow-y: auto;
		margin-right: -7px;
		padding-right: 7px;
	}
	.panel-sub .panel-items-list.panel-items-list-short {
		height: 194px;
	}
	.panel-sub .panel-items-list::-webkit-scrollbar {
		width: 6px;
	}
	.panel-sub .panel-items-list::-webkit-scrollbar-track {
		border-radius: 10px;
		background: #eee;
	}
	.panel-sub .panel-items-list::-webkit-scrollbar-thumb {
		border-radius: 10px;
		background: #888;
	}

	.panel-sub .show-all {
		font-size: 14px;
		margin: 5px 0 -5px;
		text-align: right;
	}

	#actions .panel-body .form-group {
		margin: 5px 0;
	}
	#actions .panel-body .form-group > * {
		font-size: 14px;
		line-height: 20px;
		vertical-align: top;
	}

	.toggle.btn-setting-on {
		border-color: #FFF;
	}
	.toggle.btn-setting-on .toggle-on {
		background: #FFF;
		color: #00BF51;
	}
	.toggle.btn-setting-on .toggle-handle {
		background: #00BF51;
	}
	.toggle.btn-setting-on .fa {
		color: #00BF51 !important;
	}

	.panel-settings .btn-group .btn-success {
		background: #00BF51;
	}

	.panel-sub-header {
		padding-left: 35px;
	}

	.error-wrapper {
		margin: 0;
		padding: 0;
		position: relative;
	}
	.error-wrapper .error {
		background-color: #FFEEEE;
		border: 1px solid #FF433C;
		border-radius: 3px;
		color: #D43F3A;
		display: none;
		padding: 5px 10px;
		position: absolute;
		top: 3px;
		left: 0;
		right: 0;
		z-index: 2000;
	}

	.balloon {
		padding: 6px 70px 6px 15px;
		cursor: default;
	}
	.balloon * {
		line-height: 20px;
	}
	.balloon.balloon-float {
		float: left;
		margin: 0 10px 10px 0;
		padding-right: 35px;
		width: inherit;
	}
	.balloon .button-holder {
		top: 0;
	}

	.panel-header .pull-left {
		margin-right: 5px;
	}

	#settings-modal .modal-content {
		border-radius: 0;
		padding: 0 15px;
	}
	#settings-modal .modal-title {
		text-align: center;
		color: #ff6666;
		margin-bottom: 15px;
		padding-top: 10px;
		margin-top: 3px;
		font: 24px HelveticaNeueCyr-Bold;
		text-transform: uppercase;
	}
	#settings-modal .modal-inner-html {
		min-height: 200px;
		max-height: calc(100vh - 150px);
		overflow-x: hidden;
		overflow-y: auto;
	}
	#settings-modal .modal-inner-html::-webkit-scrollbar {
		width: 6px;
	}
	#settings-modal .modal-inner-html::-webkit-scrollbar-track {
		border-radius: 10px;
		background: #eee;
	}
	#settings-modal .modal-inner-html::-webkit-scrollbar-thumb {
		border-radius: 10px;
		background: #888;
	}

	.simple-tree-view {
		font-size: 12pt;
		list-style: none;
		padding: 0;
	}
	.simple-tree-view ul {
		list-style: none;
		padding-left: 30px;
	}
	.simple-tree-view > ul {
		padding-left: 0;
	}
	.simple-tree-view li {
		padding: 4px 10px;
		border-radius: 3px;
	}
	.simple-tree-view div {
		display: none;
	}
	.simple-tree-view div.open {
		display: inherit;
	}
	.simple-tree-view li:hover {
		background: rgba(76, 175, 80, .15);
	}
	.simple-tree-view li a.toggler i {
		transition-duration: .2s;
	}
	.simple-tree-view li a.toggler.open i {
		transform: rotate(90deg);
	}
	.simple-tree-view a {
		color: #00BF51;
		cursor: pointer;
		float: left;
	}
	.simple-tree-view label {
		display: block;
		margin-left: 10px;
		margin-bottom: 0;
		overflow: hidden;
		white-space: nowrap;
	}
	.simple-tree-view a + label {
		margin-left: 20px;
	}

	@media (max-width: 992px) {
		.acc-pane-inside {
			margin-bottom: 20px;
		}
		.panel.panel-sub > .panel-header > .row > div {
			font-size: 14px;
		}
	}

	@media (max-width: 768px) {
		.panel.panel-sub {
			margin-bottom: 10px;
		}
	}

	.modal-body .popover {
		z-index: 2000;
	}
</style>
<div class="settings-container">

	<script>speeds = ['<?= implode("','", $speeds) ?>'];</script>

	<!-- Setting panels list -->
	<section class="row">

		<?= $this->render('//account/_account_inside', [
			'speeds'  => $speeds,
			'account' => $account,
			'status'  => $status,
		]); ?>

		<div class="hidden-xs hidden-sm col-md-5">
			<p class="set-title" style="padding-left: 10px;"><?= Yii::t('views', 'Main settings') ?></p>
		</div>

		<div class="col-xs-12 col-md-7 group-action-btn visible-lg account-controls" style="margin-top: 25px;">
			<?= Html::a(Yii::t('views', 'Delete account'), ['options/delete', 'id' => $account->id], ['class' => 'btn btn-danger pull-right']); ?>
			<?= Html::a(Yii::t('views', 'Reset settings'), ['options/reset', 'id' => $account->id], ['class' => 'btn btn-success pull-right', 'style' => 'margin-right: 10px;']); ?>
			<?= Html::a(Yii::t('views', 'Save settings'), ['/'], ['class' => 'btn btn-success pull-right', 'style' => 'margin-right: 10px;']); ?>
		</div>

		<div class="clearfix"></div>

		<?php
		$form = ActiveForm::begin([
			'id'          => 'options-form',
			//'options' => ['class' => 'form-horizontal'],
			'fieldConfig' => [
				//'template' => "<div class=\"col-xs-6\">{label}</div><div class=\"col-xs-2\">{input}</div><div class=\"col-lg-8\">{error}</div>",
				//'labelOptions' => ['class' => 'control-label', 'style' => 'text-align: left;'],
			],
		]); ?>

		<!-- New block -->
		<div class="panel panel-settings">

			<div class="panel-header container-fluid">
				<div class="row">
					<div class="col-xs-3 col-sm-2"><?= Yii::t('views', 'Actions') ?></div>
					<div class="col-xs-9 col-sm-10">
						<span class="visible-md visible-lg">
							<?= Yii::t('views', 'Select the actions to be performed by Zengram') ?>
						</span>
						<span class="hidden-md hidden-lg">
							<?= Yii::t('views', 'Possible actions') ?>
							<i class="fa fa-question-circle description" data-toggle="popover" data-title="<?= Yii::t('views', 'Possible actions') ?>"
							   data-content="<?= Yii::t('views', 'Select the actions to be performed by Zengram') ?>"></i>
						</span>
					</div>
				</div>
			</div>

			<div class="panel-body" id="actions">

				<div class="row options-row">
					<!-- Likes checkbox group -->
					<div class="col-sm-4">

						<div class="panel panel-sub<?= !$account->options->likes ? ' disable' : '' ?>">
							<div class="panel-header container-fluid">
								<div class="row">
									<div class="option-switch">
										<input type="checkbox" id="options-likes" class="projectOption"
											<?= $account->options->likes ? 'checked=""' : '' ?>
											   data-account="<?= $account->id; ?>" data-option="likes"
											   data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-heart&quot;></i>"
											   data-off="<i class=&quot;fa fa-heart&quot;></i>" data-onstyle="setting-on">
									</div>
									<div class="option-label">
										<label for="options-likes"><?= Yii::t('views', 'Put likes') ?></label>
										<i class="fa fa-question-circle description" data-toggle="popover" data-title="<?= Yii::t('views', 'Put likes') ?>"
										   data-content="<?= Yii::t('views', 'If this option is enabled zengram will put likes for accounts that matches the search criteria') ?>"></i>
									</div>
								</div>
							</div>

							<div class="panel-body" style="padding: 5px 15px;">
								<?= $form->field($account->options, 'likeMutuals', [
									'checkboxTemplate' => '{input} {beginLabel}{labelTitle} {beginWrapper}{endWrapper}{endLabel}',
									'enableError'      => false,
									'wrapperOptions'   => [
										'tag'   => 'i',
										'class' => 'fa fa-question-circle description',
										'data'  => [
											'toggle'  => 'popover',
											'title'   => Yii::t('views', 'Mutual likes'),
											'content' => Yii::t('views', 'If someone likes your account zengram will put like back one of the last photo of that account')
										]
									]
								])->checkbox([
									'id'    => 'option-like-some',
									'class' => 'projectOption',
									'data'  => ['account' => $account->id, 'option' => 'likeMutuals'],
									'label' => Yii::t('views', 'Mutual likes')
								]); ?>

								<?= $form->field($account->options, 'likeFollowers', [
									'checkboxTemplate' => '{input} {beginLabel}{labelTitle} {beginWrapper}{endWrapper}{endLabel}',
									'enableError'      => false,
									'wrapperOptions'   => [
										'tag'   => 'i',
										'class' => 'fa fa-question-circle description',
										'data'  => [
											'toggle'  => 'popover',
											'title'   => Yii::t('views', 'Like own followers'),
											'content' => Yii::t('views', 'Zengram will put likes to the new photos of your followers')
										]
									]
								])->checkbox([
									'id'    => 'option-like-another',
									'class' => 'projectOption',
									'data'  => ['account' => $account->id, 'option' => 'likeFollowers'],
									'label' => Yii::t('views', 'Like own followers')
								]); ?>
							</div>
						</div>
					</div>
					<!-- Likes checkbox group end -->

					<div class="col-sm-4">
						<!-- Follow -->
						<div class="panel panel-sub<?= !$account->options->follow ? ' disable' : '' ?>">
							<div class="panel-header container-fluid">
								<div class="row">
									<div class="option-switch">
										<input type="checkbox" id="options-follow" class="projectOption"
										       data-account="<?= $account->id; ?>" data-option="follow"
											<?= $account->options->follow ? 'checked=""' : '' ?>
											   data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-check&quot;></i>"
											   data-off="<i class=&quot;fa fa-check&quot;></i>" data-onstyle="setting-on">
									</div>
									<div class="option-label">
										<label for="options-follow"><?= Yii::t('views', 'Follows') ?></label>
										<i class="fa fa-question-circle description" data-toggle="popover" data-title="<?= Yii::t('views', 'Follows') ?>"
										   data-content="<?= Yii::t('views', 'If this option is enabled zengram will follows accounts that matches the search criteria') ?>"></i>
									</div>
								</div>
							</div>
						</div>
						<!-- Follow end -->

						<div class="hidden-xs" style="height: 20px;"></div>

						<!-- Comments -->
						<div class="panel panel-sub<?= !$account->options->comment ? ' disable' : '' ?>">
							<div class="panel-header container-fluid">
								<div class="row">
									<div class="option-switch">
										<input type="checkbox" id="options-comment" class="projectOption"
										       data-account="<?= $account->id; ?>" data-option="comment"
											<?= $account->options->comment ? 'checked=""' : '' ?>
											   data-toggle="toggle" data-style="ios"
											   data-on="<i class=&quot;fa fa-comment&quot;></i>"
											   data-off="<i class=&quot;fa fa-comment&quot;></i>" data-onstyle="setting-on">
									</div>
									<div class="option-label">
										<label for="options-comment"><?= Yii::t('views', 'Put comments') ?></label>
										<i class="fa fa-question-circle description" data-toggle="popover" data-title="<?= Yii::t('views', 'Put comments') ?>"
										   data-content="<?= Yii::t('views', 'If this option is enabled zengram will put comments to accounts that matches the search criteria. You can edit list of available comments below.') ?>"></i>
									</div>
								</div>
							</div>
						</div>
						<!-- Comments end -->
					</div>

					<!-- Unfollow checkbox group -->
					<div class="col-sm-4">
						<div class="panel panel-sub<?= !$account->options->unfollow ? ' disable' : '' ?>">

							<div class="panel-header container-fluid">
								<div class="row">
									<div class="option-switch">
										<input type="checkbox" id="options-unfollow" class="projectOption"
										       data-account="<?= $account->id; ?>" data-option="unfollow"
											<?= $account->options->unfollow ? 'checked=""' : '' ?>
											   data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-times&quot;></i>"
											   data-off="<i class=&quot;fa fa-times&quot;></i>" data-onstyle="setting-on">
									</div>
									<div class="option-label">
										<label for="options-unfollow"><?= Yii::t('views', 'Unfollows') ?></label>
										<i class="fa fa-question-circle description" data-toggle="popover" data-title="<?= Yii::t('views', 'Unfollows') ?>"
										   data-content="<?= Yii::t('views', 'If this option is enabled zengram will unfollow from accounts that you follow') ?>"></i>
									</div>
								</div>
							</div>

							<div class="panel-body" style="padding: 5px 15px;">
								<?= $form->field($account->options, 'mutual', [
									'checkboxTemplate' => '{input} {beginLabel}{labelTitle} {beginWrapper}{endWrapper}{endLabel}',
									'enableError'      => false,
									'wrapperOptions'   => [
										'tag'   => 'i',
										'class' => 'fa fa-question-circle description',
										'data'  => [
											'toggle'  => 'popover',
											'title'   => Yii::t('views', 'Keep mutual'),
											'content' => $account->options->getAttributeLabel('mutual')
										]
									]
								])->checkbox([
									'id'    => 'option-unfollow',
									'class' => 'projectOption',
									'data'  => ['account' => $account->id, 'option' => 'mutual'],
									'label' => Yii::t('views', 'Keep mutual')
								]); ?>

								<?= $form->field($account->options, 'autounfollow', [
									'checkboxTemplate' => '{input} {beginLabel}{labelTitle} {beginWrapper}{endWrapper}{endLabel}',
									'enableError'      => false,
									'wrapperOptions'   => [
										'tag'   => 'i',
										'class' => 'fa fa-question-circle description',
										'data'  => [
											'toggle'  => 'popover',
											'title'   => Yii::t('views', 'Autounfollows'),
											'content' => $account->options->getAttributeLabel('autounfollow')
										]
									]
								])->checkbox([
									'id'    => 'option-autounfollow',
									'class' => 'projectOption',
									'data'  => ['account' => $account->id, 'option' => 'autounfollow'],
									'label' => Yii::t('views', 'Autounfollows')
								]); ?>
							</div>
						</div>
					</div>
					<!-- Unfollow checkbox group end -->
				</div>

			</div><!-- End of .panel-body -->

		</div><!-- End of .panel-settings -->

		<div class="panel panel-settings" id="targeting-panel" data-url="<?=Url::to(['options/search-by/' . $account->id])?>">

			<div class="panel-header container-fluid">
				<div class="row">
					<div class="col-xs-3 col-sm-2"><?= Yii::t('views', 'Targeting') ?></div>
					<div class="col-xs-9 col-sm-10">
						<span class="visible-md visible-lg">
							<?= Yii::t('views', 'Select how and where zengram will search new followers') ?>
						</span>
						<span class="hidden-md hidden-lg">
							<?= Yii::t('views', 'Followers search') ?>
							<i class="fa fa-question-circle description" data-toggle="popover" data-title="<?= Yii::t('views', 'Targeting') ?>"
							   data-content="<?= Yii::t('views', 'Select how and where zengram will search new followers') ?>"></i>
						</span>
					</div>
				</div>
			</div>

			<div class="panel-body">
				<div class="row">

					<div class="col-sm-4">
						<!-- Search settings panel -->
						<div class="panel panel-sub <?= $account->options->isSearchByGeo() ? 'active' : 'disable' ?>">

							<div class="panel-header container-fluid">
								<div class="row">
									<div class="option-switch">
										<input type="checkbox" id="options-search-place" class="projectSearchBy"
											<?= $account->options->isSearchByGeo() ? 'checked' : '' ?>
											   value="<?= Options::SEARCH_PLACE ?>"
											   data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-map-marker&quot;></i>"
											   data-off="<i class=&quot;fa fa-map-marker&quot;></i>" data-onstyle="setting-on">
									</div>
									<div class="option-label">
										<label for="options-search-place">
											<div class="pull-left">
												<span class="hidden-sm"><?= Yii::t('views', 'Search by cities') ?></span>
												<span class="visible-sm"><?= Yii::t('views', 'By cities') ?></span>
											</div>
											<i class="fa fa-question-circle description" data-toggle="popover" data-title="<?= Yii::t('views', 'Search by cities') ?>"
											   data-content="<?= Yii::t('views', 'If this option is enabled zengram will search accounts in selected cities') ?>"></i>
										</label>
									</div>
								</div>
							</div>

							<div class="panel-body">
								<?php if (!empty(Yii::$app->params['use_regions'])) : ?>
									<div class="container-fluid">
										<div class="btn-group row" data-toggle="buttons" id="search-by-toggler" style="display: block">
											<label class="btn col-xs-6 btn-<?=
											(!$account->options->isSearchByGeo() || $account->options->isSearchByPlaces() ? 'success active' : 'default')
											?> <?= $account->options->isSearchByGeo() ? '' : 'disabled' ?>">
												<input type="radio" autocomplete="off" name="search-by"
													<?= (!$account->options->isSearchByGeo() || $account->options->isSearchByPlaces() ? 'checked' : '') ?>
													   value="<?= Options::SEARCH_PLACE ?>">
												<i class="fa fa-map-marker"></i>
												<?= Yii::t('views', 'cities') ?>
											</label>
											<label class="btn col-xs-6 btn-<?=
											($account->options->isSearchByRegions() ? 'success active' : 'default')
											?> <?= $account->options->isSearchByGeo() ? '' : 'disabled' ?>">
												<input type="radio" autocomplete="off" name="search-by"
													<?= ($account->options->isSearchByRegions() ? 'checked' : '') ?>
													   value="<?= Options::SEARCH_REGION ?>">
												<i class="fa fa-globe"></i>
												<?= Yii::t('views', 'regions') ?>
											</label>
										</div>
									</div>

									<div class="clearfix divide"></div>
								<?php endif; ?>

								<!-- Cities block -->
								<div <?= ($account->options->isSearchByGeo() && !$account->options->isSearchByPlaces() ? 'style="display: none;"' : '') ?> id="places-panel">

									<div id="places-list" data-url="<?=Url::to(['options/places/' . $account->id])?>"></div>

									<script type="text/mustache" id="places-list-template">
										<input type="search" class="form-control" placeholder="<?= Yii::t('views', 'Enter the city') ?>"
										       autocomplete="off" style="margin-bottom: 10px;" {{#pending}}disabled{{/pending}}>
										<div class="error-wrapper"><div class="error"></div></div>
										<?php if (empty(Yii::$app->params['use_regions'])) : ?>
										<div class="clearfix divide"></div>
										<?php endif; ?>
										<div class="panel-items-list<?= (!empty(Yii::$app->params['use_regions']) ? ' panel-items-list-short' : '') ?>">
											{{#each items}}
											<div class="balloon balloon-float" {{data 'item'}}>
												<span class="button-holder">
													<span class="delete glyphicon glyphicon-trash" title="<?= Yii::t('app', 'Remove') ?>"></span>
												</span>
												<span class="content">{{name}}</span>
											</div>
											{{/each}}
										</div>
									</script>
								</div>
								<!-- Cities block end -->

								<?php if (!empty(Yii::$app->params['use_regions'])) : ?>
									<!-- Regions block -->
									<div class="panel-items-list" <?= (!$account->options->isSearchByRegions() ? 'style="display: none;"' : '') ?>
									     id="regions-panel">

										<div id="regions-list" data-url="<?= Url::to(['options/regions', 'id' => $account->id]) ?>"></div>

										<script id="regions-initial-data" type="application/json"><?=
											json_encode(\app\models\Regions::getTree($account->getRegionsID()))
											?></script>

										<script id="regions-list-template" type="text/mustache">
										<div class="simple-tree-view">
											{{>regions-tree-template}}
										</div>
									</script>

										<script id="regions-tree-template" type="text/mustache">
										<ul>
										{{#each items}}
										{{#prune_context}}
											<li {{data 'this'}}>
												{{#if items}}
													<a class="toggler{{#collapsed}} open{{/collapsed}}">
														<i class="glyphicon glyphicon-play"></i>
													</a>
												{{/if}}
												<label title="{{label}}">
													{{#if disabled}}
														<input type="checkbox" value="{{id}}" disabled>
													{{else}}
														<input type="checkbox" value="{{id}}" {{#selected}}checked{{/selected}}>
													{{/if}}
													<span>{{label}}</span>
												</label>
											</li>
											{{#if items}}
											<div class="{{#collapsed}}open{{/collapsed}}">
												{{>regions-tree-template}}
											</div>
											{{/if}}
										{{/prune_context}}
										{{/each}}
										</ul>
									</script>

									</div>
									<!-- Regions block end -->
								<?php endif; ?>

								<div class="show-all">
									<a href="#" data-type="geo" data-caption="<?= Yii::t('views', 'Cities') ?>">
										<?= Yii::t('views', 'Show all') ?>
									</a>
								</div>
							</div>
						</div>
						<!-- Search settings panel end -->
					</div>

					<div class="col-sm-4">
						<!-- Hash settings panel -->
						<div class="panel panel-sub <?= $account->options->isSearchByHashtags() ? 'active' : 'disable' ?>">

							<div class="panel-header container-fluid">
								<div class="row">
									<div class="option-switch">
										<input type="checkbox" id="options-search-hashtag" class="projectSearchBy"
											<?= $account->options->isSearchByHashtags() ? 'checked' : '' ?>
											   value="<?= Options::SEARCH_HASHTAG ?>"
											   data-toggle="toggle" data-style="ios"
											   data-on="<i class=&quot;fa fa-slack&quot;></i>"
											   data-off="<i class=&quot;fa fa-slack&quot;></i>" data-onstyle="setting-on">
									</div>
									<div class="option-label">
										<label for="options-search-hashtag">
											<div class="pull-left">
												<span class="hidden-sm"><?= Yii::t('views', 'Search by hashtags') ?></span>
												<span class="visible-sm"><?= Yii::t('views', 'By hashtags') ?></span>
											</div>
											<i class="fa fa-question-circle description" data-toggle="popover" data-title="<?= Yii::t('views', 'Search by hashtags') ?>"
											   data-content="<?= Yii::t('views', 'If this option is enabled zengram will search accounts by selected hashtags') ?>"></i>
										</label>
									</div>
								</div>
							</div>

							<!-- Hash block -->
							<div class="panel-body">

								<div id="hashtags-list" data-url="<?=Url::to([
									'options/hashtags/' . $account->id
								])?>"></div>

								<script type="text/mustaches" id="hashtags-list-template">
									<div class="input-group">
										<input type="search" class="form-control" placeholder="<?= Yii::t('views', 'Enter the hashtag') ?>" {{#pending}}disabled{{/pending}}>
										<span class="input-group-btn">
											<button type="button" class="btn btn-success" {{#pending}}disabled{{/pending}}>
												{{#if pending}}
													<i class="fa fa-spinner fa-pulse"></i>
												{{else}}
													<span class="glyphicon glyphicon-plus" title="<?= Yii::t('app', 'Add') ?>"></span>
												{{/if}}
											</button>
										</span>
									</div>
									<div class="error-wrapper"><div class="error"></div></div>

									<div class="clearfix divide"></div>

									<div class="panel-items-list">
										{{#each items}}
										<div class="balloon balloon-float" {{data 'item'}}>
											<span class="button-holder">
												<span class="delete glyphicon glyphicon-trash" title="<?= Yii::t('app', 'Remove') ?>"></span>
											</span>
											<span class="content">#{{name}}</span>
										</div>
										{{/each}}
									</div>
								</script>

								<div class="show-all">
									<a href="#" data-type="hashtags" data-caption="<?= Yii::t('views', 'Hashtags') ?>">
										<?= Yii::t('views', 'Show all') ?>
									</a>
								</div>
							</div>
						</div>
						<!-- Hash settings panel end -->
					</div>

					<div class="col-sm-4">
						<!-- Accounts settings panel -->
						<div class="panel panel-sub <?= $account->options->isSearchByCompetitors() ? 'active' : 'disable' ?>">

							<div class="panel-header container-fluid">
								<div class="row">
									<div class="option-switch">
										<input type="checkbox" id="options-search-competitor" class="projectSearchBy"
											<?= $account->options->isSearchByCompetitors() ? 'checked' : '' ?>
											   value="<?= Options::SEARCH_COMPETITOR ?>"
											   data-toggle="toggle" data-style="ios"
											   data-on="<i class=&quot;fa fa-users&quot;></i>"
											   data-off="<i class=&quot;fa fa-users&quot;></i>" data-onstyle="setting-on">
									</div>
									<div class="option-label">
										<label for="options-search-competitor">
											<div class="pull-left">
												<span class="hidden-sm"><?= Yii::t('views', 'Search by accounts') ?></span>
												<span class="visible-sm"><?= Yii::t('views', 'By Accounts') ?></span>
											</div>
											<i class="fa fa-question-circle description" data-toggle="popover" data-title="<?= Yii::t('views', 'Search by accounts') ?>"
											   data-content="<?= Yii::t('views', 'If this option is enabled zengram will search accounts in followers of selected accounts') ?>"></i>
										</label>
									</div>
								</div>
							</div>

							<!-- Accounts block -->
							<div class="panel-body">

								<div id="competitors-list" data-url="<?= Url::to([
									'options/competitors/' . $account->id
								]) ?>"></div>

								<script type="text/mustache" id="competitors-list-template">
									<div class="input-group">
										<input type="search" placeholder="<?= Yii::t('views', 'Enter the account') ?>" class="form-control" {{#pending}}disabled{{/pending}}>
										<span class="input-group-btn">
											<button type="button" class="btn btn-success" {{#pending}}disabled{{/pending}}>
												{{#if pending}}
													<i class="fa fa-spinner fa-pulse"></i>
												{{else}}
													<span class="glyphicon glyphicon-plus" title="<?= Yii::t('app', 'Add') ?>"></span>
												{{/if}}
											</button>
										</span>
									</div>
									<div class="error-wrapper"><div class="error"></div></div>

									<div class="clearfix divide"></div>

									<div class="panel-items-list">
										{{#each items}}
										<div class="balloon balloon-float" {{data 'item'}}>
											<span class="button-holder">
												<span class="delete glyphicon glyphicon-trash" title="<?= Yii::t('app', 'Remove') ?>"></span>
											</span>
											<span class="content{{#complete}} text-muted{{/complete}}"
											      {{#complete}}title="<?= Yii::t('views', 'All followers of this account were processed') ?>"{{/complete}}>
												@{{name}}
											</span>
										</div>
										{{/each}}
									</div>
								</script>

								<div class="show-all">
									<a href="#" data-type="accounts" data-caption="<?= Yii::t('views', 'Accounts') ?>"><?= Yii::t('views', 'Show all') ?></a>
								</div>
							</div>
						</div>
						<!-- Accounts settings panel end -->
					</div>
				</div>
			</div>
		</div><!-- End of .panel-settings -->

		<?php
		if ($account->options->auto_fill):
			$query = 'SELECT * FROM auto_fill_dorons WHERE account_id='.$account->id;
			$donors = Yii::$app->db->createCommand($query)->queryAll();
			?>
			<!-- New block -->
			<div class="panel panel-settings" id="auto-fill-settings">

				<div class="panel-header container-fluid">
					<div class="row">
						<div class="col-xs-3 col-sm-2">Прокачка</div>
						<div class="col-xs-9 col-sm-10">
						<span class="visible-md visible-lg">
							<?php if ($account->options->auto_fill_start_date != NULL): ?>
								Прокачка запущена
								<?= $account->options->auto_fill_start_date ?>
								<span style="margin:20px;">|</span> В работе
								<?= floor(((time() - strtotime($account->options->auto_fill_start_date)) / 60 / 60 / 24))  ?> дней
							<?php endif; ?>
						</span>
							<span class="hidden-md hidden-lg">
							Прокачка
							<i class="fa fa-question-circle description" data-toggle="popover" data-title="Автопрокачка"
							   data-content="Автоматическая прокачка аккаунта"></i>
						</span>
						</div>
					</div>
				</div>

				<div class="panel-body" id="actions">
					<div class="row">
						<div class="pull-left" style="width:450px; padding-left: 20px;">
							<table class="table" style="width:405px;">
								<tr>
									<td colspan="3">
										<?php
										if ($account->options->auto_fill_mode == 0){
											$modeId = 'autoFillStart';
											$modeClass = 'btn btn-success';
											$modeText = 'Включить режим автопрокачки';
											$mode = 1;
										}else{
											$modeId = 'autoFillStop';
											$modeClass = 'btn btn-danger';
											$modeText = 'Выключить режим автопрокачки‘';
											$mode = 0;
										}
										?>
										<a class="<?= $modeClass ?>" style="width:100%;" onclick="changeAutoFillMode('<?= $mode ?>');" id="<?= $modeId ?>"><?= $modeText ?></a>
									</td>
								</tr>
								<tr>
									<td style="padding-top:12px">Постить фотографии раз в</td>
									<td style="width:100px;">
										<div class="input-group" style="width:100%;">
											<input type="search" placeholder="" id="autfreq" class="form-control"  value="<?= $account->options->auto_fill_freq ?>">
											<span class="input-group-btn">
										<button class="btn btn-success" type="button" onclick="saveAutoFillFreq();">
											<span title="Р РЋР С•РЎвЂ¦РЎР‚Р В°Р Р…Р С‘РЎвЂљРЎРЉ" class="glyphicon glyphicon-floppy-disk"></span>
										</button>
									</span>
										</div>
									</td>
									<td style="padding-top:12px">час(ов)</td>
								</tr>
								<tr>
									<td colspan="3">
										<div class="input-group" style="width:100%;">
											<input class="form-control"  type="search" id="new-auto-fill-donor" placeholder="Добавьте аккаунт донора">
											<span class="input-group-btn">
											<button class="btn btn-success" type="button" onclick="addAccount();">
												<span title="Добавить" class="glyphicon glyphicon-plus"></span>
											</button>
										</span>
										</div>

										<div class="donors-list" style="margin-top:20px">
											<?php foreach($donors as $donor):  ?>
												<div class="balloon balloon-float">
											<span class="button-holder">
												<span title="Удалить" class="delete glyphicon glyphicon-trash" onclick="deleteDonors(<?= $donor['id'] ?>)"></span>
											</span>
													<span class="content">
												@<?= $donor['donor_login'] ?>
											</span>
												</div>
											<?php endforeach; ?>
										</div>

									</td>
								<tr>
							</table>
						</div>
						<div class="pull-left">
							<table class="table" style="width:235px; margin-left:20px;">
								<tr>
									<td colspan="4" style="padding-top:14px; padding-bottom:12px;">Делать подписок в сутки</td>
								</tr>
								<tr>
									<td style="padding-top:14px;">От</td>
									<td><input class="form-control" style="width:50px;" value="<?= $account->options->auto_fill_follows_min_during_period ?>" type="text" id="follows_min_during_period"  placeholder=""></td>
									<td style="padding-top:14px;">До</td>
									<td><input class="form-control" style="width:50px;" value="<?= $account->options->auto_fill_follows_max_during_period ?>" type="text" id="follows_max_during_period"  placeholder=""></td>
								</tr>
								<tr>
									<td colspan="4" style="padding-top:14px; padding-bottom:12px;">Лайкать после подписки</td>
								</tr>
								<tr>
									<td style="padding-top:14px;">От</td>
									<td><input class="form-control" style="width:50px;" value="<?= $account->options->auto_fill_likes_after_follow_min ?>" type="text" id="likes_after_follow_min"  placeholder=""></td>
									<td style="padding-top:14px;">До</td>
									<td><input class="form-control" style="width:50px;" value="<?= $account->options->auto_fill_likes_after_follow_max ?>" type="text" id="likes_after_follow_max"  placeholder=""></td>
								</tr>
							</table>
						</div>
						<div class="pull-left">
							<table class="table" style="margin-left:45px;">
								<tr>
									<td colspan="4" style="padding-top:14px; padding-bottom:12px;">Распределение подписок (%)</td>
								</tr>
								<tr>
									<td colspan="3" style="padding-top:12px;">Интересные люди:</td>
									<td><input class="form-control" style="width:50px;" value="<?= 100 - $account->options->auto_fill_follow_percent_clients ?>" type="text" id="follow_percent_popular" placeholder=""></td>
								</tr>
								<tr>
									<td colspan="3" style="padding-top:12px;">Клиенты Zengram:</td>
									<td><input class="form-control" style="width:50px;" value="<?= $account->options->auto_fill_follow_percent_clients ?>" type="text" id="follow_percent_clients" placeholder=""></td>
								</tr>
								<tr>
									<td colspan="4">
										<a style="width:100%;" class="btn btn-success col-md-12 col-sm-12 col-xs-12" onclick="changeAutoFillSettings();">Сохранить настройки</a>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
			<script>
				function deleteDonors(donorId){
					document.location.href = '/options/delete-auto-fill-donors/?donor_id=' + donorId + '&account_id=' + <?=  $account->id ?>;
				}
				function addAccount(){
					var donorLogin = $('#new-auto-fill-donor').val();
					document.location.href = '/options/add-auto-fill-donors/?donor_login=' + donorLogin + '&account_id=' + <?=  $account->id ?>;
				}
				function saveAutoFillFreq(){
					var freq = $('#autfreq').val();
					document.location.href = '/options/save-auto-fill-donors-freq/?freq=' + freq + '&account_id=' + <?=  $account->id ?>;
				}
				function changeAutoFillMode(mode){
					//alert (mode);
					document.location.href = '/options/change-auto-fill-mode/?mode=' + mode + '&account_id=' + <?=  $account->id ?>;
				}
				function changeAutoFillSettings(){

					var follows_max_during_period = $('#follows_max_during_period').val();
					var follows_min_during_period = $('#follows_min_during_period').val();
					var likes_after_follow_min = $('#likes_after_follow_min').val();
					var likes_after_follow_max = $('#likes_after_follow_max').val();
					var follow_percent_clients = $('#follow_percent_clients').val();
					document.location.href = '/options/change-settings-auto-fill/?account_id=' + <?=  $account->id ?> + '&follows_max_during_period=' + follows_max_during_period + '&follows_min_during_period=' + follows_min_during_period + '&likes_after_follow_min=' + likes_after_follow_min + '&likes_after_follow_max=' + likes_after_follow_max + '&follow_percent_clients=' + follow_percent_clients;
				}
			</script>

			<?php
		endif;
		?>

		<!-- Comments panel -->
		<div class="panel panel-settings">

			<div class="panel-header container-fluid">
				<div class="row">
					<div class="col-xs-3 col-sm-2"><?= Yii::t('views', 'Comments') ?></div>
					<div class="col-xs-9 col-sm-10">
						<span class="visible-md visible-lg">
							<?= Yii::t('views', 'Select comments that zengram will put, or add your own') ?>
						</span>
						<span class="hidden-md hidden-lg">
							<?= Yii::t('views', 'Comments texts') ?>
							<i class="fa fa-question-circle description" data-toggle="popover" data-title="<?= Yii::t('views', 'Comments texts') ?>"
							   data-content="<?= Yii::t('views', 'Select comments that zengram will put, or add your own') ?>"></i>
						</span>
					</div>
				</div>
			</div>

			<div class="panel-body">

				<div class="acc-tog-set" style="margin: -10px 0 -15px 20px; padding: 0; border: none;">
					<label style="font-size: 16px;">
						<?= $form->field($account->options, 'firmcomments')->checkbox([
							'checked'      => $account->options->firmcomments,
							'id'           => 'firmCommentsCheckbox',
							'data-toggle'  => 'toggle',
							'data-style'   => 'ios',
							'data-on'      => '<i class="fa fa-check"></i>',
							'data-off'     => '<i class="fa fa-check"></i>',
							'data-onstyle' => 'success',
						]); ?>
					</label>
				</div>

				<div class="clearfix divide"></div>

				<div class="row">
					<label for="commentText" class="panel-sub-header col-md-2"><?= Yii::t('views', 'Comments') ?>:</label>

					<div class="col-md-10">

						<input id="commentId" type="hidden">
						<div id="commentInputHolder" class="input-group">
							<textarea id="commentText" type="search" class="form-control animated"
							          data-url="<?=Url::to(['options/comment/' . $account->id])?>"></textarea>
							<input id="commentId" type="hidden">

							<span class="input-group-btn">
								<button id="commentEditBtn" class="btn btn-warning" style="display: none;">
									<span class="glyphicon glyphicon-edit" title="<?= Yii::t('app', 'Edit') ?>"></span>
								</button>
								<button id="commentAddBtn" class="btn btn-success">
									<span class="glyphicon glyphicon-plus" title="<?= Yii::t('app', 'Add') ?>"></span>
								</button>
								<button id="commentClearBtn" class="btn btn-default">
									<span class="glyphicon glyphicon-ban-circle" title="<?= Yii::t('app', 'Cancel') ?>"></span>
								</button>
							</span>

						</div>
					</div>
				</div>

				<div class="row">
					<div class="panel-sub-header fs-16 col-md-2"><?= Yii::t('views', 'Leave the desired<br/>or add your own:') ?></div>
					<div class="col-md-10 comments-list" id="commentsList"></div>
					<div id="commentTemplate" class="balloon" style="display: none;">
							<span class="button-holder">
								<?php /* <span class="edit glyphicon glyphicon-pencil" title="<?= Yii::t('views', 'Edit') ?>"></span> */ ?>
								<span class="delete glyphicon glyphicon-trash" title="<?= Yii::t('views', 'Remove') ?>"></span>
							</span>
						<span class="content"></span>
					</div>
				</div>

				<div class="row hidden-xs" style="margin-top: 15px;">
					<div class="col-md-10 col-md-offset-2">
						<div class="pull-right">
							<span><?= Yii::t('views', 'Show by') ?>: </span><span id="commentsCountSwitch"></span>
						</div>
						<div id="commentsPagination" data-count="<?=$account->getCommentsCount();?>"></div>
					</div>
				</div>

				<div class="group-action-btn text-center visible-xs">
					<button type="button" id="commentsShowMore" class="btn btn-warning btn-grey"><?= Yii::t('views', 'Show next 3') ?></button>
				</div>
				<span class="clearfix"></span>

				<?php
				/** @var \app\models\Users $user */
				$user = Yii::$app->user->identity;
				if ($user->comment_popular) {
					echo Html::a(Yii::t('views', 'Propagate comments'), [
						'options/propagate-comments', 'id' => $account->id
					], [
						'class' => 'btn btn-primary',
						'style' => 'margin-top: 10px;'
					]);
				}
				?>

			</div><!-- End of .panel-body -->
		</div><!-- End of .panel-settings -->
		<!-- Comments panel end -->

		<div class="group-action-btn hidden-xs account-controls">
			<?= Html::a(Yii::t('views', 'Delete account'), ['options/delete', 'id' => $account->id], ['class' => 'btn btn-danger pull-right']); ?>
			<?= Html::a(Yii::t('views', 'Reset settings'), ['options/reset', 'id' => $account->id], ['class' => 'btn btn-success pull-right', 'style' => 'margin-right: 10px;']); ?>
			<?= Html::a(Yii::t('views', 'Save settings'), ['/'], ['class' => 'btn btn-success pull-right', 'style' => 'margin-right: 10px;']); ?>
		</div>

		<div class="group-action-btn visible-xs">
			<div class="col-xs-4">
				<div class="row">
					<?= Html::a(Yii::t('views', 'Save settings'), ['/'], ['class' => 'btn btn-success']) ?>
				</div>
			</div>
			<div class="col-xs-4 p05">
				<?= Html::a(Yii::t('views', 'Reset settings'), ['options/reset', 'id' => $account->id], ['class' => 'btn btn-warning']) ?>
			</div>
			<div class="col-xs-4">
				<div class="row">
					<?= Html::a(Yii::t('views', 'Delete account'), ['options/delete', 'id' => $account->id], ['class' => 'btn btn-danger']) ?>
				</div>
			</div>
		</div>

		<?php ActiveForm::end(); ?>
	</section>    <!-- Setting panels list end -->
	<script>
		$('input[name="Options[unfollow]"]').change(function () {
			$('.function-status').toggleClass('disable');
		});
	</script>
	<script>
		$(document).ready(function () {
			$('[data-toggle="popover"]').popover(
				{
					placement: 'top',
					trigger: 'hover',
					container: 'body'
				}
			);

			$('.helper').popover({placement: 'top'}).unbind('click').on('click', function (e) {
				$(this).popover('toggle');
			});
			<?php if($account->options->getFirstErrors()): ?>
			$('#modalAlert').modal('show');
			<?php endif; ?>
		});
	</script>


	<?php
	echo $this->render('@app/views/account/_modals.php');
	?>


	<?php if ($account->options->getFirstError('comments')): ?>
		<div class="modal fade" id="modalAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
		     aria-hidden="true">
			<div class="modal-dialog modal-lg accountErrorModal">
				<div class="modal-content">
					<?php foreach ($account->options->getFirstErrors() as $attributeError): ?>
						<h2 class="text-center" style="margin: 0; line-height: 1.428571429; font-size: 18px !important; color: #ff6666;">
							<?= $attributeError ?></h2>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>