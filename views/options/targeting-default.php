<?php
/**
 * @var \yii\web\View $this
 * @var \app\models\Account $account
 */
use app\models\Options;
use yii\helpers\Url;
?>
<div class="col-sm-4">
	<!-- Search settings panel -->
	<div class="panel panel-sub <?= $account->options->isSearchByGeo() ? 'active' : 'disable' ?>">

		<div class="panel-header container-fluid">
			<div class="row">
				<div class="option-switch">
					<input type="checkbox" id="options-search-place" class="projectSearchBy"
						<?= $account->options->isSearchByGeo() ? 'checked' : '' ?>
						   value="<?= Options::SEARCH_PLACE ?>"
						   data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-map-marker&quot;></i>"
						   data-off="<i class=&quot;fa fa-map-marker&quot;></i>" data-onstyle="setting-on">
				</div>
				<div class="option-label">
					<label for="options-search-place">
						<div class="pull-left">
							<span class="hidden-sm"><?= Yii::t('views', 'Search by cities') ?></span>
							<span class="visible-sm"><?= Yii::t('views', 'By cities') ?></span>
						</div>
						<i class="fa fa-question-circle description" data-toggle="popover"
						   data-title="<?= Yii::t('views', 'Search by cities') ?>"
						   data-content="<?= Yii::t('views', 'If this option is enabled zengram will search accounts in selected cities') ?>"></i>
					</label>
				</div>
			</div>
		</div>

		<div class="panel-body">
			<?php if (!empty(Yii::$app->params['use_regions'])) : ?>
				<div class="container-fluid">
					<div class="btn-group row" data-toggle="buttons" id="search-by-toggler" style="display: block">
						<label class="btn col-xs-6 btn-<?=
						(!$account->options->isSearchByGeo() || $account->options->isSearchByPlaces() ? 'success active' : 'default')
						?> <?= $account->options->isSearchByGeo() ? '' : 'disabled' ?>">
							<input type="radio" autocomplete="off" name="search-by"
								<?= (!$account->options->isSearchByGeo() || $account->options->isSearchByPlaces() ? 'checked' : '') ?>
								   value="<?= Options::SEARCH_PLACE ?>">
							<i class="fa fa-map-marker"></i>
							<?= Yii::t('views', 'cities') ?>
						</label>
						<label class="btn col-xs-6 btn-<?=
						($account->options->isSearchByRegions() ? 'success active' : 'default')
						?> <?= $account->options->isSearchByGeo() ? '' : 'disabled' ?>">
							<input type="radio" autocomplete="off" name="search-by"
								<?= ($account->options->isSearchByRegions() ? 'checked' : '') ?>
								   value="<?= Options::SEARCH_REGION ?>">
							<i class="fa fa-globe"></i>
							<?= Yii::t('views', 'regions') ?>
						</label>
					</div>
				</div>

				<div class="clearfix divide"></div>
			<?php endif; ?>

			<!-- Cities block -->
			<div <?= ($account->options->isSearchByGeo() && !$account->options->isSearchByPlaces() ? 'style="display: none;"' : '') ?>
				id="places-panel">

				<div id="places-list" data-url="<?= Url::to(['options/places/' . $account->id]) ?>"></div>

				<script type="text/mustache" id="places-list-template">
										<?= $this->render('places_list.mustache') ?>

				</script>
			</div>
			<!-- Cities block end -->

			<?php if (!empty(Yii::$app->params['use_regions'])) : ?>
				<!-- Regions block -->
				<div
					class="panel-items-list" <?= (!$account->options->isSearchByRegions() ? 'style="display: none;"' : '') ?>
					id="regions-panel">

					<div id="regions-list" data-url="<?= Url::to(['options/regions', 'id' => $account->id]) ?>"></div>

					<script id="regions-initial-data" type="application/json"><?=
						json_encode(\app\models\Regions::getTree($account->getRegionsID()))
						?></script>

					<script id="regions-list-template" type="text/mustache">
										<div class="simple-tree-view">
											{{>regions-tree-template}}
										</div>

					</script>

					<script id="regions-tree-template" type="text/mustache">
										<?= $this->render('regions_tree.mustache') ?>

					</script>

				</div>
				<!-- Regions block end -->
			<?php endif; ?>

			<div>
				<?php if ($account->options->experimental) { ?>
					<?= $form->field($account->options, 'use_geotags', [
						'checkboxTemplate' => '{input} {beginLabel}{labelTitle} {beginWrapper}{endWrapper}{endLabel}',
						'enableError' => false,
						'options' => [
							'class' => 'pull-left',
							'style' => 'font-size: 14px; margin: -10px 0 0;',
						],
						'wrapperOptions' => [
							'tag' => 'i',
							'class' => 'fa fa-question-circle description',
							'data' => [
								'toggle' => 'popover',
								'title' => Yii::t('views', 'Use geotags'),
								'content' => Yii::t('views', 'Some description here')
							]
						]
					])->checkbox([
						'id' => 'option-use-geotags',
						'class' => 'projectOption',
						'data' => ['account' => $account->id, 'option' => 'use_geotags'],
						'label' => Yii::t('views', 'Use geotags')
					]); ?>
				<?php } ?>
				<div class="show-all">
					<a href="#" data-type="geo" data-caption="<?= Yii::t('views', 'Cities') ?>">
						<?= Yii::t('views', 'Show all') ?>
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- Search settings panel end -->
</div>

<div class="col-sm-4">
	<!-- Hash settings panel -->
	<div class="panel panel-sub <?= $account->options->isSearchByHashtags() ? 'active' : 'disable' ?>">

		<div class="panel-header container-fluid">
			<div class="row">
				<div class="option-switch">
					<input type="checkbox" id="options-search-hashtag" class="projectSearchBy"
						<?= $account->options->isSearchByHashtags() ? 'checked' : '' ?>
						   value="<?= Options::SEARCH_HASHTAG ?>"
						   data-toggle="toggle" data-style="ios"
						   data-on="<i class=&quot;fa fa-slack&quot;></i>"
						   data-off="<i class=&quot;fa fa-slack&quot;></i>" data-onstyle="setting-on">
				</div>
				<div class="option-label">
					<label for="options-search-hashtag">
						<div class="pull-left">
							<span class="hidden-sm"><?= Yii::t('views', 'Search by hashtags') ?></span>
							<span class="visible-sm"><?= Yii::t('views', 'By hashtags') ?></span>
						</div>
						<i class="fa fa-question-circle description" data-toggle="popover"
						   data-title="<?= Yii::t('views', 'Search by hashtags') ?>"
						   data-content="<?= Yii::t('views', 'If this option is enabled zengram will search accounts by selected hashtags') ?>"></i>
					</label>
				</div>
			</div>
		</div>

		<!-- Hash block -->
		<div class="panel-body">

			<div id="hashtags-list" data-url="<?= Url::to([
				'options/hashtags/' . $account->id
			]) ?>"></div>

			<script type="text/mustaches" id="hashtags-list-template">
									<?= $this->render('hashtags_list.mustache') ?>

			</script>

			<div class="show-all">
				<a href="#" data-type="hashtags" data-caption="<?= Yii::t('views', 'Hashtags') ?>">
					<?= Yii::t('views', 'Show all') ?>
				</a>
			</div>
		</div>
	</div>
	<!-- Hash settings panel end -->
</div>

<div class="col-sm-4">
	<!-- Accounts settings panel -->
	<div class="panel panel-sub <?= $account->options->isSearchByCompetitors() ? 'active' : 'disable' ?>">

		<div class="panel-header container-fluid">
			<div class="row">
				<div class="option-switch">
					<input type="checkbox" id="options-search-competitor" class="projectSearchBy"
						<?= $account->options->isSearchByCompetitors() ? 'checked' : '' ?>
						   value="<?= Options::SEARCH_COMPETITOR ?>"
						   data-toggle="toggle" data-style="ios"
						   data-on="<i class=&quot;fa fa-users&quot;></i>"
						   data-off="<i class=&quot;fa fa-users&quot;></i>" data-onstyle="setting-on">
				</div>
				<div class="option-label">
					<label for="options-search-competitor">
						<div class="pull-left">
							<span class="hidden-sm"><?= Yii::t('views', 'Search by accounts') ?></span>
							<span class="visible-sm"><?= Yii::t('views', 'By Accounts') ?></span>
						</div>
						<i class="fa fa-question-circle description" data-toggle="popover"
						   data-title="<?= Yii::t('views', 'Search by accounts') ?>"
						   data-content="<?= Yii::t('views', 'If this option is enabled zengram will search accounts in followers of selected accounts') ?>"></i>
					</label>
				</div>
			</div>
		</div>

		<!-- Accounts block -->
		<div class="panel-body">

			<div id="competitors-list" data-url="<?= Url::to([
				'options/competitors/' . $account->id
			]) ?>"></div>

			<script type="text/mustache" id="competitors-list-template">
									<?= $this->render('competitors_list.mustache') ?>

			</script>

			<div class="show-all">
				<a href="#" data-type="accounts"
				   data-caption="<?= Yii::t('views', 'Accounts') ?>"><?= Yii::t('views', 'Show all') ?></a>
			</div>
		</div>
	</div>
	<!-- Accounts settings panel end -->
</div>