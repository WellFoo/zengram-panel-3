<?php
/* @var $this yii\web\View */
use app\models\Enotify;
use yii\helpers\Html;

/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\RegisterForm */

$this->title = Yii::t('user', 'Email Notifications');
?>
<div class="page">
	<?= Html::beginForm() ?>
	<h1 class="text-center" style="font-size: 32px; font-weight: normal; margin-bottom: 20px;"><?= $this->title ?></h1>
	<?php if (Yii::$app->session->hasFlash('success')): ?>
		<div class="alert alert-success col-sm-10 col-lg-10 col-xs-10 col-sm-offset-1 col-lg-offset-1 col-xs-offset-1"><?= Yii::$app->session->getFlash('success'); ?></div>
	<?php endif; ?>
	<div class="acc-pane col-sm-10 col-lg-10 col-xs-10 col-sm-offset-1 col-lg-offset-1 col-xs-offset-1">
		<label style="margin-bottom: 17px; font-size: 25px;"><?= Yii::t('user', 'Select options to get emails about:') ?></label>
		<!-- Toggle settings -->
		<dl class="acc-tog-set">
			<dt>
				<input type="checkbox" class="projectOption" name="enotify[<?= Enotify::ENOTIFY_TYPE_SPECIAl ?>]"
				       id="enotify-<?= Enotify::ENOTIFY_TYPE_SPECIAl ?>"
					   <?= empty($notifications[Enotify::ENOTIFY_TYPE_SPECIAl]) ? '' : 'checked=""' ?>
                       data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-check&quot;></i>"
                       data-off="<i class=&quot;fa fa-times&quot;></i>" data-onstyle="success">
				<?= Yii::t('user', 'Special offers') ?>
			</dt>
			<dd><a class="helper" href="#" data-toggle="popover" data-title="<?= Yii::t('user', 'Special offers') ?>"
			       data-content="<?= Yii::t('user', 'You will receive notifications about our sepecial offers') ?>" data-original-title=""
			       title=""><i style="color: #757575;" class="fa fa-question-circle"></i></a></dd>
			<dt>
				<input type="checkbox" class="projectOption" name="enotify[<?= Enotify::ENOTIFY_TYPE_NEWS ?>]"
				       id="enotify-<?= Enotify::ENOTIFY_TYPE_NEWS ?>"
					   <?= empty($notifications[Enotify::ENOTIFY_TYPE_NEWS]) ? '' : 'checked=""' ?>
                       data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-check&quot;></i>"
                       data-off="<i class=&quot;fa fa-times&quot;></i>" data-onstyle="success">
				<?= Yii::t('user', 'Service news') ?>
			</dt>
			<dd><a class="helper" href="#" data-toggle="popover" data-title="<?= Yii::t('user', 'Service news') ?>"
			       data-content="<?= Yii::t('user', 'You will receive notifications about service updates') ?>" data-original-title=""
			       title=""><i style="color: #757575;" class="fa fa-question-circle"></i></a></dd>
			<dt>
				<input type="checkbox" class="projectOption" name="enotify[<?= Enotify::ENOTIFY_TYPE_PAYMENTS ?>]"
				       id="enotify-<?= Enotify::ENOTIFY_TYPE_PAYMENTS ?>"
					   <?= empty($notifications[Enotify::ENOTIFY_TYPE_PAYMENTS]) ? '' : 'checked=""' ?>
                       data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-check&quot;></i>"
                       data-off="<i class=&quot;fa fa-times&quot;></i>" data-onstyle="success">
				<?= Yii::t('user', 'Payment notification') ?>
			</dt>
			<dd><a class="helper" href="#" data-toggle="popover" data-title="<?= Yii::t('user', 'Payment notification') ?>"
			       data-content="<?= Yii::t('user', 'You will receive notifications about successfully proceeded payments') ?>" data-original-title=""
			       title=""><i style="color: #757575;" class="fa fa-question-circle"></i></a></dd>
			<dt><input type="checkbox" id="enotify-disable" onchange="$('.projectOption').prop('checked', false).change().prop('disabled', $(this).prop('checked')).change()"
                       data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-check&quot;></i>"
                       data-off="<i class=&quot;fa fa-times&quot;></i>" data-onstyle="success">
				<?= Yii::t('user', 'Uncheck all') ?></dt>
		</dl>
		<!-- Toggle settings -->
		<div class="clearfix"></div>
		<button type="submit" class="btn btn-success"><?= Yii::t('app', 'Save') ?></button>
	</div>
	<?= Html::endForm() ?>
	<script>
		$('[data-toggle="popover"]').popover(
				{
					placement: 'top',
					trigger: 'hover',
					container: 'body'
				}
		);
	</script>
</div>
