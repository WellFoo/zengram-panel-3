<?php
/**
 * @var app\components\FluidableView $this
 * @var integer $size
 * @var integer $ref
 * @var boolean $nonclickable
 */
use yii\helpers\Url;

$sizes = [
	'125x125' => [125, 125],
	'200x200' => [200, 200],
	'240x400' => [240, 400],
	'468x60'  => [468, 60],
	'728x90'  => [728, 90],
	'1050x90' => [1050, 90],
];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script src="<?= Yii::getAlias('@web/banners/') ?>createjs-2015.11.26.min.js"></script>
	<script src="<?= Yii::getAlias('@web/banners/' . $size) ?>/draw.js"></script>
	<script>
		var canvas, stage, exportRoot;
		function init()
		{
			// --- write your JS code here ---

			canvas = document.getElementById("zengram_canvas_<?= $size; ?>");
			images = images || {};

			var loader = new createjs.LoadQueue(false);
			loader.addEventListener("fileload", handleFileLoad);
			loader.addEventListener("complete", handleComplete);
			loader.loadManifest(lib.properties.manifest);
		}

		function handleFileLoad(evt)
		{
			if (evt.item.type == "image") { images[evt.item.id] = evt.result; }
		}

		function handleComplete(evt)
		{
			exportRoot = new lib._image();

			stage = new createjs.Stage(canvas);
			stage.addChild(exportRoot);
			stage.update();

			createjs.Ticker.setFPS(lib.properties.fps);
			createjs.Ticker.addEventListener("tick", stage);
		}

	</script>

	<!-- write your code here -->

</head>
<body onload="init();" style="background-color:#D4D4D4;margin:0px;">
<?php if (!empty($sizes[$size])): ?>
	<?php if (empty($nonclickable)) : ?>
		<a href="<?= Url::to(['/', 'p' => $ref], true); ?>" onclick="window.top.location.href = this.href; return false; ">
	<?php endif; ?>
	<canvas id="zengram_canvas_<?= $size; ?>" width="<?= $sizes[$size][0] ?>" height="<?= $sizes[$size][1] ?>" style="background-color:#FFFFFF"></canvas>
	<?php if (empty($nonclickable)) : ?>
		</a>
	<?php endif; ?>
<?php endif; ?>
</body>
</html>
