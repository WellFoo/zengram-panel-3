<?php
/**
 * @var $this yii\web\View
 * @var \app\models\Invoice $model
 * @var \app\models\Prices $price
 * @var string $type
 * */
use yii\helpers\Url;

$desc = Yii::t('app', '{value} days on Zengram.net for #{user}', ['value' => $price->value, 'user' => $model->user_id]);

if ($price->id == 9) {
	$desc = 'Оплата услуг для анализа instagram пользователей';
}

?>
<form id="submit" method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
	<input type="hidden" name="receiver" value="<?= Yii::$app->params['ym-id']; ?>">
	<input type="hidden" name="formcomment" value="<?= Yii::t('app', 'Balance recharge on Zengram.net for #{user}', ['user' => $model->user_id]) ?>">
	<input type="hidden" name="short-dest" value="<?=$desc?>">
	<input type="hidden" name="label" value="<?= $model->id ?>">
	<input type="hidden" name="quickpay-form" value="shop">
	<input type="hidden" name="targets" value="zengram.ru">
	<input type="hidden" name="sum" value="<?= $price->price ?>" data-type="number">
	<input type="hidden" name="paymentType" value="<?= $type; ?>">
	<input type="hidden" name="is-inner-form" value="true">
	<input type="hidden" name="referer" value="<?= Url::to(['/page/price'], true); ?>">
	<input type="hidden" name="quickpay-back-url" value="<?= Url::to(['/payment/cancel', 'id' => $model->id], true); ?>">
	<input type="hidden" name="successURL" value="<?= Url::to(['/payment/success-payment', 'id' => $model->id], true); ?>">
	<input type="hidden" name="submit-button" value="Оплатить">
	<input id="submit_btn" type="submit" value="Нажмите сюда, если переадресация не произошла автоматически" style="display: none;">
</form>
<script>
	try {
		document.onreadystatechange = function ()
		{
			document.forms['submit'].submit();
		};
	} catch (e){}
	setTimeout(function(){
		document.getElementById('submit_btn').click();
		document.getElementById('submit_btn').style.display = 'block';
	}, 3000);
</script>